﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for GeneralSettingsTab.xaml
    /// </summary>
    public partial class GeneralSettingsTab : UserControl
    {
        public GeneralSettingsTab()
        {
            InitializeComponent();

            UseTransparentWindowsRadioButton.IsChecked = GlobalHelper.ApplicationSettings.UseTransparentWindows;
            UseNonTransparentWindowsRadioButton.IsChecked = !GlobalHelper.ApplicationSettings.UseTransparentWindows;
            StartWindowsCenterScreenRadioButton.IsChecked = GlobalHelper.ApplicationSettings.UseLastWindowLocation;
            StartWindowsLastLocationRadioButton.IsChecked = !GlobalHelper.ApplicationSettings.UseLastWindowLocation;
            AutoLoginTwitchCheckBox.IsChecked = GlobalHelper.ApplicationSettings.AutoLoginTwitch;
        }

        private void UseTransparentWindowsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            GlobalHelper.ApplicationSettings.UseTransparentWindows = true;
        }

        private void UseNonTransparentWindowsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            GlobalHelper.ApplicationSettings.UseTransparentWindows = false;
        }

        private void StartWindowsLastLocationRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            GlobalHelper.ApplicationSettings.UseLastWindowLocation = true;
        }

        private void StartWindowsCenterScreenRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            GlobalHelper.ApplicationSettings.UseLastWindowLocation = false;
        }

        private void AutoLoginTwitchCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            GlobalHelper.ApplicationSettings.AutoLoginTwitch = true;
        }

        private void AutoLoginTwitchCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            GlobalHelper.ApplicationSettings.AutoLoginTwitch = false;
        }
    }
}
