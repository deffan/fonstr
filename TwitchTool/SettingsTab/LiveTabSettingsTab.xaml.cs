﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;
using TwitchTool.WinEventCmdTab.EventQueue;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for LiveTabSettingsTab.xaml
    /// </summary>
    public partial class LiveTabSettingsTab : UserControl
    {
        private string m_CompareString;
        private bool m_Cancel;

        public LiveTabSettingsTab()
        {
            InitializeComponent();
            m_Cancel = false;
        }

        public void Load()
        {
            if(m_Cancel)
            {
                m_Cancel = false;
                return;
            }

            m_CompareString = "";
            LiveTabEventPanel.Children.Clear();

            for (int n = 0; n < 9; n++)
            {
                bool isChecked = GlobalHelper.ApplicationSettings.DisplayEvents.Contains(n);
                LiveTabEventPanel.Children.Add(new EventCheckBox(n, isChecked));
                m_CompareString += n + isChecked.ToString();
            }
        }

        public void Save()
        {
            List<int> disp = new List<int>();

            m_CompareString = "";
            for (int i = 0; i < LiveTabEventPanel.Children.Count; i++)
            {
                EventCheckBox box = LiveTabEventPanel.Children[i] as EventCheckBox;
                bool isChecked = box.QueueCheckBox.IsChecked == true;
                if(isChecked)
                {
                    disp.Add(box.m_EventType);
                }
                m_CompareString += box.m_EventType + isChecked.ToString();
            }

            GlobalHelper.ApplicationSettings.UpdateDisplayEvents(disp);
        }

        public bool HasChanges()
        {
            if (m_CompareString == null)    // Never loaded
                return false;

            string testCompare = "";
            for(int i = 0; i < LiveTabEventPanel.Children.Count; i++)
            {
                EventCheckBox box = LiveTabEventPanel.Children[i] as EventCheckBox;
                bool isChecked = box.QueueCheckBox.IsChecked == true;
                testCompare += box.m_EventType + isChecked.ToString();
            }
            return !m_CompareString.Equals(testCompare);
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        public MessageBoxResult SaveCheck()
        {
            MessageBoxResult result = MessageBoxResult.No;
            if (HasChanges())
            {
                result = MessageBox.Show("Save changes?", "Save?", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    Save();
                }
                else if (result == MessageBoxResult.No)
                {
                    Load();
                }
                else
                {
                    m_Cancel = true;
                }
            }
            return result;
        }
    }
}
