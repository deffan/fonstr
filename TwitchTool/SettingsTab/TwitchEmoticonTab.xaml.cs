﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for TwitchEmoticonTab.xaml
    /// </summary>
    public partial class TwitchEmoticonTab : UserControl
    {
        public TwitchEmoticonTab()
        {
            InitializeComponent();

            if (!GlobalHelper.SQL.HasTwitchEmotes())
            {
                NotInstalledPanel.Visibility = Visibility.Visible;
                DefaultPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void DownloadAndInstallTwitchEmoticonsButton_Click(object sender, RoutedEventArgs e)
        {
            NotInstalledPanel.Visibility = Visibility.Collapsed;
            InstallingPanel.Visibility = Visibility.Visible;

            Thread thread = new Thread(() =>
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressChanged);
                client.DownloadFileCompleted += DownloadFileCompleted;
                client.DownloadFileAsync(new Uri("https://api.twitch.tv/kraken/chat/emoticons"), "twitch_emoticons_download.txt");
            });
            thread.Start();
        }

        private void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            bool failed = false;
            if (e.Cancelled)
            {
                MessageBox.Show("Download was cancelled.", "Error occured.", MessageBoxButton.OK, MessageBoxImage.Error);
                failed = true;
            }

            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Error occured.", MessageBoxButton.OK, MessageBoxImage.Error);
                failed = true;
            }

            if (failed)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    NotInstalledPanel.Visibility = Visibility.Visible;
                    InstallingPanel.Visibility = Visibility.Collapsed;
                }));
                return;
            }

            Install("twitch_emoticons_download.txt");
        }

        private void InstallTwitchEmoticonsButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text|*.txt|All|*.*";
            if (openFileDialog.ShowDialog() != true)
            {
                return;
            }

            NotInstalledPanel.Visibility = Visibility.Collapsed;
            InstallingPanel.Visibility = Visibility.Visible;

            Thread thread = new Thread(() =>
            {
                Install(openFileDialog.FileName);
            });
            thread.Start();
        }

        private void Install(string data)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                DownloadProgressLabel.Content = "Download complete.";
                InstallProgressLabel.Visibility = Visibility.Visible;
            }));

            Thread.Sleep(100);

            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                using (new WaitCursor())
                {
                    SQL.SQLResult r = GlobalHelper.SQL.FillTwitchEmoteTable(data);
                    if (r.Error)
                    {
                        MessageBox.Show(r.Data, "Error occured.", MessageBoxButton.OK, MessageBoxImage.Error);
                        NotInstalledPanel.Visibility = Visibility.Visible;
                        InstallingPanel.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        InstallingPanel.Visibility = Visibility.Collapsed;
                        DefaultPanel.Visibility = Visibility.Visible;
                    }
                }

            }));
        }

        private void DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => 
            {
                double bytesIn = double.Parse(e.BytesReceived.ToString());
                double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
                int percentage = (int)(bytesIn / totalBytes * 100);
                DownloadProgressLabel.Content = string.Format("Downloading ({0}%)", percentage);
            }));
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if(SearchTextBox.Text.Length > 0)
            {
                using (new WaitCursor())
                {
                    EmoteList.Items.Clear();
                    List<string> results = GlobalHelper.SQL.SearchTwitchEmoticon(SearchTextBox.Text);
                    for (int i = 0; i < results.Count; i++)
                    {
                        EmoteList.Items.Add(results[i]);
                    }
                }
            }
        }

        private void EmoteList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EmoteList.SelectedIndex < 0)
                return;

            Image emote = GlobalHelper.EmoticonParser.GetEmoticon(EmoteList.SelectedItem.ToString());
            if(emote != null)
            {
                EmoticonImageViewer.Source = emote.Source;
            }
        }

        private void SearchTextBox_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if(e.Key == System.Windows.Input.Key.Enter && SearchTextBox.Text.Length > 0)
            {
                SearchButton_Click(null, null);
                e.Handled = true;
            }
        }
    }
}
