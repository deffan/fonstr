﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for TwitchTab.xaml
    /// </summary>
    public partial class TwitchTab : UserControl
    {
        public TwitchTab()
        {
            InitializeComponent();
        }

        public void CheckLoginStatus()
        {
            if (!string.IsNullOrEmpty(MainWindow.TwitchApi.USERNAME))
            {
                try
                {
                    UserLogo.Source = new BitmapImage(new Uri(MainWindow.TwitchApi.USERLOGO, UriKind.Absolute));
                }
                catch (Exception) { }
                LoginText.Text = "Connected as " + MainWindow.TwitchApi.USERNAME;
                LoginButton.Content = "Login as another user...";
            }
            else
            {
                LoginText.Text = "Not logged in.";
                LoginButton.Content = "Login";
                UserLogo.Source = GlobalHelper.GetImage("cancel.png");
            }
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.TwitchApi.Login(LoginText.Text.Equals("Login as another user..."));
        }
    }
}
