﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.SettingsTab.Variables;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for VariableListItem.xaml
    /// </summary>
    public partial class VariableListItem : UserControl
    {
        private IVariableView m_View;
        private VariablesTab m_Parent;
        private DataVariable m_Variable;
        private SolidColorBrush m_MouseOverBrush;
        private bool m_Selected;
        public bool IsSelected() { return m_Selected; }
        public DataVariable Variable { get { return m_Variable; } }
        public IVariableView View { get { return m_View; } }

        public VariableListItem()
        {
            InitializeComponent();
            m_MouseOverBrush = new SolidColorBrush(Color.FromArgb(255, 135, 206, 250));
        }

        public VariableListItem(VariablesTab parent, DataVariable variable)
        {
            InitializeComponent();
            m_MouseOverBrush = new SolidColorBrush(Color.FromArgb(255, 135, 206, 250));
            m_Parent = parent;
            m_Variable = variable;
            m_Selected = false;

            ItemText.Text = m_Variable.DisplayName;
            ItemLabel.Content = VARIABLE_TYPE.GetVariableName(m_Variable.Type);

            switch (m_Variable.Type)
            {
                case VARIABLE_TYPE.TEXT:
                {
                    m_View = new TextVariableView(this);
                    m_View.Load(m_Variable);
                    break;
                }
                case VARIABLE_TYPE.RANDOM_NUMBER:
                {
                    m_View = new RandomNumberView(this);
                    m_View.Load(m_Variable);
                    break;
                }
                case VARIABLE_TYPE.RANDOM_TEXT:
                {
                    m_View = new RandomTextView(this);
                    m_View.Load(m_Variable);
                    break;
                }
            }
        }

        public void SetName(string name)
        {
            ItemText.Text = name;
            m_Variable.DisplayName = name;
        }

        public void Select(bool selected)
        {
            m_Selected = selected;
            if (m_Selected)
            {
                MainStackPanel.Background = Brushes.PowderBlue;
            }
            else
            {
                MainStackPanel.Background = Brushes.White;
            }
        }

        private void RemoveButton_MouseEnter(object sender, MouseEventArgs e)
        {
            if (m_Selected)
                return;

            MainStackPanel.Background = Brushes.White;
            e.Handled = true;
        }

        private void MainStackPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            if (m_Selected)
                return;

            MainStackPanel.Background = m_MouseOverBrush;
        }

        private void MainStackPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            if (m_Selected)
                return;

            MainStackPanel.Background = Brushes.White;
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            m_Parent.Remove(this);
        }

        private void MainStackPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (m_Selected)
                return;

            m_Parent.Select(this);
        }
    }
}
