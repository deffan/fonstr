﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab.Variables
{
    /// <summary>
    /// Interaction logic for RandomTextView.xaml
    /// </summary>
    public partial class RandomTextView : UserControl, IVariableView
    {
        private DataVariable m_Variable;
        private VariableListItem m_Parent;

        public RandomTextView()
        {
            InitializeComponent();
        }

        public RandomTextView(VariableListItem parent)
        {
            InitializeComponent();
            m_Parent = parent;
        }

        public bool HasChanges()
        {
            if (!m_Variable.DisplayName.Equals(RandomTextNameTextBox.Text))
                return true;

            if (RandomTextVariableList.Children.Count != m_Variable.Listdata.Count)
                return true;

            for(int i = 0; i < RandomTextVariableList.Children.Count; i++)
            {
                if (!(RandomTextVariableList.Children[i] as RandTextVarLine).TextField.Text.Equals(m_Variable.Listdata[i]))
                    return true;
            }

            return false;
        }

        public void Load(DataVariable variable)
        {
            m_Variable = variable;
            RandomTextNameTextBox.Text = m_Variable.DisplayName;
            for(int i = 0; i < m_Variable.Listdata.Count; i++)
            {
                RandomTextVariableList.Children.Add(new RandTextVarLine(this, m_Variable.Listdata[i]));
            }
        }

        public void Remove(RandTextVarLine textline)
        {
            RandomTextVariableList.Children.Remove(textline);
        }

        public void Save()
        {
            m_Variable.DisplayName = RandomTextNameTextBox.Text;
            m_Variable.Listdata.Clear();
            for (int i = 0; i < RandomTextVariableList.Children.Count; i++)
            {
                m_Variable.Listdata.Add((RandomTextVariableList.Children[i] as RandTextVarLine).TextField.Text);
            }
        }

        private void AddNewRandomTextButton_Click(object sender, RoutedEventArgs e)
        {
            RandomTextVariableList.Children.Add(new RandTextVarLine(this, ""));
        }

        private void RandomTextNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_Parent.ItemText.Text = RandomTextNameTextBox.Text;
        }
    }
}
