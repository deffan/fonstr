﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for VariablesTab.xaml
    /// </summary>
    public partial class VariablesTab : UserControl
    {
        private VariableListItem m_Selected;
        private bool m_Cancel;

        public VariablesTab()
        {
            InitializeComponent();
            m_Cancel = false;
        }

        public void Save()
        {
            List<DataVariable> saveList = new List<DataVariable>();
            for (int i = 0; i < VariableList.Children.Count; i++)
            {
                VariableListItem item = (VariableList.Children[i] as VariableListItem);
                item.View.Save();
                saveList.Add(item.Variable);
            }
            GlobalHelper.DataVariables.SaveAndReload(saveList);
        }

        public void Load()
        {
            if (m_Cancel)
            {
                m_Cancel = false;
                return;
            }

            VariableList.Children.Clear();
            ViewPanel.Children.Clear();
            List<DataVariable> variables = GlobalHelper.DataVariables.GetCustomVariables();
            for(int i = 0; i < variables.Count; i++)
            {
                VariableList.Children.Add(new VariableListItem(this, variables[i]));
            }
        }

        public MessageBoxResult SaveCheck()
        {
            MessageBoxResult result = MessageBoxResult.No;
            if (HasChanges())
            {
                result = MessageBox.Show("Save changes?", "Save?", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    Save();
                }
                else if (result == MessageBoxResult.No)
                {
                    Load();
                }
                else 
                {
                    m_Cancel = true;
                }
            }
            return result;
        }

        public bool HasChanges()
        {
            for (int i = 0; i < VariableList.Children.Count; i++)
            {
                if ((VariableList.Children[i] as VariableListItem).View.HasChanges())
                    return true;
            }
            return false;
        }

        public void Select(VariableListItem item)
        {
            for (int i = 0; i < VariableList.Children.Count; i++)
            {
                (VariableList.Children[i] as VariableListItem).Select(false);
            }
            item.Select(true);
            m_Selected = item;

            ViewPanel.Children.Clear();
            ViewPanel.Children.Add(m_Selected.View as UserControl);
        }

        public void Remove(VariableListItem item)
        {
            // Check if the removed item is the selected one
            if (item.IsSelected())
            {
                // If so select the first not selected item instead
                for (int i = 0; i < VariableList.Children.Count; i++)
                {
                    VariableListItem varitem = VariableList.Children[i] as VariableListItem;
                    if (!varitem.IsSelected())
                    {
                        Select(varitem);
                        break;
                    }
                }
            }

            VariableList.Children.Remove(item);
        }

        private void AddTextVarButton_Click(object sender, RoutedEventArgs e)
        {
            DataVariable variable = new DataVariable(VARIABLE_TYPE.TEXT, Guid.NewGuid().ToString(), "New Text Variable", null, "", false);
            variable.Listdata.Add("");
            VariableListItem item = new VariableListItem(this, variable);
            VariableList.Children.Add(item);
        }

        private void AddRandNumberVarButton_Click(object sender, RoutedEventArgs e)
        {
            DataVariable variable = new DataVariable(VARIABLE_TYPE.RANDOM_NUMBER, Guid.NewGuid().ToString(), "New Random Number Variable", null, "", false);
            variable.Listdata.Add("0");
            variable.Listdata.Add("0");
            VariableListItem item = new VariableListItem(this, variable);
            VariableList.Children.Add(item);
        }

        private void AddRandTextVarButton_Click(object sender, RoutedEventArgs e)
        {
            VariableList.Children.Add(new VariableListItem(this, new DataVariable(VARIABLE_TYPE.RANDOM_TEXT, Guid.NewGuid().ToString(), "New Random Text Variable", null, "", false)));
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }
    }
}
