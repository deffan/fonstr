﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab.Variables
{
    /// <summary>
    /// Interaction logic for RandomNumberView.xaml
    /// </summary>
    public partial class RandomNumberView : UserControl, IVariableView
    {
        private DataVariable m_Variable;
        private VariableListItem m_Parent;

        public RandomNumberView()
        {
            InitializeComponent();
        }

        public RandomNumberView(VariableListItem parent)
        {
            InitializeComponent();
            m_Parent = parent;
        }

        public bool HasChanges()
        {
            if (!m_Variable.DisplayName.Equals(RandomNumberNameTextBox.Text))
                return true;

            if (!m_Variable.Listdata[0].Equals(RandomNumberFrom.Text))
                return true;

            if (!m_Variable.Listdata[1].Equals(RandomNumberTo.Text))
                return true;

            return false;
        }

        public void Load(DataVariable variable)
        {
            m_Variable = variable;
            RandomNumberNameTextBox.Text = m_Variable.DisplayName;
            RandomNumberFrom.Text = m_Variable.Listdata[0];
            RandomNumberTo.Text = m_Variable.Listdata[1];
        }

        public void Save()
        {
            m_Variable.DisplayName = RandomNumberNameTextBox.Text;
            m_Variable.Listdata[0] = RandomNumberFrom.Text;
            m_Variable.Listdata[1] = RandomNumberTo.Text;
        }

        private void RandomNumberNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_Parent.ItemText.Text = RandomNumberNameTextBox.Text;
        }
    }
}
