﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.SettingsTab.Variables;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for RandTextVarLine.xaml
    /// </summary>
    public partial class RandTextVarLine : UserControl
    {
        private RandomTextView m_Parent;

        public RandTextVarLine()
        {
            InitializeComponent();
        }

        public RandTextVarLine(RandomTextView parent, string text)
        {
            InitializeComponent();
            m_Parent = parent;
            TextField.Text = text;
        }

        public void SetText(string text)
        {
            TextField.Text = text;
        }

        public string GetText()
        {
            return TextField.Text;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            m_Parent.Remove(this);
        }
    }
}
