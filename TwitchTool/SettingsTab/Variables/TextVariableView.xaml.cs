﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab.Variables
{
    /// <summary>
    /// Interaction logic for TextVariableView.xaml
    /// </summary>
    public partial class TextVariableView : UserControl, IVariableView
    {
        private DataVariable m_Variable;
        private VariableListItem m_Parent;

        public TextVariableView()
        {
            InitializeComponent();
        }

        public TextVariableView(VariableListItem parent)
        {
            InitializeComponent();
            m_Parent = parent;
        }

        public bool HasChanges()
        {
            if (!TextVariableNameTextBox.Text.Equals(m_Variable.DisplayName))
                return true;

            if (!TextVariableTextBox.Text.Equals(m_Variable.Listdata[0]))
                return true;

            return false;
        }

        public void Load(DataVariable variable)
        {
            m_Variable = variable;
            TextVariableNameTextBox.Text = m_Variable.DisplayName;
            TextVariableTextBox.Text = m_Variable.Listdata[0];
        }

        public void Save()
        {
            m_Variable.DisplayName = TextVariableNameTextBox.Text;
            m_Variable.Listdata[0] = TextVariableTextBox.Text;
        }

        private void TextVariableNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_Parent.ItemText.Text = TextVariableNameTextBox.Text;
        }
    }
}
