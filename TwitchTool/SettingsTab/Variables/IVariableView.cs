﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.SettingsTab.Variables
{
    public interface IVariableView
    {
        bool HasChanges();
        void Save();
        void Load(DataVariable variable);
    }
}
