﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TwitchTool.SettingsTab
{
    /// <summary>
    /// Interaction logic for SettingsTab.xaml
    /// </summary>
    public partial class SettingsTab : UserControl
    {
        private TabItem m_Selected;
        private TabItem m_PrevSelected;
        private bool m_AlreadySelecting;

        public SettingsTab()
        {
            InitializeComponent();
            m_AlreadySelecting = false;
        }

        public bool HasChanges()
        {
            if (VariablesTab.HasChanges())
                return true;

            if (LiveTabSettings.HasChanges())
                return true;

            return false;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(sender.Equals(TabControl)))
                return;

            e.Handled = true;

            // Set selected
            m_PrevSelected = m_Selected;
            m_Selected = TabControl.SelectedItem as TabItem;

            if(m_AlreadySelecting)
            {
                return;
            }

            if (m_PrevSelected == m_Selected)
                return;

            if (m_PrevSelected != m_Selected)
            {
                if (m_PrevSelected == VariablesTabTabItem && VariablesTab.SaveCheck() == MessageBoxResult.Cancel)
                {
                    // Reselect
                    m_AlreadySelecting = true;
                    TabControl.SelectedItem = VariablesTabTabItem;
                    m_AlreadySelecting = false;
                    return;
                }
                else if (m_PrevSelected == LiveTabSettingsItem && LiveTabSettings.SaveCheck() == MessageBoxResult.Cancel)
                {
                    // Reselect
                    m_AlreadySelecting = true;
                    TabControl.SelectedItem = LiveTabSettingsItem;
                    m_AlreadySelecting = false;
                    return;
                }
            }

            if (m_Selected == VariablesTabTabItem)
            {
                VariablesTab.Load();
            }
            else if (m_Selected == TwitchTabItem)
            {
                TwitchTab.CheckLoginStatus();
            }
            else if (m_Selected == LiveTabSettingsItem)
            {
                LiveTabSettings.Load();
            }

        }
    }
}
