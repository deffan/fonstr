﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Animator;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool
{
    /// <summary>
    /// Interaction logic for WindowBuilder.xaml
    /// </summary>
    public partial class WindowBuilder : Window
    {
        private ObjectControl m_SelectedObject;
        private AddComponent m_AddComponentButtonMenu;
        private List<TabHeader> m_TabList;
        private List<AnimatorComponent> m_Animators;
        private List<CommandListenerComponent> m_Listeners;
        private List<ToolButton> m_TransformTools;
        private int m_SelectedTransformTool;
        private bool m_EditorMouseDown;
        private Point m_ClickDragAnchorPoint;
        private bool m_ObjectBorder;
        private bool m_SelectedBorder;
        private SolidColorBrush m_ObjectBorderColor;
        private SolidColorBrush m_SelectedBorderColor;
        private ObjectControl m_BaseObject;
        private string m_LatestSave;
        private string m_LastModified;
        private List<string> m_SelectionObjectCollisionList;
        private List<ObjectControl> m_CurrentObjects;
        private ObjectControl m_LastVisuallySelectedObject;
        private Rect m_MouseRect;
        private TreeViewItem m_Cut;
        private TreeViewItem m_Copy;

        public ObjectControl GetBaseObject()
        {
            return m_BaseObject;
        }

        public string GetLastModified()
        {
            return m_LastModified;
        }

        public List<CommandListenerComponent> GetListeners()
        {
            return m_Listeners;
        }

        public SolidColorBrush GetObjectBorderColor
        {
            get { return m_ObjectBorderColor; }
        }

        public SolidColorBrush GetSelectedBorderColor
        {
            get { return m_SelectedBorderColor; }
        }

        public bool UseObjectBorder
        {
            get { return m_ObjectBorder; }
        }

        public bool UseSelectedBorder
        {
            get { return m_SelectedBorder; }
        }

        public int SelectedTransformTool
        {
            get { return m_SelectedTransformTool; }
        }

        public bool EditorMouseDown
        {
            get { return m_EditorMouseDown; }
        }

        public WindowBuilder(string path)
        {
            InitializeComponent();
            GlobalHelper.EditorWindow = this;
            m_AddComponentButtonMenu = new AddComponent();
            m_TabList = new List<TabHeader>();
            m_Animators = new List<AnimatorComponent>();
            m_Listeners = new List<CommandListenerComponent>();
            m_ClickDragAnchorPoint = new Point();
            m_SelectionObjectCollisionList = new List<string>();
            m_CurrentObjects = new List<ObjectControl>();
            m_LastModified = "";
            m_MouseRect = new Rect();
            m_MouseRect.Height = 2;
            m_MouseRect.Width = 2;

            // Toolbar (transform tools)
            InitToolBar();

            // Save Button
            SaveButton.SetCallback(SaveWindow);
            SaveButton.SetUnselectable();

            LoadButton.SetCallback(NotImplementedYetFunc);
            LoadButton.SetUnselectable();

            // Borders 
            m_ObjectBorder = true;
            m_SelectedBorder = true;
            m_ObjectBorderColor = new SolidColorBrush();
            m_SelectedBorderColor = new SolidColorBrush();

            if(string.IsNullOrEmpty(path))
            {
                NewWindow();
            }
            else
            {
                LoadWindow(path);
            }

            // Load all current objects into this list
            ObjectControl.GetAllChildren(m_BaseObject, m_CurrentObjects);

            Task.Delay(100).ContinueWith(_ =>
            {
                Dispatcher.Invoke(new Action(() =>
                {
                    ObjectBorderColorPicker.SelectedColor = new SolidColorBrush(Color.FromArgb(200, 255, 200, 0)).Color;
                    SelectedBorderColorPicker.SelectedColor = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0)).Color;
                    WindowBorderColorPicker.SelectedColor = new SolidColorBrush(Color.FromArgb(100, 0, 200, 0)).Color;
                    m_BaseObject.ObjectBorder.BorderThickness = new Thickness(2);
                }));
            });
        }

        private void NotImplementedYetFunc(string s)
        {
            MessageBox.Show("Functionality not implemented yet.");
        }

        private void SaveWindow(string s)
        {
            try
            {
                // Create directory if needed
                string directory = Directory.GetCurrentDirectory() + "\\Windows\\" + m_BaseObject.ID;
                Directory.CreateDirectory(directory);

                // Save any open animator
                for (int i = 0; i < m_TabList.Count; i++)
                {
                    if (m_TabList[i].TabType == EDITORTABTYPE.Animator)
                    {
                        (m_TabList[i].TabParent.Content as AnimatorWindow).Save();
                    }
                }

                // Serialize/Save
                XDocument doc = XDocument.Load(directory + "\\Window.xml");

                // Version
                doc.Element("Root").Element("Version").Value = GlobalHelper.APPLICATION_VERSION;

                // Modified
                m_LastModified = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                doc.Element("Root").Element("Modified").Value = m_LastModified;

                // Save listener ids
                XElement listeners = doc.Element("Root").Element("Listeners");
                listeners.Nodes().Remove();
                for(int i = 0; i < m_Listeners.Count; i++)
                {
                    listeners.Add(new XElement("L", m_Listeners[i].ListenerID));
                }

                // The windows+objects+components
                doc.Element("Root").Element("Object").Remove();
                doc.Element("Root").Add(m_BaseObject.Serialize());
                doc.Save(directory + "\\Window.xml");
                m_LatestSave = doc.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save window.", "Save window", MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Log(LOG_LEVEL.ERROR, "SaveWindow" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }

        }

        private void InitToolBar()
        {
            m_TransformTools = new List<ToolButton>();
            for (int i = 0; i < TransformToolbar.Children.Count; i++)
            {
                m_TransformTools.Add((TransformToolbar.Children[i] as ToolButton));

                // Called when selection occurs. Should deselect all others and activate selected tool.
                m_TransformTools[i].SetCallback(new Action<string>((string tool) =>  
                {
                    for(int n = 0; n < m_TransformTools.Count; n++)
                    {
                        if (m_TransformTools[n].ToolIdLabel.Content.Equals(tool))
                        {
                            // Activate this tool...
                            switch (tool)
                            {
                                case "move": m_SelectedTransformTool = EDITOR_TRANSFORM_TOOL.MOVE; break;  
                                case "select": m_SelectedTransformTool = EDITOR_TRANSFORM_TOOL.SELECT; break; 
                                case "resize": m_SelectedTransformTool = EDITOR_TRANSFORM_TOOL.RESIZE; break; 
                                case "scale": m_SelectedTransformTool = EDITOR_TRANSFORM_TOOL.SCALE; break;  
                                case "rotate": m_SelectedTransformTool = EDITOR_TRANSFORM_TOOL.ROTATE; break;
                                case "skew": m_SelectedTransformTool = EDITOR_TRANSFORM_TOOL.SKEW; break;
                            }
                            continue;
                        }

                        // Deselect
                        m_TransformTools[n].Deselect();
                    }
                    m_SelectionObjectCollisionList.Clear();
                }));
            }

            // Scale animation (scale x, scale y textboxar)
            // Size animation (size x, size y textboxar)
            // Transpareny (en textbox)

            // Sen skall man nog ändra, så att man kan lägga till/ta bort relevanta animationer på varje objekt. En drop-down meny kanske på objektet i sig?
            // Med checkboxar.
            // Detta gör att vi inte ska spara eller automatiskt skapa tracks för det som inte skapades eller finns.

            // Select the select tool
            m_TransformTools[0].Select();
            m_SelectedTransformTool = EDITOR_TRANSFORM_TOOL.SELECT;
        }

        private void NewWindow()
        {
            // Create the root treeview item
            TreeViewItem root = AddTreeViewItem(null, "window.png", "New Window", FontWeights.Normal);

            m_BaseObject = new ObjectControl();
            m_BaseObject.InitDefaultObject();

            // Disable some parts of the Basic and Transform components for the window
            BasicComponent b = m_BaseObject.GetComponent<BasicComponent>() as BasicComponent;
            b.m_EditorComponent.ActiveCheckBox.IsEnabled = false;
            b.m_EditorComponent.TransparencyBox.IsEnabled = false;

            TransformComponent t = m_BaseObject.GetComponent<TransformComponent>() as TransformComponent;
            t.m_EditorComponent.XTextBox.IsEnabled = false;
            t.m_EditorComponent.YTextBox.IsEnabled = false;
            t.m_EditorComponent.XScale.IsEnabled = false;
            t.m_EditorComponent.YScale.IsEnabled = false;
            t.m_EditorComponent.RotationBox.IsEnabled = false;
            t.m_EditorComponent.RotationOriginCombo.IsEnabled = false;
            t.m_EditorComponent.ScaleOriginCombo.IsEnabled = false;
            t.m_EditorComponent.XSkewTextBox.IsEnabled = false;
            t.m_EditorComponent.YSkewTextBox.IsEnabled = false;

            // Default window size
            t.SetSize(640, 480);

            m_BaseObject.ID = Guid.NewGuid().ToString();
            m_BaseObject.TreeViewItem = root;
            m_BaseObject.ObjectName = "New Window";
            root.Tag = m_BaseObject;
            root.IsSelected = true;
            m_SelectedObject = m_BaseObject;

            m_BaseObject.ForceSelectObject(1);
            m_BaseObject.SetSelectable(false);

            EditorWindowBorder.Child = m_BaseObject;
        }

        private void LoadWindow(string pathToWindowXML)
        {
            // Load the XML
            try
            {
                XDocument doc = XDocument.Load(pathToWindowXML + "\\Window.xml");
                m_LatestSave = doc.ToString();
                m_LastModified = doc.Element("Root").Element("Modified").Value;
                EditorWindowBorder.Child = null;
                m_BaseObject = ObjectControl.Deserialize(doc.Element("Root").Element("Object"), null, true);
                EditorWindowBorder.Child = m_BaseObject;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Unable to load window due to invalid or corrupt data.", "Load window", MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Log(LOG_LEVEL.ERROR, "LoadWindow" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);

                try { Close(); } catch { }
                return;
            }

            // Disable some parts of the Basic and Transform components for the window
            BasicComponent b = m_BaseObject.GetComponent<BasicComponent>() as BasicComponent;
            b.m_EditorComponent.ActiveCheckBox.IsEnabled = false;
            b.m_EditorComponent.TransparencyBox.IsEnabled = false;

            TransformComponent t = m_BaseObject.GetComponent<TransformComponent>() as TransformComponent;
            t.m_EditorComponent.XTextBox.IsEnabled = false;
            t.m_EditorComponent.YTextBox.IsEnabled = false;
            t.m_EditorComponent.XScale.IsEnabled = false;
            t.m_EditorComponent.YScale.IsEnabled = false;
            t.m_EditorComponent.RotationBox.IsEnabled = false;
            t.m_EditorComponent.RotationOriginCombo.IsEnabled = false;
            t.m_EditorComponent.ScaleOriginCombo.IsEnabled = false;
            t.m_EditorComponent.XSkewTextBox.IsEnabled = false;
            t.m_EditorComponent.YSkewTextBox.IsEnabled = false;

            // Fill the treeview
            TreeViewItem root = AddObjectToTree(null, m_BaseObject);
            m_BaseObject.TreeViewItem = root;
            root.Tag = m_BaseObject;
            root.IsSelected = true;
            m_SelectedObject = m_BaseObject;

            m_BaseObject.ForceSelectObject(1);
            m_BaseObject.SetSelectable(false);
        }

        private TreeViewItem AddObjectToTree(TreeViewItem parent, ObjectControl o)
        {
            // Create the treeview item
            TreeViewItem item = AddTreeViewItem(parent, "opensquare.png", o.ObjectName, FontWeights.Normal);

            // Link treeviewitem with the object and back
            item.Tag = o;
            o.TreeViewItem = item;

            for (int i = 0; i < o.Children.Count; i++)
            {
                if (o.Children[i] is ObjectControl)
                {
                    AddObjectToTree(item, o.Children[i] as ObjectControl);
                }
            }
            return item;
        }

        private void AddNewObject(TreeViewItem treeParent, string name)
        {
            // Add treeviewitem
            TreeViewItem item = AddTreeViewItem(treeParent, "opensquare.png", name, FontWeights.Normal);

            // Create the ObjectControl, and references to item and back
            ObjectControl newObject = new ObjectControl();
            newObject.ID = Guid.NewGuid().ToString();
            newObject.InitDefaultObject();
            newObject.ObjectName = name;
            newObject.TreeViewItem = item;
            newObject.ObjectParent = treeParent.Tag as ObjectControl;
            newObject.GetTransformComponent().SetSize(55, 55);
           // newObject.AddComponent(new ImageComponent(newObject));          // TEST!!!!!!!!!!!!!!!!!!!!!!!
           // newObject.AddComponent(new TextComponent(newObject));           // Test..!
            item.Tag = newObject;

            // Add to the window
            if (m_BaseObject.TreeViewItem.Equals(treeParent))
            {
                m_BaseObject.Children.Add(newObject);
            }
            else
            {
                ((ObjectControl)treeParent.Tag).Children.Add(newObject);
            }
            treeParent.IsExpanded = true;
            item.IsSelected = true;
            ObjectAdded(newObject);
        }

        private void RenameObject(TreeViewItem item)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null)
                return;
        }

        private void MoveObject(TreeViewItem item)
        {
            // Move an object (and naturally all its children) to a new parent.
            ObjectMoved();
        }

        private void RemoveObject(ObjectControl o)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null)
                return;

            if (m_BaseObject.TreeViewItem.Equals(selected))
            {
                MessageBox.Show("This object cannot be removed.", "Window Object", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            // Removes an object, and all its children.
            ObjectRemoved(o);

        }


        private TreeViewItem AddTreeViewItem(TreeViewItem parent, string icon, string labelText, FontWeight fontStyle)
        {
            TreeViewItem item = new TreeViewItem();

            // Add a stack panel to contain [Image][TextLabel]
            StackPanel stack = new StackPanel();
            stack.Orientation = Orientation.Horizontal;

            // Image
            Image image = new Image();
            image.Source = GlobalHelper.GetImage(icon);
            image.Width = 16;

            // Label
            Label lbl = new Label();
            lbl.Name = "TreeViewItemTextLabel";
            lbl.Content = labelText;
            lbl.FontWeight = fontStyle;

            // Add them, and to stackpanel
            stack.Children.Add(image);
            stack.Children.Add(lbl);
            item.Header = stack;

            // Add to TreeView
            if(parent == null)
            {
                ObjectTreeView.Items.Add(item);
            }
            else
            {
                parent.Items.Add(item);
            }
            return item;
        }

        private void PrintAllChildren(ObjectControl c)
        {
            if(c.Children.Count > 0)
            {
                for (int i = 0; i < c.Children.Count; i++)
                {
                    System.Diagnostics.Debug.WriteLine(c.Children[i]);

                    if (c.Children[i] is ObjectControl)
                    {
                        PrintAllChildren(c.Children[i] as ObjectControl);
                    }
                }
            }
        }

        private void ReSelectAll(ObjectControl c)
        {
            if (c.Children.Count > 0)
            {
                for (int i = 0; i < c.Children.Count; i++)
                {
                    if (c.Children[i] is ObjectControl)
                    {
                        ReSelectAll(c.Children[i] as ObjectControl);
                    }
                }
            }
            c.ReSelect();
        }

        private void AddNewEmptyObject_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null)
            {
                AddNewObject(m_BaseObject.TreeViewItem, "New object");
            }
            else
            {
                AddNewObject(selected, "New object");
            }

            // Reload all current objects into this list
            m_CurrentObjects.Clear();
            ObjectControl.GetAllChildren(m_BaseObject, m_CurrentObjects);
        }

        private void RemoveObject_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null || sender == null)
            {
                return;
            }

            // Cannot delete baseobject
            if(((ObjectControl)selected.Tag) == m_BaseObject)
            {
                return;
            }

            if (MessageBox.Show("This will permanently delete this object and all its children.\nContinue?", "Remove", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
            {
                if (selected.Parent is TreeViewItem)
                {
                    (selected.Parent as TreeViewItem).Items.Remove(selected);
                }
                else
                {
                    ObjectTreeView.Items.Remove(ObjectTreeView.SelectedItem);
                }
            }

            // Remove object from its parent
            ObjectControl theObject = selected.Tag as ObjectControl;
            theObject.ObjectParent.Children.Remove(theObject);
            theObject.ObjectParent = null;

            // Reload all current objects into this list
            m_CurrentObjects.Clear();
            ObjectControl.GetAllChildren(m_BaseObject, m_CurrentObjects);
        }

        private void ObjectTreeView_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            // On right click, we want to select first. So we must search...
            TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                treeViewItem.Focus();
                treeViewItem.IsSelected = true;
            }
        }

        static TreeViewItem VisualUpwardSearch(DependencyObject source)
        {
            while (source != null && !(source is TreeViewItem))
                source = VisualTreeHelper.GetParent(source);

            return source as TreeViewItem;
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            EditorWindowBorder.Child = null;
            Window testWindow = null;
            try
            {
                testWindow = new Window
                {
                    Content = new Grid(),
                    Title = "My User Control Dialog",
                    SizeToContent = SizeToContent.WidthAndHeight,
                    ResizeMode = ResizeMode.NoResize
                };
                (testWindow.Content as Grid).Children.Add(m_BaseObject);

                //    testWindow.WindowStartupLocation = WindowStartupLocation.Manual;  
                //    testWindow.Left = -(m_BaseObject.ActualWidth + 100);
                //    testWindow.Top = -(m_BaseObject.ActualHeight + 100);
                //    testWindow.ShowInTaskbar = false;
                //    testWindow.Focusable = false;
                testWindow.ShowDialog();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Play Window", MessageBoxButton.OK, MessageBoxImage.Error);
                MessageBox.Show(ex.Message);
                Logger.Log(LOG_LEVEL.ERROR, "PlayButton_Click" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
            (testWindow.Content as Grid).Children.Remove(m_BaseObject);
            EditorWindowBorder.Child = m_BaseObject;
        }

        private void ObjectTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null)
                return;

            // Clear the Inspector panel, and repopulate the components
            SelectedObjectList.Children.Clear();
            ObjectControl c = selected.Tag as ObjectControl;

            List<IComponent> components = c.GetComponents();
            for (int i = 0; i < components.Count; i++)
            {
                SelectedObjectList.Children.Add(components[i].GetEditorComponent() as UserControl);
            }

            // Add the "Add Component" menu option last
            if (!m_BaseObject.TreeViewItem.Equals(selected))
            {
                m_AddComponentButtonMenu.SetObject(c);
                SelectedObjectList.Children.Add(m_AddComponentButtonMenu);
            }

            // Deselect
            if (m_SelectedObject != null)
                m_SelectedObject.Select(false);

            // Set and select
            m_SelectedObject = c;
            c.Select(true);

        }

        private void ConfigureCmdButton_Click(object sender, RoutedEventArgs e)
        {
            //   XDocument doc = new XDocument();
            //   XElement root = new XElement("Root");
            //   doc.Add(root);
            //   root.Add(m_BaseObject.Serialize());


            

           // XDocument doc = XDocument.Load("textexport.xml");
          //  m_BaseObject = ObjectControl.Deserialize(doc.Element("Root").Element("Object"));

            //doc.Save("textexport.xml");
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MainWindowContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            EditorWindowBorder.Margin = new Thickness(MainWindowContainer.ActualWidth / 2, MainWindowContainer.ActualHeight / 2, MainWindowContainer.ActualWidth / 2, MainWindowContainer.ActualHeight / 2);

            Task.Delay(100).ContinueWith(_ =>
            {
                Dispatcher.Invoke(new Action(() => 
                {
                    ScrollViewer vertScroll = MainWindowContainer.Template.FindName("_tv_scrollviewer_", MainWindowContainer) as ScrollViewer;
                    vertScroll.ScrollToVerticalOffset(vertScroll.ScrollableHeight / 2);
                    vertScroll.ScrollToHorizontalOffset(vertScroll.ScrollableWidth / 2);
                }));
            });
        }

        private void MainTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        public void ObjectRemoved(ObjectControl o)
        {
            RebuildAnimators();
        }

        public void ObjectAdded(ObjectControl o)
        {
            RebuildAnimators();
        }

        public void ObjectMoved()
        {
            RebuildAnimators();
        }

        public void RebuildAnimators()
        {
            for (int i = 0; i < m_Animators.Count; i++)
            {
                m_Animators[i].MarkForRebuild();
            }
        }

        public void AddAnimator(AnimatorComponent a)
        {
            m_Animators.Add(a);
        }

        public void RemoveAnimator(AnimatorComponent a)
        {
            m_Animators.Remove(a);
        }

        public void AddListener(CommandListenerComponent a)
        {
            m_Listeners.Add(a);
        }

        public void RemoveListener(CommandListenerComponent a)
        {
            m_Listeners.Remove(a);
        }

        public void CloseTab(TabHeader tabHeader)
        {
            for (int i = 0; i < m_TabList.Count; i++)
            {
                if (m_TabList[i].ObjectId.Equals(tabHeader.ObjectId))
                {
                    tabHeader.CloseTab();
                    MainTabControl.Items.Remove(m_TabList[i].TabParent);
                    m_TabList.RemoveAt(i);
                    return;
                }
            }
        }

        // Close the tab which is identified by this ID, either object or component
        public void CloseTab(string id)
        {
            for (int i = 0; i < m_TabList.Count; i++)
            {
                if (m_TabList[i].ObjectId.Equals(id))
                {
                    m_TabList[i].CloseTab();
                    MainTabControl.Items.Remove(m_TabList[i].TabParent);
                    m_TabList.RemoveAt(i);
                    return;
                }
            }
        }

        public void OpenTab(IEditorTab tabObject)
        {
            // Open existing tab
            for (int i = 0; i < m_TabList.Count; i++)
            {
                if(m_TabList[i].ObjectId.Equals(tabObject.GetID()))
                {
                    m_TabList[i].TabParent.IsSelected = true;
                    return;
                }
            }

            // New tab
            TabItem tabItem = null;
            TabHeader tabHeader = null;
            switch (tabObject.GetType().Name)
            {
                case "AnimatorComponent":
                {
                    AnimatorComponent animatorComponent = tabObject as AnimatorComponent;

                    // Create tabheader
                    tabItem = new TabItem();
                    tabHeader = new TabHeader("Animator - " + animatorComponent.m_Object.ObjectName, "clapper.png", animatorComponent.GetID(), EDITORTABTYPE.Animator);
                    tabHeader.TabParent = tabItem;
                    tabItem.Header = tabHeader;

                    // Create animator window, set data, and put as content of tabitem.
                    AnimatorWindow animatorWindow = new AnimatorWindow();
                    animatorWindow.SetAnimatorData(animatorComponent);
                    tabItem.Content = animatorWindow;
                    break;
                }
                default: throw new Exception("No matching IEditorTab found in OpenTab(new)");
            }

            // Add tab to tabcontrol and tablist
            int index = MainTabControl.Items.Add(tabItem);
            m_TabList.Add(tabHeader);
            tabItem.IsSelected = true;
        }

        public void RefreshTab(IEditorTab tabObject)
        {
            // Refresh open tab, otherwise do nothing...
            for (int i = 0; i < m_TabList.Count; i++)
            {
                if (m_TabList[i].ObjectId.Equals(tabObject.GetID()))
                {
                    switch (tabObject.GetType().Name)
                    {
                        case "AnimatorComponent":
                        {
                            (m_TabList[i].TabParent.Content as AnimatorWindow).Refresh();   // Refresh, because objects were added/removed (?) dont remember why.
                            break;
                        }
                        default: throw new Exception("No matching IEditorTab found in OpenTab(new)");
                    }
                    break;
                }
            }
        }

        private bool BoxCollision(Rect rect1, Rect rect2)
        {
            return (rect1.X < rect2.X + rect2.Width && rect1.X + rect1.Width > rect2.X && rect1.Y < rect2.Y + rect2.Height && rect1.Height + rect1.Y > rect2.Y);
        }

        private void SelectionCollision()
        {
            Point mouseCoords = Mouse.GetPosition(m_BaseObject);

            for(int i = 0; i < m_CurrentObjects.Count; i++)
            {
                // Must get coordinates correctly for each object
                mouseCoords = Mouse.GetPosition(m_CurrentObjects[i].ObjectParent);  
                m_MouseRect.X = mouseCoords.X;
                m_MouseRect.Y = mouseCoords.Y;

                // Do we have this object in the collision-list?
                bool isCollidedWith = m_SelectionObjectCollisionList.Contains(m_CurrentObjects[i].ID);

                // Check if mouse collides with this object
                if (BoxCollision(m_MouseRect, m_CurrentObjects[i].GetTransformComponent().GetRect()))
                {
                    // If we are, but we havent before, we visually select this one.
                    if(!isCollidedWith)
                    {
                        m_SelectionObjectCollisionList.Add(m_CurrentObjects[i].ID);

                        // Visually select
                        m_CurrentObjects[i].VisuallySelectObject(true);
                        m_LastVisuallySelectedObject = m_CurrentObjects[i];
                    }
                }
                else
                {
                    // We are not colliding with this object. Check if we did, and remove if so.
                    if (isCollidedWith)
                    {
                        // Visually deselect and remove
                        m_CurrentObjects[i].VisuallySelectObject(false);
                        m_SelectionObjectCollisionList.Remove(m_CurrentObjects[i].ID);
                    }
                }
            }
        }

        private void MainWindowContainer_MouseMove(object sender, MouseEventArgs e)
        {
            if (SelectedTransformTool == EDITOR_TRANSFORM_TOOL.SELECT && !m_EditorMouseDown)
            {
                SelectionCollision();
            }
            else if(m_EditorMouseDown)
            {
                if (m_BaseObject.Equals(m_SelectedObject))
                    return;

                Point mouseCoords = Mouse.GetPosition(this);
                Point objectCoords = m_SelectedObject.GetTransformComponent().GetCoordinates();
                Size objectSize = m_SelectedObject.GetTransformComponent().GetSize();

                if (SelectedTransformTool == EDITOR_TRANSFORM_TOOL.RESIZE)
                {
                    mouseCoords = Mouse.GetPosition(m_SelectedObject.ObjectParent);

                    double newSizeX = mouseCoords.X - objectCoords.X;
                    double newSizeY = mouseCoords.Y - objectCoords.Y;

                    if (newSizeX < 0)
                        newSizeX *= -1;

                    if (newSizeY < 0)
                        newSizeY *= -1;

                    m_SelectedObject.GetTransformComponent().SetSize(newSizeX, newSizeY);
                }
                else if (SelectedTransformTool == EDITOR_TRANSFORM_TOOL.SCALE)
                {

                    mouseCoords = Mouse.GetPosition(m_SelectedObject.ObjectParent);
                    Size objectScale = m_SelectedObject.GetTransformComponent().GetScale();

                    double scalew = Math.Abs((objectCoords.X + objectSize.Width) - mouseCoords.X) / (objectSize.Width + 0.001);
                    double scaleh = Math.Abs((objectCoords.Y + objectSize.Height) - mouseCoords.Y) / (objectSize.Height + 0.001);

                    m_SelectedObject.GetTransformComponent().SetScale(scalew, scaleh);
                }
                else if (SelectedTransformTool == EDITOR_TRANSFORM_TOOL.MOVE)
                {
                    double x = objectCoords.X;
                    double y = objectCoords.Y;
                    x += (mouseCoords.X - m_ClickDragAnchorPoint.X);
                    y += (mouseCoords.Y - m_ClickDragAnchorPoint.Y);
                    m_ClickDragAnchorPoint = mouseCoords;
                    m_SelectedObject.GetTransformComponent().SetCoordinates(x, y);
                }
                else if (SelectedTransformTool == EDITOR_TRANSFORM_TOOL.ROTATE)
                {
                    mouseCoords = Mouse.GetPosition(m_SelectedObject.ObjectParent);
                    double angle = Math.Atan2(mouseCoords.Y - objectCoords.Y, mouseCoords.X - objectCoords.X);
                    angle = angle * (180.0 / Math.PI);
                    if(angle < 0.0)
                    {
                        angle += 360;
                    }
                    m_SelectedObject.GetTransformComponent().SetRotation(angle);
                }
                else if (SelectedTransformTool == EDITOR_TRANSFORM_TOOL.SKEW)
                {
                    mouseCoords = Mouse.GetPosition(m_SelectedObject.ObjectParent);
                    double anglex = Math.Abs(objectCoords.X - mouseCoords.X) % 180;
                    double angley = Math.Abs(objectCoords.Y - mouseCoords.Y) % 180;

                    m_SelectedObject.GetTransformComponent().SetSkew(anglex, angley);
                }
            }
        }

        private void MainWindowContainer_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            m_ClickDragAnchorPoint = Mouse.GetPosition(this);
            m_EditorMouseDown = true;

            // If select, and something is visually selected, select that for real.
            if (SelectedTransformTool == EDITOR_TRANSFORM_TOOL.SELECT)
            {
                // If nothing is currently selected, set m_LastVisuallySelectedObject to null
                if (m_SelectionObjectCollisionList.Count == 0)
                {
                    m_LastVisuallySelectedObject = null;
                }

                // Do a real selection of the object we are hovering over
                if (m_LastVisuallySelectedObject != null)
                {
                    m_LastVisuallySelectedObject.TreeViewItem.IsSelected = true;
                    m_LastVisuallySelectedObject = null;
                }
            }
        }

        private void MainWindowContainer_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            m_EditorMouseDown = false;
        }

        private void MainWindowContainer_MouseLeave(object sender, MouseEventArgs e)
        {
            m_EditorMouseDown = false;
        }

        private void ObjectBorderColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            m_ObjectBorderColor = new SolidColorBrush(ObjectBorderColorPicker.SelectedColor.Value);
            if (m_BaseObject != null)
                ReSelectAll(m_BaseObject);
        }

        private void SelectedBorderColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            m_SelectedBorderColor = new SolidColorBrush(SelectedBorderColorPicker.SelectedColor.Value);
            if (m_BaseObject != null)
                ReSelectAll(m_BaseObject);
        }

        private void WindowBorderColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            if (m_BaseObject != null)
                m_BaseObject.ObjectBorder.BorderBrush = new SolidColorBrush(WindowBorderColorPicker.SelectedColor.Value);
        }

        private void ObjectBorderCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            m_ObjectBorder = true;
            if(m_BaseObject != null)
                ReSelectAll(m_BaseObject);
        }

        private void ObjectBorderCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            m_ObjectBorder = false;
            if (m_BaseObject != null)
                ReSelectAll(m_BaseObject);
        }

        private void SelectedBorderCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            m_SelectedBorder = true;
            if (m_BaseObject != null)
                ReSelectAll(m_BaseObject);
        }

        private void SelectedBorderCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            m_SelectedBorder = false;
            if (m_BaseObject != null)
                ReSelectAll(m_BaseObject);
        }

        private void WindowBorderColorCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (m_BaseObject != null)
                m_BaseObject.ObjectBorder.BorderThickness = new Thickness(2);
        }

        private void WindowBorderColorCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_BaseObject != null)
                m_BaseObject.ObjectBorder.BorderThickness = new Thickness(0);
        }

        private bool ListenerConflict()
        {
            // Check that all Listeners are unique. They must be.
            List<string> tmp = new List<string>();
            for (int i = 0; i < m_Listeners.Count; i++)
            {
                if (tmp.Contains(m_Listeners[i].ListenerID))
                {
                    MessageBox.Show(string.Format("Listener ID's must be unique.\nThe listener id '{0}' is not unique and must be changed.", m_Listeners[i].ListenerID), "Listener Id Conflict", MessageBoxButton.OK, MessageBoxImage.Information);
                    return true;
                }
                else
                {
                    tmp.Add(m_Listeners[i].ListenerID);
                }
            }
            return false;
        }

        private void EditorWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Serialize the window, then compare with latest save to see if we have unsaved changes or not
            XDocument doc = new XDocument();
            XElement root = new XElement("Root");
            doc.Add(root);

            // Version
            root.Add(new XElement("Version", GlobalHelper.APPLICATION_VERSION));

            // Modified
            root.Add(new XElement("Modified", m_LastModified));

            root.Add(new XElement("LocationX", 0));
            root.Add(new XElement("LocationY", 0));

            // Save listener ids
            XElement listeners = new XElement("Listeners");
            for (int i = 0; i < m_Listeners.Count; i++)
            {
                listeners.Add(new XElement("L", m_Listeners[i].ListenerID));
            }
            root.Add(listeners);
            root.Add(m_BaseObject.Serialize());

            if (!doc.ToString().Equals(m_LatestSave))
            {
                MessageBoxResult r = MessageBox.Show("Save changes?", "Save?", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if(r.Equals(MessageBoxResult.Yes))
                {
                    if(ListenerConflict())
                    {
                        e.Cancel = true;
                        return;
                    }

                    SaveWindow("");
                }
                else if (r.Equals(MessageBoxResult.Cancel))
                {
                    e.Cancel = true;
                }
            }
        }

        private void ObjectTreeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null || sender == null)
                return;

            // Disable/Enable Paste option
            if (m_Cut == null && m_Copy == null)
            {
                (LeftSideMenu.Items[4] as MenuItem).IsEnabled = false;
            }
            else
            {
                (LeftSideMenu.Items[4] as MenuItem).IsEnabled = true;
            }

            // No cut/copy if base object
            bool cutCopyEnable = !m_BaseObject.TreeViewItem.Equals(selected);
            (LeftSideMenu.Items[2] as MenuItem).IsEnabled = cutCopyEnable;
            (LeftSideMenu.Items[3] as MenuItem).IsEnabled = cutCopyEnable;

            ContextMenu cm = LeftSideMenu;
            cm.PlacementTarget = sender as TreeViewItem;
            cm.IsOpen = true;
        }

        private void CopyObject_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null || sender == null)
                return;

            // Set COPY null out CUT
            m_Copy = selected;
            m_Cut = null;
        }

        private void CutObject_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null || sender == null)
                return;

            // Remove treeviewitem from tree
            (selected.Parent as TreeViewItem).Items.Remove(selected);

            // Remove object from its parent
            ObjectControl theObject = selected.Tag as ObjectControl;
            theObject.ObjectParent.Children.Remove(theObject);
            theObject.ObjectParent = null;

            // Set CUT null out COPY
            m_Copy = null;
            m_Cut = selected;

            // Reload all current objects into this list
            m_CurrentObjects.Clear();
            ObjectControl.GetAllChildren(m_BaseObject, m_CurrentObjects);
        }

        private void PasteObject_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem selected = ObjectTreeView.SelectedItem as TreeViewItem;
            if (selected == null || sender == null)
                return;

            if (m_Cut != null)
            {
                // Add treeviewitem to tree
                selected.Items.Add(m_Cut);

                // Re-Add object to new parent
                ObjectControl newParent = selected.Tag as ObjectControl;
                ObjectControl theObject = m_Cut.Tag as ObjectControl;
                theObject.ObjectParent = newParent;
                newParent.Children.Add(theObject);

                m_Cut = null;
            }
            else if(m_Copy != null)
            {
                // Use cloning component to create a clone of the object.
                // Make new ID's and remove its original parent
                CloningComponent cc = new CloningComponent(null, true);
                ObjectControl clone = cc.Clone(m_Copy.Tag as ObjectControl, true);
                cc.GenerateNewIds(clone, true);

                // Add new parent / object structure
                ObjectControl newParent = selected.Tag as ObjectControl;
                clone.ObjectParent = newParent;
                newParent.Children.Add(clone);

                // Add new treeview item / treestructure
                TreeViewItem item = AddObjectToTree(selected as TreeViewItem, clone);
                item.IsSelected = true;

                m_Copy = null;
            }
            ObjectAdded(null);

            // Reload all current objects into this list
            m_CurrentObjects.Clear();
            ObjectControl.GetAllChildren(m_BaseObject, m_CurrentObjects);

        }

        private void ObjectTreeView_Drop(object sender, DragEventArgs e)
        {

        }
    }
}
