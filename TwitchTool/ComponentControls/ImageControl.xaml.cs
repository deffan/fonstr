﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Components;
using WpfAnimatedGif;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ImageControl.xaml
    /// </summary>
    public partial class ImageControl : UserControl
    {
        private ObjectControl m_Parent;
        private ImageComponent m_ImageComponent;

        public ImageControl(ObjectControl parent, ImageComponent component)
        {
            InitializeComponent();
            m_Parent = parent;
            m_ImageComponent = component;
        }

        public void SetImage(BitmapImage img)
        {
            // Below is an ugly hack... Because each time you want to set another image, it will fail the first time...
            bool animatedSet = false;
            try
            {
                TheImage.Source = img;
                ImageBehavior.SetAnimatedSource(TheImage, TheImage.Source);
                animatedSet = true;
            }
            catch (Exception) { }

            if(!animatedSet)
            {
                TheImage.Source = img;
                ImageBehavior.SetAnimatedSource(TheImage, TheImage.Source);
            }

            // If downloading, we dont have the entire image yet
            if (img.IsDownloading)
            {
                img.DownloadCompleted += Img_DownloadCompleted;
            }
            else
            {
                ImageLoaded(img.PixelHeight, img.PixelHeight);
            }

        }

        private void ImageLoaded(int width, int height)
        {
            Size s = m_Parent.GetTransformComponent().GetSize();
            TheImage.Height = s.Height;
            TheImage.Width = s.Width;
            m_ImageComponent.ImageUpdated(width, height);

        }

        private void Img_DownloadCompleted(object sender, EventArgs e)
        {
            try
            {
                BitmapImage img = sender as BitmapImage;
                ImageLoaded(img.PixelHeight, img.PixelHeight);
            }
            catch(Exception) { }
        }
    }
}

