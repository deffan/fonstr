﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using TwitchTool.Components;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for TextControl.xaml
    /// </summary>
    public partial class TextControl : UserControl
    {
        private ObjectControl m_Parent;
        private TextComponent m_TextComponent;
        private string m_OriginalText;  // Must save the pre-formatted text because it looks different afterwards
        private bool m_TextWrapping;
        private bool m_Emoticons;

        private Brush m_StrokeBrush;
        private double m_StrokeSize;
        private bool m_Stroke;

        public bool m_Underline;


        public TextControl(ObjectControl parent, TextComponent component)
        {
            InitializeComponent();
            m_Parent = parent;
            m_TextComponent = component;
            m_OriginalText = "";
            m_StrokeSize = 0;
            m_Stroke = false;
            m_Emoticons = false;
        }

        public int GetEffect()
        {
            if (m_Stroke)
                return 1;
            return 0;
        }

        public double GetStrokeSize()
        {
            return m_StrokeSize;
        }

        public string GetOriginalText()
        {
            return m_OriginalText;
        }

        private void UpdateSize()
        {
            // Only resize what is needed if needed
            Size s = m_Parent.GetTransformComponent().GetSize();
            double x = s.Width;
            double y = s.Height;

            if (TheText.ActualWidth > s.Width || TheText.ActualHeight > s.Height)
            {
                if (!m_TextWrapping && TheText.ActualWidth > s.Width)
                    x = TheText.ActualWidth;

                if (TheText.ActualHeight > s.Height)
                    y = TheText.ActualHeight;

                m_Parent.GetTransformComponent().SetSize(x, y);
            }
        }

        public void SetTextWrapping(bool on, bool rebuild)
        {
            m_TextWrapping = on;
            if (on)
            {
                TextGrid.Width = m_Parent.GetTransformComponent().GetSize().Width;
                TheText.TextWrapping = TextWrapping.Wrap;

            }
            else
            {
                TextGrid.Width = Double.NaN;
                TheText.TextWrapping = TextWrapping.NoWrap;
            }

            if(rebuild)
            {
                RebuildText();
            }
        }

        public void SetTwitchEmoticons(bool use, bool rebuild)
        {
            m_Emoticons = use;

            if (rebuild)
            {
                RebuildText();
            }
        }

        public bool UseTwitchEmoticons()
        {
            return m_Emoticons;
        }

        public void SetTextAlignment(TextAlignment alignment, bool rebuild)
        {
            // Set MinWith to the parent "TextGrid" to make TextAlignment work correctly
            if (alignment == TextAlignment.Left)
            {
                TextGrid.MinWidth = 0;
            }
            else
            {
                TextGrid.MinWidth = m_Parent.GetTransformComponent().GetSize().Width;
            }

            TheText.TextAlignment = alignment;
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetText(string txt)
        {
            m_OriginalText = txt;
            TextImageParser(txt);

            // We have to make this in a delayed fashion because there seems to be no appropiate event for this?
            Task.Delay(100).ContinueWith(_ =>
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => { UpdateSize(); }));
            });
        }

        public void RebuildText()
        {
            SetText(m_OriginalText);
        }

        public void SetAll(FontFamily font, double fontSize, bool bold, bool italic, bool underline, bool textwrapping, TextAlignment alignment, int effect, double strokesize, Brush textBrush, Brush effectBrush, bool twitchEmotes)
        {
            SetFont(font, false);
            SetTextSize(fontSize, false);
            SetBold(bold, false);
            SetItalic(italic, false);
            SetTextWrapping(textwrapping, false);
            SetTextAlignment(alignment, false);
            SetEffect(effect, false);
            SetStrokeSize(strokesize, false);
            SetTextBrush(textBrush, false);
            SetStrokeBrush(effectBrush, false);
            SetTwitchEmoticons(twitchEmotes, false);
            RebuildText();
        }

        public void SetBold(bool bold, bool rebuild)
        {
            if (bold)
            {
                TheText.FontWeight = FontWeights.Bold;
            }
            else
            {
                TheText.FontWeight = FontWeights.Normal;
            }
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetItalic(bool italic, bool rebuild)
        {
            if (italic)
            {
                TheText.FontStyle = FontStyles.Italic;

            }
            else
            {
                TheText.FontStyle = FontStyles.Normal;
            }
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetUnderline(bool underline, bool rebuild)
        {
            m_Underline = underline;
            if (underline)
            {
                TheText.TextDecorations = TextDecorations.Underline;
            }
            else
            {
                TheText.TextDecorations = null;
            }
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetFont(FontFamily font, bool rebuild)
        {
            TheText.FontFamily = font;
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetTextSize(double size, bool rebuild)
        {
            TheText.FontSize = size;
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetEffect(int effect, bool rebuild)
        {
            switch (effect)
            {
                case 0: // NONE
                {
                    m_Stroke = false;
                    break;
                }
                case 1: // STROKE
                {
                    m_Stroke = true;
                    break;
                }
            }
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetStrokeSize(double size, bool rebuild)
        {
            m_StrokeSize = size;
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetStrokeBrush(Brush brush, bool rebuild)
        {
            m_StrokeBrush = brush;
            if (rebuild)
            {
                RebuildText();
            }
        }

        public void SetShadowSize(double size, bool rebuild)
        {

        }

        public void SetShadowBrush(Brush brush, bool rebuild)
        {

        }

        public void SetTextBrush(Brush brush, bool rebuild)
        {
            TheText.Foreground = brush;
            if (rebuild)
            {
                RebuildText();
            }
        }

        private void TextImageParser(string txt)
        {
            TheText.Inlines.Clear();

            // First split into rows
            string[] rows = txt.Split('\n');
            for (int n = 0; n < rows.Length; n++)
            {
                if (rows[n].Length == 0)
                    continue;

                // Then into individual words
                string[] words = rows[n].Split(' ');
                for (int i = 0; i < words.Length; i++)
                {
                    bool text = true;

                    // If we are parsing emoticons...
                    if(m_Emoticons)
                    {
                        // Check if this word equals a emoticon
                        Image emote = GlobalHelper.EmoticonParser.GetEmoticon(words[i], (int)TheText.FontSize);
                        if (emote != null)
                        {
                            // This text is an emoticon
                            InlineUIContainer emoteContainer = new InlineUIContainer(emote);
                            emoteContainer.BaselineAlignment = BaselineAlignment.Top;   // Can look wrong on some fonts... perhaps configurable for user? Later...
                            TheText.Inlines.Add(emoteContainer);
                            text = false;
                        }
                    }

                    // This was just text
                    if(text)
                    {
                        TheText.Inlines.Add(new InlineUIContainer(GetText(words[i])));
                    }

                    // Each word/emoticon is always followed by an empty space (except the last one but we dont bother right now)
                    TheText.Inlines.Add(new Run(" "));  
                }

                TheText.Inlines.Add(new Run("\n"));
            }
        }

        private OutlinedTextBlock GetText(string txt)
        {
            OutlinedTextBlock t = new OutlinedTextBlock();
            t.FontFamily = TheText.FontFamily;
            t.FontStyle = TheText.FontStyle;
            t.FontWeight = TheText.FontWeight;
            t.FontSize = TheText.FontSize;
            t.Fill = TheText.Foreground;

            if (m_Stroke)
            {
                t.Stroke = m_StrokeBrush;
                t.StrokeThickness = m_StrokeSize;
            }
            else
            {
                t.Stroke = null;
                t.StrokeThickness = 0;
            }

            t.Text = txt;
            return t;
        }


    }
}

