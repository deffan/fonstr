﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.ComponentControls
{
    /// <summary>
    /// Interaction logic for YoutubeControl.xaml
    /// </summary>
    public partial class YoutubeControl : UserControl
    {
        private ObjectControl m_Parent;
        private YoutubeComponent m_YoutubeComponent;
        private bool m_Stopped;
        private string m_VideoId;
        private int m_Start;
        private int m_Duration;
        private BoundObject m_JSCallback;

        public class BoundObject
        {
            private YoutubeControl m_Parent;

            public BoundObject(YoutubeControl parent)
            {
                m_Parent = parent;
            }

            public void End(string s)   // This needs to be called with "end" not "End" in JavaScript or it wont work, for some odd reason.
            {
                m_Parent.Stop();
            }

            public void Err(string s)  
            {
                m_Parent.YoutubeError(s);
            }

            public void Started()
            {
                m_Parent.CtrlStarted();
            }
        }


        public YoutubeControl(ObjectControl parent, YoutubeComponent component)
        {
            InitializeComponent();
            m_Parent = parent;
            m_YoutubeComponent = component;
            m_JSCallback = new BoundObject(this);
            TheWebBrowser.RegisterJsObject("csharpcb", m_JSCallback);
            TheWebBrowser.LoadError += TheWebBrowser_LoadError;
            m_Stopped = false;
        }

        private void TheWebBrowser_LoadError(object sender, LoadErrorEventArgs e)
        {
            Logger.Log(LOG_LEVEL.ERROR, e.ErrorText);
            Stop();
        }

        private void YoutubeError(string err)
        {
            m_YoutubeComponent.YoutubeError(err);
        }

        private void CtrlStarted()
        {
            m_YoutubeComponent.CtrlStarted();
        }

        public void Stop()
        {
            if (m_Stopped)
                return;
            m_Stopped = true;

            Dispatcher.BeginInvoke((Action)(() =>
            {
                if (!TheWebBrowser.IsBrowserInitialized)
                    return;

                //TheWebBrowser.LoadHtml("<html><body style = 'background-color:black;'></html>");

                m_YoutubeComponent.StopFromCtrl();

            }));
        }

        public void Play()
        {
            if (!TheWebBrowser.IsBrowserInitialized)
                return;

            m_Stopped = false;
            TheWebBrowser.LoadHtml(CreateScript((int)WebGrid.Width - 25, (int)WebGrid.Height - 25, m_VideoId, m_Start, m_Duration));
        }

        public void Initialize(string videoId, int start, int stop)
        {
            m_VideoId = videoId;
            m_Start = start;
            m_Duration = stop;
        }

        private string CreateScript(int width, int height, string videoId, int start, int stop)
        {
            string script = "";
            try
            {
                using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("TwitchTool.Resources.Youtube.txt"))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        script = reader.ReadToEnd();
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "YoutubeControl->CreateScript - " + ex.Message);
                return script;
            }

            script = script.Replace("##HEIGHT##", height.ToString());
            script = script.Replace("##WIDTH##", width.ToString());
            script = script.Replace("##VIDEO##", videoId);

            if(start > 0)
            {
                script = script.Replace("##START##", string.Format(",start:{0}", start));
            }
            else
            {
                script = script.Replace("##START##", "");
            }

            if(stop > 0)
            {
                stop += start;
                script = script.Replace("##STOP##", string.Format(",end:{0}", stop));
            }
            else
            {
                script = script.Replace("##STOP##", "");
            }

            return script;
        }


    }
}
