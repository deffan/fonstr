﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace TwitchTool.Source
{
    public class AppSettings
    {
        private List<int> m_DisplayEvents;
        private bool m_UseTransparentWindows;
        private bool m_UseLastWindowLocation;
        private bool m_AutoLoginTwitch;

        public List<int> DisplayEvents
        {
            get { return m_DisplayEvents; }
        }

        public bool UseTransparentWindows
        {
            get { return m_UseTransparentWindows; }
            set { m_UseTransparentWindows = value; }
        }

        public bool UseLastWindowLocation
        {
            get { return m_UseLastWindowLocation; }
            set { m_UseLastWindowLocation = value; }
        }

        public bool AutoLoginTwitch
        {
            get { return m_AutoLoginTwitch; }
            set { m_AutoLoginTwitch = value; }
        }

        public AppSettings()
        {
            m_DisplayEvents = new List<int>();

            if (File.Exists("Settings\\AppSettings.xml"))
            {
                Load();
            }
            else
            {
                Create();
                Load();
            }
        }

        public void UpdateDisplayEvents(List<int> newDisplayEvents)
        {
            m_DisplayEvents = newDisplayEvents;
        }

        public void Save()
        {
            try
            {
                XDocument doc = new XDocument();
                XElement root = new XElement("Root");
                doc.Add(root);

                // DisplayEvents...
                string distEvents = "";
                for (int i = 0; i < m_DisplayEvents.Count; i++)
                {
                    distEvents += m_DisplayEvents[i].ToString();
                    if(i + 1 < m_DisplayEvents.Count)
                        distEvents += ";";
                }
                root.Add(new XElement("DisplayEvents", distEvents));

                root.Add(new XElement("UseTransparentWindows", m_UseTransparentWindows));
                root.Add(new XElement("UseLastWindowLocation", m_UseLastWindowLocation));
                root.Add(new XElement("AutoLoginTwitch", m_AutoLoginTwitch));

                doc.Save("Settings\\AppSettings.xml");
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "AppSettings Save - " + ex.Message);
            }
        }

        // Create the settings with default values
        private void Create()
        {
            try
            {
                Directory.CreateDirectory("Settings");

                XDocument doc = new XDocument();
                XElement root = new XElement("Root");
                doc.Add(root);

                // Add the event-types that we consider default. We assume these are the ones most useful to actually see there.
                string distEvents = EVENT_TYPE.TWITCH_BITS.ToString();
                distEvents += ";";
                distEvents += EVENT_TYPE.TWITCH_SUBSCRIPTION.ToString();
                distEvents += ";";
                distEvents += EVENT_TYPE.TWITCH_RAID.ToString();
                distEvents += ";";
                distEvents += EVENT_TYPE.TWITCH_GIFT.ToString();
                root.Add(new XElement("DisplayEvents", distEvents));

                root.Add(new XElement("UseTransparentWindows", true));
                root.Add(new XElement("UseLastWindowLocation", true));
                root.Add(new XElement("AutoLoginTwitch", true));
                
                doc.Save("Settings\\AppSettings.xml");
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "AppSettings Create - " + ex.Message);
            }
        }

        private void Load()
        {
            try
            {
                XDocument doc = XDocument.Load("Settings\\AppSettings.xml");

                // What events (types) will be displayed in the LiveTab
                XElement displayEvents = doc.Element("Root").Element("DisplayEvents");
                List<string> disEvents = displayEvents.Value.Split(';').ToList();
                for(int i = 0; i < disEvents.Count; i++)
                {
                    if(disEvents[i].Length > 0)
                    {
                        m_DisplayEvents.Add(Convert.ToInt32(disEvents[i]));
                    }
                }

                m_UseTransparentWindows = Convert.ToBoolean(doc.Element("Root").Element("UseTransparentWindows").Value);
                m_UseLastWindowLocation = Convert.ToBoolean(doc.Element("Root").Element("UseLastWindowLocation").Value);
                m_AutoLoginTwitch = Convert.ToBoolean(doc.Element("Root").Element("AutoLoginTwitch").Value);
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "AppSettings Load - " + ex.Message);
            }
        }
    }
}
