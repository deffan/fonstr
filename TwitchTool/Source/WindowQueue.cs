﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace fonstr.Source
{
    public class WindowQueue
    {
        private Queue<Event> m_Events;
        private List<EventQueue> m_Queues;
        private object m_Lock;

        public WindowQueue()
        {
            m_Lock = new object();
            m_Events = new Queue<Event>();
            m_Queues = new List<EventQueue>();
        }

        public void Abort()
        {
            lock (m_Lock)
            {
                m_Events.Clear();
            }
        }

        // Check if anything is running in any window's EventQueue
        private bool NoEventsRunning()
        {
            for(int i = 0; i < m_Queues.Count; i++)
            {
                if (m_Queues[i].RunningEventCount > 0)
                    return false;
            }
            return true;
        }

        // Called by an EventQueue that has a finished event (that was also queued)
        public void EventFinished()
        {
            Event e = null;
            lock (m_Lock)
            {
                if (m_Events.Count == 0)
                    return;

                e = m_Events.Dequeue();
            }

            if(e != null)
            {
                e.QueuedIn.AddEventToQueues(e);
            }
        }

        public void AddEventQueues(List<EventQueue> q)
        {
            for(int i = 0; i < q.Count; i++)
            {
                q[i].WindowQueue = this;
                m_Queues.Add(q[i]);
            }
        }

        public void AddEvent(Event e)
        {
            bool startNow = false;
            lock (m_Lock)
            {
                if (m_Events.Count == 0 && NoEventsRunning())
                {
                    startNow = true;
                }
                else
                {
                    m_Events.Enqueue(e);
                }
            }

            if(startNow)
            {
                e.QueuedIn.AddEventToQueues(e);
            }
        }

    }
}
