﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Source
{

    // Så varje usercontrol skall ärva från detta?
    // Tex Image.
    // Sen får vi trixa så att transform fungerar att sätta.
    // Hur presenterar vi objektet i UIt?
    // på vänster sida är det bara text.
    // på höger sida ska vi skapa paneler, med reflection (?)

    // Ett tomt objekt ska visa:
    // ------------------------
    // [Object Name Textbox]
    // [ Transform Box     ]
    // ------------------------

    // Sen under detta läggs komponenterna, och alla UI element blir knytna till just den komponenten.
    // Detta byggs upp i "inspector panelen" och dess "current object" antagligen. Baserat på just reflection av Objektet + Komponenterna.
    

    // Gör så att WindowComponent är bas-objektet.
    //
    // En WindowComponent kan sen _ENBART_ innehålla objekt av typen ObjectControl.
    // ObjectControl kan innehålla alla objekt, inklusive sig själv för children.




    public class Object
    {
        protected string name;
        protected List<Component> components;

        public Object()
        {
            name = "New Object";
            components = new List<Component>();
        }

        public string GetName()
        {
            return name;
        }

        public List<Component> GetComponents()
        {
            return components;
        }

        // Returns the first matching type
        // Can be used like this: Component comp = GetComponent<string>();
        public Component GetComponent<T>()
        {
            for(int i = 0; i < components.Count; i++)
            {
                if(components[i].GetType().IsEquivalentTo(typeof(T)))
                {
                    return components[i];
                }
            }
            return null;
        }

        public void SetName(string newName)
        {
            name = newName;
        }
    }
}
