﻿using System.Windows.Controls;

namespace TwitchTool.Source
{
    public class Emoticon
    {
        public enum Source { Unknown, Resource, File, Url, Database };
        private string m_Text;
        private string m_Path;
        private int m_Width;
        private int m_Height;
        private System.Windows.Size m_Size;
        private Source m_Source;
        private Image m_Image;

        public Emoticon()
        {
            m_Size = new System.Windows.Size();
            m_Text = "";
            m_Path = "";
            m_Width = 0;
            m_Height = 0;
            m_Source = Source.Unknown;
            m_Image = null;
        }

        public Emoticon(string text, string path, int width, int height, Source source)
        {
            m_Size = new System.Windows.Size();
            m_Text = text;
            m_Path = path;
            m_Width = width;
            m_Height = height;
            m_Source = source;
            m_Image = null;
        }

        public Image Image
        {
            get {  return m_Image; }
            set { m_Image = value; }
        }

        public string GetText()
        {
            return m_Text;
        }

        public string GetImagePath()
        {
            return m_Path;
        }

        public Source GetImageSource()
        {
            return m_Source;
        }

        public System.Windows.Size GetSize()
        {
            m_Size.Width = m_Width;
            m_Size.Height = m_Height;
            return m_Size;
        }

        public System.Windows.Size GetSize(int scaledByFontSize)
        {
            if (m_Width == m_Height)
            {
                m_Size.Width = scaledByFontSize;
                m_Size.Height = scaledByFontSize;
            }
            else if (m_Height > m_Width)
            {
                m_Size.Height = scaledByFontSize;
                m_Size.Width = ((double)m_Width / m_Height) * scaledByFontSize;
            }
            else
            {
                m_Size.Height = ((double)m_Height/ m_Width) * scaledByFontSize;
                m_Size.Width = scaledByFontSize;
            }

            return m_Size;
        }

    }
}
