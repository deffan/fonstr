﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Source
{
    public class ComponentSize
    {
        public int width;
        public int height;

        public ComponentSize()
        {
            width = 0;
            height = 0;
        }

        public ComponentSize(int size_x, int size_y)
        {
            width = size_x;
            height = size_y;
        }

    }
}
