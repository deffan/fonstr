﻿using fonstr.Source;
using System;
using System.Collections.Generic;
using System.Windows;

namespace TwitchTool.Source
{
    public class EventQueue
    {
        public enum QueueType { CUSTOM = 0, ALL, NO };
        private string m_Name;
        private List<string> m_EventsInQueue;
        private bool m_HasChanges;
        private static object m_Lock;
        private Queue<Event> m_Queue;
        private Queue<Event> m_DelayQueue;
        private List<Event> m_Live;
        private bool m_Shutdown;
        private LiveTab.LiveTab m_LiveTab;
        private QueueType m_QueueType;

        // Window Queue
        private WindowQueue m_WinQueue;
        private bool m_EventFinished;

        // Delay
        private TextData m_DelayedData;
        private bool m_IsDelayed;
        private int m_DelaySetting;
        private DateTime m_CurrentDelay;

        public WindowQueue WindowQueue
        {
            get { return m_WinQueue; }
            set { m_WinQueue = value; }
        }

        public bool EventFinished
        {
            get { return m_EventFinished; }
        }

        public bool HasChanges
        {
            get { return m_HasChanges; }
        }

        public string Name
        {
            get { return m_Name; }
        }

        public int RunningEventCount
        {
            get
            {
                lock (m_Lock)
                {
                    return m_Live.Count;
                }
            }
        }

        public EventQueue(string name, List<string> events, QueueType queue, LiveTab.LiveTab liveTab, TextData delayed, bool isDelayed, int delaySetting)
        {
            m_Name = name;
            m_EventsInQueue = events;
            m_Live = new List<Event>();
            m_Queue = new Queue<Event>();
            m_HasChanges = false;
            m_Lock = new object();
            m_Shutdown = false;
            m_LiveTab = liveTab;
            m_QueueType = queue;
            m_DelayedData = delayed;
            m_IsDelayed = isDelayed;
            m_DelaySetting = delaySetting;
            m_EventFinished = false;

            if (m_IsDelayed)
            {
                if(m_DelayedData == null || m_DelayedData.GetNumericDataOnce() <= 0)
                {
                    m_IsDelayed = false;
                    Logger.Log(LOG_LEVEL.WARNING, "Queue " + m_Name + " will not delay events due to invalid time or data setting.");
                }
                else
                {
                    m_DelayQueue = new Queue<Event>();
                    m_CurrentDelay = DateTime.Now;
                }
            }
        }

        public void AddEvent(Event e)
        {
            // Cannot add an event if...
            if (!MainWindow.Live || m_Shutdown || e.Commands.Count == 0 || !e.Window.Running)
            {
                if(!MainWindow.Live)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Event not added because we are not live.");
                }

                if (m_Shutdown)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Event not added because we are shutting down.");
                }

                if (e.Commands.Count == 0)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Event not added because there are 0 commands in this event.");
                }

                if (!e.Window.Running)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Event not added because this window is not running.");
                }
                return;
            }

            // Set queue reference (if we would like to restart/replay this event)
            e.QueuedIn = this;

            // Set the callback for the command, when it finishes
            for (int i = 0; i < e.Commands.Count; i++)
            {
                e.Commands[i].SetCommandCallback(CommandFinished_Callback);
            }

            // Check if this should be sent to the Window Queue first
            if (m_WinQueue != null)
            {
                Logger.Log(LOG_LEVEL.DEBUG, "Event transferred to Window Queue first.");
                m_WinQueue.AddEvent(e);
                return;
            }

            // Add to queues (or start instantly)
            AddEventToQueues(e);
        }

        public void AddEventToQueues(Event e)
        {
            // Put the event in queue or start it instantly
            lock (m_Lock)
            {
                if (m_QueueType == QueueType.CUSTOM)
                {
                    if (m_EventsInQueue.Contains(e.EventId))
                    {
                        Logger.Log(LOG_LEVEL.DEBUG, "Event queued in custom queue: " + m_Name);
                        AddToQueue(e);
                        m_HasChanges = true;
                    }
                    else
                    {
                        Logger.Log(LOG_LEVEL.DEBUG, "Event _NOT_ queued in custom queue: " + m_Name);
                        if (m_IsDelayed)
                        {
                            AddToDelayQueue(e);
                        }
                        else
                        {
                            AddToLive(e);
                            StartEvent(e);
                        }
                    }
                }
                else if (m_QueueType == QueueType.ALL)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Event queued in default 'ALL QUEUE'");
                    AddToQueue(e);
                    m_HasChanges = true;
                }
                else if (m_QueueType == QueueType.NO)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Event queued in default 'NO QUEUE'");
                    AddToLive(e);
                    StartEvent(e);
                }
            }
        }

        public void ClearQueue()
        {
            lock (m_Lock)
            {
                m_Queue.Clear();
                if(m_DelayQueue != null)
                {
                    m_DelayQueue.Clear();
                }
            }
        }

        public void Logic()
        {
            if(m_HasChanges)
            {
                Update();
            }

            if(m_IsDelayed && m_DelayQueue.Count > 0 && DateTime.Now > m_CurrentDelay)
            {
                UpdateDelayed();
            }
        }

        public void Shutdown()
        {
            m_Shutdown = true;
            lock (m_Lock)
            {
                m_Queue.Clear();
                if (m_DelayQueue != null)
                {
                    m_DelayQueue.Clear();
                }

                for (int i = 0; i < m_Live.Count; i++)
                {
                    for (int n = 0; n < m_Live[i].Commands.Count; n++)
                    {
                        m_Live[i].Commands[n].Abort(true);  // Fungerar inte på allt eller det mesta tror jag...
                    }
                }
                m_Live.Clear();
            }
        }

        public void CommandFinished_Callback(Command c)
        {
            Logger.Log(LOG_LEVEL.DEBUG, "Command finished");

            if (m_Shutdown)
                return;

            lock (m_Lock)
            {
                // This event has already finished and been handled.
                if (c.Event.Finished)
                {
                    Logger.Log(LOG_LEVEL.ERROR, "Event already finished. This shouldnt really happen?");
                    return;
                }

                // If the current order has not finished, we do nothing here.
                if (!c.Event.CurrentOrderFinished())
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Current event order is not finished. Waiting for more commands to finish...");
                    return;
                }

                Logger.Log(LOG_LEVEL.DEBUG, "Current event order IS finished.");

                // The order has finished, but if its not the last one, we should start the next.
                if (!c.Event.IsLastOrder())
                {
                    Logger.Log(LOG_LEVEL.DEBUG, "Starting next event order.");
                    c.Event.StartCommands();
                    return;
                }

                // If all commands in the event have finished, set the entire event to finished and run update.
                Logger.Log(LOG_LEVEL.DEBUG, "Event finished");
                c.Event.Finished = true;
                m_HasChanges = true;
            }

            // If Window is Queued, tell the Window Queue this event finished
            m_WinQueue?.EventFinished();

            // Update the LiveTab
            Application.Current.Dispatcher.BeginInvoke(new Action(() => { m_LiveTab.EventFinished(c.Event); }));
        }

        private void UpdateDelayed()
        {
            Event start = null;
            lock (m_Lock)
            {
                start = m_DelayQueue.Dequeue();
                m_Live.Add(start);
                m_CurrentDelay = DateTime.Now.AddSeconds(m_DelayedData.GetNumericDataOnce().Value);
            }

            if (start != null)
            {
                StartEvent(start);
            }
        }

        private void Update()
        {
            Event start = null;
            bool startEvent = false;
            lock (m_Lock)
            {
                // Loop live list and remove finished events
                bool wait = false;
                for (int i = m_Live.Count - 1; i >= 0; i--)
                {
                    if (m_Live[i].Finished)
                    {
                        m_Live.RemoveAt(i);
                    }
                }

                // Loop again to see if we have to wait or not.
                if (m_QueueType == QueueType.ALL)
                {
                    wait = m_Live.Count > 0;
                }
                else
                {
                    for (int i = m_Live.Count - 1; i >= 0; i--)
                    {
                        if (m_EventsInQueue.Contains(m_Live[i].EventId))
                        {
                            wait = true;
                            break;
                        }
                    }
                }

                // Start next event
                if ((!wait || m_Live.Count == 0) && m_Queue.Count > 0)
                {
                    start = m_Queue.Dequeue();
                    if (m_IsDelayed)
                    {
                        AddToDelayQueue(start);
                    }
                    else
                    {
                        m_Live.Add(start);
                        startEvent = true;
                    }
                }
                m_HasChanges = false;
            }

            // Must be called from outside lock to avoid deadlocks.
            if(startEvent)
            {
                StartEvent(start);
            }
        }

        public void AbortEvent(Event e)
        {
            lock (m_Lock)
            {
                for (int i = 0; i < m_Live.Count; i++)
                {
                    if (m_Live[i].EventId.Equals(e.EventId))
                    {
                        for (int n = 0; n < m_Live[i].Commands.Count; n++)
                        {
                            m_Live[i].Commands[n].Abort(true);
                        }
                    }
                }
            }
        }

        private void StartEvent(Event e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                m_LiveTab.EventStarted(e);
            }));
            e.StartCommands();
        }

        private void AddToQueue(Event e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                m_LiveTab.EventQueued(e);
            }));
            m_Queue.Enqueue(e);
        }

        private void AddToDelayQueue(Event e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                m_LiveTab.EventQueued(e);
            }));
            m_DelayQueue.Enqueue(e);
        }

        private void AddToLive(Event e)
        {
            m_Live.Add(e);
        }
    }
}
