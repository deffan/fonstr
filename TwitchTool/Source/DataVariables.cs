﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TwitchTool.Source
{
    public class DataVariables
    {
        private Dictionary<string, DataVariable> m_DataVariables;
        private string m_LatestSaveId;

        public DataVariables()
        {
            m_DataVariables = new Dictionary<string, DataVariable>();
            Load();
        }

        private void LoadDefaultVariables()
        {
            m_DataVariables.Add("NEWLINE_CHARACTER", new DataVariable(VARIABLE_TYPE.TEXT, "NEWLINE_CHARACTER", "Newline", new Func<object, string>((object data) => { return "\n"; }), "\n", true));
            m_DataVariables.Add("SPACE_CHARACTER", new DataVariable(VARIABLE_TYPE.TEXT, "SPACE_CHARACTER", "Space", new Func<object, string>((object data) => { return " "; }), " ", true));

        }

        public string LatestSave
        {
            get { return m_LatestSaveId; }
        }

        public void Load()
        {
            // In the case of reload
            m_DataVariables.Clear();

            // Default variables
            LoadDefaultVariables();

            m_LatestSaveId = Guid.NewGuid().ToString();

            // Load user variables from file here...
            try
            {
                // Create a empty file if it does not exist
                if (!File.Exists("Data/Variable.data"))
                {
                    Directory.CreateDirectory("Data");
                    File.WriteAllText("Data/Variable.data", "");
                    return;
                }

                List<DataVariable> customVariables = JsonConvert.DeserializeObject<List<DataVariable>>(File.ReadAllText("Data/Variable.data"));
                if (customVariables == null)
                    return;

                for (int i = 0; i < customVariables.Count; i++)
                {
                    int index = i;  // Local variable needed to save index
                    switch (customVariables[i].Type)
                    {
                        case VARIABLE_TYPE.TEXT:
                        {
                            customVariables[index].Data = new Func<object, string>((object data) => { return customVariables[index].Listdata[0]; });
                            break;
                        }
                        case VARIABLE_TYPE.RANDOM_NUMBER:
                        {
                            customVariables[index].Data = new Func<object, string>((object data) =>
                            {
                                Random r = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
                                string v1 = customVariables[index].Listdata[0];
                                string v2 = customVariables[index].Listdata[1];
                                double value1 = 0;
                                double value2 = 0;
                                double.TryParse(v1, out value1);
                                double.TryParse(v2, out value2);
                                int precision = 0;

                                // Invalid... we simply return 0 in this case.
                                if (value2 <= value1)
                                {
                                    return "0";
                                }

                                // Find precision
                                int p1 = v1.IndexOf('.');
                                if (p1 > 0)
                                {
                                    precision = v1.Length - p1 - 1;
                                }

                                int p2 = v2.IndexOf('.');
                                if (p2 > 0)
                                {
                                    p2 = v2.Length - p2 - 1;
                                    if (p2 > precision)
                                    {
                                        precision = p2;
                                    }
                                }

                                return Convert.ToString(Math.Round(r.NextDouble() * (value2 - value1) + value1, precision));
                            });
                            break;
                        }
                        case VARIABLE_TYPE.RANDOM_TEXT:
                        {
                            customVariables[index].Data = new Func<object, string>((object data) =>
                            {
                                Random r = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
                                int indx = r.Next(0, customVariables[index].Listdata.Count);
                                return Convert.ToString(customVariables[index].Listdata[indx]);
                            });
                            break;
                        }
                        default: throw new Exception("Invalid VariableType in DataVariables.");
                    }
                    m_DataVariables.Add(customVariables[i].Id, customVariables[i]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, ex.Message);
            }
        }

        public bool Add(DataVariable v)
        {
            if(!m_DataVariables.ContainsKey(v.Id))
            {
                m_DataVariables.Add(v.Id, v);
                return true;
            }
            return false;
        }

        public void SaveAndReload(List<DataVariable> newVariables)
        {
            m_DataVariables.Clear();
            for (int i = 0; i < newVariables.Count; i++)
            {
                Add(newVariables[i]);
            }
            Save();
            Load();
        }

        public void Remove(DataVariable v)
        {
            m_DataVariables.Remove(v.Id);
        }

        public bool Save()
        {
            try
            {
                // We need to null out the Func's before serializing
                List<DataVariable> vars = GetCustomVariables();
                for(int i = 0; i < vars.Count; i++)
                {
                    vars[i].Data = null;
                }

                File.WriteAllText("Data/Variable.data", JsonConvert.SerializeObject(vars));
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, ex.Message);
            }
            return false;
        }

        public DataVariable GetVariable(string id)
        {
            try
            {
                return m_DataVariables[id];
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, ex.Message + " > DataVariable (" + id + ")");
                return null;
            }
        }

        public List<DataVariable> GetVariables()
        {
            return m_DataVariables.Values.ToList();
        }

        public List<DataVariable> GetCustomVariables()
        {
            List<DataVariable> vars = m_DataVariables.Values.ToList();
            List<DataVariable> customVariablesOnly = new List<DataVariable>();
            for(int i = 0; i < vars.Count; i++)
            {
                if(!vars[i].DefaultVariable)
                {
                    customVariablesOnly.Add(vars[i]);
                }
            }
            return customVariablesOnly;
        }
    }

    public class DataVariable
    {
        public int Type;
        public string DisplayName;
        public string Id;
        public Func<object, string> Data;
        public string ExampleData;
        public List<string> Listdata;
        public bool DefaultVariable;

        public DataVariable()
        {

        }

        public DataVariable(int type, string id, string displayName, Func<object, string> data, string exampledata, bool defaultVariable)
        {
            Type = type;
            Id = id;
            DisplayName = displayName;
            Data = data;
            ExampleData = exampledata;
            Listdata = new List<string>();
            DefaultVariable = defaultVariable;
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public string GetExampleData()
        {
            if(string.IsNullOrEmpty(ExampleData))
            {
                ExampleData = Data(null);
            }
            return ExampleData;
        }

    }
}
