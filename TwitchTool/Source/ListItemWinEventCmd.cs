﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.UserControls;

namespace TwitchTool.Source
{
    public class ListItemWinEventCmd
    {
        public string Id;
        public string WindowId;
        public int Type;
        public int SubType;
        public WindowListItem ListItem;
        public List<ListItemWinEventCmd> Children;

        public ListItemWinEventCmd(string id, string windowid, int type, int subtype, WindowListItem listitem)
        {
            Id = id;
            WindowId = windowid;
            Type = type;
            SubType = subtype;
            ListItem = listitem;
            Children = new List<ListItemWinEventCmd>();
        }
    }
}
