﻿using System;
using System.Collections.Generic;

namespace TwitchTool.Source
{
    public class EventVariables
    {
        private Dictionary<string, EventVariable> m_EventVariables;

        public EventVariables()
        {
            m_EventVariables = new Dictionary<string, EventVariable>();

            // INPUT
            m_EventVariables.Add("KEYBOARD_KEYS", new EventVariable(EVENT_TYPE.KEYBOARD, "KEYBOARD_KEYS", "Keys", "", "A + B + C", "Any text"));

            // GENERAL TWITCH
            m_EventVariables.Add("TWITCH_USERNAME", new EventVariable(-1, "TWITCH_USERNAME", "Username", "", "John", "Any text"));
            m_EventVariables.Add("TWITCH_MESSAGE", new EventVariable(-1, "TWITCH_MESSAGE", "Message", "", "Hello", "Any text"));
            m_EventVariables.Add("TWITCH_USERTYPE", new EventVariable(-1, "TWITCH_USERTYPE", "Usertype", "", "subscriber", "admin, bits, broadcaster, global_mod, moderator, subscriber, staff, turbo"));

            // TWITCH SUBSCRIPTION
            m_EventVariables.Add("TWITCH_SUBSCRIPTION_MONTHS", new EventVariable(EVENT_TYPE.TWITCH_SUBSCRIPTION, "TWITCH_SUBSCRIPTION_MONTHS", "Months", "", "3", "Numeric value"));
            m_EventVariables.Add("TWITCH_SUBSCRIPTION_PLAN", new EventVariable(EVENT_TYPE.TWITCH_SUBSCRIPTION, "TWITCH_SUBSCRIPTION_PLAN", "Subplan", "", "1000", "Prime, 1000, 2000, 3000"));
            m_EventVariables.Add("TWITCH_SUBSCRIPTION_TYPE", new EventVariable(EVENT_TYPE.TWITCH_SUBSCRIPTION, "TWITCH_SUBSCRIPTION_TYPE", "Subtype", "", "resub", "sub, resub"));
           
            // TWITCH RAID
            m_EventVariables.Add("TWITCH_RAID_RAIDERS", new EventVariable(EVENT_TYPE.TWITCH_RAID, "TWITCH_RAID_RAIDERS", "Raiders", "", "50", "Numeric value"));

            // TWITCH GIFT
            m_EventVariables.Add("TWITCH_GIFT_RECIPIENT", new EventVariable(EVENT_TYPE.TWITCH_GIFT, "TWITCH_GIFT_RECIPIENT", "Recipient", "", "Jim", "Any text"));

            // TWITCH BITS
            m_EventVariables.Add("TWITCH_BITS", new EventVariable(EVENT_TYPE.TWITCH_BITS, "TWITCH_BITS", "Bits", "", "100", "Numeric value"));

            // TWITCH CUSTOM BOT
            m_EventVariables.Add("TWITCH_BOT_CUSTOM_TEXT1", new EventVariable(EVENT_TYPE.TWITCH_BOT_CUSTOM, "TWITCH_BOT_CUSTOM_TEXT1", "Text1", "", "text1", "Any text"));
            m_EventVariables.Add("TWITCH_BOT_CUSTOM_TEXT2", new EventVariable(EVENT_TYPE.TWITCH_BOT_CUSTOM, "TWITCH_BOT_CUSTOM_TEXT2", "Text2", "", "text2", "Any text"));
            m_EventVariables.Add("TWITCH_BOT_CUSTOM_TEXT3", new EventVariable(EVENT_TYPE.TWITCH_BOT_CUSTOM, "TWITCH_BOT_CUSTOM_TEXT3", "Text3", "", "text3", "Any text"));

        }

        public List<EventVariable> GetVariables(int eventType)
        {
            List<EventVariable> result = new List<EventVariable>();
            switch (eventType)
            {
                case EVENT_TYPE.KEYBOARD:
                {
                    result.Add(GetVariable("KEYBOARD_KEYS"));
                    break;
                }
                case EVENT_TYPE.TWITCH_SUBSCRIPTION:
                {
                    result.Add(GetVariable("TWITCH_USERNAME"));
                    result.Add(GetVariable("TWITCH_MESSAGE"));
                    result.Add(GetVariable("TWITCH_SUBSCRIPTION_MONTHS"));
                    result.Add(GetVariable("TWITCH_SUBSCRIPTION_PLAN"));
                    result.Add(GetVariable("TWITCH_SUBSCRIPTION_TYPE"));
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_JOIN:
                case EVENT_TYPE.TWITCH_BOT_PART:
                case EVENT_TYPE.TWITCH_FOLLOW:
                {
                    result.Add(GetVariable("TWITCH_USERNAME"));
                    break;
                }
                case EVENT_TYPE.TWITCH_RAID:
                {
                    result.Add(GetVariable("TWITCH_USERNAME"));
                    result.Add(GetVariable("TWITCH_RAID_RAIDERS"));
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_CUSTOM:
                {
                    result.Add(GetVariable("TWITCH_USERNAME"));
                    result.Add(GetVariable("TWITCH_MESSAGE"));
                    result.Add(GetVariable("TWITCH_USERTYPE"));
                    result.Add(GetVariable("TWITCH_BOT_CUSTOM_TEXT1"));
                    result.Add(GetVariable("TWITCH_BOT_CUSTOM_TEXT2"));
                    result.Add(GetVariable("TWITCH_BOT_CUSTOM_TEXT3"));
                    break;
                }
                case EVENT_TYPE.TWITCH_GIFT:
                {
                    result.Add(GetVariable("TWITCH_USERNAME"));
                    result.Add(GetVariable("TWITCH_GIFT_RECIPIENT"));
                    break;
                }
                case EVENT_TYPE.TWITCH_BITS:
                {
                    result.Add(GetVariable("TWITCH_USERNAME"));
                    result.Add(GetVariable("TWITCH_MESSAGE"));
                    result.Add(GetVariable("TWITCH_BITS"));
                    break;
                }
            }
            return result;
        }

        public EventVariable GetVariable(string id)
        {
            try
            {
                return m_EventVariables[id];
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, ex.Message + " > EventVariable (" + id + ")");
                return null;
            }
        }
    }

    public class EventVariable
    {
        public EventVariable()
        {

        }

        public EventVariable(int eventType, string id, string displayName, string data, string exampleData, string validValues)
        {
            EventType = eventType;
            Data = data;
            DisplayName = displayName;
            Id = id;
            ExampleData = exampleData;
            ValidValuesExample = validValues;
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public string DisplayName;
        public string Id;
        public string Data;
        public int EventType;
        public string ExampleData;
        public string ValidValuesExample;
    }
}
