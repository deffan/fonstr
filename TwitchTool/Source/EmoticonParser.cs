﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace TwitchTool.Source
{
    public class EmoticonParser
    {
        private Dictionary<string, Emoticon> m_Emoticons;

        public EmoticonParser()
        {
            m_Emoticons = new Dictionary<string, Emoticon>();
            AddEmoticon(new Emoticon("TransparentImage", "TransparentImage", 32, 32, Emoticon.Source.Resource));
            AddEmoticon(new Emoticon("TestImage", "TestImage", 32, 32, Emoticon.Source.Resource));
        }

        public void AddEmoticon(Emoticon e)
        {
            m_Emoticons.Add(e.GetText(), e);
        }

        private Image LoadImage(Emoticon emoticon)
        {
            // Check if we need to load the image or we already have it
            if (emoticon.Image == null)
            {
                emoticon.Image = new Image();
                GetImageSource(emoticon);
            }

            // However, we need to create a new Image each time, as the UI-Framework does not allow the same object in many places at the same time
            Image img = new Image();
            img.Source = emoticon.Image.Source;
            return img;
        }

        private Image GetImage(Emoticon emoticon)
        {
            Image img = LoadImage(emoticon);
            Size size = emoticon.GetSize();
            img.Height = (int)size.Height;
            img.Width = (int)size.Width;
            return img;
        }

        private Image GetImage(Emoticon emoticon, int width, int height)
        {
            Image img = LoadImage(emoticon);
            img.Height = height;
            img.Width = width;
            return img;
        }

        private Image GetImage(Emoticon emoticon, int fontSize)
        {
            Image img = LoadImage(emoticon);
            Size size = emoticon.GetSize(fontSize);
            img.Height = (int)size.Height;
            img.Width = (int)size.Width;
            return img;
        }

        private void GetImageSource(Emoticon emoticon)
        {
            switch (emoticon.GetImageSource())
            {
                case Emoticon.Source.Resource:
                {
                    emoticon.Image.Source = GlobalHelper.GetResourceImage(emoticon.GetImagePath());
                    break;
                }
                case Emoticon.Source.File:
                {
                    emoticon.Image.Source = new BitmapImage(new Uri(emoticon.GetImagePath()));
                    break;
                }
                case Emoticon.Source.Url:
                case Emoticon.Source.Database:
                {
                    emoticon.Image.Source = new BitmapImage(new Uri(emoticon.GetImagePath(), UriKind.Absolute));
                    break;
                }
                default:
                {
                    throw new Exception("EmoticonParser > GetImage.Source not set or set to invalid value");
                }
            }
        }

        private Emoticon GetEmoticonObject(string text)
        {
            // First try getting from dictionary
            if (m_Emoticons.ContainsKey(text))
            {
                return m_Emoticons[text];
            }
            else
            {
                // Get from database
                Emoticon e = GlobalHelper.SQL.GetTwitchEmoticon(text);
                if (e != null)
                {
                    // Add to dictionary
                    m_Emoticons.Add(text, e);
                    return m_Emoticons[text];
                }
            }
            return null;
        }

        /// <summary>
        /// Get an emoticon with its default size
        /// </summary>
        public Image GetEmoticon(string text)
        {
            Emoticon e = GetEmoticonObject(text);
            if (e != null)
            {
                return GetImage(e);
            }
            return null;
        }

        /// <summary>
        /// Get an emoticon with specified size
        /// </summary>
        public Image GetEmoticon(string text, int width, int height)
        {
            Emoticon e = GetEmoticonObject(text);
            if (e != null)
            {
                return GetImage(e, width, height);
            }
            return null;
        }

        /// <summary>
        /// Get an emoticon scaled by font size
        /// </summary>
        public Image GetEmoticon(string text, int fontSize)
        {
            Emoticon e = GetEmoticonObject(text);
            if (e != null)
            {
                return GetImage(e, fontSize);
            }
            return null;
        }





    }
}
