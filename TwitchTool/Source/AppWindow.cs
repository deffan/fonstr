﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.UserControls;

namespace TwitchTool.Source
{
    public class AppWindow
    {
        private bool m_Running;
        private List<CommandListenerComponent> m_Listeners;
        private ObjectControl m_WindowObject;
        private Window m_WindowDialog;
        private Thread m_Thread;
        private string m_Path;
        private string m_LastModified;
        private List<Event> m_Events;
        private Dictionary<string, EventQueue> m_Queues;
        private object m_Lock;
        public Point LastLocation;

        public AppWindow(string path)
        {
            m_Path = path;
            m_Thread = null;
            m_Events = new List<Event>();
            m_Queues = new Dictionary<string, EventQueue>();
            LastLocation = new Point();
            XDocument doc = XDocument.Load(path + "\\Window.xml");
            m_WindowObject = ObjectControl.Deserialize(doc.Element("Root").Element("Object"), null, false);
            m_LastModified = doc.Element("Root").Element("Modified").Value;
            LastLocation.X = Convert.ToInt32(doc.Element("Root").Element("LocationX").Value);
            LastLocation.Y = Convert.ToInt32(doc.Element("Root").Element("LocationY").Value);
            m_Listeners = new List<CommandListenerComponent>();
            GetAllListeners(m_WindowObject, m_Listeners);
            m_Running = false;
            m_Lock = new object();
        }

        public void Reload()
        {
            if (m_Running)
            {
                Logger.Log(LOG_LEVEL.WARNING, "Window is already running, Reload unsuccessful.");
                return;
            }

            m_WindowObject = ObjectControl.Deserialize(XDocument.Load(m_Path + "\\Window.xml").Element("Root").Element("Object"), null, false);
            m_Listeners.Clear();
            m_Events.Clear();
            m_Queues.Clear();
            GetAllListeners(m_WindowObject, m_Listeners);

            // Create a safeclone for each clone component
            //SafeClone(m_WindowObject);
        }

        private void SafeClone(ObjectControl o)
        {
            /*
            List<IComponent> components = o.GetComponents();
            for (int i = components.Count - 1; i >= 0; i--)
            {
                CloningComponent c = components[i] as CloningComponent;
                if (c != null)
                {
                    c.SafeClone();
                }
            }

            for (int i = 0; i < o.Children.Count; i++)
            {
                if (o.Children[i] is ObjectControl)
                {
                    SafeClone(o.Children[i] as ObjectControl);
                }
            }
            */
        }

        public void AddEvent(Event e)
        {
            m_Events.Add(e);
        }

        public void AddLiveEvent(Event e)
        {
            if (string.IsNullOrEmpty(e.Queue))
            {
                m_Queues["All (Default)"].AddEvent(e);
            }
            else
            {
                m_Queues[e.Queue].AddEvent(e);
            }
        }

        public void AddQueue(EventQueue q)
        {
            m_Queues.Add(q.Name, q);
        }

        public Dictionary<string, EventQueue> Queues
        {
            get { return m_Queues; }
        }

        public List<Event> Events
        {
            get { return m_Events; }
        }

        public string GetLastModified()
        {
            return m_LastModified;
        }

        public List<CommandListenerComponent> GetListeners()
        {
            return m_Listeners;
        }

        public ObjectControl WindowObject
        {
            get { return m_WindowObject; }
        }

        public bool Running
        {
            get { return m_Running; }
        }

        public string Path
        {
            get { return m_Path; }
        }

        private void GetAllListeners(ObjectControl c, List<CommandListenerComponent> listeners)
        {
            listeners.AddRange(c.GetAllListeners());
            if (c.Children.Count > 0)
            {
                for (int i = 0; i < c.Children.Count; i++)
                {
                    if (c.Children[i] is ObjectControl)
                    {
                        GetAllListeners(c.Children[i] as ObjectControl, listeners);
                    }
                }
            }
        }

        public void Start()
        {
            Activate();
        }

        public void Stop()
        {
            Deactivate();
        }

        private void Activate()
        {

            try
            {
                m_WindowDialog = new Window
                {
                    Content = new Grid(),
                    Title = m_WindowObject.ObjectName,
                    SizeToContent = SizeToContent.WidthAndHeight,
                    ResizeMode = ResizeMode.NoResize
                };
                (m_WindowDialog.Content as Grid).Children.Add(m_WindowObject);

                if(GlobalHelper.ApplicationSettings.UseLastWindowLocation)
                {
                    m_WindowDialog.WindowStartupLocation = WindowStartupLocation.Manual;
                    m_WindowDialog.Left = LastLocation.X;
                    m_WindowDialog.Top = LastLocation.Y;
                }
                else
                {
                    m_WindowDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                }

                // Skapa en "window mover window" som har size 0, 0
                //
                // sen får vi ha en window collision detection... som kanske kör en gång var 0,5 sekund?
                // den kollar bara om musen kolliderar med fönstret, baserat på mus-koord och window coord/size.
                // Ja: visa window-mover-window över fönstret, som då är draggable. den drar det andra fönstret med sig.
                // Nej, dölj. (om mover är synlig)
                //
                // Detta är enbart relevant om vi har transparent windows igång.

                m_WindowDialog.AllowsTransparency = GlobalHelper.ApplicationSettings.UseTransparentWindows;
                m_WindowDialog.WindowStyle = WindowStyle.None;
                m_WindowDialog.Background = new SolidColorBrush(Color.FromArgb(1, 0, 0, 0));//Brushes.Transparent;

                m_WindowDialog.MouseLeftButtonDown += M_WindowDialog_MouseLeftButtonDown;
                m_WindowDialog.MouseRightButtonUp += M_WindowDialog_MouseRightButtonUp;
                m_WindowDialog.Closing += M_WindowDialog_Closing;

                m_WindowDialog.Show();
               
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Window Activate" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);

                if (m_WindowDialog != null)
                {
                    m_WindowDialog.Close();
                    (m_WindowDialog.Content as Grid).Children.Remove(m_WindowObject);
                    m_WindowDialog = null;
                }
                return;
            }

            // It also starts the thread for handling everything on the window.
            m_Running = true;
            m_Thread = new Thread(new ThreadStart(Logic));
            m_Thread.Start();
        }

        private void Wnd_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            Deactivate();
        }

        private void M_WindowDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Deactivate();
        }

        private void M_WindowDialog_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ContextMenu cm = new ContextMenu();
            MenuItem mi = new MenuItem();
            mi.Header = "Close window";
            mi.Click += Mi_Click;
            cm.Items.Add(mi);
            cm.PlacementTarget = sender as Window;

            m_WindowDialog.ContextMenu = cm;
            cm.IsOpen = true;
        }

        private void Mi_Click(object sender, RoutedEventArgs e)
        {
            Deactivate();
        }

        private void M_WindowDialog_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            m_WindowDialog.DragMove();
        }



        private void Deactivate()
        {
            if (!m_Running)
                return;

            // First we should signal to all running commands that this window is now shutting down
            Abort();

            // We must sleep the main thread for a short moment, to make sure all commands can signal abort back
            Thread.Sleep(200);  // useless?

            // This stops the thread
            m_Running = false;

            MainWindow.StoppedWindow(m_WindowObject.ID);

            // Save current location
            SaveLastLocation();

            // It unattaches the object from the window.
            try
            {
                m_WindowDialog.Close();     // Will throw exception if closed by clicking on the "X" 
            }
            catch(Exception) { }
            
            (m_WindowDialog.Content as Grid).Children.Remove(m_WindowObject);
            m_WindowDialog = null;
        }

        private void SaveLastLocation()
        {
            try
            {
                LastLocation.X = m_WindowDialog.Left;
                LastLocation.Y = m_WindowDialog.Top;

                XDocument doc = XDocument.Load(m_Path + "\\Window.xml");
                doc.Element("Root").Element("LocationX").Value = Convert.ToString((int)LastLocation.X);
                doc.Element("Root").Element("LocationY").Value = Convert.ToString((int)LastLocation.Y);
                doc.Save(m_Path + "\\Window.xml");
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "SaveLastLocation > " + ex.Message);
            }
        }

        private void Logic()
        {
            Logger.Log(LOG_LEVEL.DEBUG, "Window (" + m_WindowObject.ObjectName + ") started.");

            while (m_Running)
            {
                lock (m_Lock)
                {
                    for (int i = 0; i < m_Listeners.Count; i++)
                    {
                        m_Listeners[i].ListenerLogic();
                    }
                }
                Thread.Sleep(32);
            }

            Logger.Log(LOG_LEVEL.DEBUG, "Window (" + m_WindowObject.ObjectName + ") has shutdown.");
        }

        public void AbortCommand(Command c)
        {
            lock(m_Lock)
            {
                for (int i = 0; i < m_Listeners.Count; i++)
                {
                    m_Listeners[i].AbortCommand(c);
                }
            }
        }

        public void Abort()
        {
            lock (m_Lock)
            {
                for (int i = 0; i < m_Listeners.Count; i++)
                {
                    m_Listeners[i].Abort();
                }
            }
        }

        // Called from the command itself when finished executing on a listener
        public void CommandFinished(Command c)
        {
            lock (m_Lock)
            {
                // Check all listeners. If this command still exists in any of them, the command is not finished yet.
                for (int i = 0; i < m_Listeners.Count; i++)
                {
                    if (m_Listeners[i].HasCommand(c))
                        return;
                }
                c.Finished = true;
            }
        }

        // Get all listeners that matches the ListenerID
        private List<CommandListenerComponent> GetListeners(Command c)
        {
            List<CommandListenerComponent> listeners = new List<CommandListenerComponent>();

            // Special handling for WaitCommand. 
            // It dosent care what listener it uses, but we should make sure we are not using a listener in a cloned object.. to be safe.
            if (c.Data.Type == COMMAND_TYPE.WAIT)
            {
                for (int i = 0; i < m_Listeners.Count; i++)
                {
                    if(m_Listeners[i].GetParentObject().CloneID.Equals(""))
                    {
                        listeners.Add(m_Listeners[i]);
                        return listeners;
                    }
                }
            }

            for (int i = 0; i < m_Listeners.Count; i++)
            {
                for(int n = 0; n < c.Data.Listeners.Count; n++)
                {
                    if (m_Listeners[i].ListenerID.Equals(c.Data.Listeners[n]))
                    {
                        if(!listeners.Contains(m_Listeners[i]))
                        {
                            listeners.Add(m_Listeners[i]);
                        }
                    }
                }
            }
            return listeners;
        }

        public void UpdateListeners(bool withLock)
        {
            // Refetch all listeners
            if(withLock)
            {
                lock (m_Lock)
                {
                    m_Listeners.Clear();
                    GetAllListeners(m_WindowObject, m_Listeners);
                }
            }
            else
            {
                m_Listeners.Clear();
                GetAllListeners(m_WindowObject, m_Listeners);
            }

        }

        public void StartCommand(Command c)
        {
            lock(m_Lock)
            {
                List<CommandListenerComponent> listeners = GetListeners(c);
                if (listeners.Count == 0)
                {
                    Logger.Log(LOG_LEVEL.WARNING, string.Format("Command ({0}) not started due to no matching listenerID", c.Data.Name));
                    c.Finished = true;
                    return;
                }

                for (int i = 0; i < listeners.Count; i++)
                {
                    listeners[i].StartCommand(c);
                }
            }
        }

    }
}
