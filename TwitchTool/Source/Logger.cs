﻿using System;
using System.IO;

namespace TwitchTool.Source
{
    public static class Logger
    {
        private static object m_Lock = new object();

        public static void Log(int level, object msg)
        {
            //lock(m_Lock)
            {
                try
                {
                    string logmsg = DateTime.Now.ToString("HH:mm:ss") + ": " + msg.ToString() + Environment.NewLine;
                    if (MainWindow.Live)
                    {
                        MainWindow.AddLiveLog(level, logmsg);
                    }
                    File.AppendAllText("Logfile.log", logmsg);
                }
                catch (Exception) { }
            }
        }

    }
}
