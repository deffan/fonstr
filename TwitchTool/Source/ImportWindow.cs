﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TwitchTool.Source
{
    public class ImportWindow
    {
        public bool Import()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() != DialogResult.OK)
            {
                return false;
            }

            try
            {
                // Check folder name (window id)
                ZipArchive archive = ZipFile.OpenRead(openFileDialog.FileName);
                if (archive == null || archive.Entries.Count == 0)
                {
                    MessageBox.Show("Invalid file.", "Import Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

                string windowId = archive.Entries[0].FullName;
                windowId = windowId.Substring(0, windowId.LastIndexOf('/'));
                string fullPath = Directory.GetCurrentDirectory() + "\\Windows\\" + windowId;

                // Extract the contents
                ZipFile.ExtractToDirectory(openFileDialog.FileName, Directory.GetCurrentDirectory() + "\\Windows\\");

                // Check for the existance of exported datavariables
                if(File.Exists(fullPath + "\\VariableExport.data"))
                {
                    // Add them
                    List<DataVariable> customVariables = JsonConvert.DeserializeObject<List<DataVariable>>(File.ReadAllText(fullPath + "\\VariableExport.data"));
                    GlobalHelper.DataVariables.SaveAndReload(customVariables);
                }
                return true;
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Import > " + ex.Message);
                MessageBox.Show(ex.Message, "Import Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

    }
}
