﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Source
{
    public interface ICommandInterface
    {
        void ReceiveCommand(Command command);
    }
}
