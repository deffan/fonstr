﻿using System;
using System.Collections.Generic;

namespace TwitchTool.Source
{
    public class EventListener
    {
        protected Action<Event> m_AddEventCallback;
        protected bool m_Running;

        public EventListener(Action<Event> addEventCallback)
        {
            m_AddEventCallback = addEventCallback;
        }

        public bool IsRunning
        {
            get { return m_Running; }
        }

        public virtual void Shutdown()
        {
            m_Running = false;
        }

        public virtual void Start()
        {
            m_Running = true;
        }

    }
}
