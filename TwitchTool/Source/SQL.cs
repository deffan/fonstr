﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Dynamic;
using System.IO;

namespace TwitchTool.Source
{
    public class SQL
    {
        public class SQLResult
        {
            public SQLResult()
            {
                Error = false;
                Data = "";
            }

            public bool Error;
            public string Data;
        }

        public const string ConnectionString = "Data Source=db.sqlite";

        public SQL()
        {
            // Verify the existance of the database. If not, create it with default tables and data.
            InitDatabase();
        }

        private void InitDatabase()
        {
            // Check if we have a database
            if (!File.Exists("db.sqlite"))
            {
                // Create database file with default tables
                SQLiteConnection.CreateFile("db.sqlite");
                SQLResult r = DoSQL("CREATE TABLE TWITCH_EMOTES (regex VARCHAR(100), id INT, height INT, width INT, PRIMARY KEY(regex, id))");
                if (r.Error)
                    return;
            }
        }

        public bool HasTwitchEmotes()
        {
            SQLResult r = DoSQLCount("select count(*) from TWITCH_EMOTES");
            if (r.Error || Convert.ToInt32(r.Data) <= 0)
            {
                return false;
            }
            return true;
        }

        public Emoticon GetTwitchEmoticon(string emoticonText)
        {
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
                {
                    if (connection.State != System.Data.ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    using (SQLiteCommand command = new SQLiteCommand(string.Format("SELECT id, width, height FROM TWITCH_EMOTES WHERE regex='{0}'", emoticonText), connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                return new Emoticon(emoticonText, string.Format("https://static-cdn.jtvnw.net/emoticons/v1/{0}/3.0", reader.GetInt32(0)), reader.GetInt32(1), reader.GetInt32(2), Emoticon.Source.Database);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, ex.Message);
                if (ex.InnerException != null)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, ex.InnerException.Message);
                }
            }
            return null;
        }

        public List<string> SearchTwitchEmoticon(string searchText)
        {
            List<string> results = new List<string>();
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
                {
                    if (connection.State != System.Data.ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    string sql = "";
                    if(searchText.Length == 1)
                    {
                        sql = string.Format("SELECT regex FROM TWITCH_EMOTES WHERE regex = '{0}'", searchText);
                    }
                    else
                    {
                        sql = string.Format("SELECT regex FROM TWITCH_EMOTES WHERE regex LIKE '%{0}%'", searchText);
                    }

                    using (SQLiteCommand command = new SQLiteCommand(sql, connection))
                    {
                        using (SQLiteDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                results.Add(reader.GetString(0));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, ex.Message);
                if (ex.InnerException != null)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, ex.InnerException.Message);
                }
            }
            return results;
        }

        public SQLResult DoSQL(string sql)
        {
            SQLResult result = new SQLResult();
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
                {
                    if (connection.State != System.Data.ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    using (SQLiteCommand command = new SQLiteCommand(sql, connection))
                    {
                        result.Data = Convert.ToString(command.ExecuteNonQuery());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, ex.Message);
                result.Data = ex.Message;
                if (ex.InnerException != null)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, ex.InnerException.Message);
                    result.Data += Environment.NewLine + ex.InnerException.Message;
                }
                result.Error = true;
            }
            return result;
        }

        public SQLResult DoSQLCount(string sql)
        {
            SQLResult result = new SQLResult();
            try
            {
                using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
                {
                    if (connection.State != System.Data.ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    using (SQLiteCommand command = new SQLiteCommand(sql, connection))
                    {
                        object res = command.ExecuteScalar();
                        if (res == null)
                        {
                            result.Data = "-1";
                        }
                        else
                        {
                            result.Data = Convert.ToString(res);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, ex.Message);
                result.Data = ex.Message;
                if (ex.InnerException != null)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, ex.InnerException.Message);
                    result.Data += Environment.NewLine + ex.InnerException.Message;
                }
                result.Error = true;
            }
            return result;
        }

        public SQLResult FillTwitchEmoteTable(string fileDataPath)
        {
            SQLResult result = new SQLResult();
            dynamic tw = new ExpandoObject();
            try
            {
                using (StreamReader r = new StreamReader(fileDataPath))
                {
                    using (JsonReader reader = new JsonTextReader(r))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        tw = serializer.Deserialize(reader);
                    }
                }

                using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
                {
                    if (connection.State != System.Data.ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    using (SQLiteTransaction transaction = connection.BeginTransaction())
                    {
                        using (SQLiteCommand command = new SQLiteCommand(connection))
                        {
                            for (int i = 0; i < tw.emoticons.Count; i++)
                            {
                                command.CommandText = string.Format("INSERT INTO TWITCH_EMOTES (regex, id, height, width) values ('{0}', {1}, {2}, {3})",
                                    tw.emoticons[i].regex, tw.emoticons[i].id, tw.emoticons[i].images.height, tw.emoticons[i].images.width);
                                command.ExecuteNonQuery();
                            }
                        }
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, ex.Message);
                result.Data = ex.Message;
                if (ex.InnerException != null)
                {
                    Logger.Log(LOG_LEVEL.DEBUG, ex.InnerException.Message);
                    result.Data += Environment.NewLine + ex.InnerException.Message;
                }
                result.Error = true;

            }
            return result;
        }
    }
}
