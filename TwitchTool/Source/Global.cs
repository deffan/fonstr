﻿using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TwitchTool.Animator;
using TwitchTool.UserControls;

namespace TwitchTool.Source
{
    public static class GlobalHelper
    {
        public const string APPLICATION_VERSION = "1.0";
        public const string APPLICATION_VERSION_DATE = "2019-05-05";

        private static string YoutubeDataApiKey = "AIzaSyBGP84uwpvctI-0diQib_h9yVV3k7vEduI";
        private static EmoticonParser m_EmoticonParser;
        private static WindowBuilder m_EditorWindow;
        private static DataVariables m_DataVariables;
        private static EventVariables m_EventVariables;
        private static SQL m_SQL;
        private static AppSettings m_ApplicationSettings;
        //private static CustomCursors m_Cursors;

        static GlobalHelper()
        {
            m_ApplicationSettings = new AppSettings();
            m_EmoticonParser = new EmoticonParser();
            m_DataVariables = new DataVariables();
            m_EventVariables = new EventVariables();
            m_SQL = new SQL();
            // m_Cursors = new CustomCursors();

            //SetBrowserFeatureControl();
        }

        // public static CustomCursors CustomCursor
        // {
        //     get { return m_Cursors; }
        // }

        public static AppSettings ApplicationSettings
        {
            get { return m_ApplicationSettings; }
        }

        public static SQL SQL
        {
            get { return m_SQL; }
        }

        public static EventVariables EventVariables
        {
            get { return m_EventVariables; }
        }

        public static DataVariables DataVariables
        {
            get { return m_DataVariables; }
        }

        public static EmoticonParser EmoticonParser
        {
            get { return m_EmoticonParser; }
        }

        public static WindowBuilder EditorWindow
        {
            get { return m_EditorWindow; }
            set { m_EditorWindow = value; }
        }

        public static dynamic GetYoutubeVideoInformation(string videoId)
        {
            try
            {
                WebClient myDownloader = new WebClient();
                myDownloader.Encoding = Encoding.UTF8;

                string jsonResponse = myDownloader.DownloadString("https://www.googleapis.com/youtube/v3/videos?id=" + videoId + "&key=" + YoutubeDataApiKey + "&part=contentDetails");
                dynamic dynamicObject = JsonConvert.DeserializeObject(jsonResponse);
                return dynamicObject;
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "GetYoutubeVideoInformation" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
            return null;
        }

        public static double GetYoutubeDurationInSeconds(dynamic youtube)
        {
            try
            {
                return System.Xml.XmlConvert.ToTimeSpan(Convert.ToString(youtube.items[0].contentDetails.duration)).TotalSeconds;
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "GetYoutubeDurationInSeconds" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
            return 0;
        }

        public static string GetYouTubeVideoIdFromUrl(string url)
        {
            try
            {
                Uri uri = null;
                if (!Uri.TryCreate(url, UriKind.Absolute, out uri))
                {
                    try
                    {
                        uri = new UriBuilder("http", url).Uri;
                    }
                    catch
                    {
                        // invalid url
                        return "";
                    }
                }

                string host = uri.Host;
                string[] youTubeHosts = { "www.youtube.com", "youtube.com", "youtu.be", "www.youtu.be" };
                if (!youTubeHosts.Contains(host))
                    return "";

                var query = HttpUtility.ParseQueryString(uri.Query);

                if (query.AllKeys.Contains("v"))
                {
                    return Regex.Match(query["v"], @"^[a-zA-Z0-9_-]{11}$").Value;
                }
                else if (query.AllKeys.Contains("u"))
                {
                    // some urls have something like "u=/watch?v=AAAAAAAAA16"
                    return Regex.Match(query["u"], @"/watch\?v=([a-zA-Z0-9_-]{11})").Groups[1].Value;
                }
                else
                {
                    // remove a trailing forward space
                    var last = uri.Segments.Last().Replace("/", "");
                    if (Regex.IsMatch(last, @"^v=[a-zA-Z0-9_-]{11}$"))
                        return last.Replace("v=", "");

                    string[] segments = uri.Segments;
                    if (segments.Length > 2 && segments[segments.Length - 2] != "v/" && segments[segments.Length - 2] != "watch/")
                        return "";

                    return Regex.Match(last, @"^[a-zA-Z0-9_-]{11}$").Value;
                }
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, ex.Message);
            }
            return "";
        }

        public static int GetYoutubeVideoStartTime(string url)
        {
            int start = 0;
            try
            {
                int index = url.IndexOf("?t=");
                if (index < 0)
                {
                    index = url.IndexOf("&t=");
                }

                if (index > 0)
                {
                    index += 3;

                    // This ends with a & or end of string
                    int ampIndex = url.IndexOf("&", index);
                    if (ampIndex > 0)
                    {
                        start = Convert.ToInt32(url.Substring(index, ampIndex - index));
                    }
                    else
                    {
                        start = Convert.ToInt32(url.Substring(index, url.Length - index));
                    }
                }
            }
            catch (Exception) { }

            return start;
        }

        // Color in "R;G;B;A" string format
        public static string GetColorString(SolidColorBrush brush)
        {
            if (brush == null)
                return "0;0;0;0";

            string clr = Convert.ToString(brush.Color.R) + ";";
            clr += Convert.ToString(brush.Color.G) + ";";
            clr += Convert.ToString(brush.Color.B) + ";";
            clr += Convert.ToString(brush.Color.A);
            return clr;
        }

        // SolidColorBrush from "R;G;B;A" string format
        public static SolidColorBrush GetColorFromString(string color)
        {
            string[] clrArr = color.Split(';');
            return new SolidColorBrush(Color.FromArgb(Convert.ToByte(clrArr[3]), Convert.ToByte(clrArr[0]), Convert.ToByte(clrArr[1]), Convert.ToByte(clrArr[2])));
        }


        public static string Base64Encode(string plainText)
        {
            try
            {
                return Convert.ToBase64String(Encoding.UTF8.GetBytes(plainText));
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Base64Encode " + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                return "";
            }
        }

        public static string Base64Decode(string base64EncodedData)
        {
            try
            {
                return Encoding.UTF8.GetString(Convert.FromBase64String(base64EncodedData));
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Base64Decode" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                return "";
            }
        }

        public static string GetFileNameFromPath(string path)
        {
            try
            {
                int index = path.LastIndexOf('\\');
                if (index >= 0)
                {
                    index++;
                    return path.Substring(index, path.Length - index);
                }
            }
            catch (Exception) { }
            return path;
        }

        /// <summary>
        /// Finds and returs a child object or the object itself if it matches
        /// </summary>
        /// <param name="c"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ObjectControl Find(ObjectControl c, string id)
        {
            if (c == null)
                return null;

            if (c.ID.Equals(id))
                return c;

            ObjectControl o = null;
            for (int i = 0; i < c.Children.Count; i++)
            {
                if (c.Children[i] is ObjectControl)
                {
                    o = Find(c.Children[i] as ObjectControl, id);
                    if(o != null)
                    {
                        return o;
                    }
                }
            }
            
            return null;
        }

        // This gets resources defined in App.xaml
        public static BitmapImage GetResourceImage(string resourceName)
        {
            BitmapImage bitmap = null;
            try
            {
                bitmap = Application.Current.Resources[resourceName] as BitmapImage;
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "GetResourceImage" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
            return bitmap;
        }

        // This gets images from the folder /Images
        public static BitmapImage GetImage(string imageName)
        {
            BitmapImage bitmap = null;
            try
            {
                bitmap = new BitmapImage(new Uri("pack://application:,,,/fonstr;component/Images/" + imageName));
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "GetImage" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
            return bitmap;
        }

        private static void SetBrowserFeatureControl()
        {
            // http://msdn.microsoft.com/en-us/library/ee330720(v=vs.85).aspx

            // FeatureControl settings are per-process
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            // make the control is not running inside Visual Studio Designer
            if (String.Compare(fileName, "devenv.exe", true) == 0 || String.Compare(fileName, "XDesProc.exe", true) == 0)
                return;

            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, GetBrowserEmulationMode()); // Webpages containing standards-based !DOCTYPE directives are displayed in IE10 Standards mode.
            SetBrowserFeatureControlKey("FEATURE_AJAX_CONNECTIONEVENTS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ENABLE_CLIPCHILDREN_OPTIMIZATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_MANAGE_SCRIPT_CIRCULAR_REFS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_DOMSTORAGE ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_GPU_RENDERING ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_IVIEWOBJECTDRAW_DMLT9_WITH_GDI  ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_LEGACY_COMPRESSION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_LOCALMACHINE_LOCKDOWN", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_OBJECT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_SCRIPT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_NAVIGATION_SOUNDS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SCRIPTURL_MITIGATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SPELLCHECKING", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_STATUS_BAR_THROTTLING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_TABBED_BROWSING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_VALIDATE_NAVIGATE_URL", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_DOCUMENT_ZOOM", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_POPUPMANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_MOVESIZECHILD", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ADDON_MANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBSOCKET", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WINDOW_RESTRICTIONS ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_XMLHTTP", fileName, 1);
        }

        private static void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(appName, (UInt32)value, RegistryValueKind.DWord);
            }
        }

        private static UInt32 GetBrowserEmulationMode()
        {
            int browserVersion = 7;
            using (var ieKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer",
                RegistryKeyPermissionCheck.ReadSubTree,
                System.Security.AccessControl.RegistryRights.QueryValues))
            {
                var version = ieKey.GetValue("svcVersion");
                if (null == version)
                {
                    version = ieKey.GetValue("Version");
                    if (null == version)
                        throw new ApplicationException("Microsoft Internet Explorer is required!");
                }
                int.TryParse(version.ToString().Split('.')[0], out browserVersion);
            }

            UInt32 mode = 11000; // Internet Explorer 11. Webpages containing standards-based !DOCTYPE directives are displayed in IE11 Standards mode. Default value for Internet Explorer 11.
            switch (browserVersion)
            {
                case 7:
                    mode = 7000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE7 Standards mode. Default value for applications hosting the WebBrowser Control.
                    break;
                case 8:
                    mode = 8000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE8 mode. Default value for Internet Explorer 8
                    break;
                case 9:
                    mode = 9000; // Internet Explorer 9. Webpages containing standards-based !DOCTYPE directives are displayed in IE9 mode. Default value for Internet Explorer 9.
                    break;
                case 10:
                    mode = 10000; // Internet Explorer 10. Webpages containing standards-based !DOCTYPE directives are displayed in IE10 mode. Default value for Internet Explorer 10.
                    break;
                default:
                    // use IE11 mode by default
                    break;
            }

            return mode;
        }

    }

    public static class WINDOWLISTITEM_ACTION
    {
        public const int ADD = 0;
        public const int REMOVE = 1;
        public const int EXPORT = 2;
        public const int IMPORT = 3;
    }

    public static class MAIN_TYPE
    {
        public const int WINDOW = 0;
        public const int EVENT = 1;
        public const int COMMAND = 2;
    }

    public static class VARIABLE_TYPE
    {
        public const int TEXT = 0;                      // Any text string
        public const int RANDOM_NUMBER = 1;             // Random integer between X and Y
        public const int RANDOM_TEXT = 2;               // Random string taken out of a list of strings

        public static string GetVariableName(int type)
        {
            switch (type)
            {
                case TEXT: return "Text Variable";
                case RANDOM_NUMBER: return "Random Number Variable";
                case RANDOM_TEXT: return "Random Text Variable";
            }
            return "Unknown - Check GlobalHelper.GetVariableName";
        }
    }

    public static class EVENT_TYPE
    {
        public const int KEYBOARD = 0;
        public const int TWITCH_BITS = 1;
        public const int TWITCH_SUBSCRIPTION = 2;
        public const int TWITCH_FOLLOW = 3;
        public const int TWITCH_RAID = 4;
        public const int TWITCH_BOT_JOIN = 5;
        public const int TWITCH_BOT_PART = 6;
        public const int TWITCH_BOT_CUSTOM = 7;
        public const int TWITCH_GIFT = 8;
        public const int TWITCH_STATISTICS = 9;

        public static string GetEventName(int type)
        {
            switch (type)
            {
                case KEYBOARD: return "Keyboard Event";

                case TWITCH_FOLLOW: return "Twitch Follow Event";
                case TWITCH_SUBSCRIPTION: return "Twitch Subscription Event";
                case TWITCH_BITS: return "Twitch Bits Event";
                case TWITCH_GIFT: return "Twitch Gifted Subscription Event";
                case TWITCH_RAID: return "Twitch Raid Event";
                case TWITCH_BOT_CUSTOM: return "Twitch Bot Custom Command Event";
                case TWITCH_BOT_JOIN: return "Twitch Channel Join Event";
                case TWITCH_BOT_PART: return "Twitch Channel Leave Event";

            }
            return "Unknown - Check GlobalHelper.GetEventName";
        }

        public static BitmapImage GetEventIcon(int type)
        {
            switch (type)
            {
                case KEYBOARD: return GlobalHelper.GetImage("Commands/keyboard.png");

                case TWITCH_FOLLOW:
                case TWITCH_SUBSCRIPTION:
                case TWITCH_BITS:
                case TWITCH_GIFT:
                case TWITCH_RAID:
                case TWITCH_BOT_CUSTOM: 
                case TWITCH_BOT_JOIN:
                case TWITCH_BOT_PART: return GlobalHelper.GetImage("Events/twitch.png");
            }
            return GlobalHelper.GetImage("event.png"); ;
        }
    }

    public static class COMMAND_TYPE
    {
        public const int TEXT = 0;
        public const int YOUTUBE = 1;
        public const int IMAGE = 2;
        public const int START_ANIMATION = 3;
        public const int STOP_ANIMATION = 4;
        public const int SOUND = 5;
        public const int CLONE = 6;
        public const int WAIT = 7;

        public static string GetCommandName(int type)
        {
            switch(type)
            {
                case TEXT: return "Text Command";
                case YOUTUBE: return "Youtube Command";
                case IMAGE: return "Image Command";
                case START_ANIMATION: return "Start Animation Command";
                case STOP_ANIMATION: return "Stop Animation Command";
                case SOUND: return "Sound Command";
                case CLONE: return "Cloning Command";
                case WAIT: return "Wait Command";
            }
            return "Unknown";
        }

        public static BitmapImage GetCommandIcon(int type)
        {
            switch (type)
            {
                case TEXT: return GlobalHelper.GetImage("Commands/text.png");
                case YOUTUBE: return GlobalHelper.GetImage("Events/youtube.png");
                case IMAGE: return GlobalHelper.GetImage("Commands/image.png");
                case SOUND: return GlobalHelper.GetImage("Commands/sound.png");
                case START_ANIMATION: return GlobalHelper.GetImage("Commands/play.png");
                case STOP_ANIMATION: return GlobalHelper.GetImage("Commands/stop.png");
                case CLONE: return GlobalHelper.GetImage("Commands/clone.png");
                case WAIT: return GlobalHelper.GetImage("Commands/wait.png");
            }
            return GlobalHelper.GetImage("command.png"); ;
        }
    }

    public static class ANIMATIONTYPE
    {
        public const int MOVE = 0;
        public const int ROTATE = 1;
        public const int SCALE = 2;
        public const int SKEW = 3;
        public const int TRANSPARENCY = 4;
    }

    public static class ONANIMATIONEND
    {
        public const int DONOTHING = 0;
        public const int RESET = 1;
        public const int REVERSE = 2;
    }

    public static class COMPONENTTYPE
    {
        public const int Text = 1;
        public const int Image = 2;
        public const int Youtube = 3;
        public const int Listener = 4;
        public const int Animator = 5;
        public const int TextToSpeech = 6;
        public const int Sound = 7;
        public const int Cloning = 8;
    }

    public static class MOUSEACTION
    {
        public const int NONE = 0;
        public const int MOVE = 1;
        public const int DRAG_LEFT = 2;
        public const int DRAG_RIGHT = 3;
    }

    public static class EDITORTABTYPE
    {
        public const int Animator = 0;
    }

    public static class ORIGIN
    {
        public const int TOP_LEFT = 0;
        public const int TOP_RIGHT = 1;
        public const int CENTER = 2;
        public const int BOTTOM_LEFT = 3;
        public const int BOTTOM_RIGHT = 4;
    }

    public static class EDITOR_TRANSFORM_TOOL
    {
        public const int SELECT = 0;
        public const int MOVE = 1;
        public const int RESIZE = 2;
        public const int SCALE = 3;
        public const int ROTATE = 4;
        public const int SKEW = 5;
    }

    public static class LOG_LEVEL
    {
        public const int WARNING = 0;
        public const int ERROR = 1;
        public const int DEBUG = 2;
    }
}
