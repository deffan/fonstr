﻿using fonstr.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace TwitchTool.Source
{
    public class EventMaster
    {
        private List<EventQueue> m_Queues;
        private WindowQueue m_WindowQueue;

        public EventMaster()
        {
            m_Queues = new List<EventQueue>();
            m_WindowQueue = new WindowQueue();
        }

        public void ClearQueues()
        {
            for (int i = 0; i < m_Queues.Count; i++)
            {
                m_Queues[i].ClearQueue();
            }
        }

        public void Reset()
        {
            m_Queues.Clear();
        }

        public void AddQueue(List<EventQueue> q, bool windowIsQueued)
        {
            if(windowIsQueued)
            {
                m_WindowQueue.AddEventQueues(q);
            }
            m_Queues.AddRange(q);
        }

        public void Shutdown()
        {
            m_WindowQueue.Abort();
            for (int i = 0; i < m_Queues.Count; i++)
            {
                m_Queues[i].Shutdown();
            }
        }

        public void Logic()
        {
            for(int i = 0; i < m_Queues.Count; i++)
            {
                m_Queues[i].Logic();
            }
        }
    }
}
