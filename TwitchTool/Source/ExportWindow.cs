﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Security.AccessControl;
using System.Xml.Linq;
using TwitchTool.Animations;
using TwitchTool.Animator;
using TwitchTool.Commands;
using TwitchTool.Components;
using TwitchTool.UserControls;

namespace TwitchTool.Source
{
    public class ExportWindow
    {
        private List<DataVariable> m_DataVariables;
        private AppWindow m_Window;
        private List<string> m_Messages;

        public ExportWindow()
        {
            m_DataVariables = new List<DataVariable>();
            m_Messages = new List<string>();
        }

        public List<string> Messages()
        {
            return m_Messages;
        }

        public void Export(AppWindow window)
        {
            string path = "";
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result != System.Windows.Forms.DialogResult.OK)
                    return;

                path = dialog.SelectedPath;
            }

            m_Window = window;

            // Check/Modify events & commands
            EventCommandXml();

            // Check/Modify components
            WindowXml();

            // Check which Data Variables needs exporting
            if (m_DataVariables.Count > 0)
            {
                // We need to null out the Func's before serializing
                for (int i = 0; i < m_DataVariables.Count; i++)
                {
                    m_DataVariables[i].Data = null;
                }

                // Save datavars to be exported
                try
                {
                    File.WriteAllText(Directory.GetCurrentDirectory() + "\\Windows\\" + m_Window.WindowObject.ID + "\\VariableExport.data", JsonConvert.SerializeObject(m_DataVariables));
                }
                catch (Exception ex)
                {
                    Logger.Log(LOG_LEVEL.ERROR, "Export > Datavariables > " + ex.Message);
                    m_Messages.Add(ex.Message);
                }
            }

            try
            {
                string newPath = path + "\\" + m_Window.WindowObject.ObjectName + "_Export.zip";
                if (File.Exists(newPath))
                {
                    File.Delete(newPath);
                }

                ZipFile.CreateFromDirectory(Directory.GetCurrentDirectory() + "\\Windows\\" + m_Window.WindowObject.ID, path + "\\" + m_Window.WindowObject.ObjectName + "_Export.zip", CompressionLevel.Optimal, true);

                if(m_Messages.Count > 0)
                    m_Messages.Add("--------------------");
                m_Messages.Add("Export successful.");
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Export > Zip > " + ex.Message);
                m_Messages.Add(ex.Message);
            }
        }

        private void WindowXml()
        {
            // Load the window
            MainWindow.StartWindow(m_Window.WindowObject.ID, false);

            // Check components and modify if needed
            WindowXMLModifier(m_Window.WindowObject);

            // Save
            try
            {
                // Create directory if needed
                string directory = Directory.GetCurrentDirectory() + "\\Windows\\" + m_Window.WindowObject.ID;
                Directory.CreateDirectory(directory);

                // Serialize/Save
                XDocument doc = new XDocument();
                XElement root = new XElement("Root");
                doc.Add(root);

                // Version
                root.Add(new XElement("Version", GlobalHelper.APPLICATION_VERSION));

                // Modified
                root.Add(new XElement("Modified", DateTime.Now.ToShortDateString()));

                // Save listener ids
                XElement listeners = new XElement("Listeners");
                List<CommandListenerComponent> listnrs = m_Window.GetListeners();
                for (int i = 0; i < listnrs.Count; i++)
                {
                    listeners.Add(new XElement("L", listnrs[i].ListenerID));
                }
                root.Add(listeners);

                // The windows+objects+components
                root.Add(m_Window.WindowObject.Serialize());
                doc.Save(directory + "\\Window.xml");
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Export > SaveWindow > " + ex.Message);
                m_Messages.Add(ex.Message);
            }
        }

        private void WindowXMLModifier(ObjectControl parent)
        {
            if (parent == null)
                return;

            List<IComponent> components = parent.GetComponents();
            for(int i = 0; i < components.Count; i++)
            {
                if (components[i] is ImageComponent)
                {
                    string p1 = (components[i] as ImageComponent).GetImagePath();
                    string p2 = CopyFile(p1);
                    if (!p2.Equals(p1))
                    {
                        (components[i] as ImageComponent).SetImage(p2, false);
                    }
                }
                else if (components[i] is TextComponent)
                {
                    TextComponent tc = components[i] as TextComponent;
                    if (tc.GetEffectBrushPicker().GetMode() == BrushPickerControl.MODE.IMAGE)
                    {
                        string p1 = tc.GetEffectBrushPicker().GetImagePath();
                        string p2 = CopyFile(p1);
                        if (!p2.Equals(p1))
                        {
                            tc.GetEffectBrushPicker().SetImagePath(p2);
                        }
                    }

                    if (tc.GetTextBrushPicker().GetMode() == BrushPickerControl.MODE.IMAGE)
                    {
                        string p1 = tc.GetTextBrushPicker().GetImagePath();
                        string p2 = CopyFile(p1);
                        if (!p2.Equals(p1))
                        {
                            tc.GetTextBrushPicker().SetImagePath(p2);
                        }
                    }
                }
                else if (components[i] is BasicComponent)
                {
                    BasicComponent tc = components[i] as BasicComponent;
                    if (tc.GetBrushPicker().GetMode() == BrushPickerControl.MODE.IMAGE)
                    {
                        string p1 = tc.GetBrushPicker().GetImagePath();
                        string p2 = CopyFile(p1);
                        if (!p2.Equals(p1))
                        {
                            tc.GetBrushPicker().SetImagePath(p2);
                        }
                    }
                }
                else if (components[i] is SoundComponent)
                {
                    string p1 = (components[i] as SoundComponent).GetSoundFilePath();
                    string p2 = CopyFile(p1);
                    if (!p2.Equals(p1))
                    {
                        (components[i] as SoundComponent).SetSoundFilePath(p2);
                    }
                }
                else if (components[i] is CloningComponent)
                {
                    CloningComponent tc = components[i] as CloningComponent;
                    CheckDataVariable(tc.GetSettings().LifeTimeData);
                }
                else if (components[i] is AnimatorComponent)
                {
                    AnimatorComponent tc = components[i] as AnimatorComponent;
                    List<AnimationDataHolder> animations = tc.GetObjectsToAnimate();
                    if (animations.Count > 0)
                    {
                        for (int n = 0; n < animations[0].AnimationTracks.Count; n++)
                        {
                            // Animation data
                            switch (animations[0].AnimationTracks[n].AnimationType)
                            {
                                case ANIMATIONTYPE.MOVE:
                                {
                                    for (int k = 0; k < animations[0].AnimationTracks[n].AnimationDataList.Count; k++)
                                    {
                                        MoveAnimationData d = animations[0].AnimationTracks[n].AnimationDataList[k] as MoveAnimationData;
                                        CheckDataVariable(d.ToX);
                                        CheckDataVariable(d.ToY);
                                    }
                                    break;
                                }
                                case ANIMATIONTYPE.ROTATE:
                                {
                                    for (int k = 0; k < animations[0].AnimationTracks[n].AnimationDataList.Count; k++)
                                    {
                                        RotateAnimationData d = animations[0].AnimationTracks[n].AnimationDataList[k] as RotateAnimationData;
                                        CheckDataVariable(d.ToAngle);
                                    }
                                    break;
                                }
                                case ANIMATIONTYPE.SCALE:
                                {
                                    for (int k = 0; k < animations[0].AnimationTracks[n].AnimationDataList.Count; k++)
                                    {
                                        ScaleAnimationData d = animations[0].AnimationTracks[n].AnimationDataList[k] as ScaleAnimationData;
                                        CheckDataVariable(d.ToXScale);
                                        CheckDataVariable(d.ToYScale);
                                    }
                                    break;
                                }
                                case ANIMATIONTYPE.SKEW:
                                {
                                    for (int k = 0; k < animations[0].AnimationTracks[n].AnimationDataList.Count; k++)
                                    {
                                        SkewAnimationData d = animations[0].AnimationTracks[n].AnimationDataList[k] as SkewAnimationData;
                                        CheckDataVariable(d.ToXSkew);
                                        CheckDataVariable(d.ToYSkew);
                                    }
                                    break;
                                }
                                case ANIMATIONTYPE.TRANSPARENCY:
                                {
                                    for (int k = 0; k < animations[0].AnimationTracks[n].AnimationDataList.Count; k++)
                                    {
                                        TransparencyAnimationData d = animations[0].AnimationTracks[n].AnimationDataList[k] as TransparencyAnimationData;
                                        CheckDataVariable(d.ToOpacity);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            for(int i = 0; i < parent.Children.Count; i++)
            {
                WindowXMLModifier(parent.Children[i] as ObjectControl);
            }
        }

        private void EventCommandXml()
        {
            // Open the EventCommand XML for this window
            XDocument doc = null;
            try
            {
                doc = XDocument.Load(m_Window.Path + "\\EventCommand.xml");
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Export > ModifyEventCommandXml > "+ ex.Message);
                m_Messages.Add(ex.Message);
                return;
            }

            XElement events = doc.Element("Windows").Element("Window").Element("Events");
            foreach (XElement eventObj in events.Elements())
            {
                string eventId = eventObj.Element("Id").Value;
                int eventType = Convert.ToInt32(eventObj.Element("Type").Value);

                // Loop commands
                XElement commands = eventObj.Element("Commands");
                foreach (XElement commandObj in commands.Elements())
                {
                    string commandId = commandObj.Element("Id").Value;
                    int commandType = Convert.ToInt32(commandObj.Element("Type").Value);
                    string commandData = GlobalHelper.Base64Decode(commandObj.Element("Data").Value);

                    // Open up relevant command-data, and check and modify any paths. Also collect datavariables.
                    switch (commandType)
                    {
                        case COMMAND_TYPE.IMAGE:
                        {
                            ImageCommandData tc = JsonConvert.DeserializeObject<ImageCommandData>(commandData);
                            ImgCmd(tc);
                            commandData = GlobalHelper.Base64Encode(JsonConvert.SerializeObject(tc));
                            break;
                        }
                        case COMMAND_TYPE.SOUND:
                        {
                            SoundCommandData tc = JsonConvert.DeserializeObject<SoundCommandData>(commandData);
                            SoundCmd(tc);
                            commandData = GlobalHelper.Base64Encode(JsonConvert.SerializeObject(tc));
                            break;
                        }
                        case COMMAND_TYPE.WAIT:
                        {
                            WaitCommandData tc = JsonConvert.DeserializeObject<WaitCommandData>(commandData);
                            WaitCmd(tc);
                            commandData = GlobalHelper.Base64Encode(JsonConvert.SerializeObject(tc));
                            break;
                        }
                        case COMMAND_TYPE.YOUTUBE:
                        {
                            YoutubeCommandData tc = JsonConvert.DeserializeObject<YoutubeCommandData>(commandData);
                            YoutubeCmd(tc);
                            commandData = GlobalHelper.Base64Encode(JsonConvert.SerializeObject(tc));
                            break;
                        }
                        case COMMAND_TYPE.TEXT:
                        {
                            TextCommandData tc = JsonConvert.DeserializeObject<TextCommandData>(commandData);
                            TextCmd(tc);
                            commandData = GlobalHelper.Base64Encode(JsonConvert.SerializeObject(tc));
                            break;
                        }
                    }

                    // Update
                    commandObj.Element("Data").Value = commandData;
                }
            }

            // Save
            doc.Save(m_Window.Path + "\\EventCommand.xml");
        }

        private string CopyFile(string path)
        {
            string newPath = path;

            // Check if this file exist (basically, is it a file at all?)
            if (File.Exists(path))
            {
                try
                {
                    // Create the new path
                    newPath = Directory.GetCurrentDirectory() + string.Format("\\Windows\\{0}\\{1}", m_Window.WindowObject.ID, Path.GetFileName(path));

                    // Same path? Then we dont have to do anything
                    if (newPath.Equals(path))
                    {
                        return newPath;
                    }

                    // Copy
                    File.Copy(path, newPath);
                }
                catch(Exception ex)
                {
                    Logger.Log(LOG_LEVEL.ERROR, "Export > CopyFile > " + ex.Message);
                    m_Messages.Add(ex.Message);
                }
            }

            return newPath;
        }

        private void TextDataPath(TextData textData)
        {
            // If text, see if this is a file, and if so, perhaps copy the file and change the path to local
            if (textData.IsText)
            {
                string filePath = CopyFile(textData.Data);
                if (!filePath.Equals(textData.Data))
                {
                    textData.Data = filePath;
                }
            }
            else if (textData.IsDataVariable)
            {
                CheckDataVariable(textData);
            }
        }

        private void CheckDataVariable(TextData t)
        {
            if(t == null)
            {
                return;
            }

            DataVariable d = null;
            if (t.IsDataVariable)
            {
                d = t.GetDataVariable();
            }
            else
            {
                return;
            }

            if (d == null || d.DefaultVariable)
            {
                return;
            }

            if (!m_DataVariables.Contains(d))
            {
                // This datavariable needs to be exported too
                m_DataVariables.Add(d);

                // Copy/Change any filepaths
                for (int i = 0; i < d.Listdata.Count; i++)
                {
                    d.Listdata[i] = CopyFile(d.Listdata[i]);
                }
            }
        }

        private void ImgCmd(ImageCommandData c)
        {
            if (!c.m_UseComponentImage)
            {
                TextDataPath(c.m_Data);

                if (c.m_UseBackup)
                {
                    TextDataPath(c.m_BackupData);
                }
            }
        }

        private void SoundCmd(SoundCommandData c)
        {
            if (!c.m_UseComponentSound)
            {
                TextDataPath(c.m_Data);

                if (c.m_UseBackup)
                {
                    TextDataPath(c.m_BackupData);
                }
            }
        }

        private void TextCmd(TextCommandData c)
        {
            for(int i = 0; i < c.TextDataList.Count; i++)
            {
                CheckDataVariable(c.TextDataList[i]);
            }
        }

        private void WaitCmd(WaitCommandData c)
        {
            CheckDataVariable(c.m_TextData);
        }

        private void YoutubeCmd(YoutubeCommandData c)
        {
            if(!c.m_UseComponentSettings)
            {
                CheckDataVariable(c.m_Data);
                CheckDataVariable(c.m_DurationData);
                CheckDataVariable(c.m_StartData);

                if (c.m_UseBackup)
                {
                    CheckDataVariable(c.m_BackupData);
                }
            }
        }



    }
}
