﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Source
{
    public class TextData
    {
        public string Data;
        public string Id;
        public string Type;

        private bool m_Variable;
        private bool m_Text;
        private bool m_Event;

        // Temp variables to keep values fetched only once
        private double? m_TempDouble;
        private string m_TempString;

        public TextData(string text)
        {
            Type = "text";
            Data = text;
            IsText = true;
        }

        public bool IsDataVariable
        {
            get { return m_Variable; }
            set { m_Variable = value; }
        }

        public bool IsText
        {
            get { return m_Text; }
            set { m_Text = value; }
        }

        public bool IsEventVariable
        {
            get { return m_Event; }
            set { m_Event = value; }
        }

        public string GetStringData(Event e)
        {
            if (IsText)
            {
                return Data;
            }
            else if (IsEventVariable)
            {
                return e.Data.GetVariableData(Id);
            }
            else if (IsDataVariable)
            {
                DataVariable v = GlobalHelper.DataVariables.GetVariable(Id);
                if(v != null) 
                {
                    return v.Data(null);
                }
            }
            return "";
        }

        public string GetStringData()
        {
            if (IsText)
            {
                return Data;
            }
            else if (IsDataVariable)
            {
                DataVariable v = GlobalHelper.DataVariables.GetVariable(Id);
                if (v != null) 
                {
                    return v.Data(null);
                }
            }
            return "";
        }

        public double GetNumericData(Event e)
        {
            double result = 0;
            try
            {
                result = Convert.ToDouble(GetStringData(e), CultureInfo.InvariantCulture);
            }
            catch (Exception) { }
            return result;
        }

        public double GetNumericData()
        {
            double result = 0;
            try
            {
                result = Convert.ToDouble(GetStringData(), CultureInfo.InvariantCulture);
            }
            catch (Exception) { }
            return result;
        }

        public double? GetNumericDataOnce()
        {
            if(m_TempDouble == null)
            {
                m_TempDouble = GetNumericData();
            }
            return m_TempDouble;
        }

        public string GetStringDataOnce()
        {
            if (m_TempString == null)
            {
                m_TempString = GetStringData();
            }
            return m_TempString;
        }

        public DataVariable GetDataVariable()
        {
            return GlobalHelper.DataVariables.GetVariable(Id);
        }

        public EventVariable GetEventVariable()
        {
            return GlobalHelper.EventVariables.GetVariable(Id);
        }
    }
}
