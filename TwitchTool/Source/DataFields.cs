﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Source
{
    public class DataFields
    {
        private Dictionary<int, List<string>> m_Events;

        public DataFields()
        {
            m_Events = new Dictionary<int, List<string>>();

            m_Events.Add(EVENT_TYPE.KEYBOARD, new List<string>());
            m_Events.Add(EVENT_TYPE.TWITCH_BITS, new List<string> { "Amount", "Currency", "User", "Message" });
            m_Events.Add(EVENT_TYPE.TWITCH_SUBSCRIPTION, new List<string> { "Months", "User", "Message" });
            m_Events.Add(EVENT_TYPE.TWITCH_FOLLOW, new List<string> { "Amount", "Currency", "Username", "Message" });
            m_Events.Add(EVENT_TYPE.TWITCH_HOST, new List<string> { "Amount", "Currency", "Username", "Message" });

            // Subscription och Resubscription är samma sak, dock med typ skillnaden att det antingen är första gången eller inte.
            // vill man väl kunna skilja på?
            // Ha två olika event? Men om man inte har ett resub-event, så körs sub-eventet istället. Och tvärtom (?).
            //
            // Eller samma event? Men man får ha special "if-satser" i Twitch Subscription Event (om man vill).


            // Hmm..
            // Tydligen är det många som har en TwitchBot, som LÄSER CHATTEN och där hittar subs, followers, bit-donations ?
            // Men syns det verkligen alltid ? ...

        }

        public List<string> GetEventFields(int eventType)
        {
            return m_Events[eventType];
        }

    }
}

