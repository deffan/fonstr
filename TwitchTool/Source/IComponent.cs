﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.ComponentsEditor;
using TwitchTool.UserControls;

namespace TwitchTool.Source
{
    public interface IComponent : IXML
    {
        IEditorComponent GetEditorComponent();

        ObjectControl GetParentObject();

        void NewComponentConstructor();

        void RemoveComponent();

        bool CanRun(Command c);

        bool IsRunning();

        void Stop();

        void Initialize(Command c);

        bool Logic();

        void SetID(string id);

        string GetID();
    }
}
