﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml.Linq;
using TwitchTool.Commands;
using TwitchTool.Components;
using TwitchTool.EventListeners;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.LiveTab;
using TwitchTool.Source;
using TwitchTool.Twitch;
using TwitchTool.UserControls;
using TwitchTool.WinEventCmdTab.EventQueue;
using static TwitchTool.WinEventCmdTab.EventQueue.EventQueueCtrl;

namespace TwitchTool
{
    public partial class MainWindow : Window
    {
        private static EventMaster m_EventMaster;
        private Thread m_EventMasterThread;
        private bool m_Running;
        private static Dictionary<string, AppWindow> m_Windows;
        private static bool m_Live;
        private static DateTime m_LiveStart;
        private static List<Tuple<int, string>> m_LiveLogMessages;
        private static object m_LiveLogLock;
        private static Action m_LiveTabStopCallback;
        private static WinEventCmdTab.WinEventCmdCtrl m_WinEventCmdCtrl;
        private static LiveTab.LiveTab m_LiveTab;
        private TabItem m_SelectedTab;

        // Twitch
        private static TwitchAPI m_TwitchApi;
        public static TwitchAPI TwitchApi { get { return m_TwitchApi; } }

        // Listners
        private static KeyboardEventListener m_KeyboardEventListener;

        public MainWindow()
        {
            InitializeComponent();

            ApplicationVersionLabel.Content = GlobalHelper.APPLICATION_VERSION;
            ApplicationVersionDateLabel.Content = GlobalHelper.APPLICATION_VERSION_DATE;

            try
            {
                Width = SystemParameters.WorkArea.Width - 250;
                m_SelectedTab = null;
                m_WinEventCmdCtrl = WindowEventCommandTab.WECControl;
                m_LiveTab = LiveTabControl;
                m_Windows = new Dictionary<string, AppWindow>();
                m_EventMaster = new EventMaster();
                m_KeyboardEventListener = new KeyboardEventListener();
                m_Live = false;
                m_LiveLogMessages = new List<Tuple<int, string>>();
                m_LiveLogLock = new object();
                m_LiveTabStopCallback = new Action(LiveTabControl.Stop);

                // Twitch
                m_TwitchApi = new TwitchAPI();
                m_TwitchApi.TwitchBot.Listener = new TwitchListener(m_TwitchApi);
                if(GlobalHelper.ApplicationSettings.AutoLoginTwitch)
                {
                    m_TwitchApi.Login(false);
                }

                // Load windows
                Reload();

                // Start the "Event Master" thread
                m_Running = true;
                m_EventMasterThread = new Thread(new ThreadStart(EventMasterThreadLoop));
                m_EventMasterThread.SetApartmentState(ApartmentState.STA);
                m_EventMasterThread.Start();
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "MainWindow" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }

        }

        public static void Reload()
        {
            // Check if Windows directory exits, if not, then create it.
            string windowsPath = Directory.GetCurrentDirectory() + "\\Windows";
            if (!Directory.Exists(windowsPath))
            {
                Directory.CreateDirectory(windowsPath);
            }

            // Load in all windows
            m_Windows.Clear();
            string[] dir = Directory.GetDirectories(windowsPath);
            for (int i = 0; i < dir.Length; i++)
            {
                if (!File.Exists(dir[i] + "\\Window.xml"))
                    continue;

                try
                {
                    AppWindow win = new AppWindow(dir[i]);
                    m_Windows.Add(win.WindowObject.ID, win);
                }
                catch (Exception ex)
                {
                    Logger.Log(LOG_LEVEL.ERROR, "Unable to load window: " + dir[i] + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                }
            }

            m_WinEventCmdCtrl.Load();
        }

        public static string CreateNewWindow()
        {
            string directory = "";
            try
            {
                ObjectControl window = new ObjectControl();
                window.InitDefaultObject();
                (window.GetComponent<TransformComponent>() as TransformComponent).SetSize(640, 480);
                window.ID = Guid.NewGuid().ToString();
                window.ObjectName = "New Window";

                directory = Directory.GetCurrentDirectory() + "\\Windows\\" + window.ID;
                Directory.CreateDirectory(directory);

                // Serialize/Save
                XDocument doc = new XDocument();
                XElement root = new XElement("Root");
                doc.Add(root);

                root.Add(new XElement("Version", GlobalHelper.APPLICATION_VERSION));
                root.Add(new XElement("Modified", DateTime.Now.ToShortDateString()));
                root.Add(new XElement("LocationX", 0));
                root.Add(new XElement("LocationY", 0));

                root.Add(new XElement("Listeners"));

                root.Add(window.Serialize());
                doc.Save(directory + "\\Window.xml");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to create new window.", "Create new window", MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Log(LOG_LEVEL.ERROR, "MainWindow::CreateNewWindow" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                return "";
            }

            AppWindow win = new AppWindow(directory);
            m_Windows.Add(win.WindowObject.ID, win);

            return win.WindowObject.ID;
        }

        public static WinEventCmdTab.WinEventCmdCtrl GetWinEventCmdTab()
        {
            return m_WinEventCmdCtrl;
        }

        public static bool Live
        {
            get { return m_Live; }
            set
            {
                m_Live = value;
                if(m_Live)
                {
                    m_LiveStart = DateTime.Now;
                }
            }
        }

        public static DateTime LiveStartTime
        {
            get { return m_LiveStart; }
        }

        public static EventMaster EventMaster
        {
            get { return m_EventMaster; }
        }

        public static void AddLiveLog(int level, string msg)
        {
            lock (m_LiveLogLock)
            {
                m_LiveLogMessages.Add(new Tuple<int, string>(level, msg));
            }  
        }
          
        public static void StartAllWindows()
        {
            m_EventMaster.Reset();

            for (int i = 0; i < m_Windows.Count; i++)
            {
                if(m_Windows.ElementAt(i).Value.WindowObject.GetBasicComponent().Active)
                {
                    StartWindow(m_Windows.ElementAt(i).Key, true);
                }
            }

            Live = RunningWindowCount > 0;
        }

        public static void StopAllWindows()
        {
            for (int i = 0; i < m_Windows.Count; i++)
            {
                m_Windows.ElementAt(i).Value.Stop();
            }
            Live = false;
        }

        // This function adds all events/commands to the window, then starting or just loading it
        public static void StartWindow(string id, bool start)
        {
            try
            {
                // Load the XML containing all events > commands > listeners
                XDocument doc = XDocument.Load(Windows[id].Path + "\\EventCommand.xml");
                XElement window = doc.Element("Windows").Element("Window");

                if (start && !Convert.ToBoolean(window.Element("Active").Value))
                {
                    Logger.Log(LOG_LEVEL.WARNING, "Window '" + GlobalHelper.Base64Decode(window.Element("Name").Value) + "' not started (INACTIVE)");
                    return;
                }

                // Reload
                m_Windows[id].Reload();

                // Remove any old events in History list in LiveTab. They cannot be restarted from previous window... right now
                if(start)
                {
                    m_LiveTab.WindowStarted(id);
                }

                // Add queues to Window
                if (start)
                {
                    WindowQueue winq = JsonConvert.DeserializeObject<WindowQueue>(GlobalHelper.Base64Decode(window.Element("Queue").Value));
                    List<EventQueueList> queues = winq.Queues;
                    for (int i = 0; i < queues.Count; i++)
                    {
                        m_Windows[id].AddQueue(new EventQueue(queues[i].Name, queues[i].EventIds, EventQueue.QueueType.CUSTOM, m_LiveTab, queues[i].Delayed, queues[i].IsDelayed, queues[i].DelayedSetting));
                    }
                    m_Windows[id].AddQueue(new EventQueue("All (Default)", new List<string>(), EventQueue.QueueType.ALL, m_LiveTab, null, false, 0));
                    m_Windows[id].AddQueue(new EventQueue("No Queueing", new List<string>(), EventQueue.QueueType.NO, m_LiveTab, null, false, 0));

                    // Add this windows queues to the EventMaster
                    m_EventMaster.AddQueue(m_Windows[id].Queues.Values.ToList(), winq.WindowIsQueued);
                }

                // Events
                XElement events = window.Element("Events");
                foreach (XElement eventObj in events.Elements())
                {
                    if (start && !Convert.ToBoolean(eventObj.Element("Active").Value))
                        continue;

                    Event e = null;
                    switch (Convert.ToInt32(eventObj.Element("Type").Value))
                    {
                        case EVENT_TYPE.KEYBOARD:
                        {
                            e = new KeyboardEvent(JsonConvert.DeserializeObject<KeyboardEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_KeyboardEventListener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BOT_JOIN:
                        {
                            e = new TwitchJoinEvent(JsonConvert.DeserializeObject<TwitchJoinEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BOT_PART:
                        {
                            e = new TwitchPartEvent(JsonConvert.DeserializeObject<TwitchPartEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_SUBSCRIPTION:
                        {
                            e = new TwitchSubscriptionEvent(JsonConvert.DeserializeObject<TwitchSubscriptionEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_FOLLOW:
                        {
                            e = new TwitchFollowEvent(JsonConvert.DeserializeObject<TwitchFollowEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BOT_CUSTOM:
                        {
                            e = new TwitchCustomEvent(JsonConvert.DeserializeObject<TwitchCustomEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_GIFT:
                        {
                            e = new TwitchGiftEvent(JsonConvert.DeserializeObject<TwitchGiftEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_RAID:
                        {
                            e = new TwitchRaidEvent(JsonConvert.DeserializeObject<TwitchRaidEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BITS:
                        {
                            e = new TwitchBitsEvent(JsonConvert.DeserializeObject<TwitchBitsEventData>(GlobalHelper.Base64Decode(eventObj.Element("Data").Value)));
                            if (start)
                                m_TwitchApi.TwitchBot.Listener.AddEvent(e);
                            break;
                        }
                        default: throw new Exception("Unknown EVENT_TYPE.");
                    }

                    e.EventId = eventObj.Element("Id").Value;
                    e.Data.Name = GlobalHelper.Base64Decode(eventObj.Element("Name").Value);
                    e.Data.Id = eventObj.Element("Id").Value;
                    e.Window = m_Windows[id];

                    // EventQueue
                    e.Queue = GlobalHelper.Base64Decode(eventObj.Element("Queue").Value);

                    // Commands
                    XElement commands = eventObj.Element("Commands");
                    foreach (XElement commandObj in commands.Elements())
                    {
                        if (start && !Convert.ToBoolean(commandObj.Element("Active").Value))
                            continue;

                        Command c = null;
                        switch (Convert.ToInt32(commandObj.Element("Type").Value))
                        {
                            case COMMAND_TYPE.TEXT:
                            {
                                c = new TextCommand(JsonConvert.DeserializeObject<TextCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            case COMMAND_TYPE.CLONE:
                            {
                                c = new CloningCommand(JsonConvert.DeserializeObject<CloningCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            case COMMAND_TYPE.YOUTUBE:
                            {
                                c = new YoutubeCommand(JsonConvert.DeserializeObject<YoutubeCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            case COMMAND_TYPE.SOUND:
                            {
                                c = new SoundCommand(JsonConvert.DeserializeObject<SoundCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            case COMMAND_TYPE.IMAGE:
                            {
                                c = new ImageCommand(JsonConvert.DeserializeObject<ImageCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            case COMMAND_TYPE.START_ANIMATION:
                            {
                                c = new StartAnimationCommand(JsonConvert.DeserializeObject<StartAnimationCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            case COMMAND_TYPE.STOP_ANIMATION:
                            {
                                c = new StopAnimationCommand(JsonConvert.DeserializeObject<StopAnimationCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            case COMMAND_TYPE.WAIT:
                            {
                                c = new WaitCommand(JsonConvert.DeserializeObject<WaitCommandData>(GlobalHelper.Base64Decode(commandObj.Element("Data").Value)));
                                break;
                            }
                            default: throw new Exception("Unknown COMMAND_TYPE.");
                        }
                        c.Event = e;

                        // Add listeners
                        XElement listeners = commandObj.Element("Listeners");
                        foreach (XElement listenerObj in listeners.Elements())
                        {
                            c.Data.Listeners.Add(GlobalHelper.Base64Decode(listenerObj.Element("Id").Value));
                        }

                        e.AddCommand(c);
                    }

                    if(e != null)
                    {
                        m_Windows[id].AddEvent(e);
                    }
                   
                }

                if (start)
                {
                    m_Windows[id].Start();
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "StartWindow: " + id + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
        }

        public static int RunningWindowCount
        {
            get
            {
                int runningCount = 0;
                for (int i = 0; i < m_Windows.Count; i++)
                {
                    if (m_Windows.ElementAt(i).Value.Running)
                    {
                        runningCount++;
                    }
                }
                return runningCount;
            }
        }

        // Window has stopped. So we should remove all events from eventlisteners and such.
        public static void StoppedWindow(string id)
        {
            for(int i = 0; i < m_Windows[id].Events.Count; i++)
            {
                switch(m_Windows[id].Events[i].Type)
                {
                    case EVENT_TYPE.KEYBOARD: m_KeyboardEventListener.RemoveEvent(m_Windows[id].Events[i]); break;
                    case EVENT_TYPE.TWITCH_BOT_PART: 
                    case EVENT_TYPE.TWITCH_BOT_JOIN: 
                    case EVENT_TYPE.TWITCH_SUBSCRIPTION:
                    case EVENT_TYPE.TWITCH_FOLLOW:
                    case EVENT_TYPE.TWITCH_BOT_CUSTOM:
                    case EVENT_TYPE.TWITCH_GIFT:
                    case EVENT_TYPE.TWITCH_RAID:
                    case EVENT_TYPE.TWITCH_BITS:
                    {
                        m_TwitchApi.TwitchBot.Listener.RemoveEvent(m_Windows[id].Events[i]);
                        break;
                    }
                    default: break;
                }
            }

            // Check if this was the last window running, then we should set Live to false. Also, make sure that the "Stop/Start" buttons in LiveTab is reset.
            // This needs to be done here because you can also close the windows manually on the [x] and the LiveTab wont know otherwise.
            bool last = true;
            for (int i = 0; i < m_Windows.Count; i++)
            {
                if(m_Windows.ElementAt(i).Value.Running)
                {
                    last = false;
                    break;
                }
            }

            if(last)
            {
                Live = false;
                m_LiveTabStopCallback();
            }

        }

        private void EventMasterThreadLoop()
        {
            try
            {
                while (m_Running)
                {
                    Thread.Sleep(100);
                    m_EventMaster.Logic();
                    CheckLogMessages();
                }
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "EventMasterThreadLoop - " + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
            m_EventMaster.Shutdown();
        }

        private void CheckLogMessages()
        {
            lock (m_LiveLogLock)
            {
                if (m_LiveLogMessages.Count == 0)
                    return;

                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    for (int i = 0; i < m_LiveLogMessages.Count; i++)
                    {
                        int logLevel = m_LiveLogMessages[i].Item1;
                        string text = m_LiveLogMessages[i].Item2;

                        LogListItem item = new LogListItem(false, logLevel, text);

                        m_LiveTab.LogPanel.Children.Add(item);
                        if (m_LiveTab.LogPanel.Children.Count % 2 == 0)
                        {
                            item.SetEven();
                        }
                    }
                    m_LiveLogMessages.Clear();
                }));  
            }
        }

        public static Dictionary<string, AppWindow> Windows
        {
            get { return m_Windows; }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_TwitchApi.TwitchBot.Stop();

            // Här är det ju lite fel att ha flera frågor. Borde sammanställas till en.

            if (GetWinEventCmdTab().SaveCheck() == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
                return;
            }

            if(SettingsTab.VariablesTab.SaveCheck() == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
                return;
            }

            if (SettingsTab.LiveTabSettings.SaveCheck() == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
                return;
            }

            GlobalHelper.ApplicationSettings.Save();

            StopAllWindows();
            m_Running = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // HwndSource hwndSource = PresentationSource.FromVisual(this) as HwndSource;
            // HwndTarget hwndTarget = hwndSource.CompositionTarget;
            // hwndTarget.RenderMode = RenderMode.SoftwareOnly;
            Activate();
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(sender.Equals(tabControl)))
                return;

            if (m_SelectedTab == tabControl.SelectedItem as TabItem)
                return;

            // Savecheck if we exited the WinEventCmdTab
            if(m_SelectedTab == WinEventCmdTabItem)
            {
                if(GetWinEventCmdTab().SaveCheck() == MessageBoxResult.Cancel)
                {
                    // Reselect WinEventCmdTab.
                    Dispatcher.BeginInvoke((Action)(() => tabControl.SelectedIndex = 0));
                    e.Handled = true;
                }
            }

            // Savecheck if we exited the Settingstab
            if (m_SelectedTab == SettingsTabItem)
            {
                if (SettingsTab.VariablesTab.SaveCheck() == MessageBoxResult.Cancel)
                {
                    // Reselect SettingsTab.
                    Dispatcher.BeginInvoke((Action)(() => tabControl.SelectedItem = SettingsTabItem));
                    e.Handled = true;
                }
                else if (SettingsTab.LiveTabSettings.SaveCheck() == MessageBoxResult.Cancel)
                {
                    // Reselect SettingsTab.
                    Dispatcher.BeginInvoke((Action)(() => tabControl.SelectedItem = SettingsTabItem));
                    e.Handled = true;
                }
            }

            // Set selected
            m_SelectedTab = tabControl.SelectedItem as TabItem;

            if(m_SelectedTab == TestTabItem)
            {
                TestTab.Load();
            }
            else if(m_SelectedTab == WinEventCmdTabItem)
            {
                // Refresh if needed
                GetWinEventCmdTab().Refresh();
            }

        }

        private void FonstrWebLink_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            FonstrWebLink.Foreground = new SolidColorBrush(Colors.Blue);
            Cursor = Cursors.Hand;
        }

        private void FonstrWebLink_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            FonstrWebLink.Foreground = new SolidColorBrush(Colors.Black);
            Cursor = Cursors.Arrow;
        }

        private void FonstrWebLink_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.fonstr.com/");
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.WARNING, ex.Message);
            }

        }
    }
}

