﻿using CefSharp;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

namespace TwitchTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        [STAThread]
        public static void Main()
        {
            try
            {
                // This is the ChromiumBrowser settings.
                /*
                var settings = new CefSettings();
                settings.CachePath = "Cache";
                settings.CefCommandLineArgs.Add("disable-plugins-discovery", "1");
                settings.CefCommandLineArgs.Add("no-proxy-server", "1");
                Cef.Initialize(settings);
                */

                // This is needed for C# and Javascript to communicate
                CefSharpSettings.LegacyJavascriptBindingEnabled = true;

                // Set application culture
                CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
                CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

                var application = new App();
                application.InitializeComponent();
                application.Run();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}
