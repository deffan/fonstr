﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class YoutubeCommand : Command
    {
        public YoutubeCommand()
        {
            Data = new YoutubeCommandData();
            Data.Type = COMMAND_TYPE.YOUTUBE;
        }

        public YoutubeCommand(YoutubeCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.YOUTUBE;
        }

        public override Command Clone(Event e)
        {
            YoutubeCommand t = new YoutubeCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
