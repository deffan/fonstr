﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class StopAnimationCommand : Command
    {
        public StopAnimationCommand()
        {
            Data = new StopAnimationCommandData();
            Data.Type = COMMAND_TYPE.STOP_ANIMATION;
        }

        public StopAnimationCommand(StopAnimationCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.STOP_ANIMATION;
        }

        public override Command Clone(Event e)
        {
            StopAnimationCommand t = new StopAnimationCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
