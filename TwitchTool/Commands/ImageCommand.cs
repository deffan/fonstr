﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class ImageCommand : Command
    {
        public ImageCommand()
        {
            Data = new ImageCommandData();
            Data.Type = COMMAND_TYPE.IMAGE;
        }

        public ImageCommand(ImageCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.IMAGE;
        }

        public override Command Clone(Event e)
        {
            ImageCommand t = new ImageCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
