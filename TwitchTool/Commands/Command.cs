﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Commands;
using TwitchTool.UserControls;

namespace TwitchTool.Source
{
    // Base Class for Commands
    public class Command
    {
        private Event m_Event;                  // What event that sent this command
        private bool m_Finished;                // This will be set by the window, when the command has finished its execution
        private bool m_Aborted;
        private string m_UniqueEventId;         // Created when added to event-list
        private Action<Command> m_FinishedCallback;
        private CommandData m_Data;

        public Command()
        {
            Event = null;
            m_Finished = false;
            m_Aborted = false;
            m_Data = new CommandData();
        }

        public void SetCommandCallback(Action<Command> callback)
        {
            m_FinishedCallback = callback;  // Called when finished 
        }

        public void SetUniqueId(string id)
        {
            m_UniqueEventId = id;
        }

        public CommandData Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        public virtual Command Clone(Event e)
        {
            throw new Exception("Command Clone not implemented.");
        }

        public bool Aborted
        {
            get { return m_Aborted; }
        }

        public bool Finished
        {
            get { return m_Finished; }
            set
            {
                if(!m_Finished)
                {
                    m_Finished = value;
                    m_FinishedCallback(this);
                }
            }
        }

        public void Reset()
        {
            m_Finished = false;
            m_Aborted = false;
        }

        public Event Event
        {
            get { return m_Event; }
            set { m_Event = value; }
        }

        public void Abort(bool aborted)
        {
            m_Aborted = aborted;
        }

    }
}
