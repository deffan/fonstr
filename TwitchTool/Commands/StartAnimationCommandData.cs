﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Commands
{
    public class StartAnimationCommandData : CommandData
    {
        public override CommandData Clone()
        {
            return base.Clone<StartAnimationCommandData>();
        }
    }
}
