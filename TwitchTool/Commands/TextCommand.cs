﻿using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class TextCommand : Command
    {
        public TextCommand()
        {
            Data = new TextCommandData();
            Data.Type = COMMAND_TYPE.TEXT;
        }

        public TextCommand(TextCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.TEXT;
        }

        public override Command Clone(Event e)
        {
            TextCommand t = new TextCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
