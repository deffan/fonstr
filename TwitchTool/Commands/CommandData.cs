﻿using System;
using System.Collections.Generic;

namespace TwitchTool.Commands
{
    public class CommandData
    {
        public string Id;
        public int Type;
        public string Name;
        public bool Active;
        public List<string> Listeners;
        public string EventId;
        public string WindowId;
        public int Order;
        public int OldOrder;

        public CommandData()
        {
            Id = "";
            Order = 0;
            OldOrder = 0;
            Type = -1;
            Name = "";
            Active = false;
            Listeners = new List<string>();
            EventId = "";
            WindowId = "";
        }

        protected virtual T Clone<T>() where T : CommandData, new()
        {
            T c = new T();
            c.Id = Id;
            c.Type = Type;
            c.Name = Name;
            c.Active = Active;
            c.EventId = EventId;
            c.WindowId = WindowId;
            c.Order = Order;
            c.OldOrder = OldOrder;

            for (int i = 0; i < Listeners.Count; i++)
            {
                c.Listeners.Add(Listeners[i]);
            }
            return c;
        }

   
        public virtual CommandData Clone()
        {
            throw new Exception("CommandData Clone not implemented in subclass.");
        }
 

    }
}
