﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class ImageCommandData : CommandData
    {
        public TextData m_Data = new TextData("");
        public TextData m_BackupData = new TextData("");
        public bool m_UseBackup = false;
        public bool m_UseComponentImage = true;

        public override CommandData Clone()
        {
            ImageCommandData c = base.Clone<ImageCommandData>();
            c.m_Data = m_Data;
            c.m_BackupData = m_BackupData;
            c.m_UseBackup = m_UseBackup;
            c.m_UseComponentImage = m_UseComponentImage;
            return c;
        }
    }
}
