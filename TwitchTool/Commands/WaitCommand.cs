﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class WaitCommand : Command
    {
        public WaitCommand()
        {
            Data = new WaitCommandData();
            Data.Type = COMMAND_TYPE.WAIT;
        }

        public WaitCommand(WaitCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.WAIT;
        }

        public override Command Clone(Event e)
        {
            WaitCommand t = new WaitCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
