﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class SoundCommandData : CommandData
    {
        public TextData m_Data = new TextData("");
        public TextData m_BackupData = new TextData("");
        public bool m_UseBackup = false;
        public bool m_UseComponentSound = true;

        public override CommandData Clone()
        {
            SoundCommandData c = base.Clone<SoundCommandData>();
            c.m_Data = m_Data;
            c.m_BackupData = m_BackupData;
            c.m_UseBackup = m_UseBackup;
            c.m_UseComponentSound = m_UseComponentSound;
            return c;
        }
    }
}
