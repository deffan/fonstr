﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class TextCommandData : CommandData
    {
        public List<TextData> TextDataList = new List<TextData>();

        public override CommandData Clone()
        {
            TextCommandData c = base.Clone<TextCommandData>();

            for (int i = 0; i < TextDataList.Count; i++)
            {
                c.TextDataList.Add(TextDataList[i]);
            }
            return c;
        }
    }
}
