﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class CloningCommand : Command
    {
        public CloningCommand()
        {
            Data = new CloningCommandData();
            Data.Type = COMMAND_TYPE.CLONE;
        }

        public CloningCommand(CloningCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.CLONE;
        }

        public override Command Clone(Event e)
        {
            CloningCommand t = new CloningCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
