﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class YoutubeCommandData : CommandData
    {
        public TextData m_Data = new TextData("");
        public TextData m_StartData = new TextData("");
        public TextData m_DurationData = new TextData("");
        public TextData m_BackupData = new TextData("");
        public bool m_UseBackup = false;
        public bool m_UseComponentSettings = true;
        public bool m_RunFromBeginning = true;
        public bool m_RunFromStartLink = false;
        public bool m_RunSpecific = false;
        public bool m_DurationFull = true;
        public bool m_DurationSpecific = false;

        /*
         
            This command will search the data for a valid youtube link.
            If not found, the command will not run unless you have specified a valid backup link.
         
            * Use component settings
           
            OR

            * Use
            
            Find link in: 
            [value|data-var|event-var]

            (x) The video starts at the beginning
            (x) The video starts at [value|data-var|event-var] seconds.
            (x) If the link contains a start-time, use this time.
            
            (x) The video runs to the end.
            (x) The video runs for [value|data-var|event-var] seconds.

            
            Ok, men säg att man har ett bits event.
            Och om det innehåller en youtube länk.
            Då vill man köra en video.
            Men längden på videon ska baseras på antalet bits.

            Hur gör vi då?
            

            Kanske kan vi ha "global" event variablar, fast ändå specifikt för ett event som man kan fylla i manuellt.

            CUSTOM_DATA_1, CUSTOM_DATA_2, CUSTOM_DATA_3 ...

            Som man sen kan använda och sätta i varje view. Så man kan själv ALLTID sätta custom data och skicka det vidare.
            
            Detta skulle passa i min "toolbox" 

            Dvs: Ett bits event, som bara triggas med > 1000 körs igång. Här har vi konfat en CUSTOM_DATA_1 och skrivit "20" som värde.
            I kommandot kan vi sen använda denna variabel i tex "duration time" för youtube videon...

            Fast förvisso... om man har ett event per "nivå" eller "bits nivå" så har man ju ett kommando för varje...
            Då kan man sätta tiden statiskt där ju, det får ju samma effekt.
             
            Så det behövs ej nu. Men, det är ändå en bra sak att ha i framtiden, garanterat.
            Speciellt om man kunde kombinera dem, typ att med "if-satsen" kunna sätta värden i CUSTOM_DATA_X baserat på tex bits värdet.
            Kanske också kunna utföra matematiska saker? med if-satsen då.
            if(bits > 1000)
            CUSTOM_DATA_1 = [value] > ADD > [MATH_OPERATOR] [value] (obviously only works with numerical values)
            
            i detta exempel flyttar vi då egentligen logiken (med lite extra krydda) till if-statement ISTÄLLET för att skapa flera bits-event.
        */

        public override CommandData Clone()
        {
            YoutubeCommandData c = base.Clone<YoutubeCommandData>();
            c.m_Data = m_Data;
            c.m_BackupData = m_BackupData;
            c.m_UseBackup = m_UseBackup;
            c.m_UseComponentSettings = m_UseComponentSettings;
            c.m_StartData = m_StartData;
            c.m_DurationData = m_DurationData;
            c.m_RunFromBeginning = m_RunFromBeginning;
            c.m_RunFromStartLink = m_RunFromStartLink;
            c.m_RunSpecific = m_RunSpecific;
            c.m_DurationFull = m_DurationFull;
            c.m_DurationSpecific = m_DurationSpecific;
            return c;
        }
    }
}
