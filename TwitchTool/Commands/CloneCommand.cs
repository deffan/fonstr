﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class CloneCommand : Command
    {
        public CloneCommand()
        {
            Data = new CloneCommandData();
            Data.Type = COMMAND_TYPE.CLONE;
        }

        public CloneCommand(CloneCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.CLONE;
        }

        public override Command Clone(Event e)
        {
            CloneCommand t = new CloneCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
