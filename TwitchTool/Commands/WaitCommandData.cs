﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class WaitCommandData : CommandData
    {
        public TextData m_TextData = new TextData("");

        public override CommandData Clone()
        {
            WaitCommandData c = base.Clone<WaitCommandData>();
            c.m_TextData = m_TextData;
            return c;
        }
    }
}
