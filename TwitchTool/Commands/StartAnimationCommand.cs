﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class StartAnimationCommand : Command
    {
        public StartAnimationCommand()
        {
            Data = new StartAnimationCommandData();
            Data.Type = COMMAND_TYPE.START_ANIMATION;
        }

        public StartAnimationCommand(StartAnimationCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.START_ANIMATION;
        }

        public override Command Clone(Event e)
        {
            StartAnimationCommand t = new StartAnimationCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
