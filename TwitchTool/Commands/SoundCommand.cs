﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Commands
{
    public class SoundCommand : Command
    {
        public SoundCommand()
        {
            Data = new SoundCommandData();
            Data.Type = COMMAND_TYPE.SOUND;
        }

        public SoundCommand(SoundCommandData data)
        {
            Data = data;
            Data.Type = COMMAND_TYPE.SOUND;
        }

        public override Command Clone(Event e)
        {
            SoundCommand t = new SoundCommand();
            t.Data = Data.Clone();
            t.Event = e;
            return t;
        }
    }
}
