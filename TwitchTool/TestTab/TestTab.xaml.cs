﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.Source;

namespace TwitchTool.TestTab
{
    /// <summary>
    /// Interaction logic for TestTab.xaml
    /// </summary>
    public partial class TestTab : UserControl
    {
        public TestTab()
        {
            InitializeComponent();
        }

        public void Load()
        {
            if (!MainWindow.Live)
            {
                TestErrorPanel.Visibility = Visibility.Visible;
                TestPanel.Visibility = Visibility.Hidden;
                return;
            }
            TestErrorPanel.Visibility = Visibility.Collapsed;
            TestPanel.Visibility = Visibility.Visible;

            // (Re)Build the treeview
            EventTreeView.Items.Clear();

            for (int i = 0; i < MainWindow.Windows.Count; i++)
            {
                TreeViewItem windowItem = GetTreeViewItem(null, "window.png", MainWindow.Windows.ElementAt(i).Value.WindowObject.ObjectName, FontWeights.Bold);
                for (int n = 0; n < MainWindow.Windows.ElementAt(i).Value.Events.Count; n++)
                {
                    windowItem.Items.Add(GetTreeViewItem(MainWindow.Windows.ElementAt(i).Value.Events[n], "event.png", MainWindow.Windows.ElementAt(i).Value.Events[n].Data.Name, FontWeights.Normal));
                }
                EventTreeView.Items.Add(windowItem);
                windowItem.IsExpanded = true;
            }
        }

        public void Test(TestEventCtrl e)
        {
            switch (e.m_Event.Type)
            {
                case EVENT_TYPE.KEYBOARD:
                {
                    e.m_Event.Ready<KeyboardEvent>(null).Start();
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_JOIN:
                {
                    e.m_Event.Ready<TwitchJoinEvent>(e.GetText("TWITCH_USERNAME")).Start();
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_PART:
                {
                    e.m_Event.Ready<TwitchPartEvent>(e.GetText("TWITCH_USERNAME")).Start();
                    break;
                }
                case EVENT_TYPE.TWITCH_SUBSCRIPTION:
                {
                    e.m_Event.Ready<TwitchSubscriptionEvent>(new TwitchSubscriptionReturnData(
                        e.GetText("TWITCH_USERNAME"), 
                        e.GetText("TWITCH_SUBSCRIPTION_MONTHS"),
                        e.GetText("TWITCH_SUBSCRIPTION_PLAN"),
                        e.GetText("TWITCH_SUBSCRIPTION_TYPE"),
                        e.GetText("TWITCH_MESSAGE"))).Start();
                    break;
                }
                case EVENT_TYPE.TWITCH_FOLLOW:
                {
                    e.m_Event.Ready<TwitchFollowEvent>(e.GetText("TWITCH_USERNAME")).Start();
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_CUSTOM:
                {
                    e.m_Event.Ready<TwitchCustomEvent>(new TwitchCustomReturnData(
                        e.GetText("TWITCH_MESSAGE"),
                        e.GetText("TWITCH_BOT_CUSTOM_TEXT1"),
                        e.GetText("TWITCH_BOT_CUSTOM_TEXT2"),
                        e.GetText("TWITCH_BOT_CUSTOM_TEXT3"), 
                        e.GetText("TWITCH_USERTYPE"), 
                        e.GetText("TWITCH_USERNAME"))).Start();
                    break;
                }
                case EVENT_TYPE.TWITCH_GIFT:
                    {
                        e.m_Event.Ready<TwitchGiftEvent>(new TwitchGiftReturnData(
                            e.GetText("TWITCH_USERNAME"),
                            e.GetText("TWITCH_GIFT_RECIPIENT"))).Start();
                        break;
                    }
                case EVENT_TYPE.TWITCH_RAID:
                    {
                        e.m_Event.Ready<TwitchRaidEvent>(new TwitchRaidReturnData(
                            e.GetText("TWITCH_USERNAME"),
                            e.GetText("TWITCH_RAID_RAIDERS"))).Start();
                        break;
                    }
                case EVENT_TYPE.TWITCH_BITS:
                {
                    e.m_Event.Ready<TwitchBitsEvent>(new TwitchBitsReturnData(
                        e.GetText("TWITCH_USERNAME"),
                        e.GetText("TWITCH_BITS"),
                        e.GetText("TWITCH_MESSAGE"))).Start();
                    break;
                }
                default: throw new Exception("Unknown EVENT_TYPE in TestTab.");
            }


        }

        private TreeViewItem GetTreeViewItem(Event e, string icon, string labelText, FontWeight fontStyle)
        {
            TreeViewItem item = new TreeViewItem();
            item.Tag = new TestEventCtrl(e, this);

            // Add a stack panel to contain [Image][TextLabel]
            StackPanel stack = new StackPanel();
            stack.Orientation = Orientation.Horizontal;

            // Image
            Image image = new Image();
            image.Source = GlobalHelper.GetImage(icon);
            image.Width = 16;

            // Label
            Label lbl = new Label();
            lbl.Content = labelText;
            lbl.FontWeight = fontStyle;

            // Add them, and to stackpanel
            stack.Children.Add(image);
            stack.Children.Add(lbl);
            item.Header = stack;

            return item;
        }

        private void EventTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (EventTreeView.SelectedItem == null)
                return;

            ViewPanel.Children.Clear();

            TestEventCtrl ev = (EventTreeView.SelectedItem as TreeViewItem).Tag as TestEventCtrl;
            if (ev.m_Event == null) // Window is selected
            {
                ViewPanel.Visibility = Visibility.Hidden;
                return;
            }
            ViewPanel.Visibility = Visibility.Visible;

            ViewPanel.Children.Add(ev);
        }
    }
}
