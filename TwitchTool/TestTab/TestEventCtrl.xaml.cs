﻿using System.Collections.Generic;
using System.Windows.Controls;
using TwitchTool.Source;

namespace TwitchTool.TestTab
{
    public partial class TestEventCtrl : UserControl
    {
        public Event m_Event;
        private TestTab m_Parent;
        private Dictionary<string, TestEventCtrlListItem> m_TestValues;

        public TestEventCtrl()
        {
            InitializeComponent();
        }

        public TestEventCtrl(Event e, TestTab parent)
        {
            InitializeComponent();
            if (e == null)
                return;

            m_TestValues = new Dictionary<string, TestEventCtrlListItem>();
            m_Event = e;
            m_Parent = parent;
            EventNameLabel.Content = EVENT_TYPE.GetEventName(m_Event.Type);

            List<EventVariable> vars = GlobalHelper.EventVariables.GetVariables(m_Event.Type);
            for(int i = 0; i < vars.Count; i++)
            {
                TestEventCtrlListItem item = new TestEventCtrlListItem(vars[i], i % 2 == 0);
                TheList.Children.Add(item);
                m_TestValues.Add(vars[i].Id, item);
            }
        }

        public string GetText(string variableId)
        {
            return m_TestValues[variableId].ValueTextBox.Text;
        }

        private void TestEventButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            m_Parent.Test(this);
        }
    }
}
