﻿
using System.Windows.Controls;
using System.Windows.Media;
using TwitchTool.Source;

namespace TwitchTool.TestTab
{
    public partial class TestEventCtrlListItem : UserControl
    {
        public EventVariable Variable;

        public TestEventCtrlListItem(EventVariable v, bool even)
        {
            InitializeComponent();
            Variable = v;
            EventVariableLabel.Content = Variable.DisplayName;
            ValidValuesLabel.Content = Variable.ValidValuesExample;

            if(even)
            {
                Background = Brushes.LightGray;
            }
        }
    }
}
