﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for GeneralAnimationSettings.xaml
    /// </summary>
    public partial class GeneralAnimationSettings : UserControl
    {
        public AnimationSettings m_Settings;
        private bool m_Toggle;

        public GeneralAnimationSettings()
        {
            InitializeComponent();
            m_Toggle = false;

            EasingCombo.Items.Add("None");
            EasingCombo.Items.Add("Ease in");
            EasingCombo.Items.Add("Ease out");
            EasingCombo.Items.Add("Both");
            
        }

        public void Init(AnimationSettings settings)
        {
            m_Settings = settings;
            EasingCombo.SelectedIndex = m_Settings.m_Easing;
        }

        public void Refresh()
        {
            m_Toggle = true;

            TimesTextBox.Text = Convert.ToString(m_Settings.m_RunTimes);

            if (m_Settings.m_Reverse)
            {
                CheckBoxReverse.IsChecked = true;
            }

            if (m_Settings.m_RunForever)
            {
                RadioRunTimes.IsChecked = false;
            }
            else
            {
                RadioRunTimes.IsChecked = true;
            }

            EasingCombo.SelectedIndex = m_Settings.m_Easing;

            m_Toggle = false;
        }

        private void RadioRunTimes_Checked(object sender, RoutedEventArgs e)
        {
            TimesTextBox.IsEnabled = true;
            m_Settings.m_RunForever = false;
        }

        private void RadioRunForever_Checked(object sender, RoutedEventArgs e)
        {
            TimesTextBox.IsEnabled = false;
            m_Settings.m_RunForever = true;
        }

        private void CheckBoxReverse_Checked(object sender, RoutedEventArgs e)
        {
            m_Settings.m_Reverse = true;
        }

        private void CheckBoxReverse_Unchecked(object sender, RoutedEventArgs e)
        {
            m_Settings.m_Reverse = false;
        }

        private void TimesTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_Toggle)
                return;

            try
            {
                if (TimesTextBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(TimesTextBox.Text);
                    if (value > 0)
                    {
                        m_Settings.m_RunTimes = value;
                    }
                }
            }
            catch (Exception) { }
        }

        private void EasingCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_Toggle)
                return;

            m_Settings.m_Easing = EasingCombo.SelectedIndex;
        }
    }
}
