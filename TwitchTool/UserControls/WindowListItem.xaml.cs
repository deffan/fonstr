﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for WindowListItem.xaml
    /// </summary>
    public partial class WindowListItem : UserControl
    {
        private bool m_RemoveDown;
        private bool m_AddDown;
        private bool m_CheckedToggle;
        private Action<WindowListItem, bool> m_ActiveCallback;
        private Action<WindowListItem> m_RemoveCallback;
        private Action<WindowListItem> m_AddCallback;
        private ListData m_Data;
        public TreeViewItem TreeViewItem;

        public WindowListItem()
        {
            InitializeComponent();
        }

        public WindowListItem(string itemText, string labelText, string imageSource, ListData data, Action<WindowListItem, bool> activeCallback, Action<WindowListItem> removeCallback, Action<WindowListItem> addCallback)
        {
            InitializeComponent();
            m_Data = data;
            ItemLabel.Content = labelText;
            SetChecked(data.Active);
            if(!data.Active)
            {
                InnerGrid.Opacity = 0.25;
            }
            m_CheckedToggle = false;
            m_RemoveDown = false;
            m_AddDown = false;

            if (data.Type == "event")
            {
                InnerGrid.Width = 280;
            }
            else if(data.Type == "command")
            {
                InnerGrid.Width = 260;
                AddPanel.Visibility = Visibility.Collapsed;
            }

            ItemText.Text = itemText;
            if(imageSource.Length > 0)
            {
                ItemImage.Source = new BitmapImage(new Uri(imageSource));
            }
            m_ActiveCallback = activeCallback;
            m_RemoveCallback = removeCallback;
            m_AddCallback = addCallback;
        }

        public ListData Data
        {
            get { return m_Data; }
        }

        public void SetChecked(bool isChecked)
        {
            m_CheckedToggle = true;
            ActivationCheckBox.IsChecked = isChecked;
            m_CheckedToggle = false;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            m_Data.Active = true;
            if (!m_CheckedToggle)
                m_ActiveCallback(this, true);

            InnerGrid.Opacity = 1.0;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            m_Data.Active = false;
            if (!m_CheckedToggle)
                m_ActiveCallback(this, false);

            InnerGrid.Opacity = 0.25;
        }

        private void RemovePanel_MouseEnter(object sender, MouseEventArgs e)
        {
            if (ActivationCheckBox.IsChecked == false)
                return;

            RemoveImage.Visibility = Visibility.Collapsed;
            RemoveImageOn.Visibility = Visibility.Visible;
        }

        private void RemovePanel_MouseLeave(object sender, MouseEventArgs e)
        {
            RemoveImage.Visibility = Visibility.Visible;
            RemoveImageOn.Visibility = Visibility.Collapsed;
        }

        private void RemovePanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (m_RemoveDown)
            {
                m_RemoveCallback(this);
            }
            m_RemoveDown = false;
        }

        private void RemovePanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            m_RemoveDown = true;
        }

        private void AddPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            if (ActivationCheckBox.IsChecked == false)
                return;

            AddImage.Visibility = Visibility.Collapsed;
            AddImageOn.Visibility = Visibility.Visible;
        }

        private void AddPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            AddImage.Visibility = Visibility.Visible;
            AddImageOn.Visibility = Visibility.Collapsed;
        }

        private void AddPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (m_AddDown)
            {
                m_AddCallback(this);
            }
            m_AddDown = false;
        }

        private void AddPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
            m_AddDown = true;
        }
    }
}


