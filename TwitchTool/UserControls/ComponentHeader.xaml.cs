﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ComponentHeader.xaml
    /// </summary>
    public partial class ComponentHeader : UserControl
    {
        private IComponent m_Parent;
        public Color m_OriginalColor;
        private string m_OnExitQuestion;

        public ComponentHeader()
        {
            InitializeComponent();
            m_OriginalColor = (ComponentHeaderBackground.Background as SolidColorBrush).Color;  // Used for animation in CheckBoxListItem
            m_OnExitQuestion = "";
        }

        public void Init(IComponent parent, string name, bool removable)
        {
            m_Parent = parent;
            label.Content = name;
            CloseCross.IsEnabled = removable;
            if(!removable)
            {
                CloseCross.Visibility = Visibility.Collapsed;
            }
        }

        public void SetOnRemoveQuestion(string text)
        {
            m_OnExitQuestion = text;
        }

        private void CloseCross_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!CloseCross.IsEnabled)
                return;

            bool remove = true;
            if (m_OnExitQuestion.Length > 0)
            {
                remove = MessageBox.Show(m_OnExitQuestion, "Remove component", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes;
            }

            if (remove)
            {
                m_Parent.RemoveComponent();
            }
        }
    }
}
