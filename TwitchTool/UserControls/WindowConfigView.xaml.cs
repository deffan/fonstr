﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Components;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for WindowConfigView.xaml
    /// </summary>
    public partial class WindowConfigView : UserControl
    {
        private string m_Path;
        private string m_WinId;

        public WindowConfigView()
        {
            InitializeComponent();
        }

        public void Init(string id, string windowPath)
        {
            m_Path = windowPath;
            m_WinId = id;
            Load();
        }

        private void Load()
        {
            // listeners
            ListenersList.Items.Clear();
            List<CommandListenerComponent> list = MainWindow.Windows[m_WinId].GetListeners();
            for (int i = 0; i < list.Count; i++)
            {
                ListenersList.Items.Add(list[i].ListenerID);
            }

            // name
            NameTextBox.Text = MainWindow.Windows[m_WinId].WindowObject.GetBasicComponent().GetObjectName();

            // last modified
            string lastModified = MainWindow.Windows[m_WinId].GetLastModified();
            if (lastModified.Length > 0)
            {
                ModifiedLabel.Content = lastModified;
            }

            // Size
            Size s = MainWindow.Windows[m_WinId].WindowObject.GetTransformComponent().GetSize();
            SizeLabel.Content = Convert.ToInt32(s.Width) + "x" + Convert.ToInt32(s.Height);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WindowBuilder b = new WindowBuilder(m_Path);
            b.ShowDialog();

            // Reload the window object after editor has been closed
            MainWindow.Windows[m_WinId].Reload();
            Load();
        }

    }
}
