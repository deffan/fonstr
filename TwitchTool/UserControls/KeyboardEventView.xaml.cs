﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for KeyboardEventView.xaml
    /// </summary>
    public partial class KeyboardEventView : UserControl
    {
        private List<Key> m_Keys;
        private WindowListItem m_CurrentListItem;
        private KeyboardEventData m_CurrentData;
        private Action m_SaveCallback;

        public KeyboardEventView(Action saveCallback)
        {
            InitializeComponent();
            m_Keys = new List<Key>();
            m_SaveCallback = saveCallback;
        }

        public void Init(WindowListItem listItem)
        {
            // Set new data
            m_CurrentListItem = listItem;
            m_CurrentData = JsonConvert.DeserializeObject<KeyboardEventData>(listItem.Data.Data);
            if (m_CurrentData == null)
            {
                m_CurrentData = new KeyboardEventData();
            }

            NameTextBox.Text = m_CurrentData.Name;

            // Clear all current data
            KeyTextBox.Text = "";
            KeyTextBox.Focus();
            m_Keys.Clear();

            // Refill key list and textbox
            for(int i = 0; i < m_CurrentData.Keys.Count; i++)
            {
                Key key;
                if(Enum.TryParse(m_CurrentData.Keys[i], out key))
                {
                    if (m_Keys.Count > 0)
                    {
                        KeyTextBox.Text += " + ";
                    }
                    KeyTextBox.Text += m_CurrentData.Keys[i];
                    m_Keys.Add(key);
                }
            }
        }

        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (StartKeyConfigButton.IsEnabled)
                return;

            if(!m_Keys.Contains(e.Key))
            {
                if(m_Keys.Count > 0)
                {
                    KeyTextBox.Text += " + ";
                }
                KeyTextBox.Text += Enum.GetName(typeof(Key), e.Key);

                m_Keys.Add(e.Key);
            }
            e.Handled = true;
        }

        private void Grid_KeyUp(object sender, KeyEventArgs e)
        {
            if (StartKeyConfigButton.IsEnabled)
                return;

            e.Handled = true;
            StartKeyConfigButton.IsEnabled = true;
        }

        private void StartKeyConfigButton_Click(object sender, RoutedEventArgs e)
        {
            m_Keys.Clear();

            StartKeyConfigButton.IsEnabled = false;
            KeyTextBox.Text = "";
            KeyTextBox.Focus();
            e.Handled = true;
        }

        private void SaveConfigButton_Click(object sender, RoutedEventArgs e)
        {
            m_CurrentData.Keys.Clear();

            // Save Name
            if (NameTextBox.Text.Length > 0)
            {
                m_CurrentData.Name = NameTextBox.Text;
                m_CurrentListItem.Data.Name = NameTextBox.Text;
                m_CurrentListItem.ItemText.Text = m_CurrentData.Name;
            }

            // Save Keys 
            List<string> keys = new List<string>();
            for (int i = 0; i < m_Keys.Count; i++)
            {
                keys.Add(Enum.GetName(typeof(Key), m_Keys[i]));
            }
            m_CurrentData.Keys.AddRange(keys);

            // Serialize
            m_CurrentListItem.Data.Data = JsonConvert.SerializeObject(m_CurrentData);

            // Save
            m_SaveCallback();
        }

    }
}
