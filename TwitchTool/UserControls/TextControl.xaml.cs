﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for TextControl.xaml
    /// </summary>
    public partial class TextControl : UserControl
    {
        private ObjectControl m_Parent;

        public TextControl(ObjectControl parent)
        {
            InitializeComponent();
            m_Parent = parent;
        }

        public void SetText(string txt)
        {
            TheText.Text = txt;
        }

        public void SetBold(bool bold)
        {
            if(bold)
            {
                TheText.FontWeight = FontWeights.Bold;
            }
            else
            {
                TheText.FontWeight = FontWeights.Normal;
            }
        }

        public void SetItalic(bool italic)
        {
            if (italic)
            {
                TheText.FontStyle = FontStyles.Italic;
            }
            else
            {
                TheText.FontStyle = FontStyles.Normal;
            }
        }

        public void SetUnderline(bool underline)
        {
            if (underline)
            {
                TheText.TextDecorations = TextDecorations.Underline;
            }
            else
            {
                TheText.TextDecorations = null;
            }
        }

        public void SetFont(FontFamily font)
        {
            TheText.FontFamily = font;
        }

        public void SetTextSize(double size)
        {
            TheText.FontSize = size;
        }

        public void SetTextColor(SolidColorBrush color)
        {
            TheText.Foreground = color;
        }

        public void SetBackgroundColor(SolidColorBrush color)
        {
            TheText.Background = color;
        }
    }
}
