﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for BrushPickerControl.xaml
    /// </summary>
    public partial class BrushPickerControl : UserControl
    {
        public enum MODE { NONE, COLOR, GRADIENT, IMAGE };
        private MODE m_CurrentMode;
        private Action m_RefreshTextCallback;
        private string m_ImagePath;
        private Rect m_ViewPort;

        public BrushPickerControl()
        {
            InitializeComponent();

            m_ViewPort = new Rect(0, 0, 1, 1);

            BrushCombo.Items.Add("Color");
            BrushCombo.Items.Add("Gradient");
            BrushCombo.Items.Add("Image");
            BrushCombo.SelectedIndex = 0;

            AlignComboX.Items.Add("Left");
            AlignComboX.Items.Add("Center");
            AlignComboX.Items.Add("Right");
            AlignComboX.SelectedIndex = 0;

            AlignComboY.Items.Add("Top");
            AlignComboY.Items.Add("Center");
            AlignComboY.Items.Add("Bottom");
            AlignComboY.SelectedIndex = 0;

            StretchCombo.Items.Add("None");
            StretchCombo.Items.Add("Fill");
            StretchCombo.Items.Add("Uniform");
            StretchCombo.Items.Add("UniformToFill");
            StretchCombo.SelectedIndex = 0;

            TileCombo.Items.Add("None");
            TileCombo.Items.Add("Tile");
            TileCombo.Items.Add("Flip X");
            TileCombo.Items.Add("Flip Y");
            TileCombo.Items.Add("Flip XY");
            TileCombo.SelectedIndex = 0;

            m_ImagePath = "";

            SetMode(MODE.COLOR);
            SetSolidColor(Brushes.Transparent);
        }

        public void SetCallback(Action callback)
        {
            m_RefreshTextCallback = callback;
        }

        public string GetImagePath()
        {
            return m_ImagePath;
        }

        public void SetImagePath(string path)
        {
            m_ImagePath = path;
        }

        public MODE GetMode()
        {
            return m_CurrentMode;
        }

        public Brush GetBrush()
        {
            switch (m_CurrentMode)
            {
                case MODE.COLOR:
                {
                    return GetColorBrush();
                }
                case MODE.GRADIENT:
                {
                    return GetGradientBrush();
                }
                case MODE.IMAGE:
                {
                    return GetImageBrush();
                }
            }
            return null;
        }

        public void SetMode(MODE mode)
        {
            m_CurrentMode = mode;
            switch (mode)
            {
                case MODE.NONE:
                {
                    ColorBar.Visibility = Visibility.Collapsed;
                    GradientBar.Visibility = Visibility.Collapsed;
                    ImageBar.Visibility = Visibility.Collapsed;
                    break;
                }
                case MODE.COLOR:
                {
                    ColorBar.Visibility = Visibility.Visible;
                    GradientBar.Visibility = Visibility.Collapsed;
                    ImageBar.Visibility = Visibility.Collapsed;
                    break;
                }
                case MODE.GRADIENT:
                {
                    ColorBar.Visibility = Visibility.Collapsed;
                    GradientBar.Visibility = Visibility.Visible;
                    ImageBar.Visibility = Visibility.Collapsed;
                    break;
                }
                case MODE.IMAGE:
                {
                    ColorBar.Visibility = Visibility.Collapsed;
                    GradientBar.Visibility = Visibility.Collapsed;
                    ImageBar.Visibility = Visibility.Visible;
                    break;
                }
            }
        }

        public XElement Serialize()
        {
            XElement brushPickerNode = new XElement("BrushPicker");

            brushPickerNode.Add(new XElement("Mode", (int)m_CurrentMode));
            switch (m_CurrentMode)
            {
                case MODE.COLOR:
                {
                    brushPickerNode.Add(new XElement("Color", GlobalHelper.GetColorString(GetColorBrush())));
                    break;
                }
                case MODE.GRADIENT:
                {
                    brushPickerNode.Add(new XElement("ColorFrom", GlobalHelper.GetColorString(new SolidColorBrush(ClrPcker_GradientColorFrom.SelectedColor.Value))));
                    brushPickerNode.Add(new XElement("ColorTo", GlobalHelper.GetColorString(new SolidColorBrush(ClrPcker_GradientColorTo.SelectedColor.Value))));
                    brushPickerNode.Add(new XElement("Angle", Convert.ToDouble(Angle.Value, CultureInfo.InvariantCulture)));
                    break;
                }
                case MODE.IMAGE:
                {
                    brushPickerNode.Add(new XElement("Image", m_ImagePath));
                    brushPickerNode.Add(new XElement("AlignX", AlignComboX.SelectedIndex));
                    brushPickerNode.Add(new XElement("AlignY", AlignComboY.SelectedIndex));
                    brushPickerNode.Add(new XElement("Stretch", StretchCombo.SelectedIndex));
                    brushPickerNode.Add(new XElement("Tile", TileCombo.SelectedIndex));
                    brushPickerNode.Add(new XElement("TileSizeX", m_ViewPort.Width));
                    brushPickerNode.Add(new XElement("TileSizeY", m_ViewPort.Height));
                    break;
                }
            }

            return brushPickerNode;
        }

        public static BrushPickerControl Deserialize(XElement node)
        {
            BrushPickerControl b = new BrushPickerControl();

            // Mode
            b.SetMode((MODE)Convert.ToInt32(node.Element("Mode").Value));

            switch (b.GetMode())
            {
                case MODE.COLOR:
                {
                    b.ClrPcker_SolidColor.SelectedColor = GlobalHelper.GetColorFromString(node.Element("Color").Value).Color;
                    break;
                }
                case MODE.GRADIENT:
                {
                    b.ClrPcker_GradientColorFrom.SelectedColor = GlobalHelper.GetColorFromString(node.Element("ColorFrom").Value).Color;
                    b.ClrPcker_GradientColorTo.SelectedColor = GlobalHelper.GetColorFromString(node.Element("ColorTo").Value).Color;
                    b.Angle.Value = Convert.ToDecimal(node.Element("Angle").Value);
                    break;
                }
                case MODE.IMAGE:
                {
                    b.m_ImagePath = node.Element("Image").Value;
                    b.AlignComboX.SelectedIndex = Convert.ToInt32(node.Element("AlignX").Value);
                    b.AlignComboY.SelectedIndex = Convert.ToInt32(node.Element("AlignY").Value);
                    b.StretchCombo.SelectedIndex = Convert.ToInt32(node.Element("Stretch").Value);
                    b.TileCombo.SelectedIndex = Convert.ToInt32(node.Element("Tile").Value);
                    b.TileSizeX.Value = Convert.ToDecimal(node.Element("TileSizeX").Value);
                    b.TileSizeY.Value = Convert.ToDecimal(node.Element("TileSizeY").Value);
                    break;
                }
            }

            return b;
        }

        public void SetSolidColor(SolidColorBrush b)
        {
            ClrPcker_SolidColor.SelectedColor = b.Color;
        }

        private SolidColorBrush GetColorBrush()
        {
            return new SolidColorBrush(ClrPcker_SolidColor.SelectedColor.Value);
        }

        private LinearGradientBrush GetGradientBrush()
        {
            try
            {
                return new LinearGradientBrush((Color)ClrPcker_GradientColorFrom.SelectedColor, (Color)ClrPcker_GradientColorTo.SelectedColor, Convert.ToDouble(Angle.Value, CultureInfo.InvariantCulture));
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "GetGradientBrush" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
            }
            return null;
        }

        private ImageBrush GetImageBrush()
        {
            ImageBrush img = new ImageBrush();
            try
            {
                img.ImageSource = new BitmapImage(new Uri(m_ImagePath));
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "GetImageBrush" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                return null;
            }

            switch (AlignComboX.SelectedIndex)
            {
                case 0: img.AlignmentX = AlignmentX.Left; break;
                case 1: img.AlignmentX = AlignmentX.Center; break;
                case 2: img.AlignmentX = AlignmentX.Right; break;
            }

            switch (AlignComboY.SelectedIndex)
            {
                case 0: img.AlignmentY = AlignmentY.Top; break;
                case 1: img.AlignmentY = AlignmentY.Center; break;
                case 2: img.AlignmentY = AlignmentY.Bottom; break;
            }

            switch (StretchCombo.SelectedIndex)
            {
                case 0: img.Stretch = Stretch.None; break;
                case 1: img.Stretch = Stretch.Fill; break;
                case 2: img.Stretch = Stretch.Uniform; break;
                case 3: img.Stretch = Stretch.UniformToFill; break;
            }

            switch (TileCombo.SelectedIndex)
            {
                case 0: img.TileMode = TileMode.None; break;
                case 1: img.TileMode = TileMode.Tile; break;
                case 2: img.TileMode = TileMode.FlipX; break;
                case 3: img.TileMode = TileMode.FlipY; break;
                case 4: img.TileMode = TileMode.FlipXY; break;
            }
            img.Viewport = m_ViewPort;
            return img;
        }

        private void ClrPcker_GradientColorFrom_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            if (ClrPcker_GradientColorFrom.SelectedColor != null)
            {
                m_RefreshTextCallback?.Invoke();
            }
        }

        private void ClrPcker_GradientColorTo_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            if (ClrPcker_GradientColorTo.SelectedColor != null)
            {
                m_RefreshTextCallback?.Invoke();
            }
        }

        private void ClrPcker_SolidColor_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            if (ClrPcker_SolidColor.SelectedColor != null)
            {
                m_RefreshTextCallback?.Invoke();
            }
        }

        private void Angle_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (Angle.Value != null)
            {
                m_RefreshTextCallback?.Invoke();
            }
        }

        private void BrushCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (BrushCombo.SelectedIndex)
            {
                case 0: SetMode(MODE.COLOR); break;
                case 1: SetMode(MODE.GRADIENT); break;
                case 2: SetMode(MODE.IMAGE); break;
            }
            m_RefreshTextCallback?.Invoke();
        }


        private void StretchCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            m_RefreshTextCallback?.Invoke();
        }

        private void TileCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            m_RefreshTextCallback?.Invoke();
        }

        private void AlignComboY_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            m_RefreshTextCallback?.Invoke();
        }

        private void AlignComboX_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            m_RefreshTextCallback?.Invoke();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select image file";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if (op.ShowDialog() == true)
            {
                m_ImagePath = op.FileName;
                ImagePathLabel.Content = GlobalHelper.GetFileNameFromPath(m_ImagePath);
                m_RefreshTextCallback?.Invoke();
            }
        }

        private void TileSizeX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                m_ViewPort.Width = Convert.ToDouble(TileSizeX.Value, CultureInfo.InvariantCulture);
                m_RefreshTextCallback?.Invoke();
            }
            catch (Exception) { }
        }

        private void TileSizeY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                m_ViewPort.Height = Convert.ToDouble(TileSizeY.Value, CultureInfo.InvariantCulture);
                m_RefreshTextCallback?.Invoke();
            }
            catch (Exception) { }
        }


    }
}
