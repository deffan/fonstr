﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for NameComponent.xaml
    /// </summary>
    public partial class NameComponent : UserControl
    {
        public ObjectControl m_Parent;

        public NameComponent(ObjectControl parent)
        {
            InitializeComponent();
            m_Parent = parent;
            ActiveCheckBox.IsEnabled = false;   // For now...
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (NameTextBox.Text.Length > 0)
                {
                    SetObjectName(NameTextBox.Text);
                }
            }
            catch (Exception) { }
        }

        private void NameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            // An empty name is unacceptable, so we set it to a default "New object" if empty
            if (NameTextBox.Text.Length == 0)
            {
                SetObjectName("New object");
            }
        }

        public void SetObjectName(string name)
        {
            m_Parent.ObjectName = name;
            StackPanel st = m_Parent.TreeViewItem.Header as StackPanel;
            Label lb = st.Children[1] as Label;
            lb.Content = name;
        }
    
    }
}
