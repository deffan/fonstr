﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for TransformComponent.xaml
    /// </summary>
    public partial class TransformComponent : UserControl, IComponent
    {
        public ObjectControl m_Parent;
        private TranslateTransform m_TranslateTransform;
        private Point m_Coordinates;
        private bool m_ToggleTextChangedEvent;
        private Size m_Size;

        public TransformComponent(ObjectControl p)
        {
            InitializeComponent();
            m_Parent = p;
            m_TranslateTransform = new TranslateTransform(0, 0);
            m_Coordinates = new Point();
            m_Size = new Size();
            m_Parent.RenderTransform = m_TranslateTransform;
            m_ToggleTextChangedEvent = false;
            ComponentHeader.Init(this, "Transform Component", false);
        }

        public void SetCoordinates(double x, double y)
        {
            m_TranslateTransform.X = (int)x;
            m_TranslateTransform.Y = (int)y;

            m_ToggleTextChangedEvent = true;
            XTextBox.Text = Convert.ToString((int)x);
            YTextBox.Text = Convert.ToString((int)y);
            m_ToggleTextChangedEvent = false;
        }

        public void SetSize(double x, double y)
        {
            XSizeTextBox.Text = Convert.ToString((int)x);
            YSizeTextBox.Text = Convert.ToString((int)y);
        }

        public Size GetSize()
        {
            m_Size.Width = Convert.ToInt32(XSizeTextBox.Text);
            m_Size.Height = Convert.ToInt32(YSizeTextBox.Text);
            return m_Size;
        }

        public Point GetCoordinates()
        {
            m_Coordinates.X = m_TranslateTransform.X;
            m_Coordinates.Y = m_TranslateTransform.Y;
            return m_Coordinates;
        }

        private void XTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_ToggleTextChangedEvent)
                return;

            try
            {
                if(XTextBox.Text.Length > 0)
                {
                    double pos = Convert.ToDouble(XTextBox.Text);
                    Point relativePoint = m_Parent.TransformToAncestor(m_Parent.ObjectParent).Transform(new Point(0, 0));
                    SetCoordinates(pos, m_TranslateTransform.Y);
                }
            }
            catch(Exception) { }
        }

        private void YTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_ToggleTextChangedEvent)
                return;

            try
            {
                if (YTextBox.Text.Length > 0)
                {
                    double pos = Convert.ToDouble(YTextBox.Text);
                    Point relativePoint = m_Parent.TransformToAncestor(m_Parent.ObjectParent).Transform(new Point(0, 0));
                    SetCoordinates(m_TranslateTransform.X, pos);
                }
            }
            catch (Exception) { }
        }

        private void XSizeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (XSizeTextBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(XSizeTextBox.Text);
                    if(value > -1)
                    {
                        m_Parent.MinWidth = value;
                        m_Parent.Width = value;
                        m_Parent.ObjectBorder.MinWidth = value;
                        List<IComponent> components = m_Parent.GetComponents();
                        for (int i = 0; i < components.Count; i++)
                        {
                            components[i].SetComponentSize(value, m_Parent.MinHeight);
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        private void YSizeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (YSizeTextBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(YSizeTextBox.Text);
                    if (value > -1)
                    {
                        m_Parent.MinHeight = value;
                        m_Parent.Height = value;
                        m_Parent.ObjectBorder.MinHeight = value;
                        List<IComponent> components = m_Parent.GetComponents();
                        for(int i = 0; i < components.Count; i++)
                        {
                            components[i].SetComponentSize(m_Parent.MinWidth, value);
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        public void SetComponentSize(double x, double y)
        {

        }

        public ComponentSize GetComponentSize()
        {
            return new ComponentSize(Convert.ToInt32(XSizeTextBox.Text), Convert.ToInt32(YSizeTextBox.Text));
        }

        public void RemoveComponent()
        {

        }

        XElement IXML.Serialize()
        {
            XElement transformComponentNode = new XElement("TransformComponent");

            // Width 
            XElement w = new XElement("Width", XSizeTextBox.Text);
            transformComponentNode.Add(w);

            // Height 
            XElement h = new XElement("Height", YSizeTextBox.Text);
            transformComponentNode.Add(h);

            return transformComponentNode;
        }

        public static TransformComponent Deserialize(XElement node, ObjectControl parent)
        {
            TransformComponent t = new TransformComponent(parent);

            t.XSizeTextBox.Text = node.Element("Width").Value;
            t.YSizeTextBox.Text = node.Element("Height").Value;

            return t;
        }
    }
}
