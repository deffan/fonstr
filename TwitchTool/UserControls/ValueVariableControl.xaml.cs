﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ValueVariableControl.xaml
    /// </summary>
    public partial class ValueVariableControl : UserControl
    {
        private bool m_Loaded;
        private TextData m_Data;
        private string m_CompareString;
        private bool m_NumericData;
        private string m_DataVarLatest;

        public ValueVariableControl()
        {
            InitializeComponent();
            m_Loaded = false;
            m_CompareString = "";
            m_NumericData = false;
            m_DataVarLatest = "";

            // Choice combo
            ChoiceCombo.Items.Add("Value");
            ChoiceCombo.Items.Add("Data Variable");
        }

        public void SetEventVariables(List<EventVariable> vars)
        {
            ChoiceCombo.Items.Add("Event Variable");

            for (int i = 0; i < vars.Count; i++)
            {
                EventVariableCombo.Items.Add(vars[i]);
            }
            EventVariableCombo.SelectedIndex = 0;
        }

        public void SetWidth(int width)
        {
            MainPanel.MinWidth = width;
            MainPanel.MaxWidth = width;
        }

        public bool HasChanges()
        {
            return !m_CompareString.Equals(m_Data.Type + m_Data.Id + m_Data.Data);
        }

        public TextData GetData()
        {
            return m_Data;
        }

        public void LoadDataVariables()
        {
            List<DataVariable> tmp = GlobalHelper.DataVariables.GetVariables();
            VariableCombo.Items.Clear();
            for (int i = 0; i < tmp.Count; i++)
            {
                VariableCombo.Items.Add(tmp[i]);
            }
            VariableCombo.SelectedIndex = 0;
            m_DataVarLatest = GlobalHelper.DataVariables.LatestSave;
        }

        public void Update(TextData data, bool numericData)
        {
            m_NumericData = numericData;
            m_Data = data;
            m_CompareString = "";

            // Load datavars
            m_Loaded = false;
            if (!GlobalHelper.DataVariables.LatestSave.Equals(m_DataVarLatest))
            {
                LoadDataVariables();
            }
            m_Loaded = true;

            // Select/Set the data
            bool found = false;
            if (m_Data.IsDataVariable)
            {
                for (int i = 0; i < VariableCombo.Items.Count; i++)
                {
                    if ((VariableCombo.Items[i] as DataVariable).Id.Equals(m_Data.Id))
                    {
                        VariableCombo.SelectedIndex = i;
                        ChoiceCombo.SelectedIndex = 1;
                        found = true;
                        break;
                    }
                }
            }
            else if (m_Data.IsEventVariable)
            {
                for (int i = 0; i < EventVariableCombo.Items.Count; i++)
                {
                    if ((EventVariableCombo.Items[i] as EventVariable).Id.Equals(m_Data.Id))
                    {
                        EventVariableCombo.SelectedIndex = i;
                        ChoiceCombo.SelectedIndex = 2;
                        found = true;
                        break;
                    }
                }
            }
            else 
            {
                if(m_NumericData)
                {
                    ValueText.Value = Convert.ToDecimal(m_Data.GetNumericData());
                }
                else
                {
                    StringText.Text = m_Data.Data;
                }
            }

            if (!found)
            {
                ChoiceCombo.SelectedIndex = 0;
            }

            m_CompareString = data.Type + data.Id + data.Data;
        }

        private void VariableCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!m_Loaded || VariableCombo.SelectedItem == null)
                return;

            m_Data.Type = "variable";
            m_Data.Id = (VariableCombo.SelectedItem as DataVariable).Id;
            m_Data.IsDataVariable = true;
            m_Data.IsEventVariable = false;
            m_Data.IsText = false;
            e.Handled = true;
        }

        private void EventVariableCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!m_Loaded)
                return;

            m_Data.Type = "event";
            m_Data.Id = (EventVariableCombo.SelectedItem as EventVariable).Id;
            m_Data.IsEventVariable = true;
            m_Data.IsDataVariable = false;
            m_Data.IsText = false;
            e.Handled = true;
        }

        private void ValueText_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!m_Loaded || !m_NumericData)
                return;

            m_Data.Type = "text";
            m_Data.Id = "";
            try
            {
                m_Data.Data = Convert.ToString(ValueText.Value);
            }
            catch (Exception)
            {
                m_Data.Data = "0";
            }
            m_Data.IsText = true;
            m_Data.IsDataVariable = false;
            m_Data.IsEventVariable = false;
            e.Handled = true;
        }

        private void StringText_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!m_Loaded || m_NumericData)
                return;

            m_Data.Type = "text";
            m_Data.Id = "";
            try
            {
                m_Data.Data = StringText.Text;
            }
            catch (Exception)
            {
                m_Data.Data = "";
            }
            m_Data.IsText = true;
            m_Data.IsDataVariable = false;
            m_Data.IsEventVariable = false;
            e.Handled = true;
        }

        private void ChoiceCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!m_Loaded)
                return;

            if (ChoiceCombo.SelectedIndex == 0)
            {
                if (m_NumericData)
                {
                    ValueText.Visibility = Visibility.Visible;
                }
                else
                {
                    StringText.Visibility = Visibility.Visible;
                }
                
                VariableCombo.Visibility = Visibility.Collapsed;
                EventVariableCombo.Visibility = Visibility.Collapsed;

                m_Data.Type = "text";
               
                try
                {
                    if (m_NumericData)
                    {
                        m_Data.Data = Convert.ToString(ValueText.Value);
                    }
                    else
                    {
                        m_Data.Data = StringText.Text;
                    }
                }
                catch(Exception)
                {
                    if (m_NumericData)
                        m_Data.Data = "0";
                    else
                        m_Data.Data = "";
                }

                m_Data.IsText = true;
                m_Data.IsDataVariable = false;
                m_Data.IsEventVariable = false;
            }
            else if (ChoiceCombo.SelectedIndex == 1)
            {
                VariableCombo.Visibility = Visibility.Visible;
                ValueText.Visibility = Visibility.Collapsed;
                StringText.Visibility = Visibility.Collapsed;
                EventVariableCombo.Visibility = Visibility.Collapsed;

                m_Data.Type = "variable";
                m_Data.Id = (VariableCombo.SelectedItem as DataVariable).Id;
                m_Data.IsText = false;
                m_Data.IsDataVariable = true;
                m_Data.IsEventVariable = false;

            }
            else if (ChoiceCombo.SelectedIndex == 2)
            {
                EventVariableCombo.Visibility = Visibility.Visible;
                VariableCombo.Visibility = Visibility.Collapsed;
                ValueText.Visibility = Visibility.Collapsed;
                StringText.Visibility = Visibility.Collapsed;

                m_Data.Type = "event";
                m_Data.Id = (EventVariableCombo.SelectedItem as EventVariable).Id;
                m_Data.IsEventVariable = true;
                m_Data.IsDataVariable = false;
                m_Data.IsText = false;
            }
            e.Handled = true;
        }

    }
}
