﻿using System;
using System.Windows;
using System.Windows.Controls;
using TwitchTool.Components;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    public partial class AddComponent : UserControl
    {
        public ObjectControl m_Object;

        public AddComponent()
        {
            InitializeComponent();

            AddMenuItem("Image", COMPONENTTYPE.Image);
            AddMenuItem("Text", COMPONENTTYPE.Text);
            AddMenuItem("TextToSpeech", COMPONENTTYPE.TextToSpeech);
            AddMenuItem("Sound", COMPONENTTYPE.Sound);
            AddMenuItem("Listener", COMPONENTTYPE.Listener);
            AddMenuItem("Animator", COMPONENTTYPE.Animator);
            AddMenuItem("Youtube", COMPONENTTYPE.Youtube);
            AddMenuItem("Cloning", COMPONENTTYPE.Cloning);
        }

        private void AddMenuItem(string name, int type)
        {
            MenuItem item = new MenuItem();
            item.Header = name;
            item.Click += AddNewComponent;
            item.Tag = type;
            AddComponentButtonMenu.Items.Add(item);
        }

        public void SetObject(ObjectControl o)
        {
            m_Object = o;
        }

        private void Refresh()
        {
            m_Object.TreeViewItem.IsSelected = false;
            m_Object.TreeViewItem.IsSelected = true;
        }

        private void AddComponentButton_Click(object sender, RoutedEventArgs e)
        {
            ContextMenu cm = AddComponentButton.ContextMenu;
            cm.PlacementTarget = sender as Button;
            cm.IsOpen = true;
        }

        private void AddNewComponent(object sender, RoutedEventArgs e)
        {
            IComponent c;
            switch (Convert.ToInt32((sender as MenuItem).Tag))
            {
                case COMPONENTTYPE.Text: c = new TextComponent(m_Object, true); break;
                case COMPONENTTYPE.Image: c = new ImageComponent(m_Object, true); break;
                case COMPONENTTYPE.Listener: c = new CommandListenerComponent(m_Object, true); GlobalHelper.EditorWindow.AddListener(c as CommandListenerComponent); break;
                case COMPONENTTYPE.Animator: c = new AnimatorComponent(m_Object, true); GlobalHelper.EditorWindow.AddAnimator(c as AnimatorComponent); break;
                case COMPONENTTYPE.TextToSpeech: c = new TextToSpeechComponent(m_Object, true); break;
                case COMPONENTTYPE.Youtube: c = new YoutubeComponent(m_Object, true); break;
                case COMPONENTTYPE.Sound: c = new SoundComponent(m_Object, true); break;
                case COMPONENTTYPE.Cloning: c = new CloningComponent(m_Object, true); break;
                default: throw new Exception("AddComponent, no matching component!");
            }
            c.SetID(Guid.NewGuid().ToString());
            c.NewComponentConstructor();
            m_Object.AddComponent(c);
            Refresh();
        }
    }
}
