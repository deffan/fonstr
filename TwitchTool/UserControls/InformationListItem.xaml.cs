﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for InformationListItem.xaml
    /// </summary>
    public partial class InformationListItem : UserControl
    {
        public InformationListItem()
        {
            InitializeComponent();
        }

        public InformationListItem(bool even, int type, string text)
        {
            InitializeComponent();
            InformationText.Text = text;
            if(even)
            {
                TheBack.Background = Brushes.Transparent;
            }

            if(type == MESSAGE_TYPE.INFORMATION)
            {
                InformationIcon.Source = GlobalHelper.GetImage("chat_bubble.png");
            }
            else if(type == MESSAGE_TYPE.WARNING)
            {
                InformationIcon.Source = GlobalHelper.GetImage("event.png");
            }
            else
            {
                InformationIcon.Source = GlobalHelper.GetImage("cancel_on.png");
            }
        }

        public void SetEven()
        {
            TheBack.Background = Brushes.Transparent;
        }
    }
}
