﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ImageControl.xaml
    /// </summary>
    public partial class ImageControl : UserControl
    {
        private ObjectControl m_Parent;

        public ImageControl(ObjectControl parent)
        {
            InitializeComponent();
            m_Parent = parent;
            
        }

        public void SetImage(BitmapImage img)
        {
            // Below is an ugly hack... Because each time you want to set another image, it will fail the first time...
            bool animatedSet = false;
            try
            {
                TheImage.Source = img;
                ImageBehavior.SetAnimatedSource(TheImage, TheImage.Source);
                animatedSet = true;
            }
            catch (Exception) { }

            if(!animatedSet)
            {
                TheImage.Source = img;
                ImageBehavior.SetAnimatedSource(TheImage, TheImage.Source);
            }
        }

        // https://github.com/XamlAnimatedGif/WpfAnimatedGif/wiki

        public void ScaleImage(double x, double y)
        {
            TheImage.Width = x;
            TheImage.Height = y;
        }

        private void TheImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if(e.NewSize.Width <= 1 && e.NewSize.Height <= 1)
            {
                //m_Parent.GetTransformComponent().SetSize(64, 64);
            }
            else
            {
                m_Parent.GetTransformComponent().SetSize(e.NewSize.Width, e.NewSize.Height);
            }
        }
    }
}

