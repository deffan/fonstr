﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Components;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for CheckBoxListItem.xaml
    /// </summary>
    public partial class CheckBoxListItem : UserControl
    {
        public CommandListenerComponent.ListeningComponent m_Component;
        private bool m_ToggleCheck;

        public CheckBoxListItem(CommandListenerComponent.ListeningComponent component)
        {
            InitializeComponent();
            m_Component = component;
            NameLabel.Content = m_Component.Component.GetType().Name;
            m_ToggleCheck = false;
            Refresh();
        }

        public void Refresh()
        {
            m_ToggleCheck = true;
            CheckBox.IsChecked = m_Component.Listening;
            m_ToggleCheck = false;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (m_ToggleCheck)
                return;

            ColorAnimation animation = new ColorAnimation();
            animation.From = m_Component.Component.GetEditorComponent().GetComponentHeader().m_OriginalColor;
            animation.To = Colors.Green;
            animation.Duration = new Duration(TimeSpan.FromSeconds(1));
            animation.AutoReverse = true;
            m_Component.Component.GetEditorComponent().GetComponentHeader().ComponentHeaderBackground.Background.BeginAnimation(SolidColorBrush.ColorProperty, animation);

            m_Component.Listening = true;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            m_Component.Listening = false;
        }

    }
}
