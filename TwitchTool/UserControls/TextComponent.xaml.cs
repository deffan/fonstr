﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for TextComponent.xaml
    /// </summary>
    public partial class TextComponent : UserControl, IComponent
    {
        private ObjectControl m_Parent;
        private TextControl m_TextControl;

        public TextComponent(ObjectControl parent)
        {
            InitializeComponent();
            m_Parent = parent;
            m_TextControl = new TextControl(parent);
            m_Parent.Children.Add(m_TextControl);
            ComponentHeader.Init(this, "Text Component", true);

            foreach (FontFamily F in Fonts.SystemFontFamilies)
            {
                fontComboBox.Items.Add(F);
            }
            fontComboBox.SelectedIndex = 0;
        }

        private void TheTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_TextControl.SetText(TheTextBox.Text);
        }

        private void checkBox_Bold_Checked(object sender, RoutedEventArgs e)
        {
            m_TextControl.SetBold(true);
        }

        private void checkBox_Bold_Unchecked(object sender, RoutedEventArgs e)
        {
            m_TextControl.SetBold(false);
        }

        private void checkBox_Italic_Checked(object sender, RoutedEventArgs e)
        {
            m_TextControl.SetItalic(true);
        }

        private void checkBox_Italic_Unchecked(object sender, RoutedEventArgs e)
        {
            m_TextControl.SetItalic(false);
        }

        private void checkBox_Underline_Checked(object sender, RoutedEventArgs e)
        {
            m_TextControl.SetUnderline(true);
        }

        private void checkBox_Underline_Unchecked(object sender, RoutedEventArgs e)
        {
            m_TextControl.SetUnderline(false);
        }

        private void fontComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (fontComboBox.SelectedIndex > 0)
                {
                    m_TextControl.SetFont(fontComboBox.Items[fontComboBox.SelectedIndex] as FontFamily);
                }
            }
            catch (Exception) { }
        }

        private void fontSizeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                double size = Convert.ToDouble(fontSizeTextBox.Text);
                if(size > 0)
                {
                    m_TextControl.SetTextSize(size);
                    m_Parent.ResizeIfNeeded();
                }
            }
            catch (Exception) { }
        }

        private void ClrPcker_TextColor_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            try
            {
                m_TextControl.SetTextColor(new SolidColorBrush(ClrPcker_TextColor.SelectedColor.Value));
            }
            catch (Exception) { }
        }

        private void ClrPcker_BackColor_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            try
            {
                m_TextControl.SetBackgroundColor(new SolidColorBrush(ClrPcker_BackColor.SelectedColor.Value));
            }
            catch (Exception){}
        }

        void IComponent.SetComponentSize(double x, double y)
        {

        }

        public ComponentSize GetComponentSize()
        {
            return new ComponentSize((int)m_TextControl.ActualWidth, (int)m_TextControl.ActualHeight);
        }

        public void RemoveComponent()
        {
            m_Parent.Children.Remove(m_TextControl);
            m_TextControl = null;
            m_Parent.ComponentRemoved(this);
            m_Parent = null;
        }

        XElement IXML.Serialize()
        {
            XElement textComponentNode = new XElement("TextComponent");

            // Text 
            XElement text = new XElement("Text", m_TextControl.TheText.Text);
            textComponentNode.Add(text);

            return textComponentNode;
        }

        public static TextComponent Deserialize(XElement node, ObjectControl parent)
        {
            TextComponent t = new TextComponent(parent);
            t.m_TextControl.SetText(node.Element("Text").Value);
            return t;
        }
    }
}
