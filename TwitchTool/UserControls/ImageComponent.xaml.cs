﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ImageComponent.xaml
    /// </summary>
    public partial class ImageComponent : UserControl, IComponent
    {
        private ObjectControl m_Parent;
        private ImageControl m_ImageControl;
        private bool m_IsURL;

        public ImageComponent(ObjectControl parent)
        {
            InitializeComponent();
            m_Parent = parent;
            m_ImageControl = new ImageControl(parent);
            m_Parent.Children.Add(m_ImageControl);
            Tag = true;
            m_IsURL = false;
            ComponentHeader.Init(this, "Image Component", true);
        }

        private void LoadImageFromFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select image file";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if (op.ShowDialog() == true)
            {
                try
                {
                    SetImage(op.FileName, false);
                }
                catch (Exception ex)
                {
                    Source.Logger.Log(ex.Message);
                }
            }
            m_IsURL = false;
        }

        private void LoadImageFromURLButton_Click(object sender, RoutedEventArgs e)
        {
            string url = "https://media.giphy.com/media/GNvWw0pDL6QRW/giphy.gif";

            // Här måste vi avgöra om det är en GIF eller inte. Titta på filänden?
            // Måste då använda ett annat element?

            try
            {
                SetImage(url, true);
            }
            catch (Exception ex)
            {
                Source.Logger.Log(ex.Message);
            }
            m_IsURL = true;
        }

        private void SetImage(string path, bool url)
        {
            if (path.Length == 0)
                return;

            try
            {
                if (url)
                    m_ImageControl.SetImage(new BitmapImage(new Uri(path, UriKind.Absolute)));
                else
                    m_ImageControl.SetImage(new BitmapImage(new Uri(path)));

                ImageLoadedTextBox.Text = path;
            }
            catch (Exception ex)
            {
                Logger.Log("ImageComponent.LoadImage: " + ex.Message);
            }
        }

        public void SetComponentSize(double x, double y)
        {
            m_ImageControl.ScaleImage(x, y);
        }

        public ComponentSize GetComponentSize()
        {
            return new ComponentSize((int)m_ImageControl.TheImage.ActualWidth, (int)m_ImageControl.TheImage.ActualHeight);
        }

        public void RemoveComponent()
        {
            m_Parent.Children.Remove(m_ImageControl);
            m_ImageControl = null;
            m_Parent.ComponentRemoved(this);
            m_Parent = null;
        }

        XElement IXML.Serialize()
        {
            XElement imageComponentNode = new XElement("ImageComponent");

            // URL or Filepath
            XElement isUrl = new XElement("IsUrl", m_IsURL);
            imageComponentNode.Add(isUrl);

            // Path 
            XElement path = new XElement("Path", ImageLoadedTextBox.Text);
            imageComponentNode.Add(path);

            return imageComponentNode;
        }

        public static ImageComponent Deserialize(XElement node, ObjectControl parent)
        {
            ImageComponent i = new ImageComponent(parent);
            i.m_IsURL = Convert.ToBoolean(node.Element("IsUrl").Value);
            i.SetImage(node.Element("Path").Value, i.m_IsURL);
            return i;
        }
    }
}
