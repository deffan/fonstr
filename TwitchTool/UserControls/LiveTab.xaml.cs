﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.LiveTab;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for LiveTab.xaml
    /// </summary>
    public partial class LiveTab : UserControl
    {
        public LiveTab()
        {
            InitializeComponent();
            StopButton.IsEnabled = false;
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.Live)
                return;

            MainWindow.StartAllWindows();
            StartButton.IsEnabled = false;
            StopButton.IsEnabled = true;
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Live)
                return;

            MainWindow.StopAllWindows();
            StartButton.IsEnabled = true;
            StopButton.IsEnabled = false;
        }

        public void Stop()
        {
            StartButton.IsEnabled = true;
            StopButton.IsEnabled = false;
        }

        public void EventStarted(Event e)
        {
            LivePanel.Children.Insert(0, new LiveEventPanel(e, Abort, Replay, Remove));
        }

        public void EventFinished(Event e)
        {
            for(int i = 0; i < LivePanel.Children.Count; i++)
            {
                if(e.EventId.Equals((LivePanel.Children[i] as LiveTabEvent).GetEvent().EventId))
                {
                    (LivePanel.Children[i] as LiveTabEvent).SetHistory();
                    break;
                }
            }
        }

        public void Remove(LiveTabEvent e)
        {
            for (int i = 0; i < LivePanel.Children.Count; i++)
            {
                if (e.GetEvent().EventId.Equals((LivePanel.Children[i] as LiveTabEvent).GetEvent().EventId))
                {
                    LivePanel.Children.RemoveAt(i);
                    break;
                }
            }
        }

        public void Replay(LiveTabEvent e)
        {
            if (!MainWindow.Live)
                return;

            Remove(e);
            e.GetEvent().Restart();
        }

        public void Abort(LiveTabEvent e)
        {
            e.GetEvent().Abort();
        }

    }
}
