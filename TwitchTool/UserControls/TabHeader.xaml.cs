﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animator;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for TabHeader.xaml
    /// </summary>
    public partial class TabHeader : UserControl
    {
        private string m_ObjectId;
        private int m_TabType;
        private TabItem m_Parent;
        private bool m_Down;

        public string ObjectId
        {
            get { return m_ObjectId; }
        }

        public TabItem TabParent
        {
            get { return m_Parent; }
            set { m_Parent = value; }
        }

        public int TabType
        {
            get { return m_TabType; }
        }

        public TabHeader()
        {
            InitializeComponent();
            m_Down = false;
        }

        public void CloseTab()
        {
            switch(m_TabType)
            {
                case EDITORTABTYPE.Animator:
                {
                    (m_Parent.Content as AnimatorWindow).Destroy();
                    m_Parent.Content = null;
                    break;
                }
                default: break;
            }
        }

        public TabHeader(string text, string image, string objectId, int tabType)
        {
            InitializeComponent();
            m_ObjectId = objectId;
            m_TabType = tabType;

            // Truncate and add "..." if larger than 25
            if (text.Length > 25)
            {
                string newText = text.Substring(0, 22);
                newText += "...";
                text = newText;
            }

            TheImage.Source = GlobalHelper.GetImage(image);
            TheText.Text = text;
        }

        private void BtnClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            m_Down = true;
        }

        private void BtnClose_MouseEnter(object sender, MouseEventArgs e)
        {
            BtnClose.Background = Brushes.DarkGray;
        }

        private void BtnClose_MouseLeave(object sender, MouseEventArgs e)
        {
            BtnClose.Background = Brushes.Transparent;
        }

        private void BtnClose_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(m_Down)
            {
                m_Down = false;
                GlobalHelper.EditorWindow.CloseTab(this);
            }

            // Vid ett senare tillfälle när vi behöver kunna stänga andra saker än just tabs...
            // flytta ut knappen till usercontrol.
            // som argument till konstruktorn ta en Action, som då körs vid click på knappen.
            // då kan man avsluta vad man vill hur man vill.
        }
    }
}
