﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for BasicComponent.xaml
    /// </summary>
    public partial class BasicComponent : UserControl, IComponent
    {
        public ObjectControl m_Parent;

        public BasicComponent(ObjectControl parent)
        {
            InitializeComponent();
            m_Parent = parent;
            ActiveCheckBox.IsEnabled = false;   // For now...
            ClrPcker_Background.SelectedColor = Colors.Transparent;
            ComponentHeader.Init(this, "Basic Component", false);
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (NameTextBox.Text.Length > 0)
                {
                    SetObjectName(NameTextBox.Text);
                }
            }
            catch (Exception) { }
        }

        private void NameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            // An empty name is unacceptable, so we set it to a default "New object" if empty
            if (NameTextBox.Text.Length == 0)
            {
                SetObjectName("New object");
            }
        }

        public void SetObjectName(string name)
        {
            m_Parent.ObjectName = name;
            StackPanel st = m_Parent.TreeViewItem.Header as StackPanel;
            Label lb = st.Children[1] as Label;
            lb.Content = name;
        }

        private void ClrPcker_Background_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            m_Parent.ChildCanvas.Background = new SolidColorBrush(ClrPcker_Background.SelectedColor.Value);
        }

        void IComponent.SetComponentSize(double x, double y)
        {

        }

        ComponentSize IComponent.GetComponentSize()
        {
            return null;
        }

        void IComponent.RemoveComponent()
        {

        }

        XElement IXML.Serialize()
        {
            XElement basicComponentNode = new XElement("BasicComponent");

            // Object name
            XElement name = new XElement("Name", m_Parent.ObjectName);
            basicComponentNode.Add(name);

            // Background color
            XElement background = new XElement("Background");
            if (m_Parent.ChildCanvas.Background == null)
            {
                background.Add("");
            }
            else
            {
                // Color in "R;G;B;A" string format
                string clr = Convert.ToString((m_Parent.ChildCanvas.Background as SolidColorBrush).Color.R) + ";";
                clr += Convert.ToString((m_Parent.ChildCanvas.Background as SolidColorBrush).Color.G) + ";";
                clr += Convert.ToString((m_Parent.ChildCanvas.Background as SolidColorBrush).Color.B) + ";";
                clr += Convert.ToString((m_Parent.ChildCanvas.Background as SolidColorBrush).Color.A);
                background.Add(clr);
            }
            basicComponentNode.Add(background);

            return basicComponentNode;
        }

        public static BasicComponent Deserialize(XElement node, ObjectControl parent)
        {
            BasicComponent b = new BasicComponent(parent);
            b.NameTextBox.Text = node.Element("Name").Value;
            return b;
        }
    }
}
