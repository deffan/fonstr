﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Commands;
using TwitchTool.Source;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for CommandControl.xaml
    /// </summary>
    public partial class TextCommandView : UserControl  // interface med Save, Load, ShouldSave för alla "views"
    {
        private List<string> m_DataList;
        private WindowListItem m_CurrentListItem;
        private TextCommandData m_CurrentData;
        private Action m_SaveCallback;

        public TextCommandView(Action saveCallback)
        {
            InitializeComponent();
            m_DataList = new List<string>();
            m_SaveCallback = saveCallback;
        }

        public void Init(WindowListItem listItem)
        {
            // Set new data
            m_CurrentListItem = listItem;
            m_CurrentData = JsonConvert.DeserializeObject<TextCommandData>(listItem.Data.Data);
            if(m_CurrentData == null)
            {
                m_CurrentData = new TextCommandData();
            }

            // Clear all current data
            m_DataList.Clear();
            DataText.Inlines.Clear();
            EventDataCombo.Items.Clear();
            VariableDataCombo.Items.Clear();
            ListenersList.Items.Clear();
            TextDataBox.Text = "";
            ListenersText.Text = "";

            // Get event fields for this event
            List<string> fields = GlobalHelper.DataFields.GetEventFields(Convert.ToInt32(listItem.Data.Parent.SubType));
            for (int i = 0; i < fields.Count; i++)
            {
                EventDataCombo.Items.Add(fields[i]);
            }
            EventDataCombo.SelectedIndex = 0;
            VariableDataCombo.SelectedIndex = 0;

            // Set data to controls
            if (m_CurrentData.Name.Length > 0)
            {
                NameTextBox.Text = m_CurrentData.Name;
            }
    
            Delay.Value = Convert.ToDecimal(m_CurrentData.Delay);

            for (int i = 0; i < m_CurrentData.TextDataList.Count; i++)
            {
                m_DataList.Add(m_CurrentData.TextDataList[i]);
                AddDataToTextBox(m_CurrentData.TextDataList[i]);
            }
            
            for (int i = 0; i < m_CurrentData.Listeners.Count; i++)
            {
                ListenersList.Items.Add(m_CurrentData.Listeners[i]);
            }

        }

        private void TextDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (TextDataBox.Text.Length == 0)
                return;

            m_DataList.Add(TextDataBox.Text);
            AddDataToTextBox(TextDataBox.Text);
        }

        private void AddDataToTextBox(string data)
        {
            Button b = new Button();
            b.Content = data;
            
            b.BorderBrush = null;
            b.Margin = new Thickness(0, 2, 0, 2);
            b.IsEnabled = false;
            DataText.Inlines.Add(new InlineUIContainer(b));
        }

        private void AddNewlineButton_Click(object sender, RoutedEventArgs e)
        {
            m_DataList.Add("" + '\n');
            DataText.Inlines.Add(new Run("\n"));
        }

        private void ClearDataButton_Click(object sender, RoutedEventArgs e)
        {
            m_DataList.Clear();
            DataText.Inlines.Clear();
        }


        private void ClearListenersButton_Click(object sender, RoutedEventArgs e)
        {
            ListenersList.Items.Clear();
        }

        private void EventDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (EventDataCombo.SelectedItem == null)
                return;

            m_DataList.Add(Convert.ToString(EventDataCombo.SelectedItem));
            AddDataToTextBox(Convert.ToString(EventDataCombo.SelectedItem));
        }

        private void VariableDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (VariableDataCombo.SelectedItem == null)
                return;

            m_DataList.Add(Convert.ToString(VariableDataCombo.SelectedItem));
            AddDataToTextBox(Convert.ToString(VariableDataCombo.SelectedItem));
        }

        private void ListenersComboButton_Click(object sender, RoutedEventArgs e)
        {
            if (ListenersCombo.SelectedItem == null)
                return;

            ListenersList.Items.Add(Convert.ToString(ListenersCombo.SelectedItem));
        }

        private void ListenersTextButton_Click(object sender, RoutedEventArgs e)
        {
            if (ListenersText.Text.Length == 0)
                return;

            ListenersList.Items.Add(ListenersText.Text);
        }

        private void SaveConfigButton_Click(object sender, RoutedEventArgs e)
        {
            m_CurrentData.Listeners.Clear();
            m_CurrentData.TextDataList.Clear();

            // Save Name
            if (NameTextBox.Text.Length > 0)
            {
                m_CurrentData.Name = NameTextBox.Text;
                m_CurrentListItem.Data.Name = NameTextBox.Text;
                m_CurrentListItem.ItemText.Text = m_CurrentData.Name;
            }

            // Save delay
            double delayValue = Convert.ToDouble(Delay.Value);
            m_CurrentData.Delay = (Math.Round(delayValue, 2));

            // Save m_DataList
            m_CurrentData.TextDataList.AddRange(m_DataList);

            // Save listeners 
            List<string> listeners = new List<string>();
            for(int i = 0; i < ListenersList.Items.Count; i++)
            {
                listeners.Add(ListenersList.Items[i] as string);
            }
            m_CurrentData.Listeners.AddRange(listeners);

            // Serialize
            m_CurrentListItem.Data.Data = JsonConvert.SerializeObject(m_CurrentData);

            // Save
            m_SaveCallback();
        }

    }
}
