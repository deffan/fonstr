﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for BaseWindow.xaml
    /// </summary>
    public partial class WindowComponent : UserControl, IComponent
    {
        public static readonly DependencyPropertyKey ChildrenProperty = DependencyProperty.RegisterReadOnly(
        nameof(Children),  
        typeof(UIElementCollection),
        typeof(WindowComponent),
        new PropertyMetadata());

        private ObjectControl m_Parent;
        public ObjectControl ObjectParent
        {
            get { return m_Parent; }
            set { m_Parent = value; }
        }

        public string WindowName;

        public UIElementCollection Children
        {
            get { return (UIElementCollection)GetValue(ChildrenProperty.DependencyProperty); }
            private set { SetValue(ChildrenProperty, value); }
        }

        public WindowComponent()
        {
            InitializeComponent();
            Children = ChildCanvas.Children;
        }

        public void SetComponentSize(double x, double y)
        {
 
        }

        public void RemoveComponent()
        {
            throw new NotImplementedException();
        }

        ComponentSize IComponent.GetComponentSize()
        {
            throw new NotImplementedException();
        }
    }
}
