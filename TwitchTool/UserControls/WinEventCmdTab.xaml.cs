﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for WinEventCmdTab.xaml
    /// </summary>
    public partial class WinEventCmdTab : UserControl
    {
        private WindowConfigView m_WindowConfigView;
        private KeyboardEventView m_KeyboardEventView;
        private TextCommandView m_TextCommandView;

        public class ListData
        {
            public string Id;
            public string Name;
            public bool Active;
            public string Type;
            public string SubType;
            public string Data;
            public ListData Parent; // Only used while in list
        }

        public WinEventCmdTab()
        {
            InitializeComponent();
            m_WindowConfigView = new WindowConfigView();
            m_KeyboardEventView = new KeyboardEventView(SaveXML);
            m_TextCommandView = new TextCommandView(SaveXML);
        }

        public void LoadXMLData()
        {
            for (int i = 0; i < MainWindow.Windows.Count; i++)
            {
                // Create the window data object and the listview item
                ListData windowData = new ListData();
                windowData.Id = MainWindow.Windows.ElementAt(i).Value.WindowObject.ID;
                windowData.Type = "window";
                windowData.Active = MainWindow.Windows.ElementAt(i).Value.WindowObject.GetBasicComponent().Active;
                windowData.Name = MainWindow.Windows.ElementAt(i).Value.WindowObject.ObjectName;

                WindowListItem window = new WindowListItem(
                        MainWindow.Windows.ElementAt(i).Value.WindowObject.ObjectName,
                        "Window",
                        "pack://application:,,,/TwitchTool;component/Images/window.png",
                        windowData,
                        ActiveCallback, 
                        RemoveCallback,
                        AddCallback);

                TreeViewItem windowParent = AddTreeViewItem(null, window);
                window.TreeViewItem = windowParent;

                XDocument doc = null;
                try
                {
                    doc = XDocument.Load(MainWindow.Windows.ElementAt(i).Value.Path + "\\EventCommand.xml");
                }
                catch (Exception ex)
                {
                    Logger.Log(MESSAGE_TYPE.ERROR, "LoadXMLData" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                    continue;
                }

                XElement events = doc.Element("Windows").Element("Window").Element("Events");
                foreach (XElement eventObj in events.Elements())
                {
                    // Create the event data object, then also get/set event-specific data as we GetListItem(data)
                    ListData eventData = new ListData();
                    eventData.Id = eventObj.Element("Id").Value;
                    eventData.Active = Convert.ToBoolean(eventObj.Element("Active").Value);
                    eventData.Type = "event";
                    eventData.SubType = eventObj.Element("Type").Value;
                    eventData.Name = GlobalHelper.Base64Decode(eventObj.Element("Name").Value);
                    eventData.Parent = windowData;
                    eventData.Data = GlobalHelper.Base64Decode(eventObj.Element("Data").Value);

                    WindowListItem eventListItem = GetListItem(eventData);
                    TreeViewItem eventParent = AddTreeViewItem(windowParent, eventListItem);
                    eventListItem.TreeViewItem = eventParent;

                    XElement commands = eventObj.Element("Commands");
                    foreach (XElement commandObj in commands.Elements())
                    {
                        // Create the command data object, then also get/set command-specific data as we GetListItem(data)
                        ListData commandData = new ListData();
                        commandData.Id = commandObj.Element("Id").Value;
                        commandData.Active = Convert.ToBoolean(commandObj.Element("Active").Value);
                        commandData.Type = "command";
                        commandData.SubType = commandObj.Element("Type").Value;
                        commandData.Name = GlobalHelper.Base64Decode(commandObj.Element("Name").Value);
                        commandData.Parent = eventData;
                        commandData.Data = GlobalHelper.Base64Decode(commandObj.Element("Data").Value);

                        AddTreeViewItem(eventParent, GetListItem(commandData));
                    }

                }

            }

        }

        private TreeViewItem AddTreeViewItem(TreeViewItem parent, object obj)
        {
            TreeViewItem item = new TreeViewItem();
            item.Header = obj;

            // Add to TreeView
            if (parent == null)
            {
                TheTreeView.Items.Add(item);
            }
            else
            {
                parent.Items.Add(item);
            }
            return item;
        }

        private WindowListItem GetListItem(ListData data)
        {
            WindowListItem item = null;

            switch (data.Type)
            {
                case "event":
                {
                    switch (Convert.ToInt32(data.SubType))
                    {
                        case EVENT_TYPE.KEYBOARD: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Keyboard event" : data.Name, "Keyboard event", "pack://application:,,,/TwitchTool;component/Images/Events/voice.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        case EVENT_TYPE.TWITCH_BITS: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Twitch Bits Event" : data.Name, "Twitch Donation Event", "pack://application:,,,/TwitchTool;component/Images/Events/twitch.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        case EVENT_TYPE.TWITCH_SUBSCRIPTION: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Twitch Subscription Event" : data.Name, "Twitch Subscription Event", "pack://application:,,,/TwitchTool;component/Images/Events/twitch.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        case EVENT_TYPE.TWITCH_FOLLOW: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Twitch Follow Event" : data.Name, "Twitch Follow Event", "pack://application:,,,/TwitchTool;component/Images/Events/twitch.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        case EVENT_TYPE.TWITCH_HOST: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Twitch Host Event" : data.Name, "Twitch Host Event", "pack://application:,,,/TwitchTool;component/Images/Events/twitch.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        default: throw new Exception(string.Format("MainWindow->GetListItem - Unknown EVENT_SUB_TYPE ({0})", data.SubType));
                    }
                    break;
                }
                case "command":
                {
                    switch (Convert.ToInt32(data.SubType))
                    {
                        case COMMAND_TYPE.TEXT: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Text Command" : data.Name, "Text Command", "", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        case COMMAND_TYPE.YOUTUBE: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Youtube Command" : data.Name, "Youtube Command", "pack://application:,,,/TwitchTool;component/Images/Events/youtube.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        case COMMAND_TYPE.IMAGE: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Image Command" : data.Name, "Image Command", "pack://application:,,,/TwitchTool;component/Images/image.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        case COMMAND_TYPE.START_ANIMATION: item = new WindowListItem(string.IsNullOrEmpty(data.Name) ? "Start Animation Command" : data.Name, "Start Animation Command", "pack://application:,,,/TwitchTool;component/Images/clapper.png", data, ActiveCallback, RemoveCallback, AddCallback); break;
                        default: throw new Exception(string.Format("MainWindow->GetListItem - Unknown COMMAND_SUB_TYPE ({0})", data.SubType));
                    }
                    break;
                }
                default: throw new Exception(string.Format("MainWindow->GetListItem - Unknown MAIN_TYPE ({0})", data.Type));
            }

            return item;
        }

        public void RemoveCallback(WindowListItem item)
        {

        }

        public void AddCallback(WindowListItem item)
        {
            if (item.Data.Type == "window")
            {
                // Open AddNewEvent dialog
            }
            else
            {
                // Open AddNewCommand dialog
            }
        }

        public void ActiveCallback(WindowListItem item, bool active)
        {
            if(item.Data.Type == "window" || item.Data.Type == "event")
            {
                if (item.TreeViewItem == null)
                    return;

                for(int i = 0; i < item.TreeViewItem.Items.Count; i++)
                {
                    WindowListItem eventItem = (item.TreeViewItem.Items[i] as TreeViewItem).Header as WindowListItem;
                    eventItem.SetChecked(active);

                    if (eventItem.TreeViewItem == null)
                        continue;

                    for (int n = 0; n < eventItem.TreeViewItem.Items.Count; n++)
                    {
                        WindowListItem commandItem = (eventItem.TreeViewItem.Items[n] as TreeViewItem).Header as WindowListItem;
                        commandItem.SetChecked(active);
                    }
                }

                item.TreeViewItem.IsExpanded = active;
            }

            SaveXML();
        }

        private void SaveXML()
        {
            XDocument doc = new XDocument();
            XElement windows = new XElement("Windows");
            doc.Add(windows);

            for(int i = 0; i < TheTreeView.Items.Count; i++)
            {
                WindowListItem windowListItem = (TheTreeView.Items[i] as TreeViewItem).Header as WindowListItem;

                XElement window = new XElement("Window");
                window.Add(new XElement("Id", windowListItem.Data.Id));
                window.Add(new XElement("Name", GlobalHelper.Base64Encode(windowListItem.Data.Name)));
                window.Add(new XElement("Active", windowListItem.Data.Active));

                XElement events = new XElement("Events");
                window.Add(events);
                windows.Add(window);

                if (windowListItem.TreeViewItem == null)
                    continue;

                for (int j = 0; j < windowListItem.TreeViewItem.Items.Count; j++)
                {
                    WindowListItem eventListItem = (windowListItem.TreeViewItem.Items[j] as TreeViewItem).Header as WindowListItem;

                    XElement eventNode = new XElement("Event");
                    eventNode.Add(new XElement("Id", eventListItem.Data.Id));
                    eventNode.Add(new XElement("Type", eventListItem.Data.SubType));
                    eventNode.Add(new XElement("Active", eventListItem.Data.Active));
                    eventNode.Add(new XElement("Name", GlobalHelper.Base64Encode(eventListItem.Data.Name)));
                    eventNode.Add(new XElement("Data", GlobalHelper.Base64Encode(eventListItem.Data.Data)));

                    XElement commands = new XElement("Commands");
                    eventNode.Add(commands);
                    events.Add(eventNode);

                    if (eventListItem.TreeViewItem == null)
                        continue;

                    for (int n = 0; n < eventListItem.TreeViewItem.Items.Count; n++)
                    {
                        WindowListItem cmdListItem = (eventListItem.TreeViewItem.Items[n] as TreeViewItem).Header as WindowListItem;

                        XElement commandNode = new XElement("Command");
                        commandNode.Add(new XElement("Id", cmdListItem.Data.Id));
                        commandNode.Add(new XElement("Type", cmdListItem.Data.SubType));
                        commandNode.Add(new XElement("Active", cmdListItem.Data.Active));
                        commandNode.Add(new XElement("Name", GlobalHelper.Base64Encode(cmdListItem.Data.Name)));
                        commandNode.Add(new XElement("Data", GlobalHelper.Base64Encode(cmdListItem.Data.Data)));
                        commands.Add(commandNode);
                    }
                }

                doc.Save(MainWindow.Windows[windowListItem.Data.Id].Path + "\\EventCommand.xml");
            }

        }

        private void AddNewWindowPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            AddImage.Visibility = Visibility.Collapsed;
            AddImageOn.Visibility = Visibility.Visible;
        }

        private void AddNewWindowPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            AddImage.Visibility = Visibility.Visible;
            AddImageOn.Visibility = Visibility.Collapsed;
        }

        private void AddNewWindowPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void AddNewWindowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void ImportWindowPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            ImportImage.Visibility = Visibility.Collapsed;
            ImportImageOn.Visibility = Visibility.Visible;
        }

        private void ImportWindowPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            ImportImage.Visibility = Visibility.Visible;
            ImportImageOn.Visibility = Visibility.Collapsed;
        }

        private void ImportWindowPanel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void ImportWindowPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void TheTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (TheTreeView.SelectedItem == null)
                return;

            WindowListItem item = (TheTreeView.SelectedItem as TreeViewItem).Header as WindowListItem;
            ConfigurationPanel.Children.Clear();

            if (item.Data.Type.Equals("window"))
            {
                m_WindowConfigView.Init(item.Data.Id, MainWindow.Windows[item.Data.Id].Path + "\\Window.xml");
                ConfigurationPanel.Children.Add(m_WindowConfigView);
            }
            else if(item.Data.Type.Equals("event"))
            {
                switch (Convert.ToInt32(item.Data.SubType))
                {
                    case EVENT_TYPE.KEYBOARD:
                    {
                        m_KeyboardEventView.Init(item);
                        ConfigurationPanel.Children.Add(m_KeyboardEventView);
                        break;
                    }
                    case EVENT_TYPE.TWITCH_BITS:
                    {

                        break;
                    }
                    case EVENT_TYPE.TWITCH_SUBSCRIPTION:
                    {

                        break;
                    }
                    case EVENT_TYPE.TWITCH_FOLLOW:
                    {

                        break;
                    }
                    case EVENT_TYPE.TWITCH_HOST:
                    {

                        break;
                    }
                }
            }
            else if(item.Data.Type.Equals("command"))
            {
                switch (Convert.ToInt32(item.Data.SubType))
                {
                    case COMMAND_TYPE.TEXT:
                    {
                        m_TextCommandView.Init(item);
                        ConfigurationPanel.Children.Add(m_TextCommandView);
                        break;
                    }
                    case COMMAND_TYPE.YOUTUBE:
                    {

                        break;
                    }
                    case COMMAND_TYPE.IMAGE:
                    {

                        break;
                    }
                    case COMMAND_TYPE.START_ANIMATION:
                    {

                        break;
                    }
                }
            }

        }
    }
}
