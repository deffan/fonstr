﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Source;
using TwitchTool.Components;
using TwitchTool.Animations;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ObjectControl.xaml
    /// </summary>
    public partial class ObjectControl : UserControl, IXML, System.ComponentModel.INotifyPropertyChanged
    {
        public static readonly DependencyPropertyKey ChildrenProperty = DependencyProperty.RegisterReadOnly(
        nameof(Children),
        typeof(UIElementCollection),
        typeof(ObjectControl),
        new PropertyMetadata());

        private string m_ObjectName;
        private ObjectControl m_Parent;
        private TreeViewItem m_TreeViewItem;
        private TransformComponent m_Transform;
        private BasicComponent m_Basic;
        private List<IComponent> m_Components;
        private Thickness m_NoThickness;
        private Thickness m_Thickness;
        private Thickness m_SelectionThickness;
        private bool m_Selectable;
        private bool m_Editor;
        private string m_ID;
        private string m_CloneID;

        public ObjectControl()
        {
            InitializeComponent();
            Children = ChildCanvas.Children;

            // All objects have a Basic and Transform component
            m_Components = new List<IComponent>();

            m_ObjectName = "New Object";
            m_NoThickness = new Thickness(0);
            m_Thickness = new Thickness(1);
            m_SelectionThickness = new Thickness(2);
            m_Selectable = true;
            m_CloneID = "";
        }

        // This is a continuation of the constructor, but only for "default objects" such as new objects in editor
        public void InitDefaultObject()
        {
            m_Basic = new BasicComponent(this, true);
            m_Basic.ID = Guid.NewGuid().ToString();
            m_Transform = new TransformComponent(this, true);
            m_Transform.ID = Guid.NewGuid().ToString();

            m_Components.Add(m_Basic);
            m_Components.Add(m_Transform);
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public string CloneID
        {
            get { return m_CloneID; }
            set { m_CloneID = value; }
        }

        public void SetEditorMode(bool editor)
        {
            m_Editor = editor;
        }

        public TreeViewItem TreeViewItem
        {
            get { return m_TreeViewItem; }
            set { m_TreeViewItem = value; }
        }

        public ObjectControl ObjectParent
        {
            get { return m_Parent; }
            set { m_Parent = value; }
        }

        public string ObjectName
        {
            get { return m_ObjectName; }
            set
            {
                if (value != null && value == m_ObjectName)
                    return;

                m_ObjectName = value;

                BasicComponent b = GetComponent<BasicComponent>() as BasicComponent;
                if(b != null)   // It will be null when we deserialize it
                {
                    b.SetObjectName(value);
                }
                
                OnPropertyChanged("ObjectName");
            }
        }

        // This sends the OnPropertyChanged, and can be bound to other places
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string info)
        {
            PropertyChanged?.Invoke(this, new System.ComponentModel.PropertyChangedEventArgs(info));
        }

        public UIElementCollection Children
        {
            get { return (UIElementCollection)GetValue(ChildrenProperty.DependencyProperty); }
            private set { SetValue(ChildrenProperty, value); }
        }

        // TransformComponent c = GetComponent<TransformComponent>() as TransformComponent;
        public UserControl GetComponent<T>()
        {
            for (int i = 0; i < m_Components.Count; i++)
            {
                if (m_Components[i].GetType().IsEquivalentTo(typeof(T)))
                {
                    return m_Components[i] as UserControl;
                }
            }
            return null;
        }

        public List<CommandListenerComponent> GetAllListeners()
        {
            List<CommandListenerComponent> retList = new List<CommandListenerComponent>();
            for (int i = 0; i < m_Components.Count; i++)
            {
                if (m_Components[i] is CommandListenerComponent)
                {
                    retList.Add(m_Components[i] as CommandListenerComponent);
                }
            }
            return retList;
        }

        public List<IComponent> GetComponents()
        {
            return m_Components;
        }

        public void AddComponent(IComponent component)
        {
            m_Components.Add(component);

            // Refresh listener
            if (!(component is CommandListenerComponent))
            {
                for (int i = 0; i < m_Components.Count; i++)
                {
                    if (m_Components[i] is CommandListenerComponent)
                    {
                        (m_Components[i] as CommandListenerComponent).AddComponentToListener(component);
                    }
                }
            }
        }

        public TransformComponent GetTransformComponent()
        {
            return m_Transform;
        }

        public BasicComponent GetBasicComponent()
        {
            return m_Basic;
        }

        public void SetTransformComponent(TransformComponent t)
        {
            m_Transform = t;
        }

        public void SetSelectable(bool selectable)
        {
            m_Selectable = selectable;
        }

        public void SelectObject(bool select)
        {
            if (!m_Selectable)
                return;

            SelectObject(this, select, 1);
            SelectChildren(this, select);
        }

        public void ForceSelectObject(double opacity)
        {
            SelectObject(this, true, opacity);
        }

        // Remove component from the list of the object, then re-select to show in the UI
        // Called from the component itself child->parent
        public void ComponentRemoved(IComponent component)
        {
            m_Components.Remove(component);

            // Refresh listener
            if(!(component is CommandListenerComponent))
            {
                for (int i = 0; i < m_Components.Count; i++)
                {
                    if (m_Components[i] is CommandListenerComponent)
                    {
                        (m_Components[i] as CommandListenerComponent).RemoveComponentFromListener(component);
                    }
                }
            }

            m_TreeViewItem.IsSelected = false;
            m_TreeViewItem.IsSelected = true;
        }

        public void VisuallySelectObject(bool select)
        {
            if(select)
            {
                SelectionBorder.BorderThickness = m_SelectionThickness;
            }
            else
            {
                SelectionBorder.BorderThickness = m_NoThickness;
            }
        }

        public void SelectObject(bool select, double opacity)
        {
            if (select)
            {
                ObjectBorder.BorderThickness = m_Thickness;
            }
            else
            {
                ObjectBorder.BorderThickness = m_NoThickness;
            }
            ObjectBorder.Opacity = opacity;
        }

        public void SelectOff()
        {
            ObjectBorder.BorderThickness = m_NoThickness;
        }

        public void SelectObject(ObjectControl c, bool select, double opacity)
        {
            if (select)
            {
                c.ObjectBorder.BorderThickness = m_Thickness;
            }
            else
            {
                c.ObjectBorder.BorderThickness = m_NoThickness;
            }
            c.ObjectBorder.Opacity = opacity;
        }

        public void Select(bool select)
        {
            if (!m_Selectable)
                return;

            if (select && GlobalHelper.EditorWindow.UseSelectedBorder)
            {
                ObjectBorder.BorderThickness = m_Thickness;
                ObjectBorder.BorderBrush = GlobalHelper.EditorWindow.GetSelectedBorderColor;
            }
            else
            {
                if(GlobalHelper.EditorWindow.UseObjectBorder)
                {
                    ObjectBorder.BorderThickness = m_Thickness;
                    ObjectBorder.BorderBrush = GlobalHelper.EditorWindow.GetObjectBorderColor;
                }
                else
                {
                    ObjectBorder.BorderThickness = m_NoThickness;
                }
            }
        }

        public void ReSelect()
        {
            if (m_Selectable)
                Select(m_TreeViewItem.IsSelected);
        }

        // Select child objects, with only 50% opacity
        public void SelectChildren(ObjectControl c, bool select)
        {
            if (c.Children.Count > 0)
            {
                for (int i = 0; i < c.Children.Count; i++)
                {
                    if (c.Children[i] is ObjectControl)
                    {
                        SelectObject(c.Children[i] as ObjectControl, select, 0.5);
                        SelectChildren(c.Children[i] as ObjectControl, select);
                    }
                }
            }
        }

        public static void GetAllChildren(ObjectControl c, List<ObjectControl> children)
        {
            for (int i = 0; i < c.Children.Count; i++)
            {
                ObjectControl child = c.Children[i] as ObjectControl;
                if (child != null)
                {
                    children.Add(child);
                    GetAllChildren(child, children);
                }
            }
        }

        private void ChildCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!m_Editor)
            {
                return;
            }

            if(GlobalHelper.EditorWindow.SelectedTransformTool == EDITOR_TRANSFORM_TOOL.SELECT)
            {
                if(!Name.Equals("BaseObject"))
                {
                    m_TreeViewItem.IsSelected = true;
                    e.Handled = true;
                } 
            } 
        }

        public XElement Serialize()
        {
            XElement objectNode = new XElement("Object");

            XElement idNode = new XElement("Id", ID);
            XElement componentsNode = new XElement("Components");
            XElement childrenNode = new XElement("Children");
            
            objectNode.Add(idNode);
            objectNode.Add(componentsNode);
            objectNode.Add(childrenNode);

            // Serialize all components
            List<IComponent> components = GetComponents();
            for(int i = 0; i < components.Count; i++)
            {
                componentsNode.Add(components[i].Serialize());
            }

            // Serialize all child-objects
            for (int i = 0; i < Children.Count; i++)
            {
                if (Children[i] is IXML)
                {
                    childrenNode.Add((Children[i] as IXML).Serialize());
                }
            }

            return objectNode;
        }

        public static ObjectControl Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            ObjectControl c = new ObjectControl();

            if(!editor)
            {
                c.ObjectBorder.BorderThickness = c.m_NoThickness;
            }

            c.ObjectParent = parent;
            c.ID = node.Element("Id").Value;

            XElement components = node.Element("Components");
            XElement children = node.Element("Children");

            // Listeners and Animators must be read in later/last
            List<XElement> listeners = new List<XElement>();
            List<XElement> animators = new List<XElement>();

            foreach (XElement comp in components.Elements())
            {
                string compName = comp.Name.ToString();
                switch (compName)
                {
                    case "BasicComponent":
                    {
                        c.m_Basic = BasicComponent.Deserialize(comp, c, editor);
                        c.AddComponent(c.m_Basic);
                        break;
                    }
                    case "TransformComponent":
                    {
                        TransformComponent t = TransformComponent.Deserialize(comp, c, editor);
                        c.SetTransformComponent(t);
                        c.AddComponent(t);
                        break;
                    }
                    case "ImageComponent": c.AddComponent(ImageComponent.Deserialize(comp, c, editor)); break;
                    case "TextComponent": c.AddComponent(TextComponent.Deserialize(comp, c, editor)); break;
                    case "CommandListenerComponent": listeners.Add(comp); break; 
                    case "AnimatorComponent": animators.Add(comp); break;
                    case "TextToSpeechComponent": c.AddComponent(TextToSpeechComponent.Deserialize(comp, c, editor)); break;
                    case "SoundComponent": c.AddComponent(SoundComponent.Deserialize(comp, c, editor)); break;
                    case "YoutubeComponent": c.AddComponent(YoutubeComponent.Deserialize(comp, c, editor)); break;
                    case "CloningComponent": c.AddComponent(CloningComponent.Deserialize(comp, c, editor)); break;
                    default: throw new Exception("ComponentName: " + comp.Name.ToString());
                }
            }

            // Children
            foreach (XElement child in children.Elements())
            {
                c.Children.Add(Deserialize(child, c, editor));
            }

            // Deserialize any animators, this needs to be done After all children have been loaded
            for (int i = 0; i < animators.Count; i++)
            {
                c.AddComponent(AnimatorComponent.Deserialize(animators[i], c, editor));
            }

            // Deserialize any listeners, this needs to be done last
            for (int i = 0; i < listeners.Count; i++)
            {
                c.AddComponent(CommandListenerComponent.Deserialize(listeners[i], c, editor));
            }

            return c;
        }

        public void PrepareForDestruction()
        {
            // Loop all components and stop them.
            StopAllComponents(this);
        }

        public void StopAllComponents(ObjectControl o)
        {
            List<IComponent> components = o.GetComponents();
            for (int i = components.Count - 1; i >= 0; i--)
            {
                components[i].Stop();
            }

            for (int i = 0; i < o.Children.Count; i++)
            {
                if (o.Children[i] is ObjectControl)
                {
                    StopAllComponents(o.Children[i] as ObjectControl);
                }
            }
        }

    }
}
