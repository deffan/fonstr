﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for InformationTabControl.xaml
    /// </summary>
    public partial class InformationTabControl : UserControl
    {
        public int m_NewCounter;
        public int m_Level;

        public InformationTabControl()
        {
            InitializeComponent();
            m_NewCounter = 0;
            m_Level = 0;
        }

    }
}
