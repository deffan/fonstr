﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ToolButton.xaml
    /// </summary>
    public partial class ToolButton : UserControl
    {
        public static readonly DependencyProperty ToolTextProperty =
        DependencyProperty.Register("ToolText", typeof(string), typeof(ToolButton), new PropertyMetadata(""));

        public static readonly DependencyProperty ToolImageProperty =
        DependencyProperty.Register("ToolImage", typeof(string), typeof(ToolButton));

        public static readonly DependencyProperty ToolImageActiveProperty =
        DependencyProperty.Register("ToolImageActive", typeof(string), typeof(ToolButton));

        public static readonly DependencyProperty ToolIdProperty =
        DependencyProperty.Register("ToolId", typeof(string), typeof(ToolButton));

        private bool m_Selected;
        private bool m_Left;
        private Action<string> m_SelectCallback;
        private bool m_Unselectable;

        public string ToolId
        {
            get { return (string)GetValue(ToolIdProperty); }
            set { SetValue(ToolIdProperty, value); }
        }

        public string ToolText
        {
            get { return (string)GetValue(ToolTextProperty); }
            set { SetValue(ToolTextProperty, value); }
        }

        public string ToolImage
        {
            get { return (string)GetValue(ToolImageProperty); }
            set
            {
                ToolImageImage.Source = new ImageSourceConverter().ConvertFromString(value) as ImageSource;
            }
        }

        public string ToolImageActive
        {
            get { return (string)GetValue(ToolImageActiveProperty); }
            set
            {
                ToolImageImageActive.Source = new ImageSourceConverter().ConvertFromString(value) as ImageSource;
            }
        }

        public ToolButton()
        {
            InitializeComponent();
            m_Left = false;
            m_Selected = false;
            m_Unselectable = false;
        }

        public void SetUnselectable()
        {
            m_Unselectable = true;
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            m_Left = false;

            if (m_Selected)
                return;

            Background = Brushes.LightGray;
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            m_Left = false;

            if (m_Selected)
                return;

            Background = Brushes.Transparent;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            m_Left = true;
        }

        public void SetCallback(Action<string> callback)
        {
            m_SelectCallback = callback;
        }

        public void Select()
        {
            m_SelectCallback?.Invoke(ToolId);

            if (m_Unselectable)
                return;

            m_Selected = true;
            ToolTextLabel.FontWeight = FontWeights.Bold;
            ToolTextLabel.Foreground = Brushes.OrangeRed;
            Background = Brushes.DarkSlateGray;
            ToolImageImageActive.Visibility = Visibility.Visible;
            ToolImageImage.Visibility = Visibility.Collapsed;
        }

        public void Deselect()
        {
            if (!m_Selected)
                return;

            if (m_Unselectable)
                return;

            m_Selected = false;
            ToolTextLabel.FontWeight = FontWeights.Normal;
            ToolTextLabel.Foreground = Brushes.Black;
            Background = Brushes.Transparent;
            ToolImageImageActive.Visibility = Visibility.Collapsed;
            ToolImageImage.Visibility = Visibility.Visible;
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(m_Left)
            {
                if(!m_Selected)
                {
                    Select();
                }
            }
            m_Left = false;
        }
    }
}
