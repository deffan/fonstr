﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events
{
    public class EventData
    {
        public string Id;
        public int Type;
        public string Name;
        public bool Active;
        public object ReturnData;            // Event specific return data. Null by default.

        public EventData()
        {
            Id = "";
            Type = -1;
            Name = "";
            Active = false;
        }

        public EventData(string id, string name, int type, bool active)
        {
            Id = id;
            Type = type;
            Name = name;
            Active = active;
        }

        public virtual EventData Clone(object returnData)
        {
            throw new Exception("EventData Clone not implemented.");
        }

        public virtual string GetVariableData(string variable)
        {
            throw new Exception("EventData GetVariableData not implemented.");
        }
    }
}
