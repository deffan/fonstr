﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    public class TwitchViewersEvent : Event
    {
        public TwitchViewersEvent()
        {
            Data = new TwitchViewersEventData();
            Type = EVENT_TYPE.TWITCH_VIEWERS;
        }

        public TwitchViewersEvent(TwitchViewersEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_VIEWERS;
        }
    }
}
