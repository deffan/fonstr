﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    public class TwitchCustomEvent : Event
    {
        public TwitchCustomEvent()
        {
            Data = new TwitchCustomEventData();
            Type = EVENT_TYPE.TWITCH_BOT_CUSTOM;
        }

        public TwitchCustomEvent(TwitchCustomEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_BOT_CUSTOM;
        }
    }
}
