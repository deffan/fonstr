﻿using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    class TwitchJoinEvent : Event
    {
        public TwitchJoinEvent()
        {
            Data = new TwitchJoinEventData();
            Type = EVENT_TYPE.TWITCH_BOT_JOIN;
        }

        public TwitchJoinEvent(TwitchJoinEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_BOT_JOIN;
        }
    }
}
