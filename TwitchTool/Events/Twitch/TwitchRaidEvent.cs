﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    public class TwitchRaidEvent : Event
    {
        public TwitchRaidEvent()
        {
            Data = new TwitchRaidEventData();
            Type = EVENT_TYPE.TWITCH_RAID;
        }

        public TwitchRaidEvent(TwitchRaidEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_RAID;
        }
    }
}
