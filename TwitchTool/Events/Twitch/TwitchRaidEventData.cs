﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchRaidEventData : EventData
    {
        public string UserName = "";
        public int MinimumNumberOfRaiders = 0;

        public override string GetVariableData(string variable)
        {
            string data = "";
            if (variable.Equals("TWITCH_USERNAME"))
            {
                data = (ReturnData as TwitchRaidReturnData).UserName;
            }
            else if (variable.Equals("TWITCH_RAID_RAIDERS"))
            {
                data = (ReturnData as TwitchRaidReturnData).Raiders;
            }
            return data;
        }

        public override EventData Clone(object returnData)
        {
            TwitchRaidEventData e = new TwitchRaidEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                UserName = UserName,
                MinimumNumberOfRaiders = MinimumNumberOfRaiders
            };
            return e;
        }
    }

    public class TwitchRaidReturnData
    {
        public string UserName;
        public string Raiders;

        public TwitchRaidReturnData(string name, string raiders)
        {
            UserName = name;
            Raiders = raiders;
        }
    }
}
