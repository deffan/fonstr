﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchViewersEventData : EventData
    {
        public int Limit;           // Once reached >= Limit, trigger - Btw, what do we do, if the limit is reached instantly you go live again after say a computer crash? I guess there should be an initial data-pull, check these events, and "silently trigger" if needed.
        public bool Retrigger;      // If this should be able to re-trigger on the same limit
        public int RaiseBy;         // If the limit should be raised once triggered, like by X amount. (Not saved after restart, but only valid while running).

        public override string GetVariableData(string variable)
        {
            string data = "not implemented";
            return data;
        }

        public override EventData Clone(object returnData)
        {
            TwitchViewersEventData e = new TwitchViewersEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                Limit = Limit,
                Retrigger = Retrigger,
                RaiseBy = RaiseBy
            };
            return e;
        }
    }

    public class TwitchViewersReturnData
    {
        public int Viewers;

        public TwitchViewersReturnData(int viewers)
        {
            Viewers = viewers;
        }
    }
}
