﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    public class TwitchFollowEvent : Event
    {
        public TwitchFollowEvent()
        {
            Data = new TwitchFollowEventData();
            Type = EVENT_TYPE.TWITCH_FOLLOW;
        }

        public TwitchFollowEvent(TwitchFollowEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_FOLLOW;
        }
    }
}
