﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchSubscriptionEventData : EventData
    {
        // Rules
        public string TypeOfSub = "";  
        public string UserName = "";
        public int MinMonths = -1;
        public int MaxMonths = -1;
        public string SubPlan = "";

        public override string GetVariableData(string variable)
        {
            string data = "";
            if (variable.Equals("TWITCH_USERNAME"))
            {
                data = (ReturnData as TwitchSubscriptionReturnData).UserName;
            }
            else if(variable.Equals("TWITCH_SUBSCRIPTION_MONTHS"))
            {
                data = (ReturnData as TwitchSubscriptionReturnData).Months;
            }
            else if (variable.Equals("TWITCH_SUBSCRIPTION_PLAN"))
            {
                data = (ReturnData as TwitchSubscriptionReturnData).Subplan;
            }
            else if (variable.Equals("TWITCH_SUBSCRIPTION_TYPE"))
            {
                data = (ReturnData as TwitchSubscriptionReturnData).Subtype;
            }
            else if (variable.Equals("TWITCH_MESSAGE"))
            {
                data = (ReturnData as TwitchSubscriptionReturnData).Message;
            }
            return data;
        }

        public override EventData Clone(object returnData)
        {
            TwitchSubscriptionEventData e = new TwitchSubscriptionEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                UserName = UserName,
                TypeOfSub = TypeOfSub,
                MinMonths = MinMonths,
                MaxMonths = MaxMonths,
                SubPlan = SubPlan
            };
            return e;
        }
    }

    public class TwitchSubscriptionReturnData
    {
        public string UserName;
        public string Months;
        public string Subplan;
        public string Subtype;
        public string Message;

        public TwitchSubscriptionReturnData(string name, string months, string subplan, string subtype, string message)
        {
            UserName = name;
            Months = months;
            Message = message;
            Subplan = subplan;
            Subtype = subtype;
        }
    }
}
