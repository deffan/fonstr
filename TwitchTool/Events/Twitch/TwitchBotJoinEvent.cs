﻿using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    class TwitchBotJoinEvent : Event
    {
        public TwitchBotJoinEvent()
        {
            Type = EVENT_TYPE.TWITCH_BOT_JOIN;
        }

        public TwitchBotJoinEvent(TwitchBotJoinEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_BOT_JOIN;
        }
    }
}
