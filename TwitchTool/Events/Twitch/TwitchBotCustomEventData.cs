﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchBotCustomEventData : EventData
    {
        public string UserName;
        public string Text;
    }
}
