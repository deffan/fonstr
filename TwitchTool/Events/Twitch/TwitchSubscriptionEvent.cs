﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    public class TwitchSubscriptionEvent : Event
    {
        public TwitchSubscriptionEvent()
        {
            Data = new TwitchSubscriptionEventData();
            Type = EVENT_TYPE.TWITCH_SUBSCRIPTION;
        }

        public TwitchSubscriptionEvent(TwitchSubscriptionEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_SUBSCRIPTION;
        }
    }
}
