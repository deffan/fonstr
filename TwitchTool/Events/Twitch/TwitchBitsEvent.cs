﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    public class TwitchBitsEvent : Event
    {
        public TwitchBitsEvent()
        {
            Data = new TwitchBitsEventData();
            Type = EVENT_TYPE.TWITCH_BITS;
        }

        public TwitchBitsEvent(TwitchBitsEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_BITS;
        }
    }
}
