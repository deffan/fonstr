﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    public class TwitchGiftEvent : Event
    {
        public TwitchGiftEvent()
        {
            Data = new TwitchGiftEventData();
            Type = EVENT_TYPE.TWITCH_GIFT;
        }

        public TwitchGiftEvent(TwitchGiftEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_GIFT;
        }
    }
}
