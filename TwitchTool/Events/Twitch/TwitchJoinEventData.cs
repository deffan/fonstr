﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchJoinEventData : EventData
    {
        public string UserName = "";

        public override string GetVariableData(string variable)
        {
            string data = "";
            if (variable.Equals("TWITCH_USERNAME"))
            {
                data = ReturnData as string;
            }
            return data;
        }

        public override EventData Clone(object returnData)
        {
            TwitchJoinEventData e = new TwitchJoinEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                UserName = UserName
            };
            return e;
        }
    }
}

