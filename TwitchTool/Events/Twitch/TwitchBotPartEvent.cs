﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    class TwitchBotPartEvent : Event
    {
        public TwitchBotPartEvent()
        {
            Type = EVENT_TYPE.TWITCH_BOT_PART;
        }

        public TwitchBotPartEvent(TwitchBotPartEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_BOT_PART;
        }
    }

}
