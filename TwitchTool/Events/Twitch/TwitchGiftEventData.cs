﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchGiftEventData : EventData
    {
        public string UserName = "";

        public override string GetVariableData(string variable)
        {
            string data = "";
            if (variable.Equals("TWITCH_USERNAME"))
            {
                data = (ReturnData as TwitchGiftReturnData).UserName;
            }
            else if (variable.Equals("TWITCH_GIFT_RECIPIENT"))
            {
                data = (ReturnData as TwitchGiftReturnData).Recipient;
            }
            return data;
        }

        public override EventData Clone(object returnData)
        {
            TwitchGiftEventData e = new TwitchGiftEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                UserName = UserName
            };
            return e;
        }
    }

    public class TwitchGiftReturnData
    {
        public string UserName;
        public string Recipient;

        public TwitchGiftReturnData(string name, string recipient)
        {
            UserName = name;
            Recipient = recipient;
        }
    }
}
