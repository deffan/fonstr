﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchCustomEventData : EventData
    {
        public string UserName = "";
        public string Text = "";         // Text/Command to listen for. Must be the FIRST word of the message.
        public bool CaseSensitive = false;
        public bool Contains = false;
        public bool FirstWordOnly = true;
        public bool AnyWord = false;
        public string TypeOfUser = "";   // For example, Moderator, Staff, Subscriber... (empty=any)

        public override string GetVariableData(string variable)
        {
            string data = "";
            if (variable.Equals("TWITCH_USERNAME"))
            {
                data = (ReturnData as TwitchCustomReturnData).UserName;
            }
            else if (variable.Equals("TWITCH_MESSAGE"))
            {
                data = (ReturnData as TwitchCustomReturnData).Message;
            }
            else if (variable.Equals("TWITCH_USERTYPE"))
            {
                data = (ReturnData as TwitchCustomReturnData).UserType;
            }
            else if (variable.Equals("TWITCH_BOT_CUSTOM_TEXT1"))
            {
                data = (ReturnData as TwitchCustomReturnData).Text1;
            }
            else if (variable.Equals("TWITCH_BOT_CUSTOM_TEXT2"))
            {
                data = (ReturnData as TwitchCustomReturnData).Text2;
            }
            else if (variable.Equals("TWITCH_BOT_CUSTOM_TEXT3"))
            {
                data = (ReturnData as TwitchCustomReturnData).Text3;
            }
            return data;
        }

        public override EventData Clone(object returnData)
        {
            TwitchCustomEventData e = new TwitchCustomEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                UserName = UserName,
                Text = Text,
                CaseSensitive = CaseSensitive,
                TypeOfUser = TypeOfUser
            };
            return e;
        }
    }

    public class TwitchCustomReturnData
    {
        public string Text1;
        public string Text2;
        public string Text3;
        public string UserName;
        public string UserType;
        public string Message;

        public TwitchCustomReturnData(string message, string text1, string text2, string text3, string typeOfUser, string username)
        {
            Text1 = text1;
            Text2 = text2;
        }

    }
}
