﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Events.Twitch
{
    public class TwitchBitsEventData : EventData
    {
        public string UserName = "";
        public int LowBits = -1;
        public int HighBits = -1;

        public override string GetVariableData(string variable)
        {
            string data = "";
            if (variable.Equals("TWITCH_USERNAME"))
            {
                data = (ReturnData as TwitchBitsReturnData).UserName;
            }
            else if (variable.Equals("TWITCH_BITS"))
            {
                data = (ReturnData as TwitchBitsReturnData).Bits;
            }
            else if (variable.Equals("TWITCH_MESSAGE"))
            {
                data = (ReturnData as TwitchBitsReturnData).Message;
            }
            return data;
        }

        public override EventData Clone(object returnData)
        {
            TwitchBitsEventData e = new TwitchBitsEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                UserName = UserName,
                LowBits = LowBits,
                HighBits = HighBits
            };
            return e;
        }
    }

    public class TwitchBitsReturnData
    {
        public string UserName;
        public string Bits;
        public string Message;

        public TwitchBitsReturnData(string name, string bits, string message)
        {
            UserName = name;
            Bits = bits;
            Message = message;
        }
    }

}
