﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Events.Twitch
{
    class TwitchPartEvent : Event
    {
        public TwitchPartEvent()
        {
            Data = new TwitchPartEventData();
            Type = EVENT_TYPE.TWITCH_BOT_PART;
        }

        public TwitchPartEvent(TwitchPartEventData data)
        {
            Data = data;
            Type = EVENT_TYPE.TWITCH_BOT_PART;
        }
    }

}
