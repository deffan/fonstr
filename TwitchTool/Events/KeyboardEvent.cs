﻿using TwitchTool.Source;

namespace TwitchTool.Events
{
    public class KeyboardEvent : Event
    {
        public KeyboardEvent()
        {
            Data = new KeyboardEventData();
            Type = EVENT_TYPE.KEYBOARD;
        }

        public KeyboardEvent(KeyboardEventData data) 
        {
            Data = data;
            Type = EVENT_TYPE.KEYBOARD;
        }
    }
}
