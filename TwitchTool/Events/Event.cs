﻿using System;
using System.Collections.Generic;
using TwitchTool.Events;

namespace TwitchTool.Source
{
    public class Event
    {
        private List<Command> m_Commands;
        private bool m_Finished;
        private string m_EventId;
        private string m_RandomId;
        private EventData m_Data;               // Event data (what to listen for / rules).
        private AppWindow m_Window;
        private int m_CurrentOrderNumber;
        private List<List<Command>> m_CommandOrder;
        private string m_Queue;
        private EventQueue m_QueuedIn;

        public Event()
        {
            m_Commands = new List<Command>();
            m_CommandOrder = new List<List<Command>>();
            m_Finished = false;
            m_Window = null;
            m_EventId = Guid.NewGuid().ToString();
            m_RandomId = Guid.NewGuid().ToString();
            m_CurrentOrderNumber = -1;
        }

        // This semi-clones the Event object and sets return data. 
        // This is called when its triggered and data has been fetched.
        public T Ready<T>(object returnData) where T : Event, new()
        {
            T newEvent = new T
            {
                Data = Data.Clone(returnData),
                Window = Window,
                EventId = EventId,
                Queue = Queue
            };

            for (int i = 0; i < Commands.Count; i++)
            {
                newEvent.AddCommand(Commands[i].Clone(newEvent));
            }
            return newEvent;
        }

        public void Start()
        {
            Window.AddLiveEvent(this);
        }

        public void Restart()
        {
            m_CurrentOrderNumber = -1;
            m_CommandOrder.Clear();
            m_Finished = false;

            for (int i = 0; i < Commands.Count; i++)
            {
                Commands[i].Reset();

                // If this was cloned, we might need to restore the old listenerid
                for (int n = 0; n < Commands[i].Data.Listeners.Count; n++)
                {
                    int indx = Commands[i].Data.Listeners[n].IndexOf("#CLONED_");
                    if (indx > 0)
                    {
                        Commands[i].Data.Listeners[n] = Commands[i].Data.Listeners[n].Substring(0, indx);
                    }
                }
            }

            m_QueuedIn.AddEvent(this);
        }

        public void Abort()
        {
            for (int i = 0; i < Commands.Count; i++)
            {
                Commands[i].Abort(true);
            }
        }

        public void StartCommands()
        {
            // If called again after first order, start next.
            m_CurrentOrderNumber++;
            if (m_CurrentOrderNumber > 0)
            {
                StartCommands(m_CommandOrder[m_CurrentOrderNumber]);
                return;
            }

            // Count/Sort the number of order-numbers we have to deal with.
            List<int> sorted = new List<int>();
            for (int i = 0; i < m_Commands.Count; i++)
            {
                if (!sorted.Contains((m_Commands[i].Data.Order)))
                {
                    m_CommandOrder.Add(new List<Command>());
                    sorted.Add(m_Commands[i].Data.Order);
                }
            }
            sorted.Sort();  // For example, 0,1,2 or 1,4,7 etc... we dont care, as long as its in ascending order.

            // Add each command to each command-order list. Such that m_CommandOrder[0] contains all lowest level, m_CommandOrder[1] the next, and so on...
            for (int n = 0; n < sorted.Count; n++)
            {
                for (int i = 0; i < m_Commands.Count; i++)
                {
                    if(m_Commands[i].Data.Order == sorted[n])
                    {
                        m_CommandOrder[n].Add(m_Commands[i]);
                    }
                }
            }

            // Start the first commands
            StartCommands(m_CommandOrder[0]);
        }

        public bool CurrentOrderFinished()
        {
            for(int i = 0; i < m_CommandOrder[m_CurrentOrderNumber].Count; i++)
            {
                if (!m_CommandOrder[m_CurrentOrderNumber][i].Finished)
                    return false;
            }
            return true;
        }

        public bool IsLastOrder()
        {
            return m_CurrentOrderNumber + 1 == m_CommandOrder.Count;
        }

        private void StartCommands(List<Command> commands)
        {
            for (int n = 0; n < commands.Count; n++)
            {
                Window.StartCommand(commands[n]);
            }
        }

        public AppWindow Window
        {
            get { return m_Window; }
            set { m_Window = value; }
        }

        public EventData Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        public object ReturnData
        {
            get { return m_Data.ReturnData; }
            set { m_Data.ReturnData = value; }
        }

        public string Queue
        {
            get { return m_Queue; }
            set { m_Queue = value; }
        }

        public EventQueue QueuedIn
        {
            get { return m_QueuedIn; }
            set { m_QueuedIn = value; }
        }

        public int Type
        {
            get { return m_Data.Type; }
            set { m_Data.Type = value; }
        }

        public bool Finished
        {
            get { return m_Finished; }
            set { m_Finished = value; }
        }

        // Always remains the same, even when cloning. This is important, because we use it to identify THIS event.
        public string EventId
        {
            get { return m_EventId; }
            set { m_EventId = value; }
        }

        // This is a random Id, which can be used to identify the instance 
        public string RandomId
        {
            get { return m_RandomId; }
        }

        public List<Command> Commands
        {
            get { return m_Commands; }
            set { m_Commands = value; }
        }

        public void AddCommand(Command c)
        {
            m_Commands.Add(c);
        }

        public bool RemoveCommand(Command c)
        {
            return m_Commands.Remove(c);
        }
    }
}
