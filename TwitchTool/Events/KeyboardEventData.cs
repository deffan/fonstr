﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace TwitchTool.Events
{
    public class KeyboardEventData : EventData
    {
        public List<string> Keys = new List<string>();

        public override string GetVariableData(string variable)
        {
            string data = "";
            if(variable.Equals("KEYBOARD_KEYS"))
            {
                for (int i = 0; i < Keys.Count; i++)
                {
                    Key key;
                    if (Enum.TryParse(Keys[i], out key))
                    {
                        if (data.Length > 0)
                        {
                            data += " + ";
                        }
                        data += Keys[i];
                    }
                }
            }
            return data;
        }

        public override EventData Clone(object returnData)
        {
            KeyboardEventData e = new KeyboardEventData
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Active = Active,
                ReturnData = returnData,
                Keys = Keys
            };
            return e;
        }
    }
}
