﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using TwitchTool.Events.Twitch;
using TwitchTool.Source;
using TwitchTool.Twitch;
using static TwitchTool.Twitch.TwitchAPI;

namespace TwitchTool.EventListeners
{
    public class TwitchListener : EventListener
    {
        // Events to listen for
        private List<TwitchJoinEvent> m_JoinList;
        private List<TwitchPartEvent> m_PartList;
        private List<TwitchSubscriptionEvent> m_SubscriberList;
        private List<TwitchGiftEvent> m_GiftedList;
        private List<TwitchRaidEvent> m_RaidList;
        private List<TwitchCustomEvent> m_CustomList;
        private List<TwitchBitsEvent> m_BitsList;
        private List<TwitchFollowEvent> m_FollowersList;

        // Other datamembers
        private Dictionary<string, string> m_ChannelUserList;
        private DateTime m_LastFollowerFetch;
        private TwitchAPI m_TwitchAPI;

        public TwitchListener(TwitchAPI twitchAPI)
        {
            m_JoinList = new List<TwitchJoinEvent>();
            m_PartList = new List<TwitchPartEvent>();
            m_SubscriberList = new List<TwitchSubscriptionEvent>();
            m_GiftedList = new List<TwitchGiftEvent>();
            m_RaidList = new List<TwitchRaidEvent>();
            m_BitsList = new List<TwitchBitsEvent>();
            m_CustomList = new List<TwitchCustomEvent>();
            m_FollowersList = new List<TwitchFollowEvent>();

            m_ChannelUserList = new Dictionary<string, string>();
            m_LastFollowerFetch = DateTime.UtcNow;  // Needs to be UTC because of different timezones
            m_TwitchAPI = twitchAPI;
        }

        public void Reset()
        {
            m_JoinList.Clear();
            m_PartList.Clear();
            m_SubscriberList.Clear();
            m_GiftedList.Clear();
            m_RaidList.Clear();
            m_CustomList.Clear();
            m_BitsList.Clear();
            m_FollowersList.Clear();
        }

        // Functions that simply return if we are listening for any of these events
        public bool HasJoinEvent() { return m_JoinList.Count > 0; }
        public bool HasPartEvent() { return m_PartList.Count > 0; }
        public bool HasSubscriberEvent() { return m_SubscriberList.Count > 0; }
        public bool HasGiftedEvent() { return m_GiftedList.Count > 0; }
        public bool HasRaidEvent() { return m_RaidList.Count > 0; }
        public bool HasBitsEvent() { return m_BitsList.Count > 0; }
        public bool HasCustomEvent() { return m_CustomList.Count > 0; }
        public bool HasFollowerEvent() { return m_FollowersList.Count > 0; }

        public void JoinEvent(string username)
        {
            for (int i = 0; i < m_JoinList.Count; i++)
            {
                TwitchJoinEventData eventRule = m_JoinList[i].Data as TwitchJoinEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(username.ToLower()))
                        continue;
                }

                StartEvent(m_JoinList[i].Ready<TwitchJoinEvent>(username));
            }
        }

        public void PartEvent(string username)
        {
            for (int i = 0; i < m_PartList.Count; i++)
            {
                TwitchPartEventData eventRule = m_PartList[i].Data as TwitchPartEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(username.ToLower()))
                        continue;
                }

                StartEvent(m_PartList[i].Ready<TwitchPartEvent>(username));
            }
        }

        public void RaidEvent(TwitchBotData data)
        {
            for (int i = 0; i < m_RaidList.Count; i++)
            {
                TwitchRaidEventData eventRule = m_RaidList[i].Data as TwitchRaidEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(data.UserName.ToLower()))
                        continue;
                }

                // Minimum amount of raiders required
                int raiders = Convert.ToInt32(data.AmountOfRaiders);
                if (raiders >= eventRule.MinimumNumberOfRaiders)
                {
                    StartEvent(m_RaidList[i].Ready<TwitchRaidEvent>(new TwitchRaidReturnData(data.UserName, data.AmountOfRaiders)));
                }
            }
        }

        public void GiftEvent(TwitchBotData data)
        {
            for (int i = 0; i < m_GiftedList.Count; i++)
            {
                TwitchGiftEventData eventRule = m_GiftedList[i].Data as TwitchGiftEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(data.UserName.ToLower()))
                        continue;
                }

                StartEvent(m_GiftedList[i].Ready<TwitchGiftEvent>(new TwitchGiftReturnData(data.UserName, data.SubGiftRecipient)));
            }
        }

        public void BitsEvent(TwitchBotData data)
        {
            for (int i = 0; i < m_BitsList.Count; i++)
            {
                TwitchBitsEventData eventRule = m_BitsList[i].Data as TwitchBitsEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(data.UserName.ToLower()))
                        continue;
                }

                bool success = false;
                int bits = Convert.ToInt32(data.Bits);

                if (eventRule.LowBits == -1 && eventRule.HighBits == -1)
                {
                    // Any
                    success = true;
                }
                else if (eventRule.LowBits == eventRule.HighBits && bits == eventRule.LowBits)
                {
                    // Exact match
                    success = true;
                }
                else if(bits >= eventRule.LowBits && bits <= eventRule.HighBits)
                {
                    // Within the set interval
                    success = true;
                }
                else if(eventRule.LowBits == -1 && bits > eventRule.HighBits)
                {
                    // If configured to be "more than X"
                    success = true;
                }

                if(success)
                {
                    StartEvent(m_BitsList[i].Ready<TwitchBitsEvent>(new TwitchBitsReturnData(data.UserName, data.Bits, data.Message)));
                }
            }
        }

        public void SubEvent(TwitchBotData data)
        {
            for (int i = 0; i < m_SubscriberList.Count; i++)
            {
                TwitchSubscriptionEventData eventRule = m_SubscriberList[i].Data as TwitchSubscriptionEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(data.UserName.ToLower()))
                        continue;
                }

                // If TypeOfsub is null, any subscription type counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.TypeOfSub))
                {
                    if (!eventRule.TypeOfSub.Equals(data.Id))   // "sub" or "resub"
                        continue;
                }

                // If SubPlan is null, any subscription plan counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.SubPlan))
                {
                    if (!eventRule.TypeOfSub.Equals(data.SubPlan))   // "Prime" or "1000" or something...
                        continue;
                }

                bool success = false;
                int months = Convert.ToInt32(data.Months);

                if (eventRule.MinMonths == -1 && eventRule.MaxMonths == -1)
                {
                    // Any
                    success = true;
                }
                else if (eventRule.MinMonths == eventRule.MaxMonths && months == eventRule.MinMonths)
                {
                    // Exact match
                    success = true;
                }
                else if (months >= eventRule.MinMonths && months <= eventRule.MaxMonths)
                {
                    // Within the set interval
                    success = true;
                }
                else if (eventRule.MinMonths == -1 && months > eventRule.MaxMonths)
                {
                    // If configured to be "more than X"
                    success = true;
                }

                if (success)
                {
                    StartEvent(m_SubscriberList[i].Ready<TwitchSubscriptionEvent>(new TwitchSubscriptionReturnData(data.UserName, data.Months, data.SubPlan, data.Id, data.Message)));
                }
            }
        }

        public void CustomEvent(TwitchBotData data)
        {
            string[] words = data.Message.Split(' ');
            if (words.Length == 0)
                return;

            string text1 = words[0];
            string Message = data.Message;

            for (int i = 0; i < m_CustomList.Count; i++)
            {
                TwitchCustomEventData eventRule = m_CustomList[i].Data as TwitchCustomEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(data.UserName.ToLower()))
                        continue;
                }

                // If typeOfUser is null, anyone counts. Else it must match the specific badge the user has.
                if (!string.IsNullOrEmpty(eventRule.TypeOfUser))
                {
                    bool correctUserType = false;
                    string typeOfUser = eventRule.TypeOfUser.ToLower();
                    for (int n = 0; n < data.Badges.Count; n++)
                    {
                        if (typeOfUser.Equals(data.Badges[n].ToLower()))
                        {
                            correctUserType = true;
                            break;
                        }
                    }

                    if (!correctUserType)
                        continue;
                }

                if(!string.IsNullOrEmpty(eventRule.Text))   // If text is empty, it always passes.
                {
                    // Case sensitive?
                    if (!eventRule.CaseSensitive)
                    {
                        eventRule.Text = eventRule.Text.ToLower();
                        text1 = text1.ToLower();
                        Message = Message.ToLower();
                    }

                    // First word only?
                    if (eventRule.FirstWordOnly)
                    {
                        if (!eventRule.Text.Equals(text1))
                        {
                            continue;
                        }
                    }
                    else if (eventRule.AnyWord) // The text can be anywhere in the text
                    {
                        int start = Message.IndexOf(eventRule.Text);
                        if (start < 0)
                            continue;

                        // The word/sentence/text is expected to begin/end with a space or start/end-of string
                        bool ok = false;
                        if(start == 0 || Message[start - 1] == ' ')
                        {
                            int end = start + eventRule.Text.Length;
                            if (end == Message.Length || Message[end] == ' ')
                            {
                                ok = true;
                            }
                        }

                        if(!ok)
                        {
                            continue;
                        }
                    }
                    else 
                    {
                        // Contains
                        if (!Message.Contains(eventRule.Text))
                        {
                            continue;
                        }
                    }
                }

                string text2 = "";
                string text3 = "";

                if (words.Length > 1)
                    text2 = words[1];

                if (words.Length > 2)
                    text3 = words[2];

                StartEvent(m_CustomList[i].Ready<TwitchCustomEvent>(new TwitchCustomReturnData(data.Message, text1, text2, text3, eventRule.TypeOfUser, data.UserName)));
            }
        }

        public void FollowerEvent(string username)
        {
            for (int i = 0; i < m_FollowersList.Count; i++)
            {
                TwitchFollowEventData eventRule = m_FollowersList[i].Data as TwitchFollowEventData;

                // If username is null, anyone counts. Else it must match.
                if (!string.IsNullOrEmpty(eventRule.UserName))
                {
                    if (!eventRule.UserName.ToLower().Equals(username.ToLower()))
                        continue;
                }

                StartEvent(m_FollowersList[i].Ready<TwitchFollowEvent>(username));
            }
        }

        private void StartEvent(Event e)
        {
            e.Start();
        }

        public override void AddEvent(Event newEvent)
        {

            // NOT THREAD SAFE - Lists are accessed in a different thread!

            switch(newEvent.Type)
            {
                case EVENT_TYPE.TWITCH_BOT_JOIN: m_JoinList.Add(newEvent as TwitchJoinEvent); break;
                case EVENT_TYPE.TWITCH_BOT_PART: m_PartList.Add(newEvent as TwitchPartEvent); break;
                case EVENT_TYPE.TWITCH_GIFT: m_GiftedList.Add(newEvent as TwitchGiftEvent); break;
                case EVENT_TYPE.TWITCH_RAID: m_RaidList.Add(newEvent as TwitchRaidEvent); break;
                case EVENT_TYPE.TWITCH_SUBSCRIPTION: m_SubscriberList.Add(newEvent as TwitchSubscriptionEvent); break;
                case EVENT_TYPE.TWITCH_BOT_CUSTOM: m_CustomList.Add(newEvent as TwitchCustomEvent); break;
                case EVENT_TYPE.TWITCH_BITS: m_BitsList.Add(newEvent as TwitchBitsEvent); break;
                case EVENT_TYPE.TWITCH_FOLLOW: m_FollowersList.Add(newEvent as TwitchFollowEvent); break;
                default: throw new Exception("Invalid (Add)Event type in TwitchBot: " + newEvent.Type);
            }
        }

        public override void RemoveEvent(Event removeEvent)
        {

            // NOT THREAD SAFE - Lists are accessed in a different thread!

            bool result = false;
            switch (removeEvent.Type)
            {
                case EVENT_TYPE.TWITCH_BOT_JOIN: result = RemoveEvent(removeEvent, m_JoinList); break;
                case EVENT_TYPE.TWITCH_BOT_PART: result = RemoveEvent(removeEvent, m_PartList); break;
                case EVENT_TYPE.TWITCH_GIFT: result = RemoveEvent(removeEvent, m_GiftedList); break;
                case EVENT_TYPE.TWITCH_RAID: result = RemoveEvent(removeEvent, m_RaidList); break;
                case EVENT_TYPE.TWITCH_SUBSCRIPTION: result = RemoveEvent(removeEvent, m_SubscriberList); break;
                case EVENT_TYPE.TWITCH_BOT_CUSTOM: result = RemoveEvent(removeEvent, m_CustomList); break;
                case EVENT_TYPE.TWITCH_BITS: result = RemoveEvent(removeEvent, m_BitsList); break;
                case EVENT_TYPE.TWITCH_FOLLOW: result = RemoveEvent(removeEvent, m_FollowersList); break;
                default: throw new Exception("Invalid (Remove)Event type in TwitchBot: " + removeEvent.Type);
            }

            if(!result)
            {
                throw new Exception("Could not remove event in TwitchBot, Type: " + removeEvent.Type);
            }
        }

        private bool RemoveEvent<T>(Event e, List<T> list) where T : Event
        {
            if (list.Count == 0)
                return true;

            for(int i = 0; i < list.Count; i++)
            {
                if(list[i].EventId.Equals(e.EventId))
                {
                    list.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        public void UpdateFollowers()
        {
            bool fetchMore = true;
            string cursor = "";
            while (fetchMore)
            {
                fetchMore = false;
                TwitchResponse followers = m_TwitchAPI.GetFollowers(cursor);
                if (followers.Type.Equals("GetResponse"))
                {
                    Followers data = JsonConvert.DeserializeObject<Followers>(followers.Data);
                    for (int i = 0; i < data.follows.Count; i++)
                    {
                        if (data.follows[i].created_at > m_LastFollowerFetch)
                        {
                            FollowerEvent(data.follows[i].user.name);

                            // If the last user in this list is also a new follower we should fetch more...
                            if (i + 1 == data.follows.Count)
                            {
                                cursor = data._cursor;
                                fetchMore = true;
                            }
                        }
                    }
                }
                else
                {
                    Logger.Log(LOG_LEVEL.ERROR, "TwitchAPI - UpdateFollowers - " + followers.Data);
                    break;
                }
            }
            m_LastFollowerFetch = DateTime.UtcNow;
        }


        public void UpdateUserList()
        {
            TwitchResponse userlist = m_TwitchAPI.Get(string.Format("http://tmi.twitch.tv/group/user/{0}/chatters", m_TwitchAPI.USERNAME), false);
            if (userlist.Type.Equals("GetResponse"))
            {
                Dictionary<string, string> latestChannelUserList = new Dictionary<string, string>();
                UserList data = JsonConvert.DeserializeObject<UserList>(userlist.Data);

                int count = data.chatter_count;
                if (count == m_ChannelUserList.Count)
                    return;

                // Add any new channel users to the latest list, and trigger any "twitch join" event 
                AddNewChannelUser(data.chatters.vips, "vips", latestChannelUserList, m_ChannelUserList.Count > 0);
                AddNewChannelUser(data.chatters.moderators, "moderators", latestChannelUserList, m_ChannelUserList.Count > 0);
                AddNewChannelUser(data.chatters.staff, "staff", latestChannelUserList, m_ChannelUserList.Count > 0);
                AddNewChannelUser(data.chatters.admins, "admins", latestChannelUserList, m_ChannelUserList.Count > 0);
                AddNewChannelUser(data.chatters.global_mods, "global_mods", latestChannelUserList, m_ChannelUserList.Count > 0);
                AddNewChannelUser(data.chatters.viewers, "viewers", latestChannelUserList, m_ChannelUserList.Count > 0);

                // Now check who left
                if (m_ChannelUserList.Count > 0 && HasPartEvent())
                {
                    for (int i = 0; i < m_ChannelUserList.Count; i++)
                    {
                        if (!latestChannelUserList.ContainsKey(m_ChannelUserList.Keys.ElementAt(i)))
                        {
                            PartEvent(m_ChannelUserList.Keys.ElementAt(i));
                        }
                    }
                }

                // This is now the current list.
                m_ChannelUserList = latestChannelUserList;
            }
            else
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchAPI - UpdateUserList - " + userlist.Data);
            }
        }

        private void AddNewChannelUser(List<string> userlist, string type, Dictionary<string, string> newUserDict, bool initial)
        {
            for (int i = 0; i < userlist.Count; i++)
            {
                newUserDict.Add(userlist[i], type);
                if (!initial && !m_ChannelUserList.ContainsKey(userlist[i]))
                {
                    m_ChannelUserList.Add(userlist[i], type);
                    if (HasJoinEvent())
                    {
                        JoinEvent(userlist[i]);
                    }
                }
            }
        }
    }
}
