﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TwitchTool.Events.Twitch;
using TwitchTool.Source;
using TwitchTool.Twitch;

namespace TwitchTool.EventListeners
{
    public class TwitchBotListener : EventListener
    {
        private List<TwitchBotJoinEvent> m_JoinList;
        private List<TwitchBotPartEvent> m_PartList;
        private List<TwitchSubscriptionEvent> m_SubscriberList;
        private List<TwitchGiftEvent> m_GiftedList;
        private List<TwitchRaidEvent> m_RaidList;
        private List<TwitchBotCustomEvent> m_CustomList;
        private List<TwitchBitsEvent> m_BitsList;

        public TwitchBotListener(Action<Event> addEventCallback) : base(addEventCallback)
        {
            m_JoinList = new List<TwitchBotJoinEvent>();
            m_PartList = new List<TwitchBotPartEvent>();
            m_SubscriberList = new List<TwitchSubscriptionEvent>();
            m_GiftedList = new List<TwitchGiftEvent>();
            m_RaidList = new List<TwitchRaidEvent>();
            m_BitsList = new List<TwitchBitsEvent>();
            m_CustomList = new List<TwitchBotCustomEvent>();
        }

        public void Reset()
        {
            m_JoinList.Clear();
            m_PartList.Clear();
            m_SubscriberList.Clear();
            m_GiftedList.Clear();
            m_RaidList.Clear();
            m_CustomList.Clear();
            m_BitsList.Clear();
        }

        public bool HasJoinEvent() { return m_JoinList.Count == 0; }
        public bool HasPartEvent() { return m_PartList.Count == 0; }
        public bool HasSubscriberEvent() { return m_SubscriberList.Count == 0; }
        public bool HasGiftedEvent() { return m_GiftedList.Count == 0; }
        public bool HasRaidEvent() { return m_RaidList.Count == 0; }
        public bool HasBitsEvent() { return m_BitsList.Count == 0; }
        public bool HasCustomEvent() { return m_CustomList.Count == 0; }

        public void JoinEvent(string username)
        {
            Logger.Log(0, username + " joined.");
            for (int i = 0; i < m_JoinList.Count; i++)
            {
                if ((m_JoinList[i].Data as TwitchBotJoinEventData).UserName.Equals(username))
                {
                    StartEvent(m_JoinList[i].Ready<TwitchBotJoinEvent>(DateTime.Now));
                    return;
                }
            }
           
        }

        public void PartEvent(string username)
        {
            Logger.Log(0, username + " left.");
            for (int i = 0; i < m_PartList.Count; i++)
            {
                if ((m_PartList[i].Data as TwitchBotPartEventData).UserName.Equals(username))
                {
                    StartEvent(m_PartList[i].Ready<TwitchBotPartEvent>(DateTime.Now));
                    return;
                }
            }
            
        }

        public void RaidEvent(TwitchBotData data)
        {
            // Raid: Triggas baserat på: Specifik user/alla, samt minsta antal raiders.
            Logger.Log(0, JsonConvert.SerializeObject(data, Formatting.Indented));
        }

        public void GiftEvent(TwitchBotData data)
        {
            // Gift: Triggas baserat på: Specifik user/alla
            Logger.Log(0, JsonConvert.SerializeObject(data, Formatting.Indented));
        }

        public void BitsEvent(TwitchBotData data)
        {
            // Bits: Måste matcha event-reglerna. Kanske Specifik user/alla, sen antal bits. Dvs; Kolla först om det är en specifik user, annars kolla så reglerna uppfylls
            //       reglerna kan vara tex: minst 100 bits, eller mellan 100 och 200 bits, eller exakt 1337 bits, osv.
            //       Ett problem är tex, om vi har flera bit-events, som säger > 200 > 300 > 400 sen ger någon tex 500, då triggas ju alla 3? Det blir inte bra.
            //       Loopa alla, direkt man hittar en direct-match, så används den, annars används högsta matchande. (om flera matchar, så blir det lite random, men det är inte mitt fel då).
            Logger.Log(0, JsonConvert.SerializeObject(data, Formatting.Indented));
        }

        public void SubEvent(TwitchBotData data)
        {
            // Subs: Specifik user/alla, Sub eller Resub (eller båda), antal månader (alla, minst eller över nivå, etc), nivå på subben, eller alla
            //       Tex: Alla, Sub, alla månader, alla nivåer = "someone subbed for x months with prime"
            //       Tex: Alla, Sub, alla månader, prime nivå = "someone PRIME SUBBED for x months!!!!"
            Logger.Log(0, JsonConvert.SerializeObject(data, Formatting.Indented));
        }

        public void CustomEvent(TwitchBotData data)
        {
            // Custom: Specifik användare/alla, samt att "Text" att lyssna efter måste matcha substring(0, text.length) dvs första ordet/meningen/texten.
            //         Input blir sen antingen bara att det hänt, eller vad som sades efter detta. Hela textsträngen.
            Logger.Log(0, JsonConvert.SerializeObject(data, Formatting.Indented));
        }

        private void StartEvent(Event e)
        {
            m_AddEventCallback.Invoke(e);
        }

        public override void AddEvent(Event newEvent)
        {
            switch(newEvent.Type)
            {
                case EVENT_TYPE.TWITCH_BOT_JOIN: m_JoinList.Add(newEvent as TwitchBotJoinEvent); break;
                case EVENT_TYPE.TWITCH_BOT_PART: m_PartList.Add(newEvent as TwitchBotPartEvent); break;
                case EVENT_TYPE.TWITCH_GIFT: m_GiftedList.Add(newEvent as TwitchGiftEvent); break;
                case EVENT_TYPE.TWITCH_RAID: m_RaidList.Add(newEvent as TwitchRaidEvent); break;
                case EVENT_TYPE.TWITCH_SUBSCRIPTION: m_SubscriberList.Add(newEvent as TwitchSubscriptionEvent); break;
                case EVENT_TYPE.TWITCH_BOT_CUSTOM: m_CustomList.Add(newEvent as TwitchBotCustomEvent); break;
                case EVENT_TYPE.TWITCH_BITS: m_BitsList.Add(newEvent as TwitchBitsEvent); break;
                default: throw new Exception("Invalid (Add)Event type in TwitchBot: " + newEvent.Type);
            }
        }

        public override void RemoveEvent(Event removeEvent)
        {
            bool result = false;
            switch (removeEvent.Type)
            {
                case EVENT_TYPE.TWITCH_BOT_JOIN: result = m_JoinList.Remove(removeEvent as TwitchBotJoinEvent); break;
                case EVENT_TYPE.TWITCH_BOT_PART: result = m_PartList.Remove(removeEvent as TwitchBotPartEvent); break;
                case EVENT_TYPE.TWITCH_GIFT: result = m_GiftedList.Remove(removeEvent as TwitchGiftEvent); break;
                case EVENT_TYPE.TWITCH_RAID: result = m_RaidList.Remove(removeEvent as TwitchRaidEvent); break;
                case EVENT_TYPE.TWITCH_SUBSCRIPTION: result = m_SubscriberList.Remove(removeEvent as TwitchSubscriptionEvent); break;
                case EVENT_TYPE.TWITCH_BOT_CUSTOM: result = m_CustomList.Remove(removeEvent as TwitchBotCustomEvent); break;
                case EVENT_TYPE.TWITCH_BITS: result = m_BitsList.Remove(removeEvent as TwitchBitsEvent); break;
                default: throw new Exception("Invalid (Remove)Event type in TwitchBot: " + removeEvent.Type);
            }

            if(!result)
            {
                throw new Exception("Could not remove event in TwitchBot, Type: " + removeEvent.Type);
            }
        }

    }
}
