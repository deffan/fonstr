﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TwitchTool.Events;
using TwitchTool.Source;

namespace TwitchTool.EventListeners
{
    public class KeyboardEventListener : EventListener
    {
        private List<KeyboardEvent> m_Events;
        private KeyboardListener KeyboardListener = null;
        private List<Key> m_KeyDownList;

        public KeyboardEventListener() 
        {
            m_KeyDownList = new List<Key>();
            m_Events = new List<KeyboardEvent>();
        }

        public override void AddEvent(Event newEvent)
        {
            KeyboardEvent keyboardEvent = newEvent as KeyboardEvent;
            if((keyboardEvent.Data as KeyboardEventData).Keys.Count > 0)    // If no keys, ALL/ANY keys would count/trigger the event
            {
                m_Events.Add(keyboardEvent);
            }
            
            if (!m_Running && m_Events.Count > 0)
            {
                Start();
            }
        }

        public override void RemoveEvent(Event removeEvent)
        {
            m_Events.Remove(removeEvent as KeyboardEvent);
            if (m_Events.Count == 0)
            {
                Shutdown();
            }
        }

        public override void Shutdown()
        {
            if(KeyboardListener != null)
            {
                KeyboardListener.Dispose();
                KeyboardListener = null;
            }
            base.Shutdown();
        }

        public override void Start()
        {
            KeyboardListener = new KeyboardListener();
            KeyboardListener.KeyDown += new RawKeyEventHandler(KeyboardListener_KeyDown);
            KeyboardListener.KeyUp += new RawKeyEventHandler(KeyboardListener_KeyUp);
            base.Start();
        }


        private void KeyboardListener_KeyDown(object sender, RawKeyEventArgs args)
        {
            if(!m_KeyDownList.Contains(args.Key))
            {
                m_KeyDownList.Add(args.Key);

                // Loop all events, and check if the key-combinations match
                CheckCombinations();
            }
        }

        private void CheckCombinations()
        {
            for (int i = 0; i < m_Events.Count; i++)
            {
                KeyboardEventData data = m_Events[i].Data as KeyboardEventData;

                int match = 0;
                for (int j = 0; j < m_KeyDownList.Count; j++)
                {
                    if (data.Keys.Contains(Enum.GetName(typeof(Key), m_KeyDownList[j])))
                    {
                        match++;
                    }
                }

                if(match == data.Keys.Count)
                {
                    m_Events[i].Ready<KeyboardEvent>(null).Start();
                }
            }
        }

        private void KeyboardListener_KeyUp(object sender, RawKeyEventArgs args)
        {
            m_KeyDownList.Remove(args.Key);
        }

    }
}
