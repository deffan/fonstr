﻿using System;
using System.Collections.Generic;
using TwitchTool.Source;

namespace TwitchTool.EventListeners
{
    public class EventListener
    {
        protected bool m_Running;

        public EventListener()
        {

        }

        public bool IsRunning
        {
            get { return m_Running; }
        }

        public virtual void Shutdown()
        {
            m_Running = false;
        }

        public virtual void AddEvent(Event newEvent)
        {

        }

        public virtual void RemoveEvent(Event removeEvent)
        {

        }

        public virtual void Start()
        {
            m_Running = true;
        }

    }
}
