﻿using System.Windows.Controls;
using System.Windows.Media;
using TwitchTool.Source;

namespace TwitchTool.LiveTab
{
    public partial class LogListItem : UserControl
    {
        public LogListItem()
        {
            InitializeComponent();
        }

        public LogListItem(bool even, int type, string text)
        {
            InitializeComponent();
            InformationText.Text = text;
            if (even)
            {
                TheBack.Background = Brushes.Transparent;
            }

            if (type == LOG_LEVEL.WARNING)
            {
                InformationIcon.Source = GlobalHelper.GetImage("chat_bubble.png");
            }
            else if (type == LOG_LEVEL.ERROR)
            {
                InformationIcon.Source = GlobalHelper.GetImage("cancel_on.png");
            }
            else
            {
                InformationIcon.Source = GlobalHelper.GetImage("gear.png");
            }
        }

        public void SetEven()
        {
            TheBack.Background = Brushes.Transparent;
        }
    }
}
