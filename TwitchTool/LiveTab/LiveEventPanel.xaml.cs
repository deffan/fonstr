﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Source;

namespace TwitchTool.LiveTab
{
    /// <summary>
    /// Interaction logic for LiveEventPanel.xaml
    /// </summary>
    public partial class LiveEventPanel : UserControl, LiveTabEvent
    {
        private Event m_Event;
        private LiveTab m_Parent;
        private string m_Status;

        public LiveEventPanel()
        {
            InitializeComponent();
        }

        public LiveEventPanel(Event e, LiveTab parent)
        {
            InitializeComponent();

            m_Event = e;
            m_Parent = parent;
            EventNameLabel.Content = e.Data.Name;

            // Create simple view of the data...
            List<EventVariable> variables = GlobalHelper.EventVariables.GetVariables(m_Event.Type);
            for(int i = 0; i < variables.Count; i++)
            {
                StackPanel p = new StackPanel();
                p.Orientation = Orientation.Horizontal;

                Label varTitle = new Label();
                varTitle.FontWeight = FontWeights.Bold;
                Label varData = new Label();

                varTitle.Content = variables[i].DisplayName;
                varData.Content = m_Event.Data.GetVariableData(variables[i].Id);

                p.Children.Add(varTitle);
                p.Children.Add(varData);
                DetailsPanel.Children.Add(p);
            }

            // For later...
            ApproveButton.Visibility = Visibility.Collapsed;
            DenyButton.Visibility = Visibility.Collapsed;
        }

        public void SetLive()
        {
            m_Status = "live";

            ReplayButton.Visibility = Visibility.Collapsed;
            RemoveButton.Visibility = Visibility.Collapsed;
            AbortButton.Visibility = Visibility.Visible;
        }

        public void SetHistory()
        {
            m_Status = "history";

            AbortButton.Visibility = Visibility.Collapsed;
            ReplayButton.Visibility = Visibility.Visible;
            RemoveButton.Visibility = Visibility.Visible;
        }

        public void SetQueued()
        {
            m_Status = "queue";

            AbortButton.Visibility = Visibility.Collapsed;
            ReplayButton.Visibility = Visibility.Collapsed;
            RemoveButton.Visibility = Visibility.Collapsed;
        }

        public string GetStatus()
        {
            return m_Status;
        }

        public Event GetEvent()
        {
            return m_Event;
        }

        private void ReplayButton_Click(object sender, RoutedEventArgs e)
        {
            if(!m_Event.Window.Running)
            {
                MessageBox.Show("The window this event runs on is not active.", "Replay Event", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                m_Parent.Replay(this);
            }
        }

        private void AbortButton_Click(object sender, RoutedEventArgs e)
        {
            m_Parent.Abort(this);
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            m_Parent.Remove(this);
        }

        private void ApproveButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DenyButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
