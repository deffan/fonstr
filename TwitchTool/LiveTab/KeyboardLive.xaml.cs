﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Source;

namespace TwitchTool.LiveTab
{
    /// <summary>
    /// Interaction logic for KeyboardLive.xaml
    /// </summary>
    public partial class KeyboardLive : UserControl, LiveTabEvent
    {
        private KeyboardEvent m_Event;
        private Action<LiveTabEvent> m_AbortCallback;
        private Action<LiveTabEvent> m_ReplayCallback;
        private Action<LiveTabEvent> m_RemoveCallback;

        public KeyboardLive()
        {
            InitializeComponent();
        }

        public void SetEvent(Event e, Action<LiveTabEvent> abortCallback, Action<LiveTabEvent> replayCallback, Action<LiveTabEvent> removeCallback)
        {
            m_Event = e as KeyboardEvent;
            m_AbortCallback = abortCallback;
            m_RemoveCallback = removeCallback;
            m_ReplayCallback = replayCallback;

            // Set specific event data for display.
            KeyboardEventData data = m_Event.Data as KeyboardEventData;
            EventNameLabel.Content = data.Name;
            EventDataLabel.Content = "";
            for (int i = 0; i < data.Keys.Count; i++)
            {
                EventDataLabel.Content += data.Keys[i];
                if(i + 1 < data.Keys.Count)
                {
                    EventDataLabel.Content += " + ";
                }
            }

        }

        public void SetLive()
        {
            ReplayButton.Visibility = Visibility.Collapsed;
            RemoveButton.Visibility = Visibility.Collapsed;
            AbortButton.Visibility = Visibility.Visible;
        }

        public void SetHistory()
        {
            AbortButton.Visibility = Visibility.Collapsed;
            ReplayButton.Visibility = Visibility.Visible;
            RemoveButton.Visibility = Visibility.Visible;
        }

        public Event GetEvent()
        {
            return m_Event;
        }

        private void ReplayButton_Click(object sender, RoutedEventArgs e)
        {
            m_ReplayCallback(this);
        }

        private void AbortButton_Click(object sender, RoutedEventArgs e)
        {
            m_AbortCallback(this);
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            m_RemoveCallback(this);
        }
    }
}
