﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.LiveTab
{
    public interface LiveTabEvent
    {
        Event GetEvent();
        string GetStatus();
        void SetLive();
        void SetHistory();
    }
}
