﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.LiveTab;
using TwitchTool.Source;

namespace TwitchTool.LiveTab
{
    /// <summary>
    /// Interaction logic for LiveTab.xaml
    /// </summary>
    public partial class LiveTab : UserControl
    {
        public LiveTab()
        {
            InitializeComponent();
            StopButton.IsEnabled = false;
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.Live)
                return;

            MainWindow.StartAllWindows();

            if (MainWindow.Live)
            {
                StartVisually();
            }
            else
            {
                MessageBox.Show("No window started.", "Start", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            if (!MainWindow.Live)
                return;

            MainWindow.StopAllWindows();
            StopVisually();
        }

        private void StartVisually()
        {
            StartButton.IsEnabled = false;
            StopButton.IsEnabled = true;
            StopImgRunning.Visibility = Visibility.Visible;
            StopImgStopped.Visibility = Visibility.Collapsed;
            PlayImgRunning.Visibility = Visibility.Visible;
            PlayImgStopped.Visibility = Visibility.Collapsed;
        }

        private void StopVisually()
        {
            StartButton.IsEnabled = true;
            StopButton.IsEnabled = false;
            StopImgRunning.Visibility = Visibility.Collapsed;
            StopImgStopped.Visibility = Visibility.Visible;
            PlayImgRunning.Visibility = Visibility.Collapsed;
            PlayImgStopped.Visibility = Visibility.Visible;
        }

        public void Stop()
        {
            StopVisually();
        }

        public void EventQueued(Event e)
        {
            // Things only end up here if they are supposed to be displayed
            if (!GlobalHelper.ApplicationSettings.DisplayEvents.Contains(e.Data.Type))
                return;

            // Check if its already there (because we can have it in the Queue and then later in the Delayed-Queue but here its the same visually)
            for(int i = 0; i < QueuePanel.Children.Count; i++)
            {
                LiveEventPanel item = QueuePanel.Children[i] as LiveEventPanel;
                if(item.GetEvent().RandomId.Equals(e.RandomId))
                {
                    return;
                }
            }

            LiveEventPanel panel = new LiveEventPanel(e, this);
            panel.SetQueued();
            QueuePanel.Children.Insert(0, panel);
        }

        public void EventStarted(Event e)
        {
            // Things only end up here if they are supposed to be displayed
            if (!GlobalHelper.ApplicationSettings.DisplayEvents.Contains(e.Data.Type))
                return;

            // Remove from queue 
            Remove(e.RandomId, QueuePanel);

            LiveEventPanel panel = new LiveEventPanel(e, this);
            panel.SetLive();
            LivePanel.Children.Insert(0, panel);
        }

        public void EventFinished(Event e)
        {
            // Things only end up here if they are supposed to be displayed
            if (!GlobalHelper.ApplicationSettings.DisplayEvents.Contains(e.Data.Type))
                return;

            // Remove from live 
            Remove(e.RandomId, LivePanel);

            LiveEventPanel panel = new LiveEventPanel(e, this);
            panel.SetHistory();
            HistoryPanel.Children.Insert(0, panel);
        }

        public bool Remove(LiveTabEvent e)
        {
            if(e.GetStatus().Equals("history"))
            {
                return Remove(e, HistoryPanel);
            }
            else if (e.GetStatus().Equals("live"))
            {
                return Remove(e, LivePanel);
            }
            else if (e.GetStatus().Equals("queue"))
            {
                return Remove(e, QueuePanel);
            }
            return false;
        }

        public bool Remove(string id, StackPanel panel)
        {
            for (int i = 0; i < panel.Children.Count; i++)
            {
                if (id.Equals((panel.Children[i] as LiveTabEvent).GetEvent().RandomId))
                {
                    panel.Children.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        // Remove old in history
        public void WindowStarted(string id)
        {
            for (int i = HistoryPanel.Children.Count - 1; i >= 0; i--)
            {
                Event e = (HistoryPanel.Children[i] as LiveTabEvent).GetEvent();
                if (id.Equals(e.Window.WindowObject.ID))
                {
                    HistoryPanel.Children.RemoveAt(i);
                }
            }
        }

        public bool Remove(LiveTabEvent e, StackPanel panel)
        {
            for (int i = 0; i < panel.Children.Count; i++)
            {
                if (e.GetEvent().RandomId.Equals((panel.Children[i] as LiveTabEvent).GetEvent().RandomId))
                {
                    panel.Children.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        public void Replay(LiveTabEvent e)
        {
            if (!MainWindow.Live)
                return;

            Remove(e, HistoryPanel);
            e.GetEvent().Restart();
        }

        public void Abort(LiveTabEvent e)
        {
            e.GetEvent().Abort();
        }

        private void ClearHistoryButton_Click(object sender, RoutedEventArgs e)
        {
            HistoryPanel.Children.Clear();
        }

        private void AbortAllLiveButton_Click(object sender, RoutedEventArgs e)
        {
            for (int i = LivePanel.Children.Count - 1; i >= 0; i--)
            {
               (LivePanel.Children[i] as LiveTabEvent).GetEvent().Abort();
            }
        }

        private void CancelAllQueuedButton_Click(object sender, RoutedEventArgs e)
        {
            for (int i = QueuePanel.Children.Count - 1; i >= 0; i--)
            {
                QueuePanel.Children.RemoveAt(i);
            }
            MainWindow.EventMaster.ClearQueues();
        }

        private void ClearLogButton_Click(object sender, RoutedEventArgs e)
        {
            LogPanel.Children.Clear();
        }
    }
}
