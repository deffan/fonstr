﻿using System.Windows.Controls;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.ComponentsEditor
{
    /// <summary>
    /// Interaction logic for CloningEditorComponent.xaml
    /// </summary>
    public partial class CloningEditorComponent : UserControl, IEditorComponent
    {
        public CloningComponent m_Component;

        public CloningEditorComponent()
        {
            InitializeComponent();
        }
        
        public CloningEditorComponent(CloningComponent component)
        {
            InitializeComponent();
            m_Component = component;
            ComponentHeader.Init(m_Component, "Cloning", true);
            LifetimeVariableControl.Update(new TextData(""), true);
            LifetimeVariableControl.IsEnabled = false;
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        public void SetSettings(CloningComponent.CloningSettings settings)
        {
            // Lifetime
            if(settings.LifeTime == CloningComponent.LIFETIME_FOREVER)
            {
                Lifetime_Forever.IsChecked = true;
                LifetimeVariableControl.IsEnabled = false;
            }
            else if (settings.LifeTime == CloningComponent.LIFETIME_EVENT)
            {
                Lifetime_Event.IsChecked = true;
                LifetimeVariableControl.IsEnabled = false;
            }
            else if (settings.LifeTime == CloningComponent.LIFETIME_DATA)
            {
                Lifetime_Value.IsChecked = true;
            }

            if(settings.LifeTimeData == null)
            {
                LifetimeVariableControl.Update(new TextData(""), true);
            }
            else
            {
                LifetimeVariableControl.Update(settings.LifeTimeData, true);
            }

            // Redirection of commands
            if(settings.Redirect == CloningComponent.REDIRECT_TO_CLONE)
            {
                Redirect_Cloned.IsChecked = true;
            }
            else if (settings.Redirect == CloningComponent.REDIRECT_TO_ORIGINAL)
            {
                Redirect_Original.IsChecked = true;
            }
            else if (settings.Redirect == CloningComponent.REDIRECT_TO_BOTH)
            {
                Redirect_Both.IsChecked = true;
            }

            // Removal of cloning component upon cloning
            if(settings.RemoveCloneComponent)
            {
                Remove_Yes.IsChecked = true;
            }
            else
            {
                Remove_No.IsChecked = true;
            }

        }

        public CloningComponent.CloningSettings GetSettings()
        {
            CloningComponent.CloningSettings settings = new CloningComponent.CloningSettings();

            if(Lifetime_Forever.IsChecked == true)
            {
                settings.LifeTime = CloningComponent.LIFETIME_FOREVER;
            }
            else if(Lifetime_Event.IsChecked == true)
            {
                settings.LifeTime = CloningComponent.LIFETIME_EVENT;
            }
            else if(Lifetime_Value.IsChecked == true)
            {
                settings.LifeTime = CloningComponent.LIFETIME_DATA;
                settings.LifeTimeData = LifetimeVariableControl.GetData();
            }

            if(Redirect_Cloned.IsChecked == true)
            {
                settings.Redirect = CloningComponent.REDIRECT_TO_CLONE;
            }
            else if (Redirect_Original.IsChecked == true)
            {
                settings.Redirect = CloningComponent.REDIRECT_TO_ORIGINAL;
            }
            else if (Redirect_Both.IsChecked == true)
            {
                settings.Redirect = CloningComponent.REDIRECT_TO_BOTH;
            }

            if (Remove_Yes.IsChecked == true)
            {
                settings.RemoveCloneComponent = true;
            }
            else
            {
                settings.RemoveCloneComponent = false;
            }

            return settings;
        }

        private void Lifetime_Value_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Lifetime_Value.IsChecked == false)
                return;

            LifetimeVariableControl.IsEnabled = true;
            e.Handled = true;
        }

        private void Lifetime_Value_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (Lifetime_Value.IsChecked == true)
                return;

            LifetimeVariableControl.IsEnabled = false;
            e.Handled = true;
        }
    }
}
