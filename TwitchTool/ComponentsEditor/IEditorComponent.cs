﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TwitchTool.UserControls;

namespace TwitchTool.ComponentsEditor
{
    public interface IEditorComponent
    {
        ComponentHeader GetComponentHeader();
    }
}
