﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Animator;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.AnimationsEditor
{
    public partial class AnimatorEditorComponent : UserControl, IEditorComponent
    {
        public AnimatorComponent m_Component;

        public AnimatorEditorComponent()
        {
            InitializeComponent();
        }

        public AnimatorEditorComponent(AnimatorComponent component)
        {
            InitializeComponent();
            m_Component = component;
            ComponentHeader.Init(m_Component, "Animator", true);
            ComponentHeader.SetOnRemoveQuestion("This will permanently delete all animations on this Animator.\nThis action cannot be undone.\nContinue?");
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void OpenAnimatorButton_Click(object sender, RoutedEventArgs e)
        {
            GlobalHelper.EditorWindow.OpenTab(m_Component);
        }
    }
}
