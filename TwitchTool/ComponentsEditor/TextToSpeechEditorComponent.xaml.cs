﻿using System.Collections.Generic;
using System.Speech.Synthesis;
using System.Windows;
using System.Windows.Controls;
using TwitchTool.Components;
using TwitchTool.UserControls;

namespace TwitchTool.ComponentsEditor
{
    public partial class TextToSpeechEditorComponent : UserControl, IEditorComponent
    {
        public TextToSpeechComponent m_Component;

        public TextToSpeechEditorComponent(TextToSpeechComponent component)
        {
            m_Component = component;
            InitializeComponent();
            ComponentHeader.Init(m_Component, "TextToSpeech", true);

            VoiceCombo.Items.Add("Random");
            List<InstalledVoice> voices = m_Component.GetInstalledVoices();
            for(int i = 0; i < voices.Count; i++)
            {
                VoiceCombo.Items.Add(voices[i].VoiceInfo.Name);
            }
            VoiceCombo.SelectedIndex = 0;
        }

        ComponentHeader IEditorComponent.GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void TestButton_Click(object sender, RoutedEventArgs e)
        {
            if(TestTextBox.Text.Length > 0)
            {
                m_Component.Speak(TestTextBox.Text);
            }
        }

        public void SelectVoice(string voicename)
        {
            for(int i = 0; i < VoiceCombo.Items.Count; i++)
            {
                if(VoiceCombo.Items[i].Equals(voicename))
                {
                    VoiceCombo.SelectedIndex = i;
                    break;
                }
            }
        }

        private void VoiceCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(VoiceCombo.SelectedIndex == 0)
            {
                m_Component.SetVoice("Random");
                VoiceName.Content = "?";
                VoiceGender.Content = "?";
                VoiceAge.Content = "?";
            }
            else
            {
                m_Component.SetVoice(VoiceCombo.SelectedItem as string);
                VoiceName.Content = m_Component.GetSpeechSynthesizer().Voice.Name;
                VoiceGender.Content = m_Component.GetSpeechSynthesizer().Voice.Gender;
                VoiceAge.Content = m_Component.GetSpeechSynthesizer().Voice.Age;
            }
        }
    }
}
