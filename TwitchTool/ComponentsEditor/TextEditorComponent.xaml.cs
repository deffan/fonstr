﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for TextComponent.xaml
    /// </summary>
    public partial class TextEditorComponent : UserControl, IEditorComponent
    {
        public TextComponent m_Component;
        private bool m_UpdateEditorComponentToggle;

        public TextEditorComponent(TextComponent component)
        {
            InitializeComponent();
            m_Component = component;
            ComponentHeader.Init(m_Component, "Text", true);
            m_UpdateEditorComponentToggle = false;
            radio_Left.IsChecked = true;
            RadioWrappingOn.IsChecked = true;

            EffectCombo.Items.Add("None");
            //EffectCombo.Items.Add("Shadow");
            EffectCombo.Items.Add("Stroke");
            EffectCombo.SelectedIndex = 0;

            m_Component.GetTextBrushPicker().SetCallback(UpdateTextBrush);
            m_Component.GetEffectBrushPicker().SetCallback(UpdateEffectBrush);
            TextBrushPickerPanel.Children.Clear();
            EffectBrushPanel.Children.Clear();
            TextBrushPickerPanel.Children.Add(m_Component.GetTextBrushPicker());
            EffectBrushPanel.Children.Add(m_Component.GetEffectBrushPicker());

            foreach (FontFamily F in Fonts.SystemFontFamilies)
            {
                FontComboBox.Items.Add(F);
            }
            FontComboBox.SelectedIndex = 0;
        }

        public void SetText(string txt)
        {
            m_UpdateEditorComponentToggle = true;
            TheTextBox.Text = txt;
            m_UpdateEditorComponentToggle = false;
        }

        public void SetAll(FontFamily font, double fontSize, bool bold, bool italic, bool underline, bool textwrapping, TextAlignment alignment, int effect, double strokesize, BrushPickerControl textBrush, BrushPickerControl effectBrush, bool twitchEmotes)
        {
            m_UpdateEditorComponentToggle = true;

            for(int i = 0; i < FontComboBox.Items.Count; i++)
            {
                if(FontComboBox.Items[i].Equals(font))
                {
                    FontComboBox.SelectedIndex = i;
                    break;
                }
            }

            FontSizeTextBox.Text = Convert.ToString(fontSize);
            checkBox_Bold.IsChecked = bold;
            checkBox_Italic.IsChecked = italic;
            checkBox_Underline.IsChecked = underline;
            if(textwrapping)
            {
                RadioWrappingOn.IsChecked = true;
            }
            else
            {
                RadioWrappingOff.IsChecked = true;
            }
            TwitchEmotesCheckbox.IsChecked = twitchEmotes;

            if (alignment == TextAlignment.Left)
                radio_Left.IsChecked = true;
            else if (alignment == TextAlignment.Right)
                radio_Right.IsChecked = true;
            else
                radio_Center.IsChecked = true;

            EffectCombo.SelectedIndex = effect;
            EffectSizeTextBox.Text = Convert.ToString(strokesize);

            TextBrushPickerPanel.Children.Clear();
            EffectBrushPanel.Children.Clear();
            TextBrushPickerPanel.Children.Add(textBrush);
            EffectBrushPanel.Children.Add(effectBrush);

            switch (textBrush.GetMode())
            {
                case BrushPickerControl.MODE.COLOR: textBrush.BrushCombo.SelectedIndex = 0; break;
                case BrushPickerControl.MODE.GRADIENT: textBrush.BrushCombo.SelectedIndex = 1; break;
                case BrushPickerControl.MODE.IMAGE: textBrush.BrushCombo.SelectedIndex = 2; break;
            }

            switch (effectBrush.GetMode())
            {
                case BrushPickerControl.MODE.COLOR: effectBrush.BrushCombo.SelectedIndex = 0; break;
                case BrushPickerControl.MODE.GRADIENT: effectBrush.BrushCombo.SelectedIndex = 1; break;
                case BrushPickerControl.MODE.IMAGE: effectBrush.BrushCombo.SelectedIndex = 2; break;
            }

            if (EffectCombo.SelectedIndex == 1)  // Stroke
            {
                EffectBrushPanel.Visibility = Visibility.Visible;
                EffectSizeTextBox.IsEnabled = true;
            }
            else
            {
                EffectBrushPanel.Visibility = Visibility.Collapsed;
                EffectSizeTextBox.IsEnabled = false;
            }

            textBrush.SetCallback(UpdateTextBrush);
            effectBrush.SetCallback(UpdateEffectBrush);

            m_UpdateEditorComponentToggle = false;
        }

        public BrushPickerControl GetTextBrushPicker()
        {
            return TextBrushPickerPanel.Children[0] as BrushPickerControl;
        }

        public BrushPickerControl GetEffectBrushPicker()
        {
            return EffectBrushPanel.Children[0] as BrushPickerControl;
        }

        private void FontComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (FontComboBox.SelectedIndex >= 0)
                {
                    m_Component.SetFont(FontComboBox.Items[FontComboBox.SelectedIndex] as FontFamily);
                }
            }
            catch (Exception) { }
            e.Handled = true;
        }

        private void FontSizeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                double size = Convert.ToDouble(FontSizeTextBox.Text, CultureInfo.InvariantCulture);
                if(size > 0 && size < 500)
                {
                    m_Component.SetTextSize(size);
                }
            }
            catch (Exception) { }
        }

        private void CheckBox_Bold_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetBold(false);
        }

        private void CheckBox_Bold_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetBold(true);
        }

        private void CheckBox_Underline_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetUnderline(true);
        }

        private void CheckBox_Underline_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetUnderline(false);
        }

        private void CheckBox_Italic_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetItalic(false);
        }

        private void CheckBox_Italic_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetItalic(true);
        }

        private void TheTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetText(TheTextBox.Text);
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void Radio_Left_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetTextAlignment(TextAlignment.Left);
        }

        private void Radio_Center_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetTextAlignment(TextAlignment.Center);
        }

        private void Radio_Right_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetTextAlignment(TextAlignment.Right);
        }

        private void RadioWrappingOn_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            e.Handled = true;
            m_Component.SetTextWrapping(true);
        }

        private void RadioWrappingOff_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetTextWrapping(false);
        }

        private void EffectCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            if (EffectCombo.SelectedIndex == 1)  // Stroke
            {
                EffectBrushPanel.Visibility = Visibility.Visible;
                EffectSizeTextBox.IsEnabled = true;
                UpdateEffectBrush();
            }
            else
            {
                EffectBrushPanel.Visibility = Visibility.Collapsed;
                EffectSizeTextBox.IsEnabled = false;
            }
            e.Handled = true;
            m_Component.SetEffect(EffectCombo.SelectedIndex);
        }

        private void UpdateTextBrush()
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetTextBrush(GetTextBrushPicker().GetBrush());
        }

        private void UpdateEffectBrush()
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetStrokeBrush(GetEffectBrushPicker().GetBrush());
        }

        private void EffectSizeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                double size = Convert.ToDouble(EffectSizeTextBox.Text, CultureInfo.InvariantCulture);
                if (size >= 0 && size < 500)
                {
                    m_Component.SetStrokeSize(size);
                }
            }
            catch (Exception) { }
        }

        private void TwitchEmotesCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            // Check if twitch-emotes exist.
            if (!GlobalHelper.SQL.HasTwitchEmotes())
            {
                MessageBox.Show("No twitch emoticons detected.\nGo to Settings Tab > Twitch Tab to download and install.", "Twitch Emoticons", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            e.Handled = true;
            m_Component.SetTwitchEmoticons(true);
        }

        private void TwitchEmotesCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;
            e.Handled = true;
            m_Component.SetTwitchEmoticons(false);
        }
    }
}
