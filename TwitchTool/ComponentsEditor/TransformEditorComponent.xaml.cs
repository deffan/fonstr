﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for TransformComponent.xaml
    /// </summary>
    public partial class TransformEditorComponent : UserControl, IEditorComponent
    {
        public TransformComponent m_Component;
        private bool m_UpdateEditorComponentToggle;

        public TransformEditorComponent(TransformComponent component)
        {
            m_Component = component;
            InitializeComponent();

            m_UpdateEditorComponentToggle = false;
            ComponentHeader.Init(component, "Transform", false);

            RotationOriginCombo.Items.Add("Top Left");
            RotationOriginCombo.Items.Add("Top Right");
            RotationOriginCombo.Items.Add("Center");
            RotationOriginCombo.Items.Add("Bottom Left");
            RotationOriginCombo.Items.Add("Bottom Right");
            RotationOriginCombo.SelectedIndex = 2;

            ScaleOriginCombo.Items.Add("Top Left");
            ScaleOriginCombo.Items.Add("Center");
            ScaleOriginCombo.SelectedIndex = 0;
        }

        public void SetXCoordinate(double x)
        {
            m_UpdateEditorComponentToggle = true;
            XTextBox.Text = Convert.ToString((int)x);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetYCoordinate(double y)
        {
            m_UpdateEditorComponentToggle = true;
            YTextBox.Text = Convert.ToString((int)y);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetWidth(double x)
        {
            m_UpdateEditorComponentToggle = true;
            XSizeTextBox.Text = Convert.ToString((int)x);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetHeight(double y)
        {
            m_UpdateEditorComponentToggle = true;
            YSizeTextBox.Text = Convert.ToString((int)y);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetRotation(double rotation)
        {
            m_UpdateEditorComponentToggle = true;
            RotationBox.Text = Convert.ToString((int)rotation);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetScale(double x, double y)
        {
            m_UpdateEditorComponentToggle = true;
            XScale.Value = Convert.ToDecimal(x);
            YScale.Value = Convert.ToDecimal(y);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetSkew(double x, double y)
        {
            m_UpdateEditorComponentToggle = true;
            XSkewTextBox.Text = Convert.ToString(x);
            YSkewTextBox.Text = Convert.ToString(y);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetRotationOrigin(int origin)
        {
            m_UpdateEditorComponentToggle = true;
            RotationOriginCombo.SelectedIndex = origin;
            m_UpdateEditorComponentToggle = false;
        }

        public void SetScaleOrigin(int origin)
        {
            m_UpdateEditorComponentToggle = true;
            ScaleOriginCombo.SelectedIndex = origin;
            m_UpdateEditorComponentToggle = false;
        }

        private void XTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if(XTextBox.Text.Length > 0)
                {
                    m_Component.SetXCoordinate(Convert.ToDouble(XTextBox.Text, CultureInfo.InvariantCulture));
                }
            }
            catch(Exception) { }
        }

        private void YTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (YTextBox.Text.Length > 0)
                {
                    m_Component.SetYCoordinate(Convert.ToDouble(YTextBox.Text, CultureInfo.InvariantCulture));
                }
            }
            catch (Exception) { }
        }

        private void XSizeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (XSizeTextBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(XSizeTextBox.Text);
                    if(value > -1)
                    {
                        m_Component.SetWidth(value);
                    }
                }
            }
            catch (Exception) { }
        }

        private void YSizeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (YSizeTextBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(YSizeTextBox.Text);
                    if (value > -1)
                    {
                        m_Component.SetHeight(value);
                    }
                }
            }
            catch (Exception) { }
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void RotationBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (RotationBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(RotationBox.Text);
                    if (value >= -360 && value <= 360)
                    {
                        m_Component.SetRotation(value);
                    }
                }
            }
            catch (Exception) { }
        }

        private void RotationOriginCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            m_Component.SetRotationOrigin(RotationOriginCombo.SelectedIndex);
        }

        private void ScaleOriginCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            m_Component.SetScaleOrigin(ScaleOriginCombo.SelectedIndex);
        }

        private void XSkewTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (XSkewTextBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(XSkewTextBox.Text);
                    if (value > -1)
                    {
                        m_Component.SetSkew(value, m_Component.GetSkew().Height);
                    }
                }
            }
            catch (Exception) { }
        }

        private void YSkewTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (YSkewTextBox.Text.Length > 0)
                {
                    int value = Convert.ToInt32(YSkewTextBox.Text);
                    if (value > -1)
                    {
                        m_Component.SetSkew(m_Component.GetSkew().Width, value);
                    }
                }
            }
            catch (Exception) { }
        }

        private void XScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetScale(Convert.ToDouble(XScale.Value.Value, CultureInfo.InvariantCulture), m_Component.GetScale().Height);
        }

        private void YScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetScale(m_Component.GetScale().Width, Convert.ToDouble(YScale.Value.Value, CultureInfo.InvariantCulture));
        }
    }
}
