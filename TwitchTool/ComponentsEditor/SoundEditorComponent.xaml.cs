﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Components;
using TwitchTool.UserControls;

namespace TwitchTool.ComponentsEditor
{
    /// <summary>
    /// Interaction logic for SoundEditorComponent.xaml
    /// </summary>
    public partial class SoundEditorComponent : UserControl, IEditorComponent
    {
        public SoundComponent m_Component;
        private bool m_UpdateEditorComponentToggle;

        public SoundEditorComponent(SoundComponent component)
        {
            m_Component = component;
            m_UpdateEditorComponentToggle = true;
            InitializeComponent();
            m_UpdateEditorComponentToggle = false;
            ComponentHeader.Init(m_Component, "Sound", true);
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void LoadSoundFromFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select sound file";
            op.Multiselect = false;

            if (op.ShowDialog() == true)
            {
                m_Component.SetSoundFile(op.FileName);
                SoundLoadedTextBox.Text = op.FileName;
            }
        }

        public void SetData(string path, double volume)
        {
            m_UpdateEditorComponentToggle = true;

            SoundLoadedTextBox.Text = path;
            VolumeSlider.Value = volume;
            VolumeSliderLabel.Content = string.Format("({0}%)", VolumeSlider.Value * 100);

            m_UpdateEditorComponentToggle = false;
        }

        private void VolumeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetVolume(VolumeSlider.Value);
            VolumeSliderLabel.Content = string.Format("({0}%)", Math.Round(VolumeSlider.Value * 100, 1));
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            m_Component.Play();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            m_Component.Stop();
        }

        private void SoundLoadedTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle || SoundLoadedTextBox.Text.Length < 5 || SoundLoadedTextBox.Text.LastIndexOf('.') < 1)
                return;

            m_Component.SetSoundFile(SoundLoadedTextBox.Text);
        }
    }
}

 

        
