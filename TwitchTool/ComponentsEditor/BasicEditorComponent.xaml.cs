﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for BasicComponent.xaml
    /// </summary>
    public partial class BasicEditorComponent : UserControl, IEditorComponent
    {
        public BasicComponent m_Component;
        private bool m_UpdateEditorComponentToggle;

        public BasicEditorComponent(BasicComponent component)
        {
            m_Component = component;
            InitializeComponent();
            m_UpdateEditorComponentToggle = false;
            ComponentHeader.Init(m_Component, "General", false);
            BrushPicker.SetCallback(BrushUpdated);
            BrushPicker.SetSolidColor(Brushes.Transparent);
        }

        public BrushPickerControl BrushPicker
        {
            get { return BrushPickerPanel.Children[0] as BrushPickerControl; }
        }

        public void SetNewBrushPicker(BrushPickerControl b)
        {
            BrushPickerPanel.Children.Clear();
            BrushPickerPanel.Children.Add(b);
            
            switch (b.GetMode())
            {
                case BrushPickerControl.MODE.COLOR: b.BrushCombo.SelectedIndex = 0; break;
                case BrushPickerControl.MODE.GRADIENT: b.BrushCombo.SelectedIndex = 1; break;
                case BrushPickerControl.MODE.IMAGE: b.BrushCombo.SelectedIndex = 2; break;
            }

            b.SetCallback(BrushUpdated);
        }

        public void SetText(string txt)
        {
            m_UpdateEditorComponentToggle = true;
            NameTextBox.Text = txt;
            m_UpdateEditorComponentToggle = false;
        }

        public void SetTransparency(decimal transparent)
        {
            m_UpdateEditorComponentToggle = true;
            TransparencyBox.Text = Convert.ToString(transparent);
            m_UpdateEditorComponentToggle = false;
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (NameTextBox.Text.Length > 0)
                {
                    SetObjectName(NameTextBox.Text);
                }
            }
            catch (Exception) { }
        }

        private void NameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            // An empty name is unacceptable, so we set it to a default "New object" if empty
            if (NameTextBox.Text.Length == 0)
            {
                SetObjectName("New object");
            }
        }

        private void SetObjectName(string name)
        {
            m_Component.SetObjectName(name);
            StackPanel st = m_Component.m_Object.TreeViewItem.Header as StackPanel;
            Label lb = st.Children[1] as Label;
            lb.Content = name;
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void BrushUpdated()
        {
            m_Component.SetBackgroundBrush(BrushPicker.GetBrush());
        }

        public void SetActive(bool active)
        {
            m_UpdateEditorComponentToggle = true;
            ActiveCheckBox.IsChecked = active;
            m_UpdateEditorComponentToggle = false;
        }

        private void ActiveCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetActive(true);
        }

        private void ActiveCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetActive(false);
        }

        private void TransparencyBox_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            m_Component.SetTransparency(TransparencyBox.Value.Value);
        }
    }
}
