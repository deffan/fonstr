﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Components;
using TwitchTool.UserControls;

namespace TwitchTool.ComponentsEditor
{
    /// <summary>
    /// Interaction logic for YoutubeEditorComponent.xaml
    /// </summary>
    public partial class YoutubeEditorComponent : UserControl, IEditorComponent
    {
        public YoutubeComponent m_Component;
        private bool m_Toggle;

        public YoutubeEditorComponent(YoutubeComponent component)
        {
            InitializeComponent();
            m_Component = component;
            ComponentHeader.Init(m_Component, "Youtube", true);
            m_Toggle = false;
            SetDurationTime(0);
            SetStartTime(0);
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(UrlTextBox.Text.Length > 4)
            {
                m_Component.SetUrl(UrlTextBox.Text);
                m_Component.PlayFromEditor();
            }
        }

        public void SetDurationTime(double seconds)
        {
            m_Toggle = true;
            LimitTimeTextBox.Text = Convert.ToString(seconds);
            if(seconds > 0)
            {
                LimitCheckBox.IsChecked = true;
                LimitTimeTextBox.IsEnabled = true;
            }
            else
            {
                LimitCheckBox.IsChecked = false;
                LimitTimeTextBox.IsEnabled = false;
            }
            m_Toggle = false;
        }

        public void SetStartTime(double seconds)
        {
            m_Toggle = true;
            StartTimeTextBox.Text = Convert.ToString(seconds);
            if (seconds > 0)
            {
                StartCheckBox.IsChecked = true;
                StartTimeTextBox.IsEnabled = true;
            }
            else
            {
                StartCheckBox.IsChecked = false;
                StartTimeTextBox.IsEnabled = false;
            }
            m_Toggle = false;
        }

        private void LimitCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (m_Toggle)
                return;

            LimitTimeTextBox.IsEnabled = true;
        }

        private void LimitCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_Toggle)
                return;

            LimitTimeTextBox.IsEnabled = false;

            m_Toggle = true;
            m_Component.SetDurationTime(0);
            m_Toggle = false;
        }

        public void SetUrl(string url)
        {
            m_Toggle = true;
            UrlTextBox.Text = url;
            m_Toggle = false;
        }

        private void LimitTimeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_Toggle)
                return;

            try
            {
                int value = Convert.ToInt32(LimitTimeTextBox.Text);
                m_Component.SetDurationTime(value);
            }
            catch (Exception) { }
        }

        private void StartCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (m_Toggle)
                return;

            StartTimeTextBox.IsEnabled = true;
        }

        private void StartCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_Toggle)
                return;

            StartTimeTextBox.IsEnabled = false;
            m_Component.SetStartTime(0);
        }

        private void StartTimeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_Toggle)
                return;

            try
            {
                int value = Convert.ToInt32(StartTimeTextBox.Text);
                m_Component.SetStartTime(value);
            }
            catch (Exception) { }
        }
    }
}
