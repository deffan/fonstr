﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for ImageComponent.xaml
    /// </summary>
    public partial class ImageEditorComponent : UserControl, IEditorComponent
    {
        public ImageComponent m_Component;
        private bool m_Toggle;

        public ImageEditorComponent(ImageComponent component)
        {
            InitializeComponent();
            m_Component = component;
            ComponentHeader.Init(m_Component, "Image", true);
            m_Toggle = false;
        }

        public void SetImageInformation(string path, bool url, int width, int height)
        {
            m_Toggle = true;
            ImageLoadedTextBox.Text = path;
            m_Toggle = false;
            OriginalXSizeTextBox.Text = Convert.ToString(width);
            OriginalYSizeTextBox.Text = Convert.ToString(height);
        }

        private void LoadImageFromFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select image file";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";

            if (op.ShowDialog() == true)
            {
                m_Component.SetImage(op.FileName, false);
            }
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void ImageLoadedTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_Toggle || ImageLoadedTextBox.Text.Length < 5 || ImageLoadedTextBox.Text.LastIndexOf('.') < 1)
                return;

            m_Component.SetImage(ImageLoadedTextBox.Text, null);
        }
    }
}
