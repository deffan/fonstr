﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for CommandListenerComponent.xaml
    /// </summary>
    public partial class CommandListenerEditorComponent : UserControl, IEditorComponent
    {
        public CommandListenerComponent m_Component;

        public CommandListenerEditorComponent(CommandListenerComponent component)
        {
            InitializeComponent();
            m_Component = component;
            ComponentHeader.Init(m_Component, "Command Listener", true);

            List<CommandListenerComponent.ListeningComponent> components = m_Component.GetComponentsThatAreListening();
            for(int i = 0; i < components.Count; i++)
            {
                CheckBoxListItem item = new CheckBoxListItem(components[i]);
                ComponentsListBox.Items.Add(item);
            }
        }

        public void Refresh()
        {
            List<string> tmp = new List<string>();
            for (int i = 0; i < ComponentsListBox.Items.Count; i++)
            {
                CheckBoxListItem item = ComponentsListBox.Items[i] as CheckBoxListItem;

                // Update the name each time. If several components of the same type exists, we should make it a bit less confusing by adding (1), (2) next to the name...
                int occurances = 0;
                for(int n = 0; n < tmp.Count; n++)
                {
                    if (tmp.Equals(item.NameLabel.Content.ToString()))
                        occurances++;
                }
                tmp.Add(item.NameLabel.Content.ToString());

                if(occurances > 0)
                {
                    item.NameLabel.Content = item.m_Component.Component.GetType().Name + " (" + occurances + ")";
                }

                item.Refresh();
            }
            IdTextBox.Text = m_Component.ListenerID;
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        private void CheckAllButton_Click(object sender, RoutedEventArgs e)
        {
            for(int i = 0; i < ComponentsListBox.Items.Count; i++)
            {
                (ComponentsListBox.Items[i] as CheckBoxListItem).CheckBox.IsChecked = true;
            }
        }

        private void UncheckAllButton_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < ComponentsListBox.Items.Count; i++)
            {
                (ComponentsListBox.Items[i] as CheckBoxListItem).CheckBox.IsChecked = false;
            }
        }

        private void IdTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_Component.ListenerID = IdTextBox.Text;
        }
    }

}
