﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.UserControls;

namespace TwitchTool.AnimationsEditor
{
    public partial class MoveEditorAnimation : UserControl, IEditorComponent
    {
        public MoveEditorAnimation()
        {
            InitializeComponent();
        }

        public MoveAnimation m_Component;
        private bool m_UpdateEditorComponentToggle;

        public MoveEditorAnimation(MoveAnimation component)
        {
            InitializeComponent();
            m_UpdateEditorComponentToggle = false;
            m_Component = component;

            ComponentHeader.Init(m_Component, "Move Animation", true);
            //GeneralAnimationSettings.Init(m_Component.m_GeneralAnimationSettings);
        }

        public void RefreshGeneralAnimationSettings()
        {
           // GeneralAnimationSettings.Refresh();
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        public void SetXCoordinate(double x)
        {
            m_UpdateEditorComponentToggle = true;
            XTextBox.Text = Convert.ToString(x);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetYCoordinate(double y)
        {
            m_UpdateEditorComponentToggle = true;
            YTextBox.Text = Convert.ToString(y);
            m_UpdateEditorComponentToggle = false;
        }

        public void SetMoveTime(double wait)
        {
            m_UpdateEditorComponentToggle = true;
            MoveTimeControl.Value = Convert.ToDecimal(wait);
            m_UpdateEditorComponentToggle = false;
        }

        private void MoveTimeControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                m_Component.SetMoveTime(Convert.ToDouble(MoveTimeControl.Value));
            }
            catch (Exception) { }
        }

        private void XTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (XTextBox.Text.Length > 0)
                {
                    m_Component.SetXCoordinate(Convert.ToDouble(XTextBox.Text));
                }
            }
            catch (Exception) { }
        }

        private void YTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                if (YTextBox.Text.Length > 0)
                {
                    m_Component.SetYCoordinate(Convert.ToDouble(YTextBox.Text));
                }
            }
            catch (Exception) { }
        }
    }
}
