﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.UserControls;

namespace TwitchTool.AnimationsEditor
{
    public partial class AnimationOrderEditorComponent : UserControl, IEditorComponent
    {
        public AnimationOrderEditorComponent()
        {
            InitializeComponent();
        }

        public AnimationOrderComponent m_Component;
        private bool m_UpdateEditorComponentToggle;

        public AnimationOrderEditorComponent(AnimationOrderComponent component)
        {
            InitializeComponent();
            m_UpdateEditorComponentToggle = false;
            m_Component = component;

            ComponentHeader.Init(m_Component, "Animation Order", false);
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

    }
}
