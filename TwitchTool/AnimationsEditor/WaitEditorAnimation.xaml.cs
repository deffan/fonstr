﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Components;
using TwitchTool.ComponentsEditor;
using TwitchTool.UserControls;

namespace TwitchTool.AnimationsEditor
{
    public partial class WaitEditorAnimation : UserControl, IEditorComponent
    {
        public WaitEditorAnimation()
        {
            InitializeComponent();
        }

        public WaitAnimation m_Component;
        private bool m_UpdateEditorComponentToggle;

        public WaitEditorAnimation(WaitAnimation component)
        {
            InitializeComponent();
            m_UpdateEditorComponentToggle = false;
            m_Component = component;

            ComponentHeader.Init(m_Component, "Wait Animation", true);
        }

        public ComponentHeader GetComponentHeader()
        {
            return ComponentHeader;
        }

        public void SetWaitTime(double wait)
        {
            m_UpdateEditorComponentToggle = true;
            try
            {
                WaitTimeControl.Value = Convert.ToDecimal(wait);
            }
            catch (Exception) { }
            m_UpdateEditorComponentToggle = false;
        }

        private void DecimalUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_UpdateEditorComponentToggle)
                return;

            try
            {
                m_Component.SetWaitTime(Convert.ToDouble(WaitTimeControl.Value));
            }
            catch(Exception){}
        }
    }
}
