﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for ScaleSettings.xaml
    /// </summary>
    public partial class ScaleSettings : UserControl
    {
        private AnimatorBox m_Selected;

        public ScaleSettings()
        {
            InitializeComponent();
        }

        public void SetSelected(AnimatorBox selected)
        {
            m_Selected = selected;

            ScaleAnimationData data = m_Selected.AnimationData as ScaleAnimationData;
            ToX.Update(data.ToXScale, true);
            ToY.Update(data.ToYScale, true);

            int pos = m_Selected.Track.GetBoxPosition(m_Selected);
            if (pos == 0)
            {
                Size s = m_Selected.Track.TheObject.GetTransformComponent().GetScale();
                FromX.Text = Convert.ToString(s.Width);
                FromY.Text = Convert.ToString(s.Height);
            }
            else
            {
                AnimatorBox prev = m_Selected.Track.GetBox(pos - 1);
                ScaleAnimationData prevData = prev.AnimationData as ScaleAnimationData;

                if (prevData.ToXScale.IsDataVariable)
                {
                    DataVariable var = prevData.ToXScale.GetDataVariable();
                    if (var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevData.ToXScale.Data = "0";
                        prevData.ToXScale.Type = "text";
                        prevData.ToXScale.IsText = true;
                    }
                    else
                    {
                        FromX.Text = prevData.ToXScale.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromX.Text = prevData.ToXScale.GetStringData();
                }

                if (prevData.ToYScale.IsDataVariable)
                {
                    DataVariable var = prevData.ToYScale.GetDataVariable();
                    if (var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevData.ToYScale.Data = "0";
                        prevData.ToYScale.Type = "text";
                        prevData.ToYScale.IsText = true;
                    }
                    else
                    {
                        FromY.Text = prevData.ToYScale.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromY.Text = prevData.ToYScale.GetStringData();
                }

            }
        }
    }
}
