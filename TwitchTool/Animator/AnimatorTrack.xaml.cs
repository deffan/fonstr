﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animator;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    /// <summary>
    /// Interaction logic for AnimatorTrack.xaml
    /// </summary>
    public partial class AnimatorTrack : UserControl
    {
        private int m_TotalSeconds;
        private int m_CurrentSeconds;
        private string m_TrackName;
        private int m_MouseAction;
        private AnimatorBox m_Selected;
        private AnimatorBox m_SelectedByIndex;
        private AnimatorBox m_LastRightClickedBox;
        private double m_MaxRightDrag;
        private double m_MinLeftDrag;
        private double m_MoveOffset;
        private List<AnimatorBox> m_BoxSorter;
        private AnimatorWindow m_AnimatorWindow;
        private ObjectControl m_Object;
        private AnimationTrackData m_TrackData;

        public string TrackName
        {
            get { return m_TrackName; }
            set { m_TrackName = value; }
        }

        public ObjectControl TheObject
        {
            get { return m_Object; }
        }

        public AnimationTrackData TrackData
        {
            get { return m_TrackData; }
            set { m_TrackData = value; }
        }

        public AnimatorTrack()
        {
            InitializeComponent();
        }

        public void Destroy()
        {
            (TrackMenuCanvas.ContextMenu.Items[0] as MenuItem).Click -= AnimatorTrack_MenuClick_Add;
            (TrackMenuCanvas.ContextMenu.Items[1] as MenuItem).Click -= AnimatorTrack_MenuClick_RemoveAll;
            (TrackMenuCanvas.ContextMenu.Items[3] as MenuItem).Click -= AnimatorTrack_MenuClick_PushAllLeft;
            (TrackMenuCanvas.ContextMenu.Items[4] as MenuItem).Click -= AnimatorTrack_MenuClick_PushAllRight;

            (BoxMenuCanvas.ContextMenu.Items[0] as MenuItem).Click -= AnimatorTrack_MenuClick_Remove;
            (BoxMenuCanvas.ContextMenu.Items[2] as MenuItem).Click -= AnimatorTrack_MenuClick_PushLeft;
            (BoxMenuCanvas.ContextMenu.Items[3] as MenuItem).Click -= AnimatorTrack_MenuClick_PushRight;

            m_Selected = null;
            m_SelectedByIndex = null;
            m_LastRightClickedBox = null;
            m_BoxSorter = null;
            m_AnimatorWindow = null;
            m_Object = null;
            m_TrackData = null;
        }

        public AnimatorTrack(string name, ObjectControl o, AnimationTrackData trackData, bool load)
        {
            InitializeComponent();
            m_TotalSeconds = 0;
            m_CurrentSeconds = 0;
            m_TrackName = name;
            m_MouseAction = MOUSEACTION.NONE;
            m_MaxRightDrag = 0;
            m_MinLeftDrag = 0;
            m_MoveOffset = 0;
            m_BoxSorter = new List<AnimatorBox>();
            m_SelectedByIndex = null;
            m_LastRightClickedBox = null;
            m_TrackData = trackData;
            m_Object = o;

            (TrackMenuCanvas.ContextMenu.Items[0] as MenuItem).Click += AnimatorTrack_MenuClick_Add;
            (TrackMenuCanvas.ContextMenu.Items[1] as MenuItem).Click += AnimatorTrack_MenuClick_RemoveAll;
            (TrackMenuCanvas.ContextMenu.Items[3] as MenuItem).Click += AnimatorTrack_MenuClick_PushAllLeft;
            (TrackMenuCanvas.ContextMenu.Items[4] as MenuItem).Click += AnimatorTrack_MenuClick_PushAllRight;

            (BoxMenuCanvas.ContextMenu.Items[0] as MenuItem).Click += AnimatorTrack_MenuClick_Remove;
            (BoxMenuCanvas.ContextMenu.Items[2] as MenuItem).Click += AnimatorTrack_MenuClick_PushLeft;
            (BoxMenuCanvas.ContextMenu.Items[3] as MenuItem).Click += AnimatorTrack_MenuClick_PushRight;

            if(load)
            {
                for(int i = 0; i < trackData.AnimationDataList.Count; i++)
                {
                    AddNewBox(trackData.AnimationDataList[i]);
                }
            }
        }
        
        public void SetAnimatorWindow(AnimatorWindow window)
        {
            m_AnimatorWindow = window;
        }

        public void IncreaseByTen(int howManyTimes)
        {
            for(int i = 0; i < howManyTimes; i++)
            {
                TimelinePart p = new TimelinePart();
                if (m_TotalSeconds > 0 && m_TotalSeconds % 60 == 0)
                {
                    m_CurrentSeconds = 0;

                    Label minuteLabel = new Label();
                    minuteLabel.FontSize = 9;
                    minuteLabel.Content = (m_TotalSeconds / 60) + " min";
                    minuteLabel.Margin = new Thickness((m_TotalSeconds * 25) - 12.5, -5, 0, 0);
                    MinuteGrid.Children.Add(minuteLabel);
                }

                p.Init(m_CurrentSeconds);

                m_CurrentSeconds += 10;
                m_TotalSeconds += 10;

                if(TimePanel.Children.Count == 0)
                {
                    p.Margin = new Thickness(-12.5, 0, 0, 0);
                    p.L0.Content = "";
                }
                TimePanel.Children.Add(p);
            }
        }

        public void Save()
        {
            m_TrackData.AnimationDataList.Clear();
            for (int i = 0; i < BoxGrid.Children.Count; i++)
            {
                UpdateTime(BoxGrid.Children[i] as AnimatorBox);
                m_TrackData.AnimationDataList.Add((BoxGrid.Children[i] as AnimatorBox).AnimationData);
            }
        }

        private void BoxGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < BoxGrid.Children.Count; i++)
            {
                if (BoxGrid.Children[i].IsMouseOver)
                {
                    m_Selected = (BoxGrid.Children[i] as AnimatorBox);
                    m_Selected.SetMouseAction(m_MouseAction);
                    Panel.SetZIndex(m_Selected, 5);
                    SelectBox(i + 1);

                    if ((BoxGrid.Children[i] as AnimatorBox).DragLeftRect.IsMouseOver)
                    {
                        m_MouseAction = MOUSEACTION.DRAG_LEFT;
                        SetMinLeftDrag();
                    }
                    else if ((BoxGrid.Children[i] as AnimatorBox).DragRightRect.IsMouseOver)
                    {
                        m_MouseAction = MOUSEACTION.DRAG_RIGHT;
                        SetMaxRightDrag();
                    }
                    else
                    {
                        m_MouseAction = MOUSEACTION.MOVE;
                        m_MoveOffset = Mouse.GetPosition(BoxGrid).X - m_Selected.Margin.Left;
                        m_Selected.OriginalPosition = m_Selected.Margin.Left;
                    }
                    break;
                }
            }
        }

        private AnimatorBox IsMouseOverBox()
        {
            for (int i = 0; i < BoxGrid.Children.Count; i++)
            {
                if (BoxGrid.Children[i].IsMouseOver)
                {
                    return BoxGrid.Children[i] as AnimatorBox;
                }
            }
            return null;
        }

        public int GetBoxPosition(AnimatorBox box)
        {
            for (int i = 0; i < BoxGrid.Children.Count; i++)
            {
                if (BoxGrid.Children[i].Equals(box))
                {
                    return i;
                }
            }
            return -1;
        }

        public AnimatorBox GetBox(int index)
        {
            if (index >= 0 && index < BoxGrid.Children.Count)
            {
                return BoxGrid.Children[index] as AnimatorBox;
            }
            return null;
        }

        private void BoxGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            if (m_Selected == null)
                return;

            ValidateMove();
            m_MouseAction = MOUSEACTION.NONE;
            m_Selected.SetMouseAction(m_MouseAction);
            m_Selected.InvalidPlacement.Visibility = Visibility.Hidden;
            Panel.SetZIndex(m_Selected, 0);
            m_Selected = null;
            SortBoxes();
        }

        private void BoxGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (m_Selected == null)
                return;

            ValidateMove();
            m_MouseAction = MOUSEACTION.NONE;
            m_Selected.SetMouseAction(m_MouseAction);
            m_Selected.InvalidPlacement.Visibility = Visibility.Hidden;
            Panel.SetZIndex(m_Selected, 0);
            m_Selected = null;
            SortBoxes();
        }

        private void ValidateMove()
        {
            bool collision = false;
            if (m_MouseAction == MOUSEACTION.MOVE)
            {
                collision = AnyCollision();
                if (collision)
                {
                    m_Selected.m_Margin.Left = m_Selected.OriginalPosition;
                    m_Selected.Margin = m_Selected.m_Margin;
                }
                else
                {
                    if (m_Selected.Margin.Left < 0)
                    {
                        m_Selected.m_Margin.Left = 0;
                        m_Selected.Margin = m_Selected.m_Margin;
                    }
                }
            }

            if (!collision)
            {
                m_AnimatorWindow.SelectBox(m_SelectedByIndex);
            }     
        }

        public void DeselectBox()
        {
            VisuallyDeselectBox();
        }

        public void VisuallyDeselectBox()
        {
            if (m_SelectedByIndex != null)
            {
                m_SelectedByIndex.SelectedBox.Visibility = Visibility.Hidden;
            }
        }

        public void SelectBox(int order)
        {
            DeselectBox();
            m_SelectedByIndex = (BoxGrid.Children[order - 1] as AnimatorBox);
            m_AnimatorWindow.SelectBox(m_SelectedByIndex);
            VisuallySelectBox();
        }

        public void VisuallySelectBox()
        {
            m_SelectedByIndex.SelectedBox.Visibility = Visibility.Visible;
        }


        public void AddNewBox()
        {
            AnimatorBox box = null;

            // Always add the newest box at the end
            double addBoxLocation = 0;
            if(BoxGrid.Children.Count > 0)
            {
                addBoxLocation = (BoxGrid.Children[BoxGrid.Children.Count - 1] as AnimatorBox).Margin.Left + (BoxGrid.Children[BoxGrid.Children.Count - 1] as AnimatorBox).Box.Width;
            }

            // Add the correct animationdata class
            double fromTime = addBoxLocation / 25;
            double toTime = (addBoxLocation + 25) / 25;
            switch (m_TrackData.AnimationType)
            {
                case ANIMATIONTYPE.MOVE: box = new AnimatorBox(m_TrackData.AnimationType, new MoveAnimationData(fromTime, toTime, new TextData(""), new TextData(""), 0)); break;
                case ANIMATIONTYPE.ROTATE: box = new AnimatorBox(m_TrackData.AnimationType, new RotateAnimationData(fromTime, toTime, new TextData(""), 0)); break;
                case ANIMATIONTYPE.SCALE: box = new AnimatorBox(m_TrackData.AnimationType, new ScaleAnimationData(fromTime, toTime, new TextData(""), new TextData(""))); break;
                case ANIMATIONTYPE.SKEW: box = new AnimatorBox(m_TrackData.AnimationType, new SkewAnimationData(fromTime, toTime, new TextData(""), new TextData(""))); break;
                case ANIMATIONTYPE.TRANSPARENCY: box = new AnimatorBox(m_TrackData.AnimationType, new TransparencyAnimationData(fromTime, toTime, new TextData(""))); break;
                default: throw new Exception("Wrong animation type in AddNewBox");
            }

            box.Track = this;
            box.m_Margin.Left = addBoxLocation;
            box.Margin = box.m_Margin;
            BoxGrid.Children.Add(box);
            AnimatorOrderButtons.Children.Add(new AnimatorOrderButton(this));
        }

        public void AddNewBox(AnimationData data)
        {
            AnimatorBox box = new AnimatorBox(m_TrackData.AnimationType, data);
            box.Track = this;
            box.m_Margin.Left = data.FromSeconds * 25;
            box.Margin = box.m_Margin;
            box.Box.Width = (data.ToSeconds * 25) - (data.FromSeconds * 25);
            BoxGrid.Children.Add(box);
            AnimatorOrderButtons.Children.Add(new AnimatorOrderButton(this));
        }

        public void RemoveBox(AnimatorBox box)
        {
            if (box == null)
                return;

            m_AnimatorWindow.DeselectBox(box);

            BoxGrid.Children.Remove(box);
            AnimatorOrderButtons.Children.RemoveAt(AnimatorOrderButtons.Children.Count - 1);
            SortBoxes();
        }

        private bool AnyCollision()
        {
            if (m_Selected == null)
                return false;

            for (int i = 0; i < BoxGrid.Children.Count; i++)
            {
                AnimatorBox other = (BoxGrid.Children[i] as AnimatorBox);
                if (other.Equals(m_Selected))
                    continue;

                if (BoxCollision(m_Selected.Margin.Left, m_Selected.Box.Width, other.Margin.Left, other.Box.Width))
                {
                    return true;
                }
            }
            return false;
        }

        public bool AnyCollision(AnimatorBox box)
        {
            for (int i = 0; i < BoxGrid.Children.Count; i++)
            {
                AnimatorBox other = (BoxGrid.Children[i] as AnimatorBox);
                if (other.Equals(box))
                    continue;

                if (BoxCollision(box.Margin.Left, box.Box.Width, other.Margin.Left, other.Box.Width))
                {
                    return true;
                }
            }
            return false;
        }

        // This only checks collisions on LEFT/RIGHT because we are only able to move those directions.
        private bool BoxCollision(double x1, double width1, double x2, double width2)
        {
            return !((x1 + width1) <= x2 || x1 >= (x2 + width2));
        }

        private void SortBoxes()
        {
            for (int i = BoxGrid.Children.Count - 1; i >= 0; i--)
            {
                m_BoxSorter.Add(BoxGrid.Children[i] as AnimatorBox);
                BoxGrid.Children.RemoveAt(i);
            }
            
            m_BoxSorter.Sort();

            for (int i = m_BoxSorter.Count - 1; i >= 0; i--)
            {
                BoxGrid.Children.Add(m_BoxSorter[i]);
                m_BoxSorter.RemoveAt(i);
            }
        }

        private void SetMinLeftDrag()
        {
            for (int i = 1; i < BoxGrid.Children.Count; i++)
            {
                AnimatorBox other = (BoxGrid.Children[i] as AnimatorBox);
                if (other.Equals(m_Selected))
                {
                    m_MinLeftDrag = (BoxGrid.Children[i - 1] as AnimatorBox).Margin.Left + (BoxGrid.Children[i - 1] as AnimatorBox).Box.Width;
                    return;
                }
            }
            m_MinLeftDrag = 0;
        }

        private void SetMaxRightDrag()
        {
            if (BoxGrid.Children.Count == 1)
            {
                m_MaxRightDrag = 0;
            }
            else
            {
                for (int i = 0; i < BoxGrid.Children.Count; i++)
                {
                    AnimatorBox other = (BoxGrid.Children[i] as AnimatorBox);
                    if (other.Equals(m_Selected))
                    {
                        if (i + 1 == BoxGrid.Children.Count)
                        {
                            m_MaxRightDrag = 0;
                        }
                        else
                        {
                            m_MaxRightDrag = (BoxGrid.Children[i + 1] as AnimatorBox).Margin.Left;
                        }
                        break;
                    }
                }
            }
        }

        private void BoxGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if(m_MouseAction == MOUSEACTION.NONE)
            {
                return;
            }

            if(m_MouseAction == MOUSEACTION.MOVE)
            {
                m_Selected.m_Margin.Left = Mouse.GetPosition(BoxGrid).X - m_MoveOffset;
                m_Selected.Margin = m_Selected.m_Margin;

                if(AnyCollision())
                {
                    m_Selected.InvalidPlacement.Visibility = Visibility.Visible;
                }
                else
                {
                    m_Selected.InvalidPlacement.Visibility = Visibility.Hidden;
                }

            }
            else if(m_MouseAction == MOUSEACTION.DRAG_LEFT)
            {
                double posx = Mouse.GetPosition(BoxGrid).X;
                if (posx <= m_MinLeftDrag)
                {
                    posx = m_MinLeftDrag;
                }

                double right = m_Selected.Margin.Left + m_Selected.Box.Width;
                m_Selected.m_Margin.Left = posx;
                m_Selected.Margin = m_Selected.m_Margin;
                double newWidth = right - m_Selected.Margin.Left;

                if (newWidth < 0)
                {
                    m_Selected.m_Margin.Left = right;
                    m_Selected.Margin = m_Selected.m_Margin;
                    newWidth = 0;
                }
                m_Selected.Box.Width = newWidth;
            }
            else
            {
                double newWidth = Mouse.GetPosition(BoxGrid).X - m_Selected.m_Margin.Left;
                if(newWidth < 0)
                {
                    newWidth = 0;
                }
                else if (m_MaxRightDrag > 0 && m_Selected.m_Margin.Left + newWidth > m_MaxRightDrag)
                {
                    newWidth = m_MaxRightDrag - m_Selected.m_Margin.Left;
                }
                m_Selected.Box.Width = newWidth;
            }
            UpdateTime(m_Selected);
        }

        public void UpdateTime(AnimatorBox selected)
        {
            selected.AnimationData.FromSeconds = selected.Margin.Left / 25;
            selected.AnimationData.ToSeconds = (selected.Margin.Left + selected.Box.Width) / 25;
        }

        private void BoxGrid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            AnimatorBox box = IsMouseOverBox();

            ContextMenu cm = null;
            if (box == null)
            {
                cm = TrackMenuCanvas.ContextMenu;

                // Disable/Enable Right option if not enough/enough boxes
                if (BoxGrid.Children.Count < 2)
                {
                    (cm.Items[4] as MenuItem).IsEnabled = false;
                }
                else
                {
                    (cm.Items[4] as MenuItem).IsEnabled = true;
                }

                // Disable/Enable Left option if not enough/enough boxes
                if (BoxGrid.Children.Count == 0)
                {
                    (cm.Items[3] as MenuItem).IsEnabled = false;
                }
                else
                {
                    (cm.Items[3] as MenuItem).IsEnabled = true;
                }
            }
            else
            {
                cm = BoxMenuCanvas.ContextMenu;
                m_LastRightClickedBox = box;

                // If last positioned box, we cant really move it more right
                int pos = GetBoxPosition(box);
                if (pos == BoxGrid.Children.Count - 1)
                {
                    (cm.Items[3] as MenuItem).IsEnabled = false;
                }
                else
                {
                    (cm.Items[3] as MenuItem).IsEnabled = true;
                }
            }

            cm.IsOpen = true;

        }

        private void AnimatorTrack_MenuClick_Add(object sender, RoutedEventArgs e)
        {
            AddNewBox();
        }

        private void AnimatorTrack_MenuClick_RemoveAll(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("This will remove everything on this track.\nContinue?", "Pack", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                BoxGrid.Children.Clear();
                AnimatorOrderButtons.Children.Clear();
            }
        }

        private void AnimatorTrack_MenuClick_Remove(object sender, RoutedEventArgs e)
        {
            RemoveBox(m_LastRightClickedBox);
        }

        private void AnimatorTrack_MenuClick_PushAllLeft(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("This will push everything on this track left.\nContinue?", "Pack", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                AnimatorBox first = (BoxGrid.Children[0] as AnimatorBox);
                first.m_Margin.Left = 0;
                first.Margin = first.m_Margin;
                first.OriginalPosition = 0;

                for (int i = 1; i < BoxGrid.Children.Count; i++)
                {
                    AnimatorBox box = (BoxGrid.Children[i] as AnimatorBox);
                    box.m_Margin.Left = (BoxGrid.Children[i - 1] as AnimatorBox).Margin.Left + (BoxGrid.Children[i - 1] as AnimatorBox).Box.Width;
                    box.Margin = box.m_Margin;
                    box.OriginalPosition = box.m_Margin.Left;
                }
                m_AnimatorWindow.UpdateSelected();
            }
        }

        private void AnimatorTrack_MenuClick_PushAllRight(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("This will push everything on this track right.\nContinue?", "Pack", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                for (int i = BoxGrid.Children.Count - 2; i >= 0; i--)
                {
                    AnimatorBox box = (BoxGrid.Children[i] as AnimatorBox);
                    box.m_Margin.Left = (BoxGrid.Children[i + 1] as AnimatorBox).Margin.Left - box.Box.Width;
                    box.Margin = box.m_Margin;
                    box.OriginalPosition = box.m_Margin.Left;
                }
                m_AnimatorWindow.UpdateSelected();
            }
        }

        private void AnimatorTrack_MenuClick_PushLeft(object sender, RoutedEventArgs e)
        {
            if (m_LastRightClickedBox == null)
                return;

            int pos = GetBoxPosition(m_LastRightClickedBox);
            if(pos == 0)
            {
                AnimatorBox first = (BoxGrid.Children[0] as AnimatorBox);
                first.m_Margin.Left = 0;
                first.Margin = first.m_Margin;
                first.OriginalPosition = 0;
            }
            else
            {
                m_LastRightClickedBox.m_Margin.Left = (BoxGrid.Children[pos - 1] as AnimatorBox).Margin.Left + (BoxGrid.Children[pos - 1] as AnimatorBox).Box.Width;
                m_LastRightClickedBox.Margin = m_LastRightClickedBox.m_Margin;
                m_LastRightClickedBox.OriginalPosition = m_LastRightClickedBox.m_Margin.Left;
            }
            m_AnimatorWindow.UpdateSelected();
        }

        private void AnimatorTrack_MenuClick_PushRight(object sender, RoutedEventArgs e)
        {
            if (m_LastRightClickedBox == null)
                return;

            int pos = GetBoxPosition(m_LastRightClickedBox);

            m_LastRightClickedBox.m_Margin.Left = (BoxGrid.Children[pos + 1] as AnimatorBox).Margin.Left - m_LastRightClickedBox.Box.Width;
            m_LastRightClickedBox.Margin = m_LastRightClickedBox.m_Margin;
            m_LastRightClickedBox.OriginalPosition = m_LastRightClickedBox.m_Margin.Left;
            m_AnimatorWindow.UpdateSelected();
        }

    }
}
