﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for TimeSettings.xaml
    /// </summary>
    public partial class TimeSettings : UserControl
    {
        private bool m_UserToggle;
        private AnimatorBox m_Selected;

        public TimeSettings()
        {
            InitializeComponent();
            m_Selected = null;
            m_UserToggle = false;
        }

        public void SetSelected(AnimatorBox selected)
        {
            m_Selected = selected;

            GeneralToSeconds.Background = Brushes.White;
            GeneralFromSeconds.Background = Brushes.White;
            GeneralTotalSeconds.Background = Brushes.White;
        }

        public void SetData(double from, double to, double total)
        {
            SetFromSeconds(Convert.ToDecimal(from));
            SetToSeconds(Convert.ToDecimal(to));
            SetTotalSeconds(Convert.ToDecimal(total));
        }

        public bool SetFromSeconds(decimal? value)
        {
            bool result = false;

            m_UserToggle = true;

            if (value < 0)
            {
                GeneralFromSeconds.Value = 0;
            }
            else
            {
                GeneralFromSeconds.Value = value;
            }

            m_UserToggle = false;

            return result;
        }

        public bool SetToSeconds(decimal? value)
        {
            bool result = false;

            m_UserToggle = true;

            if (value < 0)
            {
                GeneralToSeconds.Value = 0;
            }
            else
            {
                GeneralToSeconds.Value = value;
            }

            m_UserToggle = false;

            return result;
        }

        public bool SetTotalSeconds(decimal? value)
        {
            bool result = false;

            m_UserToggle = true;

            if (value < 0)
            {
                GeneralTotalSeconds.Value = 0;
            }
            else
            {
                GeneralTotalSeconds.Value = value;
            }

            m_UserToggle = false;

            return result;
        }

        private void GeneralFromSeconds_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_Selected == null)
                return;

            if (m_UserToggle)
                return;

            if (GeneralFromSeconds.Value < 0)
            {
                SetFromSeconds(0);
            }
            else
            {
                GeneralToSeconds.Background = Brushes.White;
                GeneralFromSeconds.Background = Brushes.White;

                decimal? originalToSeconds = null;
                if (GeneralFromSeconds.Value > GeneralToSeconds.Value)
                {
                    originalToSeconds = GeneralToSeconds.Value;
                    SetToSeconds(GeneralFromSeconds.Value + 1);
                }

                double originalLeftPosition = m_Selected.Margin.Left;
                m_Selected.m_Margin.Left = Convert.ToDouble(GeneralFromSeconds.Value, CultureInfo.InvariantCulture) * 25;
                m_Selected.Margin = m_Selected.m_Margin;

                double toValue = Convert.ToDouble(GeneralToSeconds.Value, CultureInfo.InvariantCulture) * 25; ;
                double newWidth = toValue - m_Selected.Margin.Left;

                if (newWidth < 0)
                    newWidth = 0;

                double originalWidth = m_Selected.Box.Width;
                m_Selected.Box.Width = newWidth;

                if (m_Selected.Track.AnyCollision(m_Selected))
                {
                    GeneralFromSeconds.Background = Brushes.Red;
                    m_Selected.m_Margin.Left = originalLeftPosition;
                    m_Selected.Margin = m_Selected.m_Margin;
                    m_Selected.Box.Width = originalWidth;

                    if (originalToSeconds != null)
                    {
                        SetToSeconds(originalToSeconds);
                    }
                }
                else
                {
                    GeneralToSeconds.Background = Brushes.White;
                    SetTotalSeconds(GeneralToSeconds.Value - GeneralFromSeconds.Value);
                    m_Selected.Track.UpdateTime(m_Selected);
                }
            }

        }

        private void GeneralToSeconds_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_Selected == null)
                return;

            if (m_UserToggle)
                return;

            if (GeneralToSeconds.Value < 0)
            {
                SetToSeconds(0);
            }
            else
            {
                GeneralToSeconds.Background = Brushes.White;
                GeneralFromSeconds.Background = Brushes.White;

                double toValue = Convert.ToDouble(GeneralToSeconds.Value, CultureInfo.InvariantCulture) * 25; ;
                double newWidth = toValue - m_Selected.Margin.Left;

                if (newWidth < 0)
                    newWidth = 0;

                double originalWidth = m_Selected.Box.Width;
                m_Selected.Box.Width = newWidth;

                if (m_Selected.Track.AnyCollision(m_Selected))
                {
                    GeneralToSeconds.Background = Brushes.Red;
                    m_Selected.Box.Width = originalWidth;
                }
                else
                {
                    GeneralToSeconds.Background = Brushes.White;
                    SetTotalSeconds(GeneralToSeconds.Value - GeneralFromSeconds.Value);
                    m_Selected.Track.UpdateTime(m_Selected);
                }
            }
        }
    }
}
