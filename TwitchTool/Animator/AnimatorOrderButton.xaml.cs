﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for AnimatorOrderButton.xaml
    /// </summary>
    public partial class AnimatorOrderButton : UserControl
    {
        private int m_Order;
        private AnimatorTrack m_Track;

        public AnimatorOrderButton()
        {
            InitializeComponent();
            m_Order = 0;
        }

        public AnimatorOrderButton(AnimatorTrack track)
        {
            InitializeComponent();
            m_Track = track;
            SetOrder(m_Track.BoxGrid.Children.Count);
        }

        public void SetOrder(int order)
        {
            m_Order = order;
            OrderLabel.Content = Convert.ToString(m_Order);
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            m_Track.SelectBox(m_Order);
        }

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            OrderLabel.Background = Brushes.DimGray;
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            OrderLabel.Background = Brushes.Transparent;
        }
    }
}
