﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animator;
using TwitchTool.Source;

namespace TwitchTool.Animations
{
    /// <summary>
    /// Interaction logic for AnimatorBox.xaml
    /// </summary>
    public partial class AnimatorBox : UserControl, IComparable
    {
        public Thickness m_Margin;
        private int m_CurrentAction;
        public double m_OriginalPosition;
        private AnimatorTrack m_Track;
        private int m_AnimationType;
        private AnimationData m_AnimationData;

        public AnimatorBox(int type, AnimationData data)
        {
            InitializeComponent();
            m_Margin = new Thickness();
            m_CurrentAction = MOUSEACTION.NONE;
            m_OriginalPosition = 0;
            m_AnimationType = type;
            m_AnimationData = data;
        }

        public AnimatorTrack Track
        {
            get { return m_Track; }
            set { m_Track = value; }
        }

        public int AnimationType
        {
            get { return m_AnimationType; }
            private set { m_AnimationType = value; }
        }

        public AnimationData AnimationData
        {
            get { return m_AnimationData; }
            set { m_AnimationData = value; }
        }

        public void SetMouseAction(int action)
        {
            m_CurrentAction = action;
        }

        public double OriginalPosition
        {
            get { return m_OriginalPosition; }
            set { m_OriginalPosition = value; }
        }

        private void DragLeftRect_MouseEnter(object sender, MouseEventArgs e)
        {
            if (m_CurrentAction > 0)
                return;

            Cursor = Cursors.SizeWE;
        }

        private void DragRightRect_MouseEnter(object sender, MouseEventArgs e)
        {
            if (m_CurrentAction > 0)
                return;

            Cursor = Cursors.SizeWE;
        }

        private void DragLeftRect_MouseLeave(object sender, MouseEventArgs e)
        {
            if (m_CurrentAction > 0)
                return;

            Cursor = Cursors.Arrow;
        }

        private void DragRightRect_MouseLeave(object sender, MouseEventArgs e)
        {
            if (m_CurrentAction > 0)
                return;

            Cursor = Cursors.Arrow;
        }

        public int CompareTo(object obj)
        {
            AnimatorBox other = obj as AnimatorBox;

            if (Margin.Left > other.Margin.Left)
            {
                return -1;
            }
            else if (other.Margin.Left > Margin.Left)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

    }
}
