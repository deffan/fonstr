﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TwitchTool.UserControls;

namespace TwitchTool.Animator
{
    public class AnimationTrackData
    {
        public int AnimationType;
        public List<AnimationData> AnimationDataList;

        public AnimationTrackData()
        {
            AnimationType = -1;
            AnimationDataList = new List<AnimationData>();
        }

    }

    public class AnimationDataHolder
    {
        public ObjectControl Object;
        public List<AnimationTrackData> AnimationTracks;
        public bool Selected;

        public AnimationDataHolder()
        {
            Object = null;
            Selected = false;
            AnimationTracks = new List<AnimationTrackData>();
        }

        public double GetTotalTrackTime()
        {
            double total = 0;

            for(int i = 0; i < AnimationTracks.Count; i++)
            {
                if(AnimationTracks[i].AnimationDataList.Count > 0)
                {
                    if (AnimationTracks[i].AnimationDataList[AnimationTracks[i].AnimationDataList.Count - 1].ToSeconds > total)
                    {
                        total = AnimationTracks[i].AnimationDataList[AnimationTracks[i].AnimationDataList.Count - 1].ToSeconds;
                    }
                }
            }

            return total;
        }
    }
}
