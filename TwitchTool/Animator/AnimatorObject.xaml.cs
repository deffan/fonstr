﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Animator;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for AnimatorObject.xaml
    /// </summary>
    public partial class AnimatorObject : UserControl
    {
        private ObjectControl m_Object;
        private List<AnimatorTrack> m_Tracks;
        private AnimatorWindow m_AnimatorWindow;

        public AnimatorObject()
        {
            InitializeComponent();
        }

        public List<AnimatorTrack> TrackList
        {
            get { return m_Tracks; }
        }

        public ObjectControl TheObject
        {
            get { return m_Object; }
        }

        public void Destroy()
        {
            for (int i = 0; i < m_Tracks.Count; i++)
            {
                m_Tracks[i].Destroy();
            }
            m_Tracks.Clear();
            m_Tracks = null;
            m_Object = null;
            m_AnimatorWindow = null;
        }

        public AnimatorObject(AnimationDataHolder animation, AnimatorWindow window)
        {
            InitializeComponent();
            m_Object = animation.Object;
            ObjectLabelName.Content = m_Object.ObjectName;
            m_Tracks = new List<AnimatorTrack>();
            m_AnimatorWindow = window;

            if (animation.AnimationTracks.Count == 0)
            {
                // These always exist
                AddTrack("Move", animation, ANIMATIONTYPE.MOVE);
                AddTrack("Rotate", animation, ANIMATIONTYPE.ROTATE);
                AddTrack("Scale", animation, ANIMATIONTYPE.SCALE);
                AddTrack("Skew",  animation, ANIMATIONTYPE.SKEW);
                AddTrack("Transparency", animation, ANIMATIONTYPE.TRANSPARENCY);

                /*
                // Only deal with components that can be animated somehow
                List<IComponent> components = m_Object.GetComponents();
                for (int i = 0; i < components.Count; i++)
                {
                    if (components[i] is TextComponent)
                    {
                        AddTrack("Text", animation, -1);
                        // Animate font-size?
                        // Animate color-change?
                    }
                    else if (components[i] is ImageComponent)
                    {
                        AddTrack("Image", animation, -1);
                        // special image effects? blur?
                    }
                }
                */
            }
            else
            {
                // Add existing data
                for(int i = 0; i < animation.AnimationTracks.Count; i++)
                {
                    switch(animation.AnimationTracks[i].AnimationType)
                    {
                        case ANIMATIONTYPE.MOVE: LoadTrack("Move", animation, i); break;
                        case ANIMATIONTYPE.SCALE: LoadTrack("Scale", animation, i); break;
                        case ANIMATIONTYPE.ROTATE: LoadTrack("Rotate", animation, i); break;
                        case ANIMATIONTYPE.SKEW: LoadTrack("Skew", animation, i); break;
                        case ANIMATIONTYPE.TRANSPARENCY: LoadTrack("Transparency", animation, i); break;
                        default: throw new Exception("Invalid AnimationType in AnimatorObject");
                    }
                }
            }

            UpDownExpander last = TimeLinePanel.Children[TimeLinePanel.Children.Count - 1] as UpDownExpander;
            last.m_Last = true;
            last.VertRect.Height = 13;

            Label spacelabel = new Label();
            spacelabel.Content = " ";
            TimeLinePanel.Children.Add(spacelabel);


        }

        public void Save()
        {
            for(int i = 0; i < TimeLinePanel.Children.Count; i++)
            {
                if((TimeLinePanel.Children[i] is UpDownExpander))
                {
                    ((TimeLinePanel.Children[i] as UpDownExpander).TheContent.Children[0] as AnimatorTrack).Save();
                }
            }
        }

        private void LoadTrack(string name, AnimationDataHolder animation, int index)
        {
            UpDownExpander expander = new UpDownExpander();

            AnimatorTrack track = new AnimatorTrack(name, animation.Object, animation.AnimationTracks[index], true);
            track.SetAnimatorWindow(m_AnimatorWindow);
            track.IncreaseByTen(6);

            expander.TheContent.Children.Add(track);
            expander.ObjectLabelName.Content = name;

            TimeLinePanel.Children.Add(expander);
            m_Tracks.Add(track);
        }

        private void AddTrack(string name, AnimationDataHolder animation, int type)
        {
            UpDownExpander expander = new UpDownExpander();

            AnimationTrackData animationTrackData = new AnimationTrackData();
            animationTrackData.AnimationType = type;
            animation.AnimationTracks.Add(animationTrackData);

            AnimatorTrack track = new AnimatorTrack(name, animation.Object, animationTrackData, false);
            track.SetAnimatorWindow(m_AnimatorWindow);
            track.IncreaseByTen(6);

            expander.TheContent.Children.Add(track);
            expander.ObjectLabelName.Content = name;

            TimeLinePanel.Children.Add(expander);
            m_Tracks.Add(track);
        }

        private Rectangle GetLineSeparator()
        {
            Rectangle r = new Rectangle();
            r.Height = 1;
            r.Fill = Brushes.Black;
            return r;
        }

        private void IncreaseAllByTen()
        {
            for(int i = 0; i < m_Tracks.Count; i++)
            {
                m_Tracks[i].IncreaseByTen(1);
            }
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (TrackScroll.ScrollableWidth <= 50)
                return;

            if (TrackScroll.HorizontalOffset + 50 >= TrackScroll.ScrollableWidth)
            {
                IncreaseAllByTen();
            }
        }

        private void TrackScroll_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            // This is so the scroll does not automatically scroll to where a mouse click happened (annoying)
            e.Handled = true;
        }
    }
}
