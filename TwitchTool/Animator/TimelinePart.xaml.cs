﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for TimelinePart.xaml
    /// </summary>
    public partial class TimelinePart : UserControl
    {
        public TimelinePart()
        {
            InitializeComponent();
        }

        public TimelinePart(int value)
        {
            InitializeComponent();
            Init(value);
        }

        public void Init(int value)
        {
            L0.Content = Convert.ToString(value);
            L1.Content = Convert.ToString(++value);
            L2.Content = Convert.ToString(++value);
            L3.Content = Convert.ToString(++value);
            L4.Content = Convert.ToString(++value);
            L5.Content = Convert.ToString(++value);
            L6.Content = Convert.ToString(++value);
            L7.Content = Convert.ToString(++value);
            L8.Content = Convert.ToString(++value);
            L9.Content = Convert.ToString(++value);
        }
    }
}
