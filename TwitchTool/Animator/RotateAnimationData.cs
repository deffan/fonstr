﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Animator
{
    public class RotateAnimationData : AnimationData
    {
        public double ToAngle;

        public RotateAnimationData(double fromTime, double toTime, double toAngle) : base (fromTime, toTime)
        {
            ToAngle = toAngle;
        }
    }
}
