﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Animations;

namespace TwitchTool.Animator
{
    public class MoveAnimationData : AnimationData
    {
        public double ToX;
        public double ToY;
        public int Easing;

        public MoveAnimationData(double fromTime, double toTime, double toX, double toY, int easing) : base (fromTime, toTime)
        {
            ToX = toX;
            ToY = toY;
            Easing = easing;
        }
    }
}
