﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for UpDownExpander.xaml
    /// </summary>
    public partial class UpDownExpander : UserControl
    {
        private bool m_Showing;
        public bool m_Last;

        public UpDownExpander()
        {
            InitializeComponent();
            HideOpenButton.Content = "Show";
            TheContent.Height = 0;
            m_Last = false;
        }

        private void HideOpenButton_Click(object sender, RoutedEventArgs e)
        {
            if (m_Showing)
            {
                HideOpenButton.Content = "Show";
                TheContent.Height = 0;
                m_Showing = false;

                if (m_Last)
                {
                    VertRect.Height = 13;
                }
                else
                {
                    VertRect.Height = 28;
                }
            }
            else
            {
                HideOpenButton.Content = "Hide";
                TheContent.Height = Double.NaN;
                m_Showing = true;

                if (!m_Last)
                {
                    VertRect.Height = 173;
                }

            }
        }

        private void ViewPathButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
