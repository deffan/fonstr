﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for TransparencySettings.xaml
    /// </summary>
    public partial class TransparencySettings : UserControl
    {
        private AnimatorBox m_Selected;

        public TransparencySettings()
        {
            InitializeComponent();
        }

        public void SetSelected(AnimatorBox selected)
        {
            m_Selected = selected;

            TransparencyAnimationData data = m_Selected.AnimationData as TransparencyAnimationData;
            ToOpacity.Update(data.ToOpacity, true);

            int pos = m_Selected.Track.GetBoxPosition(m_Selected);
            if (pos == 0)
            {
                FromOpacity.Text = Convert.ToString(m_Selected.Track.TheObject.GetBasicComponent().Opacity * 100);
            }
            else
            {
                AnimatorBox prev = m_Selected.Track.GetBox(pos - 1);
                TransparencyAnimationData prevData = prev.AnimationData as TransparencyAnimationData;

                if (prevData.ToOpacity.IsDataVariable)
                {
                    DataVariable var = prevData.ToOpacity.GetDataVariable();
                    if (var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevData.ToOpacity.Data = "0";
                        prevData.ToOpacity.Type = "text";
                        prevData.ToOpacity.IsText = true;
                    }
                    else
                    {
                        FromOpacity.Text = prevData.ToOpacity.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromOpacity.Text = prevData.ToOpacity.GetStringData();
                }
            }
        }



    }
}
