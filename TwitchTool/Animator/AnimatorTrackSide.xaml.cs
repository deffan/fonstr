﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for AnimatorTrackSide.xaml
    /// </summary>
    public partial class AnimatorTrackSide : UserControl
    {
        private AnimatorTrack m_Track;

        public AnimatorTrackSide()
        {
            InitializeComponent();
        }

        public AnimatorTrackSide(AnimatorTrack track)
        {
            InitializeComponent();
            m_Track = track;
            TrackNameLabel.Content = track.TrackName;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            m_Track.BoxGrid.Children.Add(new AnimatorBox());
        }
    }
}
