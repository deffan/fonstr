﻿using System;
using System.Windows;
using System.Windows.Controls;
using TwitchTool.Animations;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for MoveSettings.xaml
    /// </summary>
    public partial class MoveSettings : UserControl
    {
        private AnimatorBox m_Selected;
        private bool m_Toggle;

        public MoveSettings()
        {
            m_Toggle = true;
            InitializeComponent();
            m_Toggle = false;
        }

        public void SetToX(TextData value)
        {
            m_Toggle = true;
            ToX.Update(value, true);
            m_Toggle = false;
        }

        public void SetToY(TextData value)
        {
            m_Toggle = true;
            ToY.Update(value, true);
            m_Toggle = false;
        }

        public void SetEasing(int value)
        {
            m_Toggle = true;
            EasingCombo.SelectedIndex = value;
            m_Toggle = false;
        }

        public void SetSelected(AnimatorBox selected)
        {
            m_Selected = selected;

            MoveAnimationData moveData = m_Selected.AnimationData as MoveAnimationData;
            SetToX(moveData.ToX);
            SetToY(moveData.ToY);
            SetEasing(moveData.Easing);

            int pos = m_Selected.Track.GetBoxPosition(m_Selected);
            if(pos == 0)
            {
                Point p = m_Selected.Track.TheObject.GetTransformComponent().GetCoordinates();
                FromX.Text = Convert.ToString(p.X);
                FromY.Text = Convert.ToString(p.Y);
            }
            else
            {
                AnimatorBox prev = m_Selected.Track.GetBox(pos - 1);
                MoveAnimationData prevModeData = prev.AnimationData as MoveAnimationData;

                if (prevModeData.ToX.IsDataVariable)
                {
                    DataVariable var = prevModeData.ToX.GetDataVariable();
                    if(var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevModeData.ToX.Data = "0";
                        prevModeData.ToX.Type = "text";
                        prevModeData.ToX.IsText = true;
                    }
                    else
                    {
                        FromX.Text = prevModeData.ToX.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromX.Text = prevModeData.ToX.GetStringData();
                }

                if (prevModeData.ToY.IsDataVariable)
                {
                    DataVariable var = prevModeData.ToY.GetDataVariable();
                    if (var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevModeData.ToY.Data = "0";
                        prevModeData.ToY.Type = "text";
                        prevModeData.ToY.IsText = true;
                    }
                    else
                    {
                        FromY.Text = prevModeData.ToY.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromY.Text = prevModeData.ToY.GetStringData();
                }
            }
        }

        private void EasingCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_Toggle || m_Selected == null)
                return;

            MoveAnimationData moveData = m_Selected.AnimationData as MoveAnimationData;
            moveData.Easing = EasingCombo.SelectedIndex;
        }
    }
}
