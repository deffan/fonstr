﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Animations;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for Animator.xaml
    /// </summary>
    public partial class AnimatorWindow : UserControl
    {
        private AnimatorBox m_Selected;
        private AnimatorComponent m_Component;
        private TimeSettings m_TimeSettings;
        private MoveSettings m_MoveSettings;
        private RotateSettings m_RotateSettings;
        private ScaleSettings m_ScaleSettings;
        private SkewSettings m_SkewSettings;
        private TransparencySettings m_TransparencySettings;


        public AnimatorWindow()
        {
            InitializeComponent();
            m_Selected = null;
            m_Component = null;
            m_TimeSettings = new TimeSettings();
            m_MoveSettings = new MoveSettings();
            m_RotateSettings = new RotateSettings();
            m_ScaleSettings = new ScaleSettings();
            m_SkewSettings = new SkewSettings();
            m_TransparencySettings = new TransparencySettings();
        }

        public void Save()
        {
            for (int i = 0; i < AnimatorObjects.Children.Count; i++)
            {
                AnimatorObject obj = (AnimatorObjects.Children[i] as AnimatorObject);
                obj.Save();
            }
        }

        public void Destroy()
        {
            for (int i = 0; i < AnimatorObjects.Children.Count; i++)
            {
                AnimatorObject obj = (AnimatorObjects.Children[i] as AnimatorObject);
                obj.Save();
                obj.Destroy();
            }
            AnimatorObjects.Children.Clear();
            GeneralSettingsPanel.Children.Clear();
            SpecificSetting.Children.Clear();
            m_Selected = null;
            m_Component = null;
            m_TimeSettings = null;
            m_MoveSettings = null;
            m_RotateSettings = null;
        }

        public void Refresh()
        {
            // Compare selected/deselected objects with loaded ones and apply changes.
            List<AnimationDataHolder> objects = m_Component.GetObjectsToAnimate();
            for (int n = 0; n < objects.Count; n++)
            {
                // Add new if selected
                if (objects[n].Selected)
                {
                    bool found = false;
                    for (int i = 0; i < AnimatorObjects.Children.Count; i++)
                    {
                        AnimatorObject obj = (AnimatorObjects.Children[i] as AnimatorObject);
                        if (obj.TheObject.ID.Equals(objects[n].Object.ID))
                        {
                            found = true;
                            break;
                        }
                    }

                    // Only add if not already here 
                    if(!found)
                    {
                        AnimatorObjects.Children.Add(new AnimatorObject(objects[n], this));
                    }
                    
                }
                else // Remove if deselected
                {
                    for (int i = 0; i < AnimatorObjects.Children.Count; i++)
                    {
                        AnimatorObject obj = (AnimatorObjects.Children[i] as AnimatorObject);
                        if (obj.TheObject.ID.Equals(objects[n].Object.ID))
                        {
                            obj.Save();
                            obj.Destroy();
                            AnimatorObjects.Children.RemoveAt(i);
                            break;
                        }
                    }
                }
            }
        }

        public void SetAnimatorData(AnimatorComponent component)
        {
            m_Component = component;

            GeneralSettingsPanel.Children.Add(component.GetGeneralSettings());

            List<AnimationDataHolder> objects = component.GetObjectsToAnimate();
            for (int i = 0; i < objects.Count; i++)
            {
                if(objects[i].Selected)
                {
                    AnimatorObjects.Children.Add(new AnimatorObject(objects[i], this));
                }
            }

        }

        public void SelectBox(AnimatorBox box)
        {
            m_Selected = box;
            DeselectAllExceptSelected();
            UpdateSelected();
        }

        public void DeselectAllExceptSelected()
        {
            for (int i = 0; i < AnimatorObjects.Children.Count; i++)
            {
                AnimatorObject obj = (AnimatorObjects.Children[i] as AnimatorObject);
                for (int n = 0; n < obj.TrackList.Count; n++)
                {
                    if (!obj.TrackList[n].Equals(m_Selected.Track))
                    {
                        obj.TrackList[n].VisuallyDeselectBox();
                    }
                }
            }
        }

        public void DeselectBox(AnimatorBox box)
        {
            // if this is selected, remove, else do nothing.
            if (m_Selected == null)
                return;

            if (box.Equals(m_Selected))
            {
                m_Selected = null;
            }
        }

        public void UpdateSelected()
        {
            double from = m_Selected.Margin.Left / 25;
            double to = (m_Selected.Margin.Left + m_Selected.Box.Width) / 25;
            double total = to - from;
            m_Selected.Track.UpdateTime(m_Selected);

            // Update visual time control
            m_TimeSettings.SetSelected(m_Selected);
            m_TimeSettings.SetData(from, to, total);

            // Update specific track settings
            SpecificSetting.Children.Clear();
            SpecificSetting.Children.Add(m_TimeSettings);
            switch (m_Selected.AnimationType)
            {
                case ANIMATIONTYPE.MOVE:
                {
                    m_MoveSettings.SetSelected(m_Selected);
                    SpecificSetting.Children.Add(m_MoveSettings);
                    break;
                }
                case ANIMATIONTYPE.ROTATE:
                {
                    m_RotateSettings.SetSelected(m_Selected);
                    SpecificSetting.Children.Add(m_RotateSettings);
                    break;
                }
                case ANIMATIONTYPE.SCALE:
                {
                    m_ScaleSettings.SetSelected(m_Selected);
                    SpecificSetting.Children.Add(m_ScaleSettings);
                    break;
                }
                case ANIMATIONTYPE.SKEW:
                {
                    m_SkewSettings.SetSelected(m_Selected);
                    SpecificSetting.Children.Add(m_SkewSettings);
                    break;
                }
                case ANIMATIONTYPE.TRANSPARENCY:
                {
                    m_TransparencySettings.SetSelected(m_Selected);
                    SpecificSetting.Children.Add(m_TransparencySettings);
                    break;
                }
                default:
                    break;
            }
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            for (int i = 0; i < AnimatorObjects.Children.Count; i++) 
            {
                (AnimatorObjects.Children[i] as AnimatorObject).TrackScroll.Width = AnimatorWindowName.ActualWidth - SystemParameters.VerticalScrollBarWidth;
            }
            AnimatorObjectsScroll.Height = AnimatorWindowName.ActualHeight - 150;
        }


    }

}
