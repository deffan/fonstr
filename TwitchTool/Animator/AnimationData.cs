﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Animator
{
    public class AnimationData
    {
        public double FromSeconds;
        public double ToSeconds;
        
        public AnimationData(double from, double to)
        {
            FromSeconds = from;
            ToSeconds = to;
        }
    }
}
