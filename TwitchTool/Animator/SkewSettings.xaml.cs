﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for SkewSettings.xaml
    /// </summary>
    public partial class SkewSettings : UserControl
    {
        private AnimatorBox m_Selected;

        public SkewSettings()
        {
            InitializeComponent();
        }

        public void SetSelected(AnimatorBox selected)
        {
            m_Selected = selected;

            SkewAnimationData data = m_Selected.AnimationData as SkewAnimationData;
            ToX.Update(data.ToXSkew, true);
            ToY.Update(data.ToYSkew, true);

            int pos = m_Selected.Track.GetBoxPosition(m_Selected);
            if (pos == 0)
            {
                Size s = m_Selected.Track.TheObject.GetTransformComponent().GetScale();
                FromX.Text = Convert.ToString(s.Width);
                FromY.Text = Convert.ToString(s.Height);
            }
            else
            {
                AnimatorBox prev = m_Selected.Track.GetBox(pos - 1);
                SkewAnimationData prevData = prev.AnimationData as SkewAnimationData;

                if (prevData.ToXSkew.IsDataVariable)
                {
                    DataVariable var = prevData.ToXSkew.GetDataVariable();
                    if (var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevData.ToXSkew.Data = "0";
                        prevData.ToXSkew.Type = "text";
                        prevData.ToXSkew.IsText = true;
                    }
                    else
                    {
                        FromX.Text = prevData.ToXSkew.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromX.Text = prevData.ToXSkew.GetStringData();
                }

                if (prevData.ToYSkew.IsDataVariable)
                {
                    DataVariable var = prevData.ToYSkew.GetDataVariable();
                    if (var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevData.ToYSkew.Data = "0";
                        prevData.ToYSkew.Type = "text";
                        prevData.ToYSkew.IsText = true;
                    }
                    else
                    {
                        FromY.Text = prevData.ToYSkew.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromY.Text = prevData.ToYSkew.GetStringData();
                }
            }
        }

    }
}
