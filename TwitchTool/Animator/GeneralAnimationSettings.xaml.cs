﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for GeneralAnimationSettings.xaml
    /// </summary>
    public partial class GeneralAnimationSettings : UserControl
    {
        private bool m_UserToggle;

        public int m_RunTimes;
        public bool m_LoopForever;
        public bool m_Reset;
        public bool m_Reverse;

        public GeneralAnimationSettings()
        {
            InitializeComponent();
            m_UserToggle = true;
            RunTimesRadioButton.IsChecked = true;
            RunTimes.Value = 1;
            DoNothingRadioButton.IsChecked = true;
            m_UserToggle = false;

            // These values are used when running animations "live". So we dont have to access the control.
            // Be aware of bugs, as these are only set when deserializing.
            m_RunTimes = 0;
            m_LoopForever = false;
            m_Reset = false;
            m_Reverse = false;
        }

        public void SetData(bool loop, int runTimes, int OnEnd)
        {
            m_UserToggle = true;

            if (loop)
            {
                m_LoopForever = true;
                RunTimes.IsEnabled = false;
                RunForeverRadioButton.IsChecked = true;
            }
            else
            {
                RunTimes.IsEnabled = true;
                RunTimes.Value = runTimes;
                m_RunTimes = runTimes;
                RunTimesRadioButton.IsChecked = true;
            }

            switch (OnEnd)
            {
                case ONANIMATIONEND.DONOTHING: DoNothingRadioButton.IsChecked = true; break;
                case ONANIMATIONEND.RESET: ResetRadioButton.IsChecked = true; m_Reset = true; break;
                case ONANIMATIONEND.REVERSE: ReverseRadioButton.IsChecked = true; m_Reverse = true; break;
                default: break;
            }

            m_UserToggle = false;
        }

        private void RunTimes_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_UserToggle)
                return;


        }

        private void RunTimesRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void RunTimesRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void RunForeverRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void RunForeverRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void DoNothingRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void DoNothingRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void ResetRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void ResetRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void ReverseRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }

        private void ReverseRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_UserToggle)
                return;


        }
    }
}
