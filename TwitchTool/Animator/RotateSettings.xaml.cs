﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animations;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    /// <summary>
    /// Interaction logic for RotateSettings.xaml
    /// </summary>
    public partial class RotateSettings : UserControl
    {
        private AnimatorBox m_Selected;

        public RotateSettings()
        {
            InitializeComponent();
        }

        public void SetSelected(AnimatorBox selected)
        {
            m_Selected = selected;
            RotateAnimationData rotateData = m_Selected.AnimationData as RotateAnimationData;
            ToAngle.Update(rotateData.ToAngle, true);

            int pos = m_Selected.Track.GetBoxPosition(m_Selected);
            if (pos == 0)
            {
                FromAngle.Text = Convert.ToString(m_Selected.Track.TheObject.GetTransformComponent().GetRotationAngle());
            }
            else
            {
                AnimatorBox prev = m_Selected.Track.GetBox(pos - 1);
                RotateAnimationData prevData = prev.AnimationData as RotateAnimationData;

                if (prevData.ToAngle.IsDataVariable)
                {
                    DataVariable var = prevData.ToAngle.GetDataVariable();
                    if (var == null)
                    {
                        // Variable was removed, default to 0 value.
                        prevData.ToAngle.Data = "0";
                        prevData.ToAngle.Type = "text";
                        prevData.ToAngle.IsText = true;
                    }
                    else
                    {
                        FromAngle.Text = prevData.ToAngle.GetNumericData() + " (" + var.DisplayName + ")";
                    }
                }
                else
                {
                    FromAngle.Text = prevData.ToAngle.GetStringData();
                }
            }
        }

        private void EasingCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (m_Selected == null)
                return;

            RotateAnimationData data = m_Selected.AnimationData as RotateAnimationData;
            data.Easing = EasingCombo.SelectedIndex;
        }
    }
}
