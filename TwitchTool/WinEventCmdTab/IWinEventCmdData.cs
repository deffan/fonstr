﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab
{
    public interface IWinEventCmdData
    {
        string Save();
        void Load();
        void AbortSave();
        bool HasChanges();
        void AddMyListItem(WinEventCmdListItem mylistitem);
        WinEventCmdListItem GetMyListItem();
        WinEventCmdList GetList();
        object GetCurrentData();
        ListData GetListData();
        
    }
}
