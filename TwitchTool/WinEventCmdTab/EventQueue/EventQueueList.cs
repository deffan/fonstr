﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.WinEventCmdTab.EventQueue
{
    public class EventQueueList
    {
        public string Name;
        public string Description;
        public List<string> EventIds;
        public TextData Delayed;
        public bool IsDelayed;
        public int DelayedSetting;

        public EventQueueList(string name, string desc, List<string> ids, TextData delayed, bool isDelayed, int delayedSetting)
        {
            Name = name;
            Description = desc;
            EventIds = ids;
            Delayed = delayed;
            IsDelayed = isDelayed;
            DelayedSetting = delayedSetting;
        }
    }
}
