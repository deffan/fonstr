﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.WinEventCmdTab.EventQueue
{
    /// <summary>
    /// Interaction logic for EventQueueListItem.xaml
    /// </summary>
    public partial class EventQueueListItem : UserControl
    {
        public const int DELAYED_CHECKED = 0;
        public const int DELAYED_UNCHECKED = 1;
        public const int DELAYED_BOTH = 2;
        private EventQueueCtrl m_Parent;
        private SolidColorBrush m_MouseOverBrush;
        private bool m_Selected;
        private List<EventCheckBox> m_Events;
        public bool DefaultQueue;
        public bool IsSelected() { return m_Selected; }
        public List<EventCheckBox> GetEvents() { return m_Events; }

        // Delayed queueing
        public TextData DelayedValue;
        public bool Delayed;
        public int DelayedSetting;
        public string DelayCompareString;

        public void BuildCompareString()
        {
            DelayCompareString = "";

            if (Delayed)
                DelayCompareString = "Yes_";
            else
                DelayCompareString = "No_";

            DelayCompareString += DelayedSetting;

            if(DelayedValue != null)
            {
                DelayCompareString += DelayedValue.Type;
                DelayCompareString += DelayedValue.Id;
                DelayCompareString += DelayedValue.Data;
            }
        }

        public EventQueueListItem()
        {
            InitializeComponent();
            m_MouseOverBrush = new SolidColorBrush(Color.FromArgb(255, 135, 206, 250));
        }

        public EventQueueListItem(EventQueueCtrl parent, string name, string description, 
            bool defaultQueue, UIElementCollection eventlist, List<string> checkedEvents, TextData delayed, bool isDelayed, int delayedSetting)
        {
            InitializeComponent();
            m_MouseOverBrush = new SolidColorBrush(Color.FromArgb(255, 135, 206, 250));
            m_Parent = parent;
            m_Selected = false;
            ItemText.Text = name;
            ItemLabel.Content = description;
            m_Events = new List<EventCheckBox>();
            DefaultQueue = defaultQueue;
            DelayedValue = delayed;
            Delayed = isDelayed;
            DelayedSetting = delayedSetting;

            if(defaultQueue)
            {
                RemoveButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                // Loop events
                for (int i = 0; i < eventlist.Count; i++)
                {
                    WinEventCmdListItem item = eventlist[i] as WinEventCmdListItem;
                    if (item != null)
                    {
                        // Loop the List<string> and check if this EventId matches, then it should be selected, otherwise not.
                        bool isChecked = false;
                        if(checkedEvents != null)
                        {
                            for (int n = 0; n < checkedEvents.Count; n++)
                            {
                                if (checkedEvents[n].Equals(item.GetData().Id))
                                {
                                    isChecked = true;
                                    break;
                                }
                            }
                        }

                        // Add
                        AddEvent(new EventCheckBox(item, isChecked));
                    }
                }
            }
        }

        public string GetQueueName()
        {
            return ItemText.Text;
        }

        public string GetQueueDescription()
        {
            return ItemLabel.Content as string;
        }

        public void SetName(string name)
        {
            ItemText.Text = name;
        }

        public void SetDescription(string desc)
        {
            ItemLabel.Content = desc;
        }

        public List<string> GetCheckedEventIds()
        {
            List<string> eventIds = new List<string>();

            for (int i = 0; i < m_Events.Count; i++)
            {
                string id = m_Events[i].GetCheckedEventId();
                if (!string.IsNullOrEmpty(id))
                {
                    eventIds.Add(id);
                }
            }

            return eventIds;
        }

        public void AddEvent(EventCheckBox e)
        {
            m_Events.Add(e);
        }

        public void RemoveEvent(WinEventCmdListItem e)
        {
            for(int i = 0; i < m_Events.Count; i++)
            {
                if(m_Events[i].m_ListViewItem.m_IdString.Equals(e.m_IdString))
                {
                    m_Events.RemoveAt(i);
                    break;
                }
            }
        }

        public void Select(bool selected)
        {
            m_Selected = selected;
            if(m_Selected)
            {
                MainStackPanel.Background = Brushes.PowderBlue;
            }
            else
            {
                MainStackPanel.Background = Brushes.White;
            }
        }

        private void RemoveButton_MouseEnter(object sender, MouseEventArgs e)
        {
            if (m_Selected)
                return;

            MainStackPanel.Background = Brushes.White;
            e.Handled = true;
        }

        private void MainStackPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            if (m_Selected)
                return;

            MainStackPanel.Background = m_MouseOverBrush;
        }

        private void MainStackPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            if (m_Selected)
                return;

            MainStackPanel.Background = Brushes.White;
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            m_Parent.Remove(this);
        }

        private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (m_Selected)
                return;

            m_Parent.Select(this);
        }
    }
}
