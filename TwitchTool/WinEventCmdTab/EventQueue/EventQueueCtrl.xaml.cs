﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace TwitchTool.WinEventCmdTab.EventQueue
{
    /// <summary>
    /// Interaction logic for EventQueueCtrl.xaml
    /// </summary>
    public partial class EventQueueCtrl : UserControl
    {
        public class WindowQueue
        {
            public List<EventQueueList> Queues;
            public bool WindowIsQueued;

            public WindowQueue()
            {
                Queues = new List<EventQueueList>();
                WindowIsQueued = true;
            }
        }

        private List<EventQueueListItem> m_Queues;
        private WindowQueue m_Queue;
        private string m_CompareString;
        private UIElementCollection m_EventList;
        private EventQueueListItem m_Selected;
        public bool m_OrgWindowIsQueued;


        public EventQueueCtrl()
        {
            InitializeComponent();
            m_Queues = new List<EventQueueListItem>();
            m_Queue = new WindowQueue();
            m_CompareString = "";
            m_OrgWindowIsQueued = false;
        }

        public List<EventQueueListItem> GetQueues()
        {
            return m_Queues;
        }

        public WindowQueue GetWindowQueue()
        {
            return m_Queue;
        }

        public void SetWindowQueue(bool isqueued)
        {
            m_Queue.WindowIsQueued = isqueued;
        }

        public void Init(WindowQueue queue)
        {
            m_Queue = queue;
        }

        public void AddEventItem(WinEventCmdListItem item)
        {
            EventCheckBox newItem = new EventCheckBox(item, false);
            for (int i = 0; i < m_Queues.Count; i++)
            {
                m_Queues[i].AddEvent(newItem);
            }
            CheckboxPanel.Children.Add(newItem);
        }

        public void RemoveEventItem(WinEventCmdListItem item)
        {
            for (int i = 0; i < m_Queues.Count; i++)
            {
                m_Queues[i].RemoveEvent(item);
            }

            for (int i = 0; i < CheckboxPanel.Children.Count; i++)
            {
                if ((CheckboxPanel.Children[i] as EventCheckBox).m_ListViewItem.m_IdString.Equals(item.m_IdString))
                {
                    CheckboxPanel.Children.RemoveAt(i);
                    break;
                }
            }
        }

        public bool HasQueueName(string name)
        {
            for (int i = 0; i < m_Queues.Count; i++)
            {
                if (name.Equals(m_Queues[i].ItemText.Text))
                    return true;
            }
            return false;
        }

        public bool HasUniqueQueueNames()
        {
            for (int i = 0; i < m_Queues.Count; i++)
            {
                for (int n = 0; n < m_Queues.Count; n++)
                {
                    if(n != i && m_Queues[n].ItemText.Text.Equals(m_Queues[i].ItemText.Text))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void Load(UIElementCollection eventlist)
        {
            m_EventList = eventlist;
            QueueList.Children.Clear();
            m_Queues.Clear();

            m_Queues.Add(new EventQueueListItem(this, "All (Default)", "Queued with all other events in the window. This is a default queue and cannot be changed or removed.", true, null, null, null, false, 0));
            m_Queues.Add(new EventQueueListItem(this, "No Queueing", "Events will start instantly. This is a default queue and cannot be changed or removed.", true, null, null, null, false, 0));
            QueueList.Children.Add(m_Queues[0]);
            QueueList.Children.Add(m_Queues[1]);

            for (int i = 0; i < m_Queue.Queues.Count; i++)
            {
                EventQueueListItem eventItem = new EventQueueListItem(
                    this,
                    m_Queue.Queues[i].Name,
                    m_Queue.Queues[i].Description, 
                    false, 
                    m_EventList,
                    m_Queue.Queues[i].EventIds,
                    m_Queue.Queues[i].Delayed,
                    m_Queue.Queues[i].IsDelayed,
                    m_Queue.Queues[i].DelayedSetting);

                m_Queues.Add(eventItem);
                QueueList.Children.Add(eventItem);
            }

            // Build compare string
            BuildCompareString();

            Select(m_Queues[0]);
        }

        private void BuildCompareString()
        {
            m_CompareString = "";
            for (int i = 0; i < m_Queues.Count; i++)
            {
                if (m_Queues[i].DefaultQueue)
                    continue;

                m_Queues[i].BuildCompareString();

                m_CompareString += m_Queues[i].GetQueueName() + m_Queues[i].GetQueueDescription() + m_Queues[i].DelayCompareString;
                List<string> eventIds = m_Queues[i].GetCheckedEventIds();
                for (int n = 0; n < eventIds.Count; n++)
                {
                    m_CompareString += eventIds[n];
                }
            }
            m_CompareString += Convert.ToString(m_Queue.WindowIsQueued);
        }

        public bool HasChanges()
        {
            string compareWith = "";
            for(int i = 0; i < m_Queues.Count; i++)
            {
                if (m_Queues[i].DefaultQueue)
                    continue;

                m_Queues[i].BuildCompareString();

                compareWith += m_Queues[i].GetQueueName() + m_Queues[i].GetQueueDescription() + m_Queues[i].DelayCompareString;
                List<string> eventIds = m_Queues[i].GetCheckedEventIds();
                for(int n = 0; n < eventIds.Count; n++)
                {
                    compareWith += eventIds[n];
                }
            }
            compareWith += Convert.ToString(m_Queue.WindowIsQueued);
            return !m_CompareString.Equals(compareWith);
        }

        public string Save()
        {
            m_Queue.Queues.Clear();
            m_OrgWindowIsQueued = m_Queue.WindowIsQueued;

            for (int i = 0; i < m_Queues.Count; i++)
            {
                if (m_Queues[i].DefaultQueue)
                    continue;

                m_Queue.Queues.Add(new EventQueueList(m_Queues[i].GetQueueName(), m_Queues[i].GetQueueDescription(), m_Queues[i].GetCheckedEventIds(), m_Queues[i].DelayedValue, m_Queues[i].Delayed, m_Queues[i].DelayedSetting));
            }

            // ReBuild compare string
            BuildCompareString();

            return JsonConvert.SerializeObject(m_Queue);
        }

        public void Select(EventQueueListItem eventQueueListItem)
        {
            for(int i = 0; i < QueueList.Children.Count; i++)
            {
                (QueueList.Children[i] as EventQueueListItem).Select(false);
            }
            eventQueueListItem.Select(true);
            m_Selected = eventQueueListItem;

            // Display the events this queue contains.
            NameTextBox.Text = eventQueueListItem.ItemText.Text;
            DescriptionTextBox.Text = eventQueueListItem.ItemLabel.Content as string;
            CheckboxPanel.Children.Clear();

            if (eventQueueListItem.DefaultQueue)
            {
                InfoPanel.Visibility = Visibility.Hidden;
            }
            else
            {
                InfoPanel.Visibility = Visibility.Visible;
                for (int i = 0; i < eventQueueListItem.GetEvents().Count; i++)
                {
                    CheckboxPanel.Children.Add(eventQueueListItem.GetEvents()[i]);
                }

                // Delayed queueing
                UseDelayedQueueingCheckBox.IsChecked = eventQueueListItem.Delayed;
                DelayedValue.Update(eventQueueListItem.DelayedValue, true);
                if (eventQueueListItem.DelayedSetting == EventQueueListItem.DELAYED_CHECKED)
                {
                    DelayedCheckedOnly.IsChecked = true;
                }
                else if(eventQueueListItem.DelayedSetting == EventQueueListItem.DELAYED_UNCHECKED)
                {
                    DelayedUnCheckedOnly.IsChecked = true;
                }
                else
                {
                    DelayedBoth.IsChecked = true;
                }
            }
        }

        public void Remove(EventQueueListItem eventQueueListItem)
        {
            // Check if the removed item is the selected one
            if(eventQueueListItem.IsSelected())
            {
                // If so select the first not selected item instead
                for (int i = 0; i < QueueList.Children.Count; i++)
                {
                    EventQueueListItem queue = QueueList.Children[i] as EventQueueListItem;
                    if(!queue.IsSelected())
                    {
                        Select(queue);
                        break;
                    }
                }
            }

            // Remove
            QueueList.Children.Remove(eventQueueListItem);
            m_Queues.Remove(eventQueueListItem);
        }

        private void AddNewQueueButton_Click(object sender, RoutedEventArgs e)
        {
            string newName = "New Queue";
            int count = 0;
            while(HasQueueName(newName))
            {
                count++;
                newName = "New Queue " + count;
            }
            EventQueueListItem queueItem = new EventQueueListItem(this, newName, "", false, m_EventList, null, new Source.TextData(""), false, 0);
            m_Queues.Add(queueItem);
            QueueList.Children.Add(queueItem);
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(!HasQueueName(NameTextBox.Text))
            {
                m_Selected.SetName(NameTextBox.Text);
            }
        }

        private void DescriptionTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            m_Selected.SetDescription(DescriptionTextBox.Text);
        }

        private void UseDelayedQueueingCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            m_Selected.Delayed = true;
            UseDelayedPanel.IsEnabled = true;
            e.Handled = true;
        }

        private void UseDelayedQueueingCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            m_Selected.Delayed = false;
            UseDelayedPanel.IsEnabled = false;
            e.Handled = true;
        }

        private void DelayedCheckedOnly_Checked(object sender, RoutedEventArgs e)
        {
            m_Selected.DelayedSetting = EventQueueListItem.DELAYED_CHECKED;
            e.Handled = true;
        }

        private void DelayedUnCheckedOnly_Checked(object sender, RoutedEventArgs e)
        {
            m_Selected.DelayedSetting = EventQueueListItem.DELAYED_UNCHECKED;
            e.Handled = true;
        }

        private void DelayedBoth_Checked(object sender, RoutedEventArgs e)
        {
            m_Selected.DelayedSetting = EventQueueListItem.DELAYED_BOTH;
            e.Handled = true;
        }
    }
}
