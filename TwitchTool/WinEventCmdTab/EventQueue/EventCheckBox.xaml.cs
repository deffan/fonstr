﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.WinEventCmdTab.EventQueue
{
    /// <summary>
    /// Interaction logic for EventCheckBox.xaml
    /// </summary>
    public partial class EventCheckBox : UserControl
    {
        public WinEventCmdListItem m_ListViewItem;
        public int m_EventType;

        public EventCheckBox()
        {
            InitializeComponent();
        }

        // Constructor used in EventQueues
        public EventCheckBox(WinEventCmdListItem item, bool isChecked)
        {
            InitializeComponent();
            m_ListViewItem = item;
            m_EventType = item.GetData().SubType;
            ItemText.Text = item.GetData().Name;
            ItemImage.Source = EVENT_TYPE.GetEventIcon(item.GetData().SubType);
            ItemLabel.Content = EVENT_TYPE.GetEventName(item.GetData().SubType);
            QueueCheckBox.IsChecked = isChecked;
        }

        // Constructor used for LiveTab displayed events
        public EventCheckBox(int eventType, bool isChecked)
        {
            InitializeComponent();
            m_EventType = eventType;
            ItemText.Text = EVENT_TYPE.GetEventName(eventType);
            ItemImage.Source = EVENT_TYPE.GetEventIcon(eventType);
            ItemLabel.Visibility = Visibility.Collapsed;
            QueueCheckBox.IsChecked = isChecked;
        }

        public string GetCheckedEventId()
        {
            if (m_ListViewItem == null)
                return "";

            if(QueueCheckBox.IsChecked == true)
            {
                return m_ListViewItem.GetData().Id;
            }
            return "";
        }

        private void ActiveCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            MainStackPanel.Background = Brushes.LawnGreen;
        }

        private void ActiveCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            MainStackPanel.Background = Brushes.White;
        }
    }
}
