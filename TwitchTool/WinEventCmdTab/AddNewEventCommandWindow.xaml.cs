﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TwitchTool.Source;

namespace TwitchTool.WinEventCmdTab
{
    /// <summary>
    /// Interaction logic for AddNewEventCommandWindow.xaml
    /// </summary>
    public partial class AddNewEventCommandWindow : Window
    {
        private bool m_Cancel;
        private bool m_IsEvent;
        public bool Cancelled { get { return m_Cancel; } }

        public AddNewEventCommandWindow()
        {
            InitializeComponent();
        }

        public AddNewEventCommandWindow(bool isEvent)
        {
            InitializeComponent();
            m_IsEvent = isEvent;
            m_Cancel = true;

            if (isEvent)
            {
                Title = "Add new event";
                AddButton.Content = "Add event";

                // Input
                TreeViewItem input = AddTreeViewItem(null, "Commands/keyboard.png", "Input Events", 0, FontWeights.Bold);
                AddTreeViewItem(input, "Commands/keyboard.png", "Keyboard", EVENT_TYPE.KEYBOARD, FontWeights.Normal);

                // Twitch
                TreeViewItem twitch = AddTreeViewItem(null, "Events/twitch.png", "Twitch Events", 0, FontWeights.Bold);
                AddTreeViewItem(twitch, "Events/twitch.png", "Follow", EVENT_TYPE.TWITCH_FOLLOW, FontWeights.Normal);
                AddTreeViewItem(twitch, "Events/twitch.png", "Subscription", EVENT_TYPE.TWITCH_SUBSCRIPTION, FontWeights.Normal);
                AddTreeViewItem(twitch, "Events/twitch.png", "Bits", EVENT_TYPE.TWITCH_BITS, FontWeights.Normal);
                AddTreeViewItem(twitch, "Events/twitch.png", "Gifted Subscription", EVENT_TYPE.TWITCH_GIFT, FontWeights.Normal);
                AddTreeViewItem(twitch, "Events/twitch.png", "Raid", EVENT_TYPE.TWITCH_RAID, FontWeights.Normal);
                AddTreeViewItem(twitch, "Events/twitch.png", "Bot Command", EVENT_TYPE.TWITCH_BOT_CUSTOM, FontWeights.Normal);
                AddTreeViewItem(twitch, "Events/twitch.png", "Channel Join", EVENT_TYPE.TWITCH_BOT_JOIN, FontWeights.Normal);
                AddTreeViewItem(twitch, "Events/twitch.png", "Channel Leave", EVENT_TYPE.TWITCH_BOT_PART, FontWeights.Normal);
            }
            else
            {
                Title = "Add new command";
                AddButton.Content = "Add command";

                // Text
                TreeViewItem input = AddTreeViewItem(null, "Commands/text.png", "Text Commands", 0, FontWeights.Bold);
                AddTreeViewItem(input, "Commands/text.png", "Text", COMMAND_TYPE.TEXT, FontWeights.Normal);

                // Image
                TreeViewItem image = AddTreeViewItem(null, "Commands/image.png", "Image Commands", 0, FontWeights.Bold);
                AddTreeViewItem(image, "Commands/image.png", "Image", COMMAND_TYPE.IMAGE, FontWeights.Normal);

                // Sound
                TreeViewItem sound = AddTreeViewItem(null, "Commands/sound.png", "Sound Commands", 0, FontWeights.Bold);
                AddTreeViewItem(sound, "Commands/sound.png", "Sound", COMMAND_TYPE.SOUND, FontWeights.Normal);

                // Video
                TreeViewItem video = AddTreeViewItem(null, "Events/youtube.png", "Video Commands", 0, FontWeights.Bold);
                AddTreeViewItem(video, "Events/youtube.png", "Youtube", COMMAND_TYPE.YOUTUBE, FontWeights.Normal);

                // Object
                TreeViewItem obj = AddTreeViewItem(null, "opensquare.png", "Object Commands", 0, FontWeights.Bold);
                AddTreeViewItem(obj, "Commands/clone.png", "Cloning", COMMAND_TYPE.CLONE, FontWeights.Normal);

                // Animation
                TreeViewItem twitch = AddTreeViewItem(null, "clapper.png", "Animation Commands", 0, FontWeights.Bold);
                AddTreeViewItem(twitch, "Commands/play.png", "Start", COMMAND_TYPE.START_ANIMATION, FontWeights.Normal);
                AddTreeViewItem(twitch, "Commands/stop.png", "Stop", COMMAND_TYPE.STOP_ANIMATION, FontWeights.Normal);
                AddTreeViewItem(twitch, "Commands/wait.png", "Wait", COMMAND_TYPE.WAIT, FontWeights.Normal);
            }
            (TheTreeView.Items[0] as TreeViewItem).IsSelected = true;
        }

        private TreeViewItem AddTreeViewItem(TreeViewItem parent, string icon, string labelText, int type, FontWeight fontStyle)
        {
            TreeViewItem item = new TreeViewItem();

            // Create an object contaning the items parent node and object identity and set it to the tag
            object[] dataHolder = new object[2];
            dataHolder[0] = parent;
            dataHolder[1] = type;
            item.Tag = dataHolder;

            // Add a stack panel to contain [Image][TextLabel]
            StackPanel stack = new StackPanel();
            stack.Orientation = Orientation.Horizontal;

            // Image
            Image image = new Image();
            image.Source = GlobalHelper.GetImage(icon);
            image.Width = 16;

            // Label
            Label lbl = new Label();
            lbl.Name = "TreeViewItemTextLabel";
            lbl.Content = labelText;
            lbl.FontWeight = fontStyle;

            // Add them, and to stackpanel
            stack.Children.Add(image);
            stack.Children.Add(lbl);
            item.Header = stack;

            // Add to TreeView
            if (parent == null)
            {
                TheTreeView.Items.Add(item);
            }
            else
            {
                parent.Items.Add(item);
            }
            return item;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            m_Cancel = false;
            Close();
        }

        public int GetItemType()
        {
            TreeViewItem item = TheTreeView.SelectedItem as TreeViewItem;
            object[] data = item.Tag as object[];
            return Convert.ToInt32(data[1]);
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TheTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeViewItem selected = TheTreeView.SelectedItem as TreeViewItem;
            object[] data = selected.Tag as object[];
            if(data[0] == null)
            {
                AddButton.IsEnabled = false;
            }
            else
            {
                AddButton.IsEnabled = true;

                if(m_IsEvent)
                {
                    NameTextBox.Text = "New " + EVENT_TYPE.GetEventName(GetItemType());
                }
                else
                {
                    NameTextBox.Text = "New " + COMMAND_TYPE.GetCommandName(GetItemType());
                }
            }
        }
    }
}
