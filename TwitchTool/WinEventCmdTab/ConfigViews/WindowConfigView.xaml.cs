﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using TwitchTool.Commands;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.WinEventCmdTab;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for WindowConfigView.xaml
    /// </summary>
    public partial class WindowConfigView : UserControl, IWinEventCmdData
    {
        private string m_Path;
        private string m_WinId;
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public WindowConfigView()
        {
            InitializeComponent();
        }

        public List<CommandListenerComponent> GetListeners() { return MainWindow.Windows[m_WinId].GetListeners(); }
        public AppWindow GetWindow() { return MainWindow.Windows[m_WinId]; }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public WindowConfigView(ListData data)
        {
            InitializeComponent();
            m_ListData = data;
            m_Path = data.Data;
            m_WinId = data.Id;
            ItemList.SetEventQueue(EventQueueControl);
        }

        public void Load()
        {
            ItemList.Load();
            EventQueueControl.Load(ItemList.GetList());
            WindowQueueCheckBox.IsChecked = EventQueueControl.GetWindowQueue().WindowIsQueued;

            // Name
            ItemList.NameTextBox.Text = MainWindow.Windows[m_WinId].WindowObject.GetBasicComponent().GetObjectName();
            m_ListItem.SetName(ItemList.NameTextBox.Text);
            ItemList.NameTextBox.IsEnabled = false;
        }

        public void OpenEditor()
        {
            try
            {
                WindowBuilder b = new WindowBuilder(m_Path);
                b.ShowDialog();
                
                // Reload the window object after editor has been closed
                MainWindow.Windows[m_WinId].Reload();
                Load();
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "WindowConfigView->OpenEditor-> " + ex.Message);
                Logger.Log(LOG_LEVEL.ERROR, "WindowConfigView->OpenEditor-> " + ex.StackTrace);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenEditor();
        }

        public bool HasChanges()
        {
            if (ItemList.HasChanges())
                return true;

            if (EventQueueControl.HasChanges())
                return true;

            return false;
        }

        public string Save()
        {
            ItemList.Save();
            return EventQueueControl.Save();
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public object GetCurrentData()
        {
            return null;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
            EventQueueControl.Load(ItemList.GetList());

            if(EventQueueControl.m_OrgWindowIsQueued != EventQueueControl.GetWindowQueue().WindowIsQueued)
            {
                EventQueueControl.GetWindowQueue().WindowIsQueued = EventQueueControl.m_OrgWindowIsQueued;
                WindowQueueCheckBox.IsChecked = EventQueueControl.GetWindowQueue().WindowIsQueued;
            }
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        private void ExportWinButton_Click(object sender, RoutedEventArgs e)
        {
            ExportWindow winExport = new ExportWindow();
            winExport.Export(MainWindow.Windows[m_WinId]);

            // Display result
            List<string> msg = winExport.Messages();
            if(msg.Count > 0)
            {
                string msgs = "";
                for (int i = 0; i < msg.Count; i++)
                {
                    msgs += msg[i] + Environment.NewLine;
                }

                MessageBox.Show(msgs, "Export", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void QueueButton_Click(object sender, RoutedEventArgs e)
        {
            EventQueueControl.Visibility = Visibility.Visible;
        }

        private void MainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            WinScrollViewer.Width = MainGrid.ActualWidth - 425;
        }

        private void WindowQueueCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            EventQueueControl.SetWindowQueue(true);
            e.Handled = true;
        }

        private void WindowQueueCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            EventQueueControl.SetWindowQueue(false);
            e.Handled = true;
        }
    }
}
