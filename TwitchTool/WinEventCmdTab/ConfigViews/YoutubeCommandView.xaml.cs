﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Commands;
using TwitchTool.Source;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;
namespace TwitchTool.WinEventCmdTab.ConfigViews
{
    /// <summary>
    /// Interaction logic for YoutubeCommandView.xaml
    /// </summary>
    public partial class YoutubeCommandView : UserControl, IWinEventCmdData
    {
        private bool m_Loaded;
        private YoutubeCommandData m_CurrentData;
        private WinEventCmdListItem m_ListItem;
        private string m_CurrentSerializedData;
        private ListData m_ListData;

        public YoutubeCommandView(YoutubeCommandData data, ListData listData)
        {
            InitializeComponent();
            m_Loaded = false;
            m_CurrentData = data;
            m_ListData = listData;
            m_CurrentData.Listeners.Clear();    // Listeners are not set here ...
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Because apparently, this SelectionChanged is THE_SAME as tabControl_SelectionChanged, which causes weird behaviour across the application if not handeled...
            e.Handled = true;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;

            List<EventVariable> tmp = GlobalHelper.EventVariables.GetVariables(m_ListItem.m_Parent.GetData().SubType);
            LinkCtrl.SetEventVariables(tmp);
            BackupLinkCtrl.SetEventVariables(tmp);
            StartAtCtrl.SetEventVariables(tmp);
            RunTimeCtrl.SetEventVariables(tmp);

            LinkCtrl.SetWidth(600);
            BackupLinkCtrl.SetWidth(600);
            StartAtCtrl.SetWidth(600);
            RunTimeCtrl.SetWidth(600);
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            if (LinkCtrl.HasChanges())
                return true;

            if (BackupLinkCtrl.HasChanges())
                return true;

            if (StartAtCtrl.HasChanges())
                return true;

            if (RunTimeCtrl.HasChanges())
                return true;

            // Create temporary data for compare
            YoutubeCommandData newData = JsonConvert.DeserializeObject<YoutubeCommandData>(m_CurrentSerializedData);

            // Save Name
            newData.Name = ItemList.NameTextBox.Text;

            // Use component or specific, backup
            newData.m_UseBackup = BackupCheckbox.IsChecked == true;
            newData.m_UseComponentSettings = UseComponentRadioButton.IsChecked == true;

            // Start time
            newData.m_RunFromBeginning = StartBeginningRadioButton.IsChecked == true;
            newData.m_RunFromStartLink = StartLinkTimeRadioButton.IsChecked == true;
            newData.m_RunSpecific = StartSpecificRadioButton.IsChecked == true;

            // Duration
            newData.m_DurationFull = RunToEndRadioButton.IsChecked == true;
            newData.m_DurationSpecific = RunSpecificRadioButton.IsChecked == true;

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            if (!m_Loaded)
            {
                // Load only occurs when we open the view. So if we try to save an unopened view, we need to first load to save...
                Load();
            }

            ItemList.Save();

            LinkCtrl.Update(m_CurrentData.m_Data, false);
            BackupLinkCtrl.Update(m_CurrentData.m_BackupData, false);
            RunTimeCtrl.Update(m_CurrentData.m_DurationData, true);
            StartAtCtrl.Update(m_CurrentData.m_StartData, true);

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            m_CurrentData.m_UseBackup = BackupCheckbox.IsChecked == true;
            m_CurrentData.m_UseComponentSettings = UseComponentRadioButton.IsChecked == true;

            m_CurrentData.m_RunFromBeginning = StartBeginningRadioButton.IsChecked == true;
            m_CurrentData.m_RunFromStartLink = StartLinkTimeRadioButton.IsChecked == true;
            m_CurrentData.m_RunSpecific = StartSpecificRadioButton.IsChecked == true;

            m_CurrentData.m_DurationFull = RunToEndRadioButton.IsChecked == true;
            m_CurrentData.m_DurationSpecific = RunSpecificRadioButton.IsChecked == true;

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void Load()
        {
            m_Loaded = true;

            ItemList.Load();
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            LinkCtrl.Update(m_CurrentData.m_Data, false);
            BackupLinkCtrl.Update(m_CurrentData.m_BackupData, false);
            RunTimeCtrl.Update(m_CurrentData.m_DurationData, true);
            StartAtCtrl.Update(m_CurrentData.m_StartData, true);

            BackupCheckbox.IsChecked = m_CurrentData.m_UseBackup;
            BackupLinkCtrl.IsEnabled = m_CurrentData.m_UseBackup;
            UseComponentRadioButton.IsChecked = m_CurrentData.m_UseComponentSettings;
            SetNewRadioButton.IsChecked = !m_CurrentData.m_UseComponentSettings;

            if(m_CurrentData.m_RunFromBeginning)
            {
                StartBeginningRadioButton.IsChecked = true;
                StartAtCtrl.IsEnabled = false;
            }
            else if(m_CurrentData.m_RunFromStartLink)
            {
                StartLinkTimeRadioButton.IsChecked = true;
                StartAtCtrl.IsEnabled = false;
            }
            else
            {
                StartSpecificRadioButton.IsChecked = true;
                StartAtCtrl.IsEnabled = true;
            }

            if(m_CurrentData.m_DurationFull)
            {
                RunToEndRadioButton.IsChecked = true;
                RunTimeCtrl.IsEnabled = false;
            }
            else
            {
                RunSpecificRadioButton.IsChecked = true;
                RunTimeCtrl.IsEnabled = true;
            }

        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        private void BackupCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            BackupLinkCtrl.IsEnabled = true;
            e.Handled = true;
        }

        private void BackupCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            BackupLinkCtrl.IsEnabled = false;
            e.Handled = true;
        }

        private void UseComponentRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SetNewPanel.Visibility = Visibility.Collapsed;
            e.Handled = true;
        }

        private void SetNewRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SetNewPanel.Visibility = Visibility.Visible;
            e.Handled = true;
        }

        private void StartBeginningRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            StartAtCtrl.IsEnabled = false;
            e.Handled = true;
        }

        private void StartBeginningRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void StartLinkTimeRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            StartAtCtrl.IsEnabled = false;
            e.Handled = true;
        }

        private void StartLinkTimeRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void StartSpecificRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            StartAtCtrl.IsEnabled = true;
            e.Handled = true;
        }

        private void StartSpecificRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void RunToEndRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RunTimeCtrl.IsEnabled = false;
            e.Handled = true;
        }

        private void RunToEndRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void RunSpecificRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RunTimeCtrl.IsEnabled = true;
            e.Handled = true;
        }

        private void RunSpecificRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
