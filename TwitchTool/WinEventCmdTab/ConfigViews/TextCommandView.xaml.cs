﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using TwitchTool.Commands;
using TwitchTool.Components;
using TwitchTool.Events;
using TwitchTool.Source;
using TwitchTool.WinEventCmdTab;
using TwitchTool.WinEventCmdTab.ConfigViews;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.UserControls
{
    public partial class TextCommandView : UserControl, IWinEventCmdData
    {
        private bool m_Loaded;
        private TextCommandData m_CurrentData;
        private WinEventCmdListItem m_ListItem;
        private string m_CurrentSerializedData;
        private ListData m_ListData;

        public TextCommandView(TextCommandData data, ListData listData)
        {
            InitializeComponent();
            m_Loaded = false;
            m_CurrentData = data;
            m_CurrentData.Listeners.Clear();    // Listeners are not set here ...
            m_ListData = listData;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Because apparently, this SelectionChanged is THE_SAME as tabControl_SelectionChanged, which causes weird behaviour across the application if not handeled...
            e.Handled = true;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;

            // Fill the EventData combobox
            List<EventVariable> tmp = GlobalHelper.EventVariables.GetVariables(m_ListItem.m_Parent.GetData().SubType);
            for (int i = 0; i < tmp.Count; i++)
            {
                EventDataCombo.Items.Add(tmp[i]);
            }

            // Fill the DataVariables combobox
            List<DataVariable> tmp2 = GlobalHelper.DataVariables.GetVariables();
            for (int i = 0; i < tmp2.Count; i++)
            {
                VariableDataCombo.Items.Add(tmp2[i]);
            }

            EventDataCombo.SelectionChanged += Combo_SelectionChanged;
            VariableDataCombo.SelectionChanged += Combo_SelectionChanged;

            EventDataCombo.SelectedIndex = 0;
            VariableDataCombo.SelectedIndex = 0;
        }

        public void RemoveBlock(TextCommandDataBlock block)
        {
            DataPanel.Children.Remove(block);
            RebuildExampleData();
        }

        public void MoveBlockForward(TextCommandDataBlock block)
        {
            int index = DataPanel.Children.IndexOf(block);
            if (index + 1 == DataPanel.Children.Count)
                return;
            DataPanel.Children.RemoveAt(index);
            DataPanel.Children.Insert(index + 1, block);
            RebuildExampleData();
        }

        public void MoveBlockBackwards(TextCommandDataBlock block)
        {
            int index = DataPanel.Children.IndexOf(block);
            if (index == 0)
                return;
            DataPanel.Children.RemoveAt(index);
            DataPanel.Children.Insert(index - 1, block);
            RebuildExampleData();
        }

        private void RebuildExampleData()
        {
            ExampleText.Text = "";
            for(int i = 0; i < DataPanel.Children.Count; i++)
            {
                TextCommandDataBlock block = DataPanel.Children[i] as TextCommandDataBlock;
                switch(block.Datatype)
                {
                    case TextCommandDataBlock.DataType.Text: ExampleText.Text += block.m_TextData; break;
                    case TextCommandDataBlock.DataType.EventVariable: ExampleText.Text += block.m_EventVariableData.ExampleData; break;
                    case TextCommandDataBlock.DataType.DataVariable: ExampleText.Text += block.m_DataVariableData.GetExampleData(); break;
                }
            }
        }

        private void TextDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (TextDataBox.Text.Length == 0)
                return;

            TextCommandDataBlock b = new TextCommandDataBlock(this);
            b.SetData(TextDataBox.Text);
            DataPanel.Children.Add(b);
            RebuildExampleData();
        }

        private void VariableDataButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (VariableDataCombo.SelectedItem == null)
                return;

            TextCommandDataBlock b = new TextCommandDataBlock(this);
            b.SetData(VariableDataCombo.SelectedItem as DataVariable);
            DataPanel.Children.Add(b);
            RebuildExampleData();
        }

        private void EventDataButton_Click_1(object sender, RoutedEventArgs e)
        {
            if (EventDataCombo.SelectedItem == null)
                return;

            TextCommandDataBlock b = new TextCommandDataBlock(this);
            b.SetData(EventDataCombo.SelectedItem as EventVariable);
            DataPanel.Children.Add(b);
            RebuildExampleData();
        }

        private TextCommandDataBlock GetDataBlock(TextData data)
        {
            TextCommandDataBlock block = new TextCommandDataBlock(this);

            if (data.Type.Equals("text"))
            {
                block.SetData(data.Data);
            }
            else if (data.Type.Equals("event"))
            {
                block.SetData(GlobalHelper.EventVariables.GetVariable(data.Id));
            }
            else if (data.Type.Equals("variable"))
            {
                DataVariable v = GlobalHelper.DataVariables.GetVariable(data.Id);
                if(v == null)
                {
                    // Variable was probably removed, default it to empty text.
                    block.SetData("");
                }
                else
                {
                    block.SetData(v);
                }
            }

            return block;
        }

        private TextData GetTextData(TextCommandDataBlock block)
        {
            TextData text = new TextData("");
            switch (block.Datatype)
            {
                case TextCommandDataBlock.DataType.Text:
                {
                    text.Type = "text";
                    text.Data = block.m_TextData;
                    text.IsDataVariable = false;
                    text.IsText = true;
                    text.IsEventVariable = false;
                    break;
                }
                case TextCommandDataBlock.DataType.EventVariable:
                {
                    text.Type = "event";
                    text.Id = block.m_EventVariableData.Id;
                    text.Data = ""; // Data is fetched from event variable
                    text.IsDataVariable = false;
                    text.IsText = false;
                    text.IsEventVariable = true;
                    break;
                }
                case TextCommandDataBlock.DataType.DataVariable:
                {
                    text.Type = "variable";
                    text.Id = block.m_DataVariableData.Id;
                    text.Data = ""; // Data is fetched from data variable
                    text.IsDataVariable = true;
                    text.IsText = false;
                    text.IsEventVariable = false;
                    break;
                }
            }
            return text;
        }

        private void ClearDataButton_Click(object sender, RoutedEventArgs e)
        {
            DataPanel.Children.Clear();
            RebuildExampleData();
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            // Create temporary data for compare
            TextCommandData newData = JsonConvert.DeserializeObject<TextCommandData>(m_CurrentSerializedData);
            newData.TextDataList.Clear();

            // Save Name
            newData.Name = ItemList.NameTextBox.Text;

            // Save Data
            for(int i = 0; i < DataPanel.Children.Count; i++)
            {
                newData.TextDataList.Add(GetTextData(DataPanel.Children[i] as TextCommandDataBlock));
            }

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            if(!m_Loaded)
            {
                // Load only occurs when we open the view. So if we try to save an unopened view, we need to first load to save...
                Load();
            }

            ItemList.Save();
        
            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            // Save Data
            m_CurrentData.TextDataList.Clear();
            for (int i = 0; i < DataPanel.Children.Count; i++)
            {
                m_CurrentData.TextDataList.Add(GetTextData(DataPanel.Children[i] as TextCommandDataBlock));
            }

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        private void ReloadDataButton_Click(object sender, RoutedEventArgs e)
        {
            DataPanel.Children.Clear();
            for (int i = 0; i < m_CurrentData.TextDataList.Count; i++)
            {
                DataPanel.Children.Add(GetDataBlock(m_CurrentData.TextDataList[i]));
            }
            RebuildExampleData();
        }

        public void Load()
        {
            m_Loaded = true;

            // Clear all current data
            DataPanel.Children.Clear();
            ExampleText.Text = "";
            TextDataBox.Text = "";

            ItemList.Load();
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            // Add blocks of data
            for (int i = 0; i < m_CurrentData.TextDataList.Count; i++)
            {
                DataPanel.Children.Add(GetDataBlock(m_CurrentData.TextDataList[i]));
            }
            RebuildExampleData();
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }
    }
}
