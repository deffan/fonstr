﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TwitchTool.Events;
using TwitchTool.Source;
using TwitchTool.WinEventCmdTab;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for KeyboardEventView.xaml
    /// </summary>
    public partial class KeyboardEventView : UserControl, IWinEventCmdData
    {
        private bool m_Loaded;
        private List<Key> m_Keys;
        private KeyboardEventData m_CurrentData;
        private string m_CurrentSerializedData;
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public KeyboardEventView(EventData data, ListData listData)
        {
            InitializeComponent();
            m_Keys = new List<Key>();
            m_Loaded = false;
            m_CurrentData = data as KeyboardEventData;
            m_ListData = listData;
            m_CurrentData.Name = m_ListData.Name;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public void Load()
        {
            m_Loaded = true;

            ItemList.Load();

            // Name
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            KeyTextBox.Text = "";
            KeyTextBox.Focus();
            m_Keys.Clear();

            // Refill key list and textbox
            for (int i = 0; i < m_CurrentData.Keys.Count; i++)
            {
                Key key;
                if(Enum.TryParse(m_CurrentData.Keys[i], out key))
                {
                    if (m_Keys.Count > 0)
                    {
                        KeyTextBox.Text += " + ";
                    }
                    KeyTextBox.Text += m_CurrentData.Keys[i];
                    m_Keys.Add(key);
                }
            }
        }

        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if (StartKeyConfigButton.IsEnabled)
                return;

            if(!m_Keys.Contains(e.Key))
            {
                if(m_Keys.Count > 0)
                {
                    KeyTextBox.Text += " + ";
                }
                KeyTextBox.Text += Enum.GetName(typeof(Key), e.Key);

                m_Keys.Add(e.Key);
            }
            e.Handled = true;
        }

        private void Grid_KeyUp(object sender, KeyEventArgs e)
        {
            if (StartKeyConfigButton.IsEnabled)
                return;

            e.Handled = true;
            StartKeyConfigButton.IsEnabled = true;
        }

        private void StartKeyConfigButton_Click(object sender, RoutedEventArgs e)
        {
            m_Keys.Clear();

            StartKeyConfigButton.IsEnabled = false;
            KeyTextBox.Text = "";
            KeyTextBox.Focus();
            e.Handled = true;
        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            KeyboardEventData newData = JsonConvert.DeserializeObject<KeyboardEventData>(m_CurrentSerializedData);
            newData.Keys.Clear();

            // Save Name
            newData.Name = ItemList.NameTextBox.Text;

            // Save Keys 
            List<string> keys = new List<string>();
            for (int i = 0; i < m_Keys.Count; i++)
            {
                keys.Add(Enum.GetName(typeof(Key), m_Keys[i]));
            }
            newData.Keys.AddRange(keys);

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            if (!m_Loaded)
            {
                // Load only occurs when we open the view. So if we try to save an unopened view, we need to first load to save...
                Load();
            }

            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(m_CurrentData.Name);

            // Save Keys 
            m_CurrentData.Keys.Clear();
            List<string> keys = new List<string>();
            for (int i = 0; i < m_Keys.Count; i++)
            {
                keys.Add(Enum.GetName(typeof(Key), m_Keys[i]));
            }
            m_CurrentData.Keys.AddRange(keys);

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }
    }
}
