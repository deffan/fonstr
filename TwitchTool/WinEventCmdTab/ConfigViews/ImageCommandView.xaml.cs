﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Commands;
using TwitchTool.Source;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews
{
    /// <summary>
    /// Interaction logic for ImageCommandView.xaml
    /// </summary>
    public partial class ImageCommandView : UserControl, IWinEventCmdData
    {
        private bool m_Loaded;
        private ImageCommandData m_CurrentData;
        private WinEventCmdListItem m_ListItem;
        private string m_CurrentSerializedData;
        private ListData m_ListData;

        public ImageCommandView(ImageCommandData data, ListData listData)
        {
            InitializeComponent();
            m_Loaded = false;
            m_CurrentData = data;
            m_ListData = listData;
            m_CurrentData.Listeners.Clear();    // Listeners are not set here ...
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Because apparently, this SelectionChanged is THE_SAME as tabControl_SelectionChanged, which causes weird behaviour across the application if not handeled...
            e.Handled = true;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;

            List<EventVariable> tmp = GlobalHelper.EventVariables.GetVariables(m_ListItem.m_Parent.GetData().SubType);
            ImgCtrl.SetEventVariables(tmp);
            BackupImgCtrl.SetEventVariables(tmp);
            ImgCtrl.SetWidth(600);
            BackupImgCtrl.SetWidth(600);
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            if (ImgCtrl.HasChanges())
                return true;

            if (BackupImgCtrl.HasChanges())
                return true;

            // Create temporary data for compare
            ImageCommandData newData = JsonConvert.DeserializeObject<ImageCommandData>(m_CurrentSerializedData);

            // Save Name
            newData.Name = ItemList.NameTextBox.Text;

            newData.m_UseBackup = BackupCheckbox.IsChecked == true;
            newData.m_UseComponentImage = UseComponentRadioButton.IsChecked == true;

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            if (!m_Loaded)
            {
                // Load only occurs when we open the view. So if we try to save an unopened view, we need to first load to save...
                Load();
            }

            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            ImgCtrl.Update(m_CurrentData.m_Data, false);
            BackupImgCtrl.Update(m_CurrentData.m_BackupData, false);

            m_CurrentData.m_UseBackup = BackupCheckbox.IsChecked == true;
            m_CurrentData.m_UseComponentImage = UseComponentRadioButton.IsChecked == true;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void Load()
        {
            m_Loaded = true;

            ItemList.Load();
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            ImgCtrl.Update(m_CurrentData.m_Data, false);
            BackupImgCtrl.Update(m_CurrentData.m_BackupData, false);

            BackupCheckbox.IsChecked = m_CurrentData.m_UseBackup;
            BackupImgCtrl.IsEnabled = m_CurrentData.m_UseBackup;
            if(m_CurrentData.m_UseComponentImage)
            {
                UseComponentRadioButton.IsChecked = true;
            }
            else
            {
                SetNewRadioButton.IsChecked = true;
            }
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        private void BackupCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            BackupImgCtrl.IsEnabled = true;
            e.Handled = true;
        }

        private void BackupCheckbox_Unchecked(object sender, RoutedEventArgs e)
        {
            BackupImgCtrl.IsEnabled = false;
            e.Handled = true;
        }

        private void UseComponentRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SetNewPanel.Visibility = Visibility.Collapsed;
            e.Handled = true;
        }

        private void SetNewRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            SetNewPanel.Visibility = Visibility.Visible;
            e.Handled = true;
        }
    }
}
