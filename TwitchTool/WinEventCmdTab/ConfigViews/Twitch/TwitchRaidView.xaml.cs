﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews.Twitch
{
    /// <summary>
    /// Interaction logic for TwitchRaidView.xaml
    /// </summary>
    public partial class TwitchRaidView : UserControl, IWinEventCmdData
    {
        private TwitchRaidEventData m_CurrentData;
        private string m_CurrentSerializedData;
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public TwitchRaidView(EventData data, ListData listData)
        {
            InitializeComponent();
            m_CurrentData = data as TwitchRaidEventData;
            m_ListData = listData;
            m_CurrentData.Name = m_ListData.Name;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public void Load()
        {
            ItemList.Load();

            // Name
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            if (string.IsNullOrEmpty(m_CurrentData.UserName))
            {
                AnyRadioButton.IsChecked = true;
            }
            else
            {
                SpecificNameRadioButton.IsChecked = true;
                UserNameTextBox.Text = m_CurrentData.UserName;
            }

            if(m_CurrentData.MinimumNumberOfRaiders < 1)
            {
                AnyRaiderAmountRadioButton.IsChecked = true;
            }
            else
            {
                MoreThanRadioButton.IsChecked = true;
                MoreThanRaiders.Value = Convert.ToDecimal(m_CurrentData.MinimumNumberOfRaiders);
            }

        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            TwitchRaidEventData newData = JsonConvert.DeserializeObject<TwitchRaidEventData>(m_CurrentSerializedData);
            newData.Name = ItemList.NameTextBox.Text;
            if (AnyRadioButton.IsChecked == true)
            {
                newData.UserName = "";
            }
            else
            {
                newData.UserName = UserNameTextBox.Text;
            }

            if(AnyRaiderAmountRadioButton.IsChecked == true)
            {
                newData.MinimumNumberOfRaiders = 0;
            }
            else
            {
                newData.MinimumNumberOfRaiders = Convert.ToInt32(MoreThanRaiders.Value.Value);
            }
            
            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(m_CurrentData.Name);

            if (AnyRadioButton.IsChecked == true)
            {
                m_CurrentData.UserName = "";
            }
            else
            {
                m_CurrentData.UserName = UserNameTextBox.Text;
            }

            if (AnyRaiderAmountRadioButton.IsChecked == true)
            {
                m_CurrentData.MinimumNumberOfRaiders = 0;
            }
            else
            {
                m_CurrentData.MinimumNumberOfRaiders = Convert.ToInt32(MoreThanRaiders.Value.Value);
            }

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        private void AnyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Hidden;
            e.Handled = true;
        }

        private void SpecificNameRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Visible;
            e.Handled = true;
        }

        private void AnyRaiderAmountRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void MoreThanRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (MoreThanRadioButton.IsChecked == false)
                return;

            e.Handled = true;
            MoreThanRaiders.IsEnabled = true;
        }

        private void MoreThanRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (MoreThanRadioButton.IsChecked == true)
                return;

            e.Handled = true;
            MoreThanRaiders.IsEnabled = false;
        }
    }
}
