﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews.Twitch
{
    /// <summary>
    /// Interaction logic for TwitchJoinView.xaml
    /// </summary>
    public partial class TwitchJoinView : UserControl, IWinEventCmdData
    {
        private TwitchJoinEventData m_CurrentData;
        private string m_CurrentSerializedData;
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public TwitchJoinView(EventData data, ListData listData)
        {
            InitializeComponent();
            m_CurrentData = data as TwitchJoinEventData;
            m_ListData = listData;
            m_CurrentData.Name = m_ListData.Name;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public void Load()
        {
            ItemList.Load();

            // Name
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            if (string.IsNullOrEmpty(m_CurrentData.UserName))
            {
                AnyRadioButton.IsChecked = true;
            }
            else
            {
                SpecificNameRadioButton.IsChecked = true;
                UserNameTextBox.Text = m_CurrentData.UserName;
            }
        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            TwitchJoinEventData newData = JsonConvert.DeserializeObject<TwitchJoinEventData>(m_CurrentSerializedData);
            newData.Name = ItemList.NameTextBox.Text;
            if (AnyRadioButton.IsChecked == true)
            {
                newData.UserName = "";
            }
            else
            {
                newData.UserName = UserNameTextBox.Text;
            }

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(m_CurrentData.Name);

            if (AnyRadioButton.IsChecked == true)
            {
                m_CurrentData.UserName = "";
            }
            else
            {
                m_CurrentData.UserName = UserNameTextBox.Text;
            }

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        private void AnyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Hidden;
            e.Handled = true;
        }

        private void SpecificNameRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Visible;
            e.Handled = true;
        }

    }
}
