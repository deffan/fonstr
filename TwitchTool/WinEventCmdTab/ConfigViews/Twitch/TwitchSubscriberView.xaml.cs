﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews.Twitch
{
    public partial class TwitchSubscriberView : UserControl, IWinEventCmdData
    {
        private TwitchSubscriptionEventData m_CurrentData;
        private string m_CurrentSerializedData;
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public TwitchSubscriberView(EventData data, ListData listData)
        {
            InitializeComponent();
            m_CurrentData = data as TwitchSubscriptionEventData;
            m_ListData = listData;
            m_CurrentData.Name = m_ListData.Name;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public void Load()
        {
            ItemList.Load();

            // Name
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            // Username
            if (string.IsNullOrEmpty(m_CurrentData.UserName))
            {
                AnyRadioButton.IsChecked = true;
            }
            else
            {
                SpecificNameRadioButton.IsChecked = true;
                UserNameTextBox.Text = m_CurrentData.UserName;
            }

            // Subplan
            switch(m_CurrentData.SubPlan)
            {
                case "Prime": PrimeSubPlanRadioButton.IsChecked = true; break;
                case "1000": SubPlan1000RadioButton.IsChecked = true; break;
                case "2000": SubPlan2000RadioButton.IsChecked = true; break;
                case "3000": SubPlan3000RadioButton.IsChecked = true; break;
                default: AnySubPlanRadioButton.IsChecked = true; break;
            }

            // Sub/Resub/Both
            switch (m_CurrentData.TypeOfSub)
            {
                case "sub": SubOnlyRadioButton.IsChecked = true; break;
                case "resub": ResubOnlyRadioButton.IsChecked = true; break;
                default: AnySubRadioButton.IsChecked = true; break;
            }

            // Months
            if (m_CurrentData.MinMonths == -1 && m_CurrentData.MaxMonths == -1)
            {
                // Any
                AnyMonthsRadioButton.IsChecked = true;
            }
            else if (m_CurrentData.MinMonths >= 1 && m_CurrentData.MaxMonths >= 1)
            {
                // Between
                BetweenMonthsRadioButton.IsChecked = true;
                FromMonths.Value = Convert.ToDecimal(m_CurrentData.MinMonths);
                ToMonths.Value = Convert.ToDecimal(m_CurrentData.MaxMonths);
            }
            else if (m_CurrentData.MinMonths >= 1 && m_CurrentData.MaxMonths == -1)
            {
                // Exacly
                AnyExaclyRadioButton.IsChecked = true;
                ExaclyMonths.Value = Convert.ToDecimal(m_CurrentData.MinMonths);
            }
            else if (m_CurrentData.MinMonths == -1 && m_CurrentData.MaxMonths >= 1)
            {
                // More than
                AboveMonthsRadioButton.IsChecked = true;
                MoreThanMonths.Value = Convert.ToDecimal(m_CurrentData.MaxMonths);
            }

        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            TwitchSubscriptionEventData newData = JsonConvert.DeserializeObject<TwitchSubscriptionEventData>(m_CurrentSerializedData);
            newData.Name = ItemList.NameTextBox.Text;
            if (AnyRadioButton.IsChecked == true)
            {
                newData.UserName = "";
            }
            else
            {
                newData.UserName = UserNameTextBox.Text;
            }

            // Sub type
            if(SubOnlyRadioButton.IsChecked == true)
            {
                newData.TypeOfSub = "sub";
            }
            else if (ResubOnlyRadioButton.IsChecked == true)
            {
                newData.TypeOfSub = "resub";
            }
            else
            {
                newData.TypeOfSub = "";
            }

            // Sub plan
            if (PrimeSubPlanRadioButton.IsChecked == true)
            {
                newData.SubPlan = "Prime";
            }
            else if (SubPlan1000RadioButton.IsChecked == true)
            {
                newData.SubPlan = "1000";
            }
            else if (SubPlan2000RadioButton.IsChecked == true)
            {
                newData.SubPlan = "2000";
            }
            else if (SubPlan3000RadioButton.IsChecked == true)
            {
                newData.SubPlan = "3000";
            }
            else
            {
                newData.SubPlan = "";
            }

            // Months
            if(AnyMonthsRadioButton.IsChecked == true)
            {
                newData.MinMonths = -1;
                newData.MaxMonths = -1;
            }
            else if (BetweenMonthsRadioButton.IsChecked == true)
            {
                newData.MinMonths = Convert.ToInt32(FromMonths.Value.Value);
                newData.MaxMonths = Convert.ToInt32(ToMonths.Value.Value);
            }
            else if (AnyExaclyRadioButton.IsChecked == true)
            {
                newData.MinMonths = Convert.ToInt32(ExaclyMonths.Value.Value);
                newData.MaxMonths = -1;
            }
            else if (AboveMonthsRadioButton.IsChecked == true)
            {
                newData.MinMonths = -1;
                newData.MaxMonths = Convert.ToInt32(MoreThanMonths.Value.Value);
            }

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(m_CurrentData.Name);

            if (AnyRadioButton.IsChecked == true)
            {
                m_CurrentData.UserName = "";
            }
            else
            {
                m_CurrentData.UserName = UserNameTextBox.Text;
            }

            // Sub type
            if (SubOnlyRadioButton.IsChecked == true)
            {
                m_CurrentData.TypeOfSub = "sub";
            }
            else if (ResubOnlyRadioButton.IsChecked == true)
            {
                m_CurrentData.TypeOfSub = "resub";
            }
            else
            {
                m_CurrentData.TypeOfSub = "";
            }

            // Sub plan
            if (PrimeSubPlanRadioButton.IsChecked == true)
            {
                m_CurrentData.SubPlan = "Prime";
            }
            else if (SubPlan1000RadioButton.IsChecked == true)
            {
                m_CurrentData.SubPlan = "1000";
            }
            else if (SubPlan2000RadioButton.IsChecked == true)
            {
                m_CurrentData.SubPlan = "2000";
            }
            else if (SubPlan3000RadioButton.IsChecked == true)
            {
                m_CurrentData.SubPlan = "3000";
            }
            else
            {
                m_CurrentData.SubPlan = "";
            }

            // Months
            if (AnyMonthsRadioButton.IsChecked == true)
            {
                m_CurrentData.MinMonths = -1;
                m_CurrentData.MaxMonths = -1;
            }
            else if (BetweenMonthsRadioButton.IsChecked == true)
            {
                m_CurrentData.MinMonths = Convert.ToInt32(FromMonths.Value.Value);
                m_CurrentData.MaxMonths = Convert.ToInt32(ToMonths.Value.Value);
            }
            else if (AnyExaclyRadioButton.IsChecked == true)
            {
                m_CurrentData.MinMonths = Convert.ToInt32(ExaclyMonths.Value.Value);
                m_CurrentData.MaxMonths = -1;
            }
            else if (AboveMonthsRadioButton.IsChecked == true)
            {
                m_CurrentData.MinMonths = -1;
                m_CurrentData.MaxMonths = Convert.ToInt32(MoreThanMonths.Value.Value);
            }

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        private void AnyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Hidden;
            e.Handled = true;
        }

        private void SpecificNameRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Visible;
            e.Handled = true;
        }

        private void SubOnlyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void ResubOnlyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void AnySubRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void AnySubPlanRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void PrimeSubPlanRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void SubPlan1000RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void SubPlan2000RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void SubPlan3000RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void AnyMonthsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void AnyExaclyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (AnyExaclyRadioButton.IsChecked == false)
                return;

            e.Handled = true;
            ExaclyMonths.IsEnabled = true;
        }

        private void AnyExaclyRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (AnyExaclyRadioButton.IsChecked == true)
                return;

            e.Handled = true;
            ExaclyMonths.IsEnabled = false;
        }

        private void BetweenMonthsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (BetweenMonthsRadioButton.IsChecked == false)
                return;

            e.Handled = true;
            FromMonths.IsEnabled = true;
            ToMonths.IsEnabled = true;
        }

        private void BetweenMonthsRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (BetweenMonthsRadioButton.IsChecked == true)
                return;

            e.Handled = true;
            FromMonths.IsEnabled = false;
            ToMonths.IsEnabled = false;
        }

        private void AboveMonthsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (AboveMonthsRadioButton.IsChecked == false)
                return;

            e.Handled = true;
            MoreThanMonths.IsEnabled = true;
        }

        private void AboveMonthsRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (AboveMonthsRadioButton.IsChecked == true)
                return;

            e.Handled = true;
            MoreThanMonths.IsEnabled = false;
        }
    }
}
