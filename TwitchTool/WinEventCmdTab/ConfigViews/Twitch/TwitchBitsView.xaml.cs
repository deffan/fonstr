﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews.Twitch
{
    /// <summary>
    /// Interaction logic for TwitchBitsView.xaml
    /// </summary>
    public partial class TwitchBitsView : UserControl, IWinEventCmdData
    {
        private TwitchBitsEventData m_CurrentData;
        private string m_CurrentSerializedData;
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public TwitchBitsView(EventData data, ListData listData)
        {
            InitializeComponent();
            m_CurrentData = data as TwitchBitsEventData;
            m_ListData = listData;
            m_CurrentData.Name = m_ListData.Name;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public void Load()
        {
            ItemList.Load();

            // Name
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            // Username
            if (string.IsNullOrEmpty(m_CurrentData.UserName))
            {
                AnyRadioButton.IsChecked = true;
            }
            else
            {
                SpecificNameRadioButton.IsChecked = true;
                UserNameTextBox.Text = m_CurrentData.UserName;
            }

            // Bits
            if (m_CurrentData.LowBits == -1 && m_CurrentData.HighBits == -1)
            {
                // Any
                AnyBitsRadioButton.IsChecked = true;
            }
            else if (m_CurrentData.LowBits >= 1 && m_CurrentData.HighBits >= 1)
            {
                // Between
                BetweenBitsRadioButton.IsChecked = true;
                FromBits.Value = Convert.ToDecimal(m_CurrentData.LowBits);
                ToBits.Value = Convert.ToDecimal(m_CurrentData.HighBits);
            }
            else if (m_CurrentData.LowBits >= 1 && m_CurrentData.HighBits == -1)
            {
                // Exacly
                ExaclyBitsButton.IsChecked = true;
                ExaclyBits.Value = Convert.ToDecimal(m_CurrentData.LowBits);
            }
            else if (m_CurrentData.LowBits == -1 && m_CurrentData.HighBits >= 1)
            {
                // More than
                AboveBitsRadioButton.IsChecked = true;
                MoreThanBits.Value = Convert.ToDecimal(m_CurrentData.HighBits);
            }

        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            TwitchBitsEventData newData = JsonConvert.DeserializeObject<TwitchBitsEventData>(m_CurrentSerializedData);
            newData.Name = ItemList.NameTextBox.Text;
            if (AnyRadioButton.IsChecked == true)
            {
                newData.UserName = "";
            }
            else
            {
                newData.UserName = UserNameTextBox.Text;
            }

            // Bits
            if (AnyBitsRadioButton.IsChecked == true)
            {
                newData.LowBits = -1;
                newData.HighBits = -1;
            }
            else if (BetweenBitsRadioButton.IsChecked == true)
            {
                newData.LowBits = Convert.ToInt32(FromBits.Value.Value);
                newData.HighBits = Convert.ToInt32(ToBits.Value.Value);
            }
            else if (ExaclyBitsButton.IsChecked == true)
            {
                newData.LowBits = Convert.ToInt32(ExaclyBits.Value.Value);
                newData.HighBits = -1;
            }
            else if (AboveBitsRadioButton.IsChecked == true)
            {
                newData.LowBits = -1;
                newData.HighBits = Convert.ToInt32(MoreThanBits.Value.Value);
            }

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(m_CurrentData.Name);

            if (AnyRadioButton.IsChecked == true)
            {
                m_CurrentData.UserName = "";
            }
            else
            {
                m_CurrentData.UserName = UserNameTextBox.Text;
            }

            // Bits
            if (AnyBitsRadioButton.IsChecked == true)
            {
                m_CurrentData.LowBits = -1;
                m_CurrentData.HighBits = -1;
            }
            else if (BetweenBitsRadioButton.IsChecked == true)
            {
                m_CurrentData.LowBits = Convert.ToInt32(FromBits.Value.Value);
                m_CurrentData.HighBits = Convert.ToInt32(ToBits.Value.Value);
            }
            else if (ExaclyBitsButton.IsChecked == true)
            {
                m_CurrentData.LowBits = Convert.ToInt32(ExaclyBits.Value.Value);
                m_CurrentData.HighBits = -1;
            }
            else if (AboveBitsRadioButton.IsChecked == true)
            {
                m_CurrentData.LowBits = -1;
                m_CurrentData.HighBits = Convert.ToInt32(MoreThanBits.Value.Value);
            }

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        private void AnyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Hidden;
            e.Handled = true;
        }

        private void SpecificNameRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Visible;
            e.Handled = true;
        }

        private void AnyMonthsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void AnyBitsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void ExaclyBitsButton_Checked(object sender, RoutedEventArgs e)
        {
            if (ExaclyBitsButton.IsChecked == false)
                return;

            e.Handled = true;
            ExaclyBits.IsEnabled = true;
        }

        private void ExaclyBitsButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (ExaclyBitsButton.IsChecked == true)
                return;

            e.Handled = true;
            ExaclyBits.IsEnabled = false;
        }

        private void BetweenBitsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (BetweenBitsRadioButton.IsChecked == false)
                return;

            e.Handled = true;
            FromBits.IsEnabled = true;
            ToBits.IsEnabled = true;
        }

        private void BetweenBitsRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (BetweenBitsRadioButton.IsChecked == true)
                return;

            e.Handled = true;
            FromBits.IsEnabled = false;
            ToBits.IsEnabled = false;
        }

        private void AboveBitsRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (BetweenBitsRadioButton.IsChecked == false)
                return;

            e.Handled = true;
            ExaclyBits.IsEnabled = true;
        }

        private void AboveBitsRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (BetweenBitsRadioButton.IsChecked == true)
                return;

            e.Handled = true;
            ExaclyBits.IsEnabled = false;
        }
    }
}
