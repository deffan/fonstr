﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews.Twitch
{
    /// <summary>
    /// Interaction logic for TwitchBotView.xaml
    /// </summary>
    public partial class TwitchBotView : UserControl, IWinEventCmdData
    {
        private TwitchCustomEventData m_CurrentData;
        private string m_CurrentSerializedData;
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public TwitchBotView(EventData data, ListData listData)
        {
            InitializeComponent();
            m_CurrentData = data as TwitchCustomEventData;
            m_ListData = listData;
            m_CurrentData.Name = m_ListData.Name;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);

            UserTypeCombo.Items.Add("admin");
            UserTypeCombo.Items.Add("broadcaster");
            UserTypeCombo.Items.Add("global_mod");
            UserTypeCombo.Items.Add("moderator");
            UserTypeCombo.Items.Add("subscriber");
            UserTypeCombo.Items.Add("staff");
            UserTypeCombo.Items.Add("turbo");
            UserTypeCombo.Items.Add("vip");
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public void Load()
        {
            ItemList.Load();

            // Name
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            // Username
            if (string.IsNullOrEmpty(m_CurrentData.UserName))
            {
                AnyRadioButton.IsChecked = true;
            }
            else
            {
                SpecificNameRadioButton.IsChecked = true;
                UserNameTextBox.Text = m_CurrentData.UserName;
            }

            // Case sensitivity
            if(m_CurrentData.CaseSensitive == true)
            {
                CaseCheckboxButton.IsChecked = true;
            }
            else
            {
                CaseCheckboxButton.IsChecked = false;
            }

            // The text
            TextField.Text = m_CurrentData.Text;

            // Rules for text matching
            if(m_CurrentData.Contains == true)
            {
                ContainsWordRadioButton.IsChecked = true;
            }
            else if(m_CurrentData.AnyWord == true)
            {
                AnyWordRadioButton.IsChecked = true;
            }
            else 
            {
                FirstWordRadioButton.IsChecked = true;
            }

            // Type of user
            if(string.IsNullOrEmpty(m_CurrentData.TypeOfUser))
            {
                AnyUserTypeRadioButton.IsChecked = true;
                UserTypeCombo.IsEnabled = false;
            }
            else
            {
                UserTypeCombo.IsEnabled = true;
                SpecificUserTypeRadioButton.IsChecked = true;
                for(int i = 0; i < UserTypeCombo.Items.Count; i++)
                {
                    if(m_CurrentData.TypeOfUser.Equals(UserTypeCombo.Items[i].ToString()))
                    {
                        UserTypeCombo.SelectedIndex = i;
                        break;
                    }
                }
            }

        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            TwitchCustomEventData newData = JsonConvert.DeserializeObject<TwitchCustomEventData>(m_CurrentSerializedData);
            newData.Name = ItemList.NameTextBox.Text;

            if (AnyRadioButton.IsChecked == true)
            {
                newData.UserName = "";
            }
            else
            {
                newData.UserName = UserNameTextBox.Text;
            }

            newData.CaseSensitive = CaseCheckboxButton.IsChecked == true;
            newData.Text = TextField.Text;
            newData.Contains = ContainsWordRadioButton.IsChecked == true;
            newData.AnyWord = AnyWordRadioButton.IsChecked == true;
            newData.FirstWordOnly = FirstWordRadioButton.IsChecked == true;

            if (SpecificUserTypeRadioButton.IsChecked == true)
            {
                if(UserTypeCombo.Items.Count > 0 && UserTypeCombo.SelectedIndex >= 0)
                {
                    newData.TypeOfUser = UserTypeCombo.Items[UserTypeCombo.SelectedIndex].ToString();
                }
            }

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(m_CurrentData.Name);

            if (AnyRadioButton.IsChecked == true)
            {
                m_CurrentData.UserName = "";
            }
            else
            {
                m_CurrentData.UserName = UserNameTextBox.Text;
            }

            if (AnyRadioButton.IsChecked == true)
            {
                m_CurrentData.UserName = "";
            }
            else
            {
                m_CurrentData.UserName = UserNameTextBox.Text;
            }

            m_CurrentData.CaseSensitive = CaseCheckboxButton.IsChecked == true;
            m_CurrentData.Text = TextField.Text;
            m_CurrentData.Contains = ContainsWordRadioButton.IsChecked == true;
            m_CurrentData.AnyWord = AnyWordRadioButton.IsChecked == true;
            m_CurrentData.FirstWordOnly = FirstWordRadioButton.IsChecked == true;

            if (SpecificUserTypeRadioButton.IsChecked == true)
            {
                if (UserTypeCombo.Items.Count > 0 && UserTypeCombo.SelectedIndex >= 0)
                {
                    m_CurrentData.TypeOfUser = UserTypeCombo.Items[UserTypeCombo.SelectedIndex].ToString();
                }
            }

            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        private void AnyRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Hidden;
            e.Handled = true;
        }

        private void SpecificNameRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            UserNameTextBox.Visibility = Visibility.Visible;
            e.Handled = true;
        }

        private void CaseCheckboxButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void FirstWordRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void AnyWordRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void ContainsWordRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void AnyUserTypeRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void SpecificUserTypeRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (SpecificUserTypeRadioButton.IsChecked == false)
                return;

            e.Handled = true;
            UserTypeCombo.IsEnabled = true;
        }

        private void SpecificUserTypeRadioButton_Unchecked(object sender, RoutedEventArgs e)
        {
            if (SpecificUserTypeRadioButton.IsChecked == true)
                return;

            e.Handled = true;
            UserTypeCombo.IsEnabled = false;
        }

        private void UserTypeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
