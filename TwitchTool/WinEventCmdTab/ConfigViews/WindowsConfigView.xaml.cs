﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Source;
using TwitchTool.UserControls;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews
{
    public partial class WindowsConfigView : UserControl, IWinEventCmdData
    {
        private WinEventCmdListItem m_ListItem;
        private ListData m_ListData;

        public WindowsConfigView()
        {
            InitializeComponent();
        }

        public WindowsConfigView(ListData data)
        {
            InitializeComponent();
            m_ListData = data;

            WindowsBrowser.Navigate("https://www.strombergsson.com");
            WindowsBrowser.Navigating += WindowsBrowser_Navigating;

            // Create a page that communicates C# <> JavaScript
            // Mainly so you can import directly to the program from here.
            // Click download -> Download here -> Import

            // Kanske en specifik sida som normalt sätt inte surfas till på webben.
            // Med bara möjligheten att importera sidor?
            // Men, man vill ju kunna titta på exempel, bilder, osv?
            // Samt, kanske skapa en Forward/Backward/Refresh knapp.

            // Meeen som sagt då behövs en hel del exempel också.
            //
            // Och något fiffigt sätt för folk att ladda upp sina egna exempel...
            // Och så kan folk vota på dem....
            // Sen kan vi visa top-10?
            //
            // Most popular / Most downloaded
            //
            // Search forums
            //
            // Guide för hur man laddar upp?
            // Speciell uppladdningssida? Max-filstorlek.
            //
            // Ska vi ha ett program på servern som prov öppnar filen, och gör en slags kontroll?
            //
            // Kanske är detta något man får tänka ut i efterhand om detta program leder någonvart...


        }

        private void WindowsBrowser_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            // Disallow navigating out from the site
            if(!e.Uri.AbsoluteUri.Contains("strombergsson.com"))
            {
                e.Cancel = true;
            }
           
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public object GetCurrentData()
        {
            return null;
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        public bool HasChanges()
        {
            if (ItemList.HasChanges())
                return true;

            return false;
        }

        public void Load()
        {
            ItemList.Load();
            ItemList.NameTextBox.Text = "Windows";
            ItemList.NameTextBox.IsEnabled = false;
        }

        public void DeleteWindows()
        {
            List<WinEventCmdListItem> orgList = ItemList.GetOriginalList();
            for(int i = 0; i < orgList.Count; i++)
            {
                if(orgList[i].Removed)
                {
                    try
                    {
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        Directory.Delete(Directory.GetCurrentDirectory() + "\\Windows\\" + orgList[i].GetData().Id, true);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(LOG_LEVEL.ERROR, "Save > Delete Window > " + ex.Message);
                    }
                }
            }
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }

        public string Save()
        {
            // Delete any window folder that was removed
            DeleteWindows();

            ItemList.Save();
            return "";
        }

        private void MainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            WindowsBrowser.Width = MainGrid.ActualWidth - 425;
            WindowsBrowser.Height = MainGrid.ActualHeight - 40;
        }
    }
}
