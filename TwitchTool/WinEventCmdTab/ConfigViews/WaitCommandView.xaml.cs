﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Commands;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.ConfigViews
{
    /// <summary>
    /// Interaction logic for WaitCommandView.xaml
    /// </summary>
    public partial class WaitCommandView : UserControl, IWinEventCmdData
    {
        private bool m_Loaded;
        private WaitCommandData m_CurrentData;
        private WinEventCmdListItem m_ListItem;
        private string m_CurrentSerializedData;
        private ListData m_ListData;

        public WaitCommandView(WaitCommandData data, ListData listData)
        {
            InitializeComponent();
            m_Loaded = false;
            m_CurrentData = data;
            m_CurrentData.Listeners.Clear();    // Listeners are not set here ...
            m_ListData = listData;
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
        }

        public ListData GetListData()
        {
            return m_ListData;
        }

        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Because apparently, this SelectionChanged is THE_SAME as tabControl_SelectionChanged, which causes weird behaviour across the application if not handeled...
            e.Handled = true;
        }

        public void AddMyListItem(WinEventCmdListItem mylistitem)
        {
            m_ListItem = mylistitem;
        }

        public void AbortSave()
        {
            ItemList.AbortSave();
        }

        public bool HasChanges()
        {
            if (!ItemList.NameTextBox.Text.Equals(m_ListData.Name))
                return true;

            if (ItemList.HasChanges())
                return true;

            if (WaitCtrl.HasChanges())
                return true;

            // Create temporary data for compare
            WaitCommandData newData = JsonConvert.DeserializeObject<WaitCommandData>(m_CurrentSerializedData);

            // Save Name
            newData.Name = ItemList.NameTextBox.Text;

            return !JsonConvert.SerializeObject(newData).Equals(m_CurrentSerializedData);
        }

        public string Save()
        {
            if (!m_Loaded)
            {
                // Load only occurs when we open the view. So if we try to save an unopened view, we need to first load to save...
                Load();
            }

            ItemList.Save();

            // Save Name
            m_CurrentData.Name = ItemList.NameTextBox.Text;
            m_ListItem.SetName(ItemList.NameTextBox.Text);
            WaitCtrl.Update(m_CurrentData.m_TextData, true);
            m_CurrentSerializedData = JsonConvert.SerializeObject(m_CurrentData);
            return m_CurrentSerializedData;
        }


        public void Load()
        {
            m_Loaded = true;

            ItemList.Load();
            ItemList.DisableList();
            ItemList.NameTextBox.Text = m_CurrentData.Name;
            m_ListItem.SetName(ItemList.NameTextBox.Text);

            WaitCtrl.Update(m_CurrentData.m_TextData, true);
        }

        public WinEventCmdList GetList()
        {
            return ItemList;
        }

        public object GetCurrentData()
        {
            return m_CurrentData;
        }

        public WinEventCmdListItem GetMyListItem()
        {
            return m_ListItem;
        }
    }
}
