﻿using System.Windows;
using System.Windows.Controls;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.WinEventCmdTab.ConfigViews
{
    /// <summary>
    /// Interaction logic for TextCommandDataBlock.xaml
    /// </summary>
    public partial class TextCommandDataBlock : UserControl
    {
        public enum DataType { Text, EventVariable, DataVariable };
        private TextCommandView m_View;
        public string m_TextData;
        public EventVariable m_EventVariableData;
        public DataVariable m_DataVariableData;
        public DataType Datatype;

        public TextCommandDataBlock()
        {
            InitializeComponent();
        }

        public TextCommandDataBlock(TextCommandView view)
        {
            InitializeComponent();
            m_View = view;
        }

        public void SetData(string data)
        {
            Datatype = DataType.Text;
            m_TextData = data;
            TheText.Text = m_TextData;
        }

        public void SetData(EventVariable eventVariable)
        {
            Datatype = DataType.EventVariable;
            m_EventVariableData = eventVariable;
            TheText.Text = m_EventVariableData.DisplayName;
        }

        public void SetData(DataVariable dataVariable)
        {
            Datatype = DataType.DataVariable;
            m_DataVariableData = dataVariable;
            TheText.Text = dataVariable.DisplayName;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            m_View.MoveBlockBackwards(this);
        }

        private void ForwardButton_Click(object sender, RoutedEventArgs e)
        {
            m_View.MoveBlockForward(this);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            m_View.RemoveBlock(this);
        }
    }
}
