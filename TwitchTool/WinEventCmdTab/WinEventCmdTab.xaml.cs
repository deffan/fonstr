﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.WinEventCmdTab;

namespace TwitchTool.UserControls
{
    /// <summary>
    /// Interaction logic for WinEventCmdTab.xaml
    /// </summary>
    public partial class WinEventCmdTab : UserControl
    {
        public class ListData
        {
            public string Id;
            public string Name;
            public bool Active;
            public bool ActiveOld;
            public string Type;
            public int SubType;
            public string Data;
            public string Queue;
            public IWinEventCmdData View;

            public ListData()
            {
                Type = "";
                Id = "";
                Active = false;
                SubType = -1;
                Data = "";
                Queue = "";
                View = null;
            }
        }

        public WinEventCmdTab()
        {
            InitializeComponent();
        }

        public void LoadXMLData()
        {
            WECControl.Load();
        }

    }
}
