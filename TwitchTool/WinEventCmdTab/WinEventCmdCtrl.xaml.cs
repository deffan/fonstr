﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;
using TwitchTool.Commands;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.Source;
using TwitchTool.UserControls;
using TwitchTool.WinEventCmdTab.ConfigViews;
using TwitchTool.WinEventCmdTab.ConfigViews.Twitch;
using TwitchTool.WinEventCmdTab.EventQueue;
using TwitchTool.WinEventCmdTab.Views;
using static TwitchTool.UserControls.WinEventCmdTab;
using static TwitchTool.WinEventCmdTab.EventQueue.EventQueueCtrl;

namespace TwitchTool.WinEventCmdTab
{
    /// <summary>
    /// Interaction logic for WinEventCmdCtrl.xaml
    /// </summary>
    public partial class WinEventCmdCtrl : UserControl
    {
        private SolidColorBrush m_ColorOn;
        private SolidColorBrush m_ColorOff;
        private static WinEventCmdListItem m_CurrentItem;
        private static WinEventCmdListItem m_CurrentWindowItem;
        private static WinEventCmdListItem m_CurrentEventItem;
        private WindowsConfigView m_WindowsView;
        private WinEventCmdListItem m_WindowsItem;

        public WinEventCmdCtrl()
        {
            m_ColorOn = (SolidColorBrush)(new BrushConverter().ConvertFrom("#33000000"));
            m_ColorOff = (SolidColorBrush)(new BrushConverter().ConvertFrom("#337A7A7A"));
            InitializeComponent();
            
            ListData windowsData = new ListData();
            m_WindowsView = new WindowsConfigView(windowsData);
            windowsData.View = m_WindowsView;
            windowsData.Type = "windows";
            m_WindowsItem = new WinEventCmdListItem(windowsData, null, WindowsSelectionLabel);
            WindowsContent.Children.Add(m_WindowsView);
        }

        public static WinEventCmdListItem GetCurrentItem() { return m_CurrentItem; }
        public static WinEventCmdListItem GetCurrentWindowItem() { return m_CurrentWindowItem; }
        public static WinEventCmdListItem GetCurrentEventItem() { return m_CurrentEventItem; }

        public MessageBoxResult SaveCheck()
        {
            MessageBoxResult result = MessageBoxResult.No;
            if(m_CurrentItem == null)
            {
                if(HasChanges())
                {
                    result = MessageBox.Show("Save changes?", "Save?", MessageBoxButton.YesNoCancel);
                    if (result == MessageBoxResult.Yes)
                    {
                        Save();
                    }
                    else if (result == MessageBoxResult.No)
                    {
                        AbortSave();
                    }
                }
            }
            else if(m_CurrentItem.GetData().View.HasChanges())
            {
                result = MessageBox.Show("Save changes?", "Save?", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    Save();
                }
                else if (result == MessageBoxResult.No)
                {
                    m_CurrentItem.GetData().View.AbortSave();
                }
            }
            return result;
        }

        public void Load()
        {
            // Clear previous
            m_WindowsView.ItemList.Clear();

            for (int i = 0; i < MainWindow.Windows.Count; i++)
            {
                // Open the EventCommand XML for this window
                XDocument doc = null;
                try
                {
                    doc = XDocument.Load(MainWindow.Windows.ElementAt(i).Value.Path + "\\EventCommand.xml");
                }
                catch (Exception ex)
                {
                    Logger.Log(LOG_LEVEL.ERROR, "LoadXMLData" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                    continue;
                }

                // Create the window data object and the listview item
                ListData windowData = new ListData();
                windowData.Id = MainWindow.Windows.ElementAt(i).Value.WindowObject.ID;
                windowData.Type = "window";
                windowData.Active = Convert.ToBoolean(doc.Element("Windows").Element("Window").Element("Active").Value);
                windowData.Name = MainWindow.Windows.ElementAt(i).Value.WindowObject.ObjectName;
                windowData.Data = MainWindow.Windows.ElementAt(i).Value.Path;
                WindowConfigView windowView = new WindowConfigView(windowData);
                windowData.View = windowView;

                // Add the window list item to the "Windows view" which is always there (logic is handeled in this file instead of specific usercontrol).
                WinEventCmdListItem window = new WinEventCmdListItem(windowData, null, WindowSelectionLabel);
                windowView.ItemList.Init("window", null, window, windowView);
                window.SetCallbacks(EditItem, RemoveItem);

                // Queues
                windowView.EventQueueControl.Init(JsonConvert.DeserializeObject<WindowQueue>(GlobalHelper.Base64Decode(doc.Element("Windows").Element("Window").Element("Queue").Value)));

                m_WindowsView.ItemList.Add(window);

                XElement events = doc.Element("Windows").Element("Window").Element("Events");
                foreach (XElement eventObj in events.Elements())
                {
                    // Create the listdata for event
                    ListData eventData = new ListData();
                    eventData.Id = eventObj.Element("Id").Value;
                    eventData.Active = Convert.ToBoolean(eventObj.Element("Active").Value);
                    eventData.Type = "event";
                    eventData.SubType = Convert.ToInt32(eventObj.Element("Type").Value);
                    eventData.Name = GlobalHelper.Base64Decode(eventObj.Element("Name").Value);
                    eventData.Data = GlobalHelper.Base64Decode(eventObj.Element("Data").Value);
                    eventData.Queue = GlobalHelper.Base64Decode(eventObj.Element("Queue").Value);

                    // Deserialize the event-data and create the view
                    WinEventCmdList commandList = null;
                    Event newEvent = null;
                    IWinEventCmdData view = null;

                    switch (eventData.SubType)
                    {
                        case EVENT_TYPE.KEYBOARD:
                        {
                            newEvent  = new KeyboardEvent(JsonConvert.DeserializeObject<KeyboardEventData>(eventData.Data));
                            view = new KeyboardEventView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BOT_PART:
                        {
                            newEvent = new TwitchPartEvent(JsonConvert.DeserializeObject<TwitchPartEventData>(eventData.Data));
                            view = new TwitchPartView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BOT_JOIN:
                        {
                            newEvent = new TwitchJoinEvent(JsonConvert.DeserializeObject<TwitchJoinEventData>(eventData.Data));
                            view = new TwitchJoinView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_SUBSCRIPTION:
                        {
                            newEvent = new TwitchSubscriptionEvent(JsonConvert.DeserializeObject<TwitchSubscriptionEventData>(eventData.Data));
                            view = new TwitchSubscriberView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_FOLLOW:
                        {
                            newEvent = new TwitchFollowEvent(JsonConvert.DeserializeObject<TwitchFollowEventData>(eventData.Data));
                            view = new TwitchFollowView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BOT_CUSTOM:
                        {
                            newEvent = new TwitchCustomEvent(JsonConvert.DeserializeObject<TwitchCustomEventData>(eventData.Data));
                            view = new TwitchBotView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_GIFT:
                        {
                            newEvent = new TwitchGiftEvent(JsonConvert.DeserializeObject<TwitchGiftEventData>(eventData.Data));
                            view = new TwitchGiftView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_RAID:
                        {
                            newEvent = new TwitchRaidEvent(JsonConvert.DeserializeObject<TwitchRaidEventData>(eventData.Data));
                            view = new TwitchRaidView(newEvent.Data, eventData);
                            break;
                        }
                        case EVENT_TYPE.TWITCH_BITS:
                        {
                            newEvent = new TwitchBitsEvent(JsonConvert.DeserializeObject<TwitchBitsEventData>(eventData.Data));
                            view = new TwitchBitsView(newEvent.Data, eventData);
                            break;
                        }
                        default: throw new Exception("Unknown event type in WinEventCmdCtrl > Load");
                    }

                    view.GetList().Init("event", eventData, window, view);
                    commandList = view.GetList();
                    eventData.View = view;

                    // Add the event to window-view-list
                    WinEventCmdListItem eventli = new WinEventCmdListItem(eventData, window, EventSelectionLabel);
                    eventli.SetCallbacks(EditItem, RemoveItem);
                    windowView.ItemList.Add(eventli);

                    // Loop commands
                    XElement commands = eventObj.Element("Commands");
                    foreach (XElement commandObj in commands.Elements())
                    {
                        // Create the command data object, then also get/set command-specific data as we GetListItem(data)
                        ListData commandData = new ListData();
                        commandData.Id = commandObj.Element("Id").Value;
                        commandData.Active = Convert.ToBoolean(commandObj.Element("Active").Value);
                        commandData.Type = "command";
                        commandData.SubType = Convert.ToInt32(commandObj.Element("Type").Value);
                        commandData.Name = GlobalHelper.Base64Decode(commandObj.Element("Name").Value);
                        commandData.Data = GlobalHelper.Base64Decode(commandObj.Element("Data").Value);

                        WinEventCmdList listenerList = null;
                        switch (commandData.SubType)
                        {
                            case COMMAND_TYPE.TEXT:
                            {
                                TextCommandData tc = JsonConvert.DeserializeObject<TextCommandData>(commandData.Data);
                                TextCommandView tcv = new TextCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                            case COMMAND_TYPE.IMAGE:
                            {
                                ImageCommandData tc = JsonConvert.DeserializeObject<ImageCommandData>(commandData.Data);
                                ImageCommandView tcv = new ImageCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                            case COMMAND_TYPE.SOUND:
                            {
                                SoundCommandData tc = JsonConvert.DeserializeObject<SoundCommandData>(commandData.Data);
                                SoundCommandView tcv = new SoundCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                            case COMMAND_TYPE.YOUTUBE:
                            {
                                YoutubeCommandData tc = JsonConvert.DeserializeObject<YoutubeCommandData>(commandData.Data);
                                YoutubeCommandView tcv = new YoutubeCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                            case COMMAND_TYPE.CLONE:
                            {
                                CloningCommandData tc = JsonConvert.DeserializeObject<CloningCommandData>(commandData.Data);
                                CloningCommandView tcv = new CloningCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                            case COMMAND_TYPE.START_ANIMATION:
                            {
                                StartAnimationCommandData tc = JsonConvert.DeserializeObject<StartAnimationCommandData>(commandData.Data);
                                StartAnimationCommandView tcv = new StartAnimationCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                            case COMMAND_TYPE.STOP_ANIMATION:
                            {
                                StopAnimationCommandData tc = JsonConvert.DeserializeObject<StopAnimationCommandData>(commandData.Data);
                                StopAnimationCommandView tcv = new StopAnimationCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                            case COMMAND_TYPE.WAIT:
                            {
                                WaitCommandData tc = JsonConvert.DeserializeObject<WaitCommandData>(commandData.Data);
                                WaitCommandView tcv = new WaitCommandView(tc, commandData);
                                commandData.View = tcv;
                                tcv.ItemList.Init("command", commandData, eventli, tcv);
                                listenerList = tcv.ItemList;
                                break;
                            }
                        }

                        WinEventCmdListItem commandli = new WinEventCmdListItem(commandData, eventli, CommandSelectionLabel);
                        commandli.SetCallbacks(EditItem, RemoveItem);
                        commandList.Add(commandli);

                        // Loop listeners
                        XElement listeners = commandObj.Element("Listeners");
                        foreach (XElement listenerObj in listeners.Elements())
                        {
                            ListData listenerData = new ListData();
                            listenerData.Id = Guid.NewGuid().ToString();
                            listenerData.Name = GlobalHelper.Base64Decode(listenerObj.Element("Id").Value);
                            listenerData.Active = true;
                            listenerData.Type = "listener";
                            listenerData.SubType = 0;

                            WinEventCmdListItem listenerItem = new WinEventCmdListItem(listenerData, commandli, null);
                            listenerItem.SetCallbacks(null, MainWindow.GetWinEventCmdTab().RemoveItem);
                            listenerList.Add(listenerItem);
                        }
                        listenerList.Save();    // Listeners
                    }
                    windowView.ItemList.Save(); // Events
                    commandList.Save();         // Commands
                }
            }
            m_WindowsView.ItemList.Init("windows", null, null, m_WindowsView);
            m_WindowsView.ItemList.Save();    // Windows
            m_WindowsView.Load();
        }

        public void Save()
        {
            if(MainWindow.Live)
            {
                MessageBox.Show("You need to exit Live Mode for the changes to become visible.", "Save", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            m_WindowsView.Save();
            UIElementCollection list = m_WindowsView.ItemList.GetList();
            for (int i = 0; i < list.Count; i++)
            {
                WinEventCmdListItem windowItem = list[i] as WinEventCmdListItem;
                if (windowItem == null)
                    continue;

                IWinEventCmdData windowConfigView = windowItem.GetData().View;

                XDocument doc = new XDocument();
                XElement windows = new XElement("Windows");
                doc.Add(windows);

                XElement window = new XElement("Window");
                window.Add(new XElement("Id", windowItem.GetData().Id));
                window.Add(new XElement("Name", GlobalHelper.Base64Encode(windowItem.GetData().Name)));
                window.Add(new XElement("Active", windowItem.GetData().Active));
                window.Add(new XElement("Queue", GlobalHelper.Base64Encode(windowConfigView.Save())));

                XElement events = new XElement("Events");
                window.Add(events);
                windows.Add(window);

                for (int j = 0; j < windowConfigView.GetList().GetList().Count; j++)
                {
                    WinEventCmdListItem eventListItem = windowConfigView.GetList().GetList()[j] as WinEventCmdListItem;
                    if (eventListItem == null)
                        continue;

                    XElement eventNode = new XElement("Event");
                    eventNode.Add(new XElement("Id", eventListItem.GetData().Id));
                    eventNode.Add(new XElement("Type", eventListItem.GetData().SubType));
                    eventNode.Add(new XElement("Active", eventListItem.GetData().Active));
                    eventNode.Add(new XElement("Data", GlobalHelper.Base64Encode(eventListItem.GetData().View.Save())));
                    eventNode.Add(new XElement("Name", GlobalHelper.Base64Encode(eventListItem.GetData().Name)));
                    eventNode.Add(new XElement("Queue", GlobalHelper.Base64Encode(eventListItem.GetData().Queue)));

                    XElement commands = new XElement("Commands");
                    eventNode.Add(commands);
                    events.Add(eventNode);

                    IWinEventCmdData eventConfigView = eventListItem.GetData().View;
                    for (int n = 0; n < eventConfigView.GetList().GetList().Count; n++)
                    {
                        WinEventCmdListItem cmdListItem = eventConfigView.GetList().GetList()[n] as WinEventCmdListItem;
                        if (cmdListItem == null)
                            continue;

                        XElement commandNode = new XElement("Command");
                        commandNode.Add(new XElement("Id", cmdListItem.GetData().Id));
                        commandNode.Add(new XElement("Type", cmdListItem.GetData().SubType));
                        commandNode.Add(new XElement("Active", cmdListItem.GetData().Active));
                        commandNode.Add(new XElement("Data", GlobalHelper.Base64Encode(cmdListItem.GetData().View.Save())));
                        commandNode.Add(new XElement("Name", GlobalHelper.Base64Encode(cmdListItem.GetData().Name)));
                        commands.Add(commandNode);
                        
                        XElement listeners = new XElement("Listeners");
                        commandNode.Add(listeners);

                        IWinEventCmdData commandConfigView = cmdListItem.GetData().View;
                        for (int c = 0; c < commandConfigView.GetList().GetList().Count; c++)
                        {
                            WinEventCmdListItem listenerListItem = commandConfigView.GetList().GetList()[c] as WinEventCmdListItem;
                            if (listenerListItem == null)
                                continue;

                            XElement listenerNode = new XElement("Listener");
                            listenerNode.Add(new XElement("Id", GlobalHelper.Base64Encode(listenerListItem.GetData().Name)));
                            listeners.Add(listenerNode);
                        }

                    }
                }

                doc.Save(MainWindow.Windows[windowItem.GetData().Id].Path + "\\EventCommand.xml");
            }
        }

        public void EditItem(WinEventCmdListItem item)
        {
            // Savecheck on current item.
            MessageBoxResult r = SaveCheck();
            if (r == MessageBoxResult.Cancel)
                return;

            // The possibility exist that we are trying to open an item that was not saved due to answeing no.
            if(r == MessageBoxResult.No)
            {
                bool hasItem = false;
                UIElementCollection list = null;
                if (m_CurrentItem == null)
                {
                    list = m_WindowsItem.GetData().View.GetList().GetList();
                }
                else
                {
                    list = m_CurrentItem.GetData().View.GetList().GetList();
                }
               
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Equals(item))
                    {
                        hasItem = true;
                        break;
                    }
                }
                if (!hasItem)
                    return;
            }

            m_CurrentItem = item;

            WindowSelectionPanel.Visibility = Visibility.Collapsed;
            EventSelectionPanel.Visibility = Visibility.Collapsed;
            CommandSelectionPanel.Visibility = Visibility.Collapsed;

            WindowsContent.Visibility = Visibility.Collapsed;
            WindowContent.Visibility = Visibility.Collapsed;
            EventContent.Visibility = Visibility.Collapsed;
            CommandContent.Visibility = Visibility.Collapsed;

            switch (item.GetData().Type)
            {
                case "window":
                {
                    m_CurrentWindowItem = m_CurrentItem;
                    WindowSelectionPanel.Visibility = Visibility.Visible;
                    WindowContent.Visibility = Visibility.Visible;
                    WindowSelectionLabel.Text = item.GetData().Name;
                    WindowContent.Children.Clear();
                    WindowContent.Children.Add(item.GetData().View as UserControl);
                    item.GetData().View.Load();
                    WindowContent.Visibility = Visibility.Visible;
                    break;
                }
                case "event":
                {
                    m_CurrentEventItem = m_CurrentItem;
                    WindowSelectionPanel.Visibility = Visibility.Visible;
                    EventSelectionPanel.Visibility = Visibility.Visible;
                    if(string.IsNullOrEmpty(item.GetData().Name))
                    {
                        EventSelectionLabel.Text = EVENT_TYPE.GetEventName(item.GetData().SubType);
                    }
                    else
                    {
                        EventSelectionLabel.Text = item.GetData().Name;
                    }
                    EventSelectionTypeLabel.Content = EVENT_TYPE.GetEventName(item.GetData().SubType);
                    EventSelectionIcon.Source = EVENT_TYPE.GetEventIcon(item.GetData().SubType);
                    EventContent.Children.Clear();
                    EventContent.Children.Add(item.GetData().View as UserControl);
                    item.GetData().View.Load();
                    EventContent.Visibility = Visibility.Visible;
                    break;
                }
                case "command":
                {
                    EventSelectionPanel.Visibility = Visibility.Visible;
                    CommandSelectionPanel.Visibility = Visibility.Visible;
                    WindowSelectionPanel.Visibility = Visibility.Visible;
                    if (string.IsNullOrEmpty(item.GetData().Name))
                    {
                        CommandSelectionLabel.Text = COMMAND_TYPE.GetCommandName(item.GetData().SubType);
                    }
                    else
                    {
                        CommandSelectionLabel.Text = item.GetData().Name;
                    }
                    CommandSelectionTypeLabel.Content = COMMAND_TYPE.GetCommandName(item.GetData().SubType);
                    CommandSelectionIcon.Source = COMMAND_TYPE.GetCommandIcon(item.GetData().SubType);
                    CommandContent.Children.Clear();
                    CommandContent.Children.Add(item.GetData().View as UserControl);
                    item.GetData().View.Load();
                    CommandContent.Visibility = Visibility.Visible;
                    break;
                }
                default: throw new Exception("Invalid itemtype: " + item.GetData().Type);
            }
        }

        public void RemoveItem(WinEventCmdListItem item)
        {
            if (item.m_Parent == null)
            {
                m_WindowsView.ItemList.Remove(item);
            }
            else
            {
                item.m_Parent.GetData().View.GetList().Remove(item);
            }
        }

        private void WindowsSelectionPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            WindowsSelectionArrowPanel.Background = m_ColorOn;
            WindowsSelectionArrowOff.Visibility = Visibility.Collapsed;
            WindowsSelectionArrowOn.Visibility = Visibility.Visible;
        }

        private void WindowsSelectionPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            WindowsSelectionArrowPanel.Background = m_ColorOff;
            WindowsSelectionArrowOff.Visibility = Visibility.Visible;
            WindowsSelectionArrowOn.Visibility = Visibility.Collapsed;
        }

        private void WindowSelectionPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            WindowSelectionArrowPanel.Background = m_ColorOn;
            WindowSelectionArrowOff.Visibility = Visibility.Collapsed;
            WindowSelectionArrowOn.Visibility = Visibility.Visible;
        }

        private void WindowSelectionPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            WindowSelectionArrowPanel.Background = m_ColorOff;
            WindowSelectionArrowOff.Visibility = Visibility.Visible;
            WindowSelectionArrowOn.Visibility = Visibility.Collapsed;
        }

        private void EventSelectionPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            EventSelectionArrowPanel.Background = m_ColorOn;
            EventSelectionArrowOff.Visibility = Visibility.Collapsed;
            EventSelectionArrowOn.Visibility = Visibility.Visible;
        }

        private void EventSelectionPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            EventSelectionArrowPanel.Background = m_ColorOff;
            EventSelectionArrowOff.Visibility = Visibility.Visible;
            EventSelectionArrowOn.Visibility = Visibility.Collapsed;
        }

        private void CommandSelectionPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            CommandSelectionArrowPanel.Background = m_ColorOn;
            CommandSelectionArrowOff.Visibility = Visibility.Collapsed;
            CommandSelectionArrowOn.Visibility = Visibility.Visible;
        }

        private void CommandSelectionPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            CommandSelectionArrowPanel.Background = m_ColorOff;
            CommandSelectionArrowOff.Visibility = Visibility.Visible;
            CommandSelectionArrowOn.Visibility = Visibility.Collapsed;
        }

        public void Refresh()
        {
            if(m_CurrentItem != null && m_CurrentItem.GetData().Type == "command")
            {
                m_CurrentItem.GetData().View.Load();
            }
        }

        private void EventSelectionPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Clicking on the same (current) tab does nothing.
            if (m_CurrentItem == m_CurrentEventItem)
                return;

            // Savecheck on current item.
            if (SaveCheck() == MessageBoxResult.Cancel)
                return;

            m_CurrentItem = m_CurrentEventItem;

            CommandSelectionPanel.Visibility = Visibility.Collapsed;

            WindowsContent.Visibility = Visibility.Collapsed;
            WindowContent.Visibility = Visibility.Collapsed;
            EventContent.Visibility = Visibility.Visible;
            CommandContent.Visibility = Visibility.Collapsed;
        }

        private void WindowSelectionPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Clicking on the same (current) tab does nothing.
            if (m_CurrentItem == m_CurrentWindowItem)
                return;

            // Savecheck on current item.
            if (SaveCheck() == MessageBoxResult.Cancel)
                return;

            m_CurrentItem = m_CurrentWindowItem;

            EventSelectionPanel.Visibility = Visibility.Collapsed;
            CommandSelectionPanel.Visibility = Visibility.Collapsed;

            WindowsContent.Visibility = Visibility.Collapsed;
            WindowContent.Visibility = Visibility.Visible;
            EventContent.Visibility = Visibility.Collapsed;
            CommandContent.Visibility = Visibility.Collapsed;
        }

        private void WindowsSelectionPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // Clicking on the same (current) tab does nothing.
            if (m_CurrentItem == null)
                return;

            // Savecheck on current item.
            if (SaveCheck() == MessageBoxResult.Cancel)
                return;

            m_CurrentItem = null;

            WindowSelectionPanel.Visibility = Visibility.Collapsed;
            EventSelectionPanel.Visibility = Visibility.Collapsed;
            CommandSelectionPanel.Visibility = Visibility.Collapsed;

            WindowsContent.Visibility = Visibility.Visible;
            WindowContent.Visibility = Visibility.Collapsed;
            EventContent.Visibility = Visibility.Collapsed;
            CommandContent.Visibility = Visibility.Collapsed;
        }

        private bool HasChanges()
        {
            return m_WindowsView.HasChanges();
        }

        private void AbortSave()
        {
            m_WindowsView.AbortSave();
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double newHeight = ActualHeight - 45;
            WindowsContent.Height = newHeight;
            WindowContent.Height = newHeight;
            EventContent.Height = newHeight;
            CommandContent.Height = newHeight;
        }
    }
}
