﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Commands;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.UserControls;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab
{
    /// <summary>
    /// Interaction logic for WinEventCmdListItem.xaml
    /// </summary>
    public partial class WinEventCmdListItem : UserControl
    {
        private Action<WinEventCmdListItem> m_EditCallback;
        private Action<WinEventCmdListItem> m_RemoveCallback;
        private Action<WinEventCmdListItem> m_StartDragCallback;
        private Action<WinEventCmdListItem> m_StopDragCallback;
        private TextBlock m_TabLabel;
        private ListData m_Data;
        private bool m_Toggle;
        private SolidColorBrush m_MouseOverBrush;
        public WinEventCmdListItem m_Parent;
        public string m_IdString;
        public bool Removed;

        public WinEventCmdListItem()
        {
            InitializeComponent();
        }

        public WinEventCmdListItem(ListData data, WinEventCmdListItem parent, TextBlock tabLabel)
        {
            InitializeComponent();
            Removed = false;
            m_IdString = Guid.NewGuid().ToString();
            m_TabLabel = tabLabel;
            m_Data = data;
            m_Parent = parent;

            if (data.View != null)   // Is null when listener
            {
                // Needed for updating the listitem-name on save
                data.View.AddMyListItem(this);  
            }
            SetName(data.Name);
            
            m_Toggle = true;
            ActiveCheckBox.IsChecked = data.Active;
            m_Toggle = false;
            m_MouseOverBrush = new SolidColorBrush(Color.FromArgb(255, 135, 206, 250));

            switch (data.Type)
            {
                case "windows":
                {
                    ItemImage.Source = GlobalHelper.GetImage("grid.png");
                    ItemLabel.Content = "Windows";
                    break;
                }
                case "window":
                {
                    ItemImage.Source = GlobalHelper.GetImage("window.png");
                    ItemLabel.Content = "Window";
                    break;
                }
                case "event":
                {
                    ItemImage.Source = EVENT_TYPE.GetEventIcon(data.SubType);
                    ItemLabel.Content = EVENT_TYPE.GetEventName(data.SubType);
                    break;
                }
                case "command":
                {
                    OrderControl.Visibility = Visibility.Visible;

                    m_Toggle = true;
                    OrderControl.Value = (m_Data.View.GetCurrentData() as CommandData).Order;
                    (m_Data.View.GetCurrentData() as CommandData).OldOrder = (m_Data.View.GetCurrentData() as CommandData).Order;
                    m_Toggle = false;

                    ItemImage.Source = COMMAND_TYPE.GetCommandIcon(data.SubType);
                    ItemLabel.Content = COMMAND_TYPE.GetCommandName(data.SubType);
                    break;
                }
                case "listener":
                {
                    ActiveCheckBox.Visibility = Visibility.Collapsed;
                    ItemImage.Source = GlobalHelper.GetImage("3dotbubble.png");
                    ItemLabel.Content = "Command listener";
                    break;
                }
                default: throw new Exception("Unknown TYPE in WinEventCmdListItem constructor: " + data.Type);
            }

        }

        public void SetCallbacks(Action<WinEventCmdListItem> editCallback, Action<WinEventCmdListItem> removeCallback)
        {
            m_EditCallback = editCallback;
            m_RemoveCallback = removeCallback;
        }

        public void SetDragAndDropCallbacks(Action<WinEventCmdListItem> start, Action<WinEventCmdListItem> stop)
        {
            m_StartDragCallback = start;
            m_StopDragCallback = stop;
        }

        public ListData GetData()
        {
            return m_Data;
        }

        public void SetName(string text)
        {
            ItemText.Text = text;
            m_Data.Name = text;

            // Only if current item...
            if(WinEventCmdCtrl.GetCurrentItem() == this && m_TabLabel != null)
                m_TabLabel.Text = text;
        }

        private void OrderControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (m_Data == null || m_Toggle)
                return;

            int oldOrder = (m_Data.View.GetCurrentData() as CommandData).Order;
            int newOrder = Convert.ToInt32(OrderControl.Value);
            if (oldOrder == newOrder)
                return;

            (m_Data.View.GetCurrentData() as CommandData).Order = newOrder;

            /*
             *  This works but does not really look or feel good...
             * 
           
            // Sort
            UIElementCollection list = m_Parent.GetData().View.GetList();
            List<WinEventCmdListItem> tmp = new List<WinEventCmdListItem>();
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i] is WinEventCmdListItem)
                {
                    tmp.Add(list[i] as WinEventCmdListItem);
                    list.RemoveAt(i);
                }
            }
            tmp.Sort((s1, s2) => (s1.GetData().View.GetCurrentData() as CommandData).Order.CompareTo((s2.GetData().View.GetCurrentData() as CommandData).Order));
            for (int i = 0; i < tmp.Count; i++)
            {
                list.Add(tmp[i]);
            }
            */
        }

        private void TheBorder_MouseEnter(object sender, MouseEventArgs e)
        {
            MainStackPanel.Background = m_MouseOverBrush;
        }

        private void TheBorder_MouseLeave(object sender, MouseEventArgs e)
        {
            MainStackPanel.Background = Brushes.White;
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            m_RemoveCallback(this);
            e.Handled = true;
        }

        private void ActiveCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (m_Toggle)
                return;

            m_Data.View.GetListData().Active = true;
            e.Handled = true;
        }

        private void ActiveCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (m_Toggle)
                return;

            m_Data.View.GetListData().Active = false;
            e.Handled = true;
        }

        private void RemoveButton_MouseEnter(object sender, MouseEventArgs e)
        {
            MainStackPanel.Background = Brushes.White;
            e.Handled = true;
        }

        private void TheBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(m_StartDragCallback != null)
            {
                m_StartDragCallback.Invoke(this);
            }
            else
            {
                m_EditCallback?.Invoke(this);
            }
        }

        private void UserControl_Drop(object sender, DragEventArgs e)
        {
            if (m_StopDragCallback != null)
            {
                string data = e.Data.GetData(DataFormats.StringFormat).ToString();
                if(data.Equals(m_IdString))
                {
                    m_EditCallback?.Invoke(this);
                }
                else
                {
                    m_StopDragCallback.Invoke(this);
                }
            }
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            m_EditCallback?.Invoke(this);
        }
    }
}
