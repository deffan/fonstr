﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Commands;
using TwitchTool.Components;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.Source;
using TwitchTool.UserControls;
using TwitchTool.WinEventCmdTab.ConfigViews;
using TwitchTool.WinEventCmdTab.ConfigViews.Twitch;
using TwitchTool.WinEventCmdTab.EventQueue;
using static TwitchTool.UserControls.WinEventCmdTab;

namespace TwitchTool.WinEventCmdTab.Views
{
    /// <summary>
    /// Interaction logic for WinEventCmdList.xaml
    /// </summary>
    public partial class WinEventCmdList : UserControl
    {
        private string m_Type;
        private string m_LastList;
        private List<WinEventCmdListItem> m_OriginalList;
        private WinEventCmdListItem m_Parent;
        private WinEventCmdListItem m_Move;
        private ListData m_ListData;
        private IWinEventCmdData m_View;
        private EventQueueCtrl m_QueueCtrl;
        private bool m_MoveFinished;

        public WinEventCmdList()
        {
            InitializeComponent();
            m_LastList = "";
            m_OriginalList = new List<WinEventCmdListItem>();
        }

        public void SetEventQueue(EventQueueCtrl eventQueueCtrl)
        {
            m_QueueCtrl = eventQueueCtrl;
        }

        public List<WinEventCmdListItem> GetOriginalList()
        {
            return m_OriginalList;
        }

        public void DisableList()
        {
            ListenersPanel.IsEnabled = false;
            InfoLabel.Content = "(No listeners required).";
        }

        public void Clear()
        {
            for (int i = ListPanel.Children.Count - 1; i >= 0; i--)
            {
                WinEventCmdListItem item = GetList()[i] as WinEventCmdListItem;
                if(item != null)
                {
                    ListPanel.Children.RemoveAt(i);
                }
            }
        }

        public void Init(string type, ListData listData, WinEventCmdListItem parent, IWinEventCmdData view)
        {
            m_View = view;
            m_Parent = parent;
            m_Type = type;
            m_ListData = listData;
            m_MoveFinished = false;

            if (type.Equals("window"))
            {
                InfoLabel.Content = "Events";
                AddNewButton.Content = "Add new event";
                DescriptionLabel.Content = "Window";
            }
            else if (type.Equals("event"))
            {
                InfoLabel.Content = "Commands";
                AddNewButton.Content = "Add new command";
                ListHeader.SetCommand();
                ItemImage.Source = EVENT_TYPE.GetEventIcon(m_ListData.SubType);
                DescriptionLabel.Content = EVENT_TYPE.GetEventName(m_ListData.SubType);
                EventQueuePanel.Visibility = Visibility.Visible;
                if(string.IsNullOrEmpty(m_ListData.Queue))
                {
                    m_ListData.Queue = "All (Default)";
                }
            }
            else if (type.Equals("windows"))
            {
                ImportButton.Visibility = Visibility.Visible;
                InfoLabel.Content = "Windows";
                AddNewButton.Content = "Add new window";
                DescriptionLabel.Content = "Windows";
                ItemImage.Source = GlobalHelper.GetImage("grid.png");
            }
            else if (type.Equals("command"))
            {
                InfoLabel.Content = "Send this command to listener(s)"; 
                AddPanel.Visibility = Visibility.Collapsed;
                ListenersPanel.Visibility = Visibility.Visible;
                ItemImage.Source = COMMAND_TYPE.GetCommandIcon(m_ListData.SubType);
                DescriptionLabel.Content = COMMAND_TYPE.GetCommandName(m_ListData.SubType);
                ListHeader.SetListener();
            }
        }

        private void Combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Because apparently, this SelectionChanged is THE_SAME as tabControl_SelectionChanged, which causes weird behaviour across the application if not handeled...
            e.Handled = true;
        }

        public void AbortSave()
        {
            // Reset checkbox
            for (int i = 0; i < m_OriginalList.Count; i++)
            {
                m_OriginalList[i].GetData().Active = m_OriginalList[i].GetData().ActiveOld;
                m_OriginalList[i].ActiveCheckBox.IsChecked = m_OriginalList[i].GetData().Active;
                m_OriginalList[i].Removed = false;

                if (m_Type.Equals("event"))
                {
                    (m_OriginalList[i].GetData().View.GetCurrentData() as CommandData).Order = (m_OriginalList[i].GetData().View.GetCurrentData() as CommandData).OldOrder;
                    m_OriginalList[i].OrderControl.Value = (m_OriginalList[i].GetData().View.GetCurrentData() as CommandData).Order;
                }
            }

            // Reload
            Load();
        }

        public void Load()
        {
            // Clear list
            for (int i = ListPanel.Children.Count - 1; i >= 0; i--)
            {
                if (ListPanel.Children[i] is WinEventCmdListItem)
                {
                    ListPanel.Children.RemoveAt(i);
                }
            }

            // If event, refill the Queue combobox
            if(m_Type.Equals("event"))
            {
                string queuedIn = m_ListData.Queue;
                int putIndex = 0;
                QueueCombo.Items.Clear();
                WindowConfigView windowView = WinEventCmdCtrl.GetCurrentWindowItem().GetData().View as WindowConfigView;
                List<EventQueueListItem> queues = windowView.EventQueueControl.GetQueues();
                for(int i = 0; i < queues.Count; i++)
                {
                    string queueName = queues[i].GetQueueName();
                    if(queueName.Equals(queuedIn))
                    {
                        putIndex = i;
                    }
                    QueueCombo.Items.Add(queueName);
                }
                QueueCombo.SelectedIndex = putIndex;
            }

            // Refill from original list
            for (int i = 0; i < m_OriginalList.Count; i++)
            {
                m_OriginalList[i].GetData().ActiveOld = m_OriginalList[i].GetData().Active;
                m_OriginalList[i].ActiveCheckBox.IsChecked = m_OriginalList[i].GetData().Active;

                if (m_Type.Equals("event"))
                {
                    (m_OriginalList[i].GetData().View.GetCurrentData() as CommandData).OldOrder = (m_OriginalList[i].GetData().View.GetCurrentData() as CommandData).Order;
                    m_OriginalList[i].OrderControl.Value = (m_OriginalList[i].GetData().View.GetCurrentData() as CommandData).Order;
                }

                ListPanel.Children.Add(m_OriginalList[i]);
            }

            if (m_Type.Equals("command"))
            {
                // Listeners combobox
                ListenersCombo.Items.Clear();

                if (WinEventCmdCtrl.GetCurrentWindowItem() == null)
                    return;

                WindowConfigView windowView = WinEventCmdCtrl.GetCurrentWindowItem().GetData().View as WindowConfigView;
                List<CommandListenerComponent> listeners = windowView.GetListeners();
                for (int i = 0; i < listeners.Count; i++)
                {
                    if (!ListenersCombo.Items.Contains(listeners[i].ListenerID))
                        ListenersCombo.Items.Add(listeners[i].ListenerID);
                }
                ListenersCombo.SelectionChanged += Combo_SelectionChanged;
                ListenersCombo.SelectedIndex = 0;
            }
        }

        public void Save()
        {
            // Clear original list
            m_LastList = "";
            m_OriginalList.Clear();

            if (m_Type.Equals("event") && QueueCombo.SelectedItem != null)
            {
                m_ListData.Queue = QueueCombo.SelectedItem.ToString();
            }

            // Refill original list
            for (int i = 0; i < ListPanel.Children.Count; i++)
            {
                if (ListPanel.Children[i] is WinEventCmdListItem)
                {
                    WinEventCmdListItem item = ListPanel.Children[i] as WinEventCmdListItem;
                    item.GetData().ActiveOld = item.GetData().Active;
                    item.ActiveCheckBox.IsChecked = item.GetData().Active;
                    item.Removed = false;
                    m_OriginalList.Add(item);
                    m_LastList += item.m_IdString;
                    if (m_Type.Equals("event"))
                    {
                        m_LastList += Convert.ToString((item.GetData().View.GetCurrentData() as CommandData).Order);
                    }
                }
            }
        }

        public bool HasChanges()
        {
            m_Move = null;
            m_MoveFinished = false;

            if (m_Type != null && m_Type.Equals("event"))
            {
                if (!m_ListData.Queue.Equals(QueueCombo.SelectedItem.ToString()))
                    return true;
            }

            // Each listitem is given a id-string, which we use to compare. If this string changes (new item, deleted item, changed order) something has changed.
            string match = "";
            for (int i = 0; i < ListPanel.Children.Count; i++)
            {
                if (ListPanel.Children[i] is WinEventCmdListItem)
                {
                    WinEventCmdListItem item = ListPanel.Children[i] as WinEventCmdListItem;

                    // Check if "Active" checkbox has been changed
                    if (item.GetData().Active != item.GetData().ActiveOld)
                        return true;

                    // Otherwise, keep building the ID-string for compare
                    match += item.m_IdString;
                    if (m_Type.Equals("event"))
                    {
                        match += Convert.ToString((item.GetData().View.GetCurrentData() as CommandData).Order);
                    }
                }
            }
            return !m_LastList.Equals(match);
        }

        public void Remove(WinEventCmdListItem item)
        {
            GetList().Remove(item);
            item.Removed = true;
            if(m_QueueCtrl != null)
            {
                m_QueueCtrl.RemoveEventItem(item);
            }
        }

        public void Add(WinEventCmdListItem item)
        {
            item.SetDragAndDropCallbacks(MoveThis, MoveComplete);
            GetList().Add(item);
        }

        public void AddListener(WinEventCmdListItem item)
        {
            // Dont add if listenerid already exist
            for(int i = 0; i < GetList().Count; i++)
            {
                WinEventCmdListItem listener = GetList()[i] as WinEventCmdListItem;
                if (listener != null && listener.ItemText.Text.Equals(item.ItemText.Text))
                    return;
            }
            GetList().Add(item);
        }

        public UIElementCollection GetList()
        {
            return ListPanel.Children;
        }

        private void AddNewButton_Click(object sender, RoutedEventArgs e)
        {
            if(m_Type.Equals("windows"))
            {
                string windowId = MainWindow.CreateNewWindow();
                if (windowId.Length == 0)
                    return;

                ListData windowData = new ListData();
                windowData.Id = windowId;
                windowData.Type = "window";
                windowData.Active = true;
                windowData.Name = MainWindow.Windows[windowId].WindowObject.ObjectName;
                windowData.Data = MainWindow.Windows[windowId].Path;
                WindowConfigView windowView = new WindowConfigView(windowData);
                windowData.View = windowView;
                windowView.ItemList.Init("window", windowData, null, windowView);

                WinEventCmdListItem window = new WinEventCmdListItem(windowData, null, MainWindow.GetWinEventCmdTab().WindowSelectionLabel);
                window.SetCallbacks(MainWindow.GetWinEventCmdTab().EditItem, MainWindow.GetWinEventCmdTab().RemoveItem);
                Add(window);
                return;
            }

            bool isEvent = m_Type.Equals("window");
            AddNewEventCommandWindow w = new AddNewEventCommandWindow(isEvent);
            var position = Mouse.GetPosition(Application.Current.MainWindow);
            w.Left = position.X + Application.Current.MainWindow.Left;
            w.Top = position.Y + Application.Current.MainWindow.Top;
            w.ShowDialog();
            if (!w.Cancelled)
            {
                int type = w.GetItemType();
                WinEventCmdListItem item = null;
                if (isEvent)
                {
                    item = GetNewEventListItem(type, w.NameTextBox.Text);
                    m_QueueCtrl.AddEventItem(item);
                }
                else
                {
                    item = GetNewCommandListItem(type, w.NameTextBox.Text);
                }
                Add(item);
            }
        }

        private WinEventCmdListItem GetNewEventListItem(int type, string name)
        {
            ListData data = new ListData();
            data.Id = Guid.NewGuid().ToString();
            data.Active = true;
            data.Type = "event";
            data.SubType = type;
            data.Name = name;

            EventData eventData = null;
            IWinEventCmdData view = null;

            switch (data.SubType)
            {
                case EVENT_TYPE.KEYBOARD:
                {
                    eventData = new KeyboardEventData();
                    view = new KeyboardEventView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_PART:
                {
                    eventData = new TwitchPartEventData();
                    view = new TwitchPartView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_JOIN:
                {
                    eventData = new TwitchJoinEventData();
                    view = new TwitchJoinView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_SUBSCRIPTION:
                {
                    eventData = new TwitchSubscriptionEventData();
                    view = new TwitchSubscriberView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_FOLLOW:
                {
                    eventData = new TwitchFollowEventData();
                    view = new TwitchFollowView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_BOT_CUSTOM:
                {
                    eventData = new TwitchCustomEventData();
                    view = new TwitchBotView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_GIFT:
                {
                    eventData = new TwitchGiftEventData();
                    view = new TwitchGiftView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_RAID:
                {
                    eventData = new TwitchRaidEventData();
                    view = new TwitchRaidView(eventData, data);
                    break;
                }
                case EVENT_TYPE.TWITCH_BITS:
                {
                    eventData = new TwitchBitsEventData();
                    view = new TwitchBitsView(eventData, data);
                    break;
                }
                default: throw new Exception("Unknown event type in GetNewEventListItem");
            }

            eventData.Id = data.Id;
            eventData.Name = data.Name;
            view.GetList().Init("event", data, m_Parent, view);
            data.View = view;

            WinEventCmdListItem item = new WinEventCmdListItem(data, m_View.GetMyListItem(), MainWindow.GetWinEventCmdTab().EventSelectionLabel);
            item.SetCallbacks(MainWindow.GetWinEventCmdTab().EditItem, MainWindow.GetWinEventCmdTab().RemoveItem);
            return item;
        }

        private WinEventCmdListItem GetNewCommandListItem(int type, string name)
        {
            ListData data = new ListData();
            data.Id = Guid.NewGuid().ToString();
            data.Active = true;
            data.Type = "command";
            data.SubType = type;
            data.Name = name;

            switch (data.SubType)
            {
                case COMMAND_TYPE.TEXT:
                {
                    TextCommandData tcd = new TextCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    TextCommandView tcv = new TextCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
                case COMMAND_TYPE.IMAGE:
                {
                    ImageCommandData tcd = new ImageCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    ImageCommandView tcv = new ImageCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
                case COMMAND_TYPE.YOUTUBE:
                {
                    YoutubeCommandData tcd = new YoutubeCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    YoutubeCommandView tcv = new YoutubeCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
                case COMMAND_TYPE.SOUND:
                {
                    SoundCommandData tcd = new SoundCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    SoundCommandView tcv = new SoundCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
                case COMMAND_TYPE.CLONE:
                {
                    CloningCommandData tcd = new CloningCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    CloningCommandView tcv = new CloningCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
                case COMMAND_TYPE.START_ANIMATION:
                {
                    StartAnimationCommandData tcd = new StartAnimationCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    StartAnimationCommandView tcv = new StartAnimationCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
                case COMMAND_TYPE.STOP_ANIMATION:
                {
                    StopAnimationCommandData tcd = new StopAnimationCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    StopAnimationCommandView tcv = new StopAnimationCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
                case COMMAND_TYPE.WAIT:
                {
                    WaitCommandData tcd = new WaitCommandData();
                    tcd.Id = data.Id;
                    tcd.Name = data.Name;
                    WaitCommandView tcv = new WaitCommandView(tcd, data);
                    tcv.ItemList.Init("command", data, m_View.GetMyListItem(), tcv);
                    data.View = tcv;
                    break;
                }
            }

            WinEventCmdListItem item = new WinEventCmdListItem(data, m_View.GetMyListItem(), MainWindow.GetWinEventCmdTab().CommandSelectionLabel);
            item.SetCallbacks(MainWindow.GetWinEventCmdTab().EditItem, MainWindow.GetWinEventCmdTab().RemoveItem);
            return item;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if(m_View.HasChanges())
            {
                MainWindow.GetWinEventCmdTab().Save();
            }
            e.Handled = true;
        }

        private void ListenersTextButton_Click(object sender, RoutedEventArgs e)
        {
            if(ListenersText.Text.Length > 0)
            {
                AddListener(ListenersText.Text);
            }
            e.Handled = true;
        }

        private void ListenersComboButton_Click(object sender, RoutedEventArgs e)
        {
            if(ListenersCombo.SelectedItem != null)
            {
                AddListener(ListenersCombo.SelectedItem as string);
            }
            e.Handled = true;
        }

        private void AddListener(string listenerId)
        {
            ListData data = new ListData();
            data.Id = Guid.NewGuid().ToString();
            data.Active = true;
            data.Type = "listener";
            data.SubType = 0;
            data.Name = listenerId;

            WinEventCmdListItem item = new WinEventCmdListItem(data, m_View.GetMyListItem(), null);
            item.SetCallbacks(null, MainWindow.GetWinEventCmdTab().RemoveItem);
            AddListener(item);
        }

        private void QueueCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        public void MoveThis(WinEventCmdListItem from)
        {
            m_Move = from;
            m_MoveFinished = false;
        }

        private void Swap(WinEventCmdListItem from, WinEventCmdListItem to)
        {
            // Find indexes
            int fromIndex = 0;
            int toIndex = 0;
            List<UIElement> tmpList = new List<UIElement>();
            UIElementCollection list = GetList();
            for(int i = 0; i < list.Count; i++)
            {
                WinEventCmdListItem item = list[i] as WinEventCmdListItem;
                if(item != null)
                {
                    if (from.m_IdString.Equals(item.m_IdString))
                    {
                        fromIndex = i;
                        tmpList.Add(null);
                    }
                    else if (to.m_IdString.Equals(item.m_IdString))
                    {
                        toIndex = i;
                        tmpList.Add(null);
                    }
                    else
                    {
                        tmpList.Add(item);
                    }
                }
                else
                {
                    tmpList.Add(list[i]);
                }
            }
            list.Clear();

            tmpList[toIndex] = from;
            tmpList[fromIndex] = to;

            for (int i = 0; i < tmpList.Count; i++)
            {
                list.Add(tmpList[i]);
            }
        }

        public void MoveComplete(WinEventCmdListItem to)
        {
            if(m_Move != null && !to.m_IdString.Equals(m_Move.m_IdString))
            {
                Swap(m_Move, to);
            }
            m_Move = null;
            m_MoveFinished = false;
        }

        private void ListPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if(m_Move != null)
            {
                if(m_MoveFinished)
                {
                    m_Move = null;
                    e.Handled = true;
                    return;
                }
                DragDrop.DoDragDrop(m_Move, m_Move.m_IdString, DragDropEffects.Move);
                m_MoveFinished = true;
            }
        }

        private void ImportButton_Click(object sender, RoutedEventArgs e)
        {
            ImportWindow importWin = new ImportWindow();
            if(importWin.Import())
            {
                MainWindow.Reload();
            }
        }
    }
}
