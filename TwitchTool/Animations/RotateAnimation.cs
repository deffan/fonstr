﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using TwitchTool.Animator;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    public class RotateAnimation : UserControl, IAnimation
    {
        private class RotateAnimationHolder
        {
            public DoubleAnimation angle;
        }

        public ObjectControl m_Object;
        private bool m_Forward;
        private bool m_Finished;
        private bool m_Rotate_Finished;
        private bool m_Reverse;
        private int m_Index;
        private double m_Original_Angle;
        private double m_BackwardStartTime;
        private List<RotateAnimationHolder> m_AnimationListForward;
        private List<RotateAnimationHolder> m_AnimationListBackward;
        private List<RotateAnimationData> m_AnimationData;
        private bool m_NeedRebuild;

        public RotateAnimation(ObjectControl obj, List<AnimationData> list, double totalTrackTime, bool reverse)
        {
            m_AnimationData = list.Cast<RotateAnimationData>().ToList();  // Ineffektivt?
            m_Object = obj;
            m_Rotate_Finished = true;
            m_Finished = false;
            m_Index = -1;
            m_Reverse = reverse;
            m_BackwardStartTime = totalTrackTime - m_AnimationData[m_AnimationData.Count - 1].ToSeconds;
            m_NeedRebuild = false;

            // Original angle
            m_Original_Angle = m_Object.GetTransformComponent().GetRotationAngle();

            m_AnimationListForward = new List<RotateAnimationHolder>();
            if(m_Reverse)
            {
                m_AnimationListBackward = new List<RotateAnimationHolder>();
            }
            RebuildData();
        }

        private void RebuildData()
        {
            m_AnimationListForward.Clear();
            for (int i = 0; i < m_AnimationData.Count; i++)
            {
                if (!m_NeedRebuild)
                {
                    if (m_AnimationData[i].ToAngle.IsDataVariable)
                        m_NeedRebuild = true;
                }

                RotateAnimationHolder holder = new RotateAnimationHolder();
                holder.angle = new DoubleAnimation();

                if (i == 0)
                {
                    holder.angle.From = m_Original_Angle;
                }
                else
                {
                    holder.angle.From = m_AnimationListForward[i - 1].angle.To;
                }
                holder.angle.To = m_AnimationData[i].ToAngle.GetNumericData();

                // Adjust zero seconds a bit. Having it zero seems to fail occasionally...
                double totalSeconds = m_AnimationData[i].ToSeconds - m_AnimationData[i].FromSeconds;
                if (totalSeconds <= 0)
                    totalSeconds = 0.05;

                holder.angle.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));

                if (m_AnimationData[i].Easing > 0)
                {
                    SineEase s = new SineEase();
                    s.EasingMode = (EasingMode)m_AnimationData[i].Easing - 1;
                    holder.angle.EasingFunction = s;
                }

                holder.angle.Completed += AnimRotate_Completed;

                m_AnimationListForward.Add(holder);
            }

            // Also reverse?
            if (m_Reverse)
            {
                m_AnimationListBackward.Clear();

                for (int i = m_AnimationListForward.Count - 1; i >= 0; i--)
                {
                    RotateAnimationHolder holder = new RotateAnimationHolder();
                    holder.angle = new DoubleAnimation();
                    holder.angle.From = m_AnimationListForward[i].angle.To;
                    holder.angle.To = m_AnimationListForward[i].angle.From;

                    // Adjust zero seconds a bit. Having it zero seems to fail occasionally...
                    double totalSeconds = m_AnimationData[i].ToSeconds - m_AnimationData[i].FromSeconds;
                    if (totalSeconds <= 0)
                        totalSeconds = 0.05;

                    holder.angle.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));

                    // Flip the easing (0=None, 1=In, 2=Out, 3=Both)
                    if (m_AnimationData[i].Easing == 1 || m_AnimationData[i].Easing == 2)
                    {
                        SineEase s = new SineEase();

                        if (m_AnimationData[i].Easing == 1)
                            s.EasingMode = EasingMode.EaseOut;
                        else
                            s.EasingMode = EasingMode.EaseIn;

                        holder.angle.EasingFunction = s;
                        holder.angle.EasingFunction = s;
                    }

                    holder.angle.Completed += AnimRotate_Completed;

                    m_AnimationListBackward.Add(holder);
                }

            }
        }

        private void AnimRotate_Completed(object sender, EventArgs e)
        {
            m_Rotate_Finished = true;
        }

        public void Initialize(bool forward)
        {
            m_Finished = false;
            m_Rotate_Finished = true;
            m_Forward = forward;
            m_Index = -1;

            // Only on forward rebuild.
            if (m_Forward && m_NeedRebuild)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => { RebuildData(); }));
            }
        }

        private void StartNext()
        {
            if (m_Finished) // This can happen if we have stopped the animation, but due to the Task.Delay havent noticed yet.
                return;

            if (m_Forward)
            {
                m_Object.GetTransformComponent().m_RotateTransform.BeginAnimation(RotateTransform.AngleProperty, m_AnimationListForward[m_Index].angle);
            }
            else
            {
                m_Object.GetTransformComponent().m_RotateTransform.BeginAnimation(RotateTransform.AngleProperty, m_AnimationListBackward[m_Index].angle);
            }
        }

        public bool Logic()
        {
            if (m_Finished)
                return true;

            // Animation finished?
            if (m_Rotate_Finished)
            {
                m_Rotate_Finished = false;
                int millisecondsToNext = 0;

                m_Index++;

                // All Done?
                if (m_Index == m_AnimationData.Count)
                {
                    m_Finished = true;
                    m_Rotate_Finished = true;
                    return true;
                }

                if (m_Forward)
                {
                    // Start next...
                    if (m_Index == 0)
                    {
                        millisecondsToNext = (int)(m_AnimationData[m_Index].FromSeconds * 1000);
                    }
                    else
                    {
                        millisecondsToNext = (int)((m_AnimationData[m_Index].FromSeconds * 1000) - (m_AnimationData[m_Index - 1].ToSeconds * 1000));
                    }
                }
                else
                {
                    // Start next...
                    if (m_Index == 0)
                    {
                        millisecondsToNext = (int)(m_BackwardStartTime * 1000);     // Start time backwards is longest-track's end time - this tracks end time
                    }
                    else
                    {
                        millisecondsToNext = (int)((m_AnimationData[m_Index].FromSeconds * 1000) - (m_AnimationData[m_Index - 1].ToSeconds * 1000));
                    }

                }

                // Possibly wait before starting next
                Task.Delay(millisecondsToNext).ContinueWith(_ =>
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => { StartNext(); }));
                });

            }

            return m_Finished;
        }

        public void Stop()
        {
            m_Finished = true;
            Reset();
        }

        public void Reset()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => { ResetLogic(); }));
        }

        private void ResetLogic()
        {
            m_Object.GetTransformComponent().m_RotateTransform.BeginAnimation(RotateTransform.AngleProperty, null);
            m_Object.GetTransformComponent().SetRotation(m_Original_Angle);
        }
    }
}
