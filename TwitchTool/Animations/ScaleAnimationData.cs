﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Animator;
using TwitchTool.Source;

namespace TwitchTool.Animations
{
    public class ScaleAnimationData : AnimationData
    {
        public TextData ToXScale;
        public TextData ToYScale;

        public ScaleAnimationData(double fromTime, double toTime, TextData toXScale, TextData toYScale) : base(fromTime, toTime)
        {
            ToXScale = toXScale;
            ToYScale = toYScale;
        }
    }
}
