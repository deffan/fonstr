﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Animations;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    public class MoveAnimationData : AnimationData
    {
        public TextData ToX;
        public TextData ToY;
        public int Easing;

        public MoveAnimationData(double fromTime, double toTime, TextData toX, TextData toY, int easing) : base (fromTime, toTime)
        {
            ToX = toX;
            ToY = toY;
            Easing = easing;
        }
    }
}
