﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TwitchTool.Animator;
using TwitchTool.Components;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    /// <summary>
    /// Interaction logic for AnimatorObject.xaml
    /// </summary>
    public partial class AnimatorObject : UserControl
    {
        private ObjectControl m_Object;
        private List<AnimatorTrack> m_Tracks;
        private bool m_Showing;
        private AnimatorWindow m_AnimatorWindow;
        private Thickness m_OpenClosedMargin;

        public AnimatorObject()
        {
            InitializeComponent();

        }

        public AnimatorObject(ObjectControl obj, AnimatorWindow window)
        {
            InitializeComponent();
            m_Object = obj;
            m_Showing = false;
            ObjectLabelName.Content = m_Object.ObjectName;
            m_Tracks = new List<AnimatorTrack>();
            m_AnimatorWindow = window;
            m_OpenClosedMargin = new Thickness();

            // These always exist
            AddTrack("Move", "");
            AddTrack("Scale", "");
            AddTrack("Rotate", "");
            AddTrack("Skew", "");
            AddTrack("Transparency", "");

            // Only deal with components that can be animated somehow
            List<IComponent> components = m_Object.GetComponents();
            for (int i = 0; i < components.Count; i++)
            {
                if (components[i] is TextComponent)
                {
                    AddTrack("Text", components[i].GetID());
                    // Animate font-size?
                    // Animate color-change?
                }
                else if (components[i] is ImageComponent)
                {
                    AddTrack("Image", components[i].GetID());
                    // special image effects? blur?
                }
            }

            UpDownExpander last = TimeLinePanel.Children[TimeLinePanel.Children.Count - 1] as UpDownExpander;
            last.m_Last = true;
            last.VertRect.Height = 13;

            Label spacelabel = new Label();
            spacelabel.Content = " ";
            TimeLinePanel.Children.Add(spacelabel);

            HideOpenButton.Content = "Show";
            Tracks.Height = 0;
        }

        public List<AnimatorTrack> TrackList
        {
            get { return m_Tracks; }
        }

        private void AddTrack(string name, string objectId)
        {
            UpDownExpander expander = new UpDownExpander();

            AnimatorTrack track = new AnimatorTrack(name, objectId);
            track.SetAnimatorWindow(m_AnimatorWindow);
            track.IncreaseByTen(6);

            expander.TheContent.Children.Add(track);
            expander.ObjectLabelName.Content = name;

            TimeLinePanel.Children.Add(expander);
            m_Tracks.Add(track);
        }

        private void HideOpenButton_Click(object sender, RoutedEventArgs e)
        {
            if(m_Showing)
            {
                HideOpenButton.Content = "Show";
                Tracks.Height = 0;
                m_Showing = false;
                VertRect.Height = 0;
                m_OpenClosedMargin.Bottom = 0;
                ObjectPanel.Margin = m_OpenClosedMargin;
            }
            else
            {
                HideOpenButton.Content = "Hide";
                Tracks.Height = Double.NaN;
                m_Showing = true;
                VertRect.Height = 28;
                m_OpenClosedMargin.Bottom = 10;
                ObjectPanel.Margin = m_OpenClosedMargin;
            }
        }

        private Rectangle GetLineSeparator()
        {
            Rectangle r = new Rectangle();
            r.Height = 1;
            r.Fill = Brushes.Black;
            return r;
        }

        private void IncreaseAllByTen()
        {
            for(int i = 0; i < m_Tracks.Count; i++)
            {
                m_Tracks[i].IncreaseByTen(1);
            }
        }

        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (TrackScroll.ScrollableWidth == TrackScroll.HorizontalOffset)
            {
                IncreaseAllByTen();
            }
        }

        private void TrackScroll_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            // This is so the scroll does not automatically scroll to where a mouse click happened (annoying)
            e.Handled = true;
        }
    }
}
