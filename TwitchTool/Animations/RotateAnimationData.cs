﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Source;

namespace TwitchTool.Animator
{
    public class RotateAnimationData : AnimationData
    {
        public TextData ToAngle;
        public int Easing;

        public RotateAnimationData(double fromTime, double toTime, TextData toAngle, int easing) : base (fromTime, toTime)
        {
            ToAngle = toAngle;
            Easing = easing;
        }
    }
}
