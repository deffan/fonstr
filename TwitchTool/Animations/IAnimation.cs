﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    public interface IAnimation
    {
        void Initialize(bool forward);

        void Stop();

        bool Logic();

        void Reset();
    }
}
