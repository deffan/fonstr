﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using TwitchTool.Animator;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    public class TransparencyAnimation : UserControl, IAnimation
    {
        private class TransparencyAnimationHolder
        {
            public DoubleAnimation opacity;
        }

        public ObjectControl m_Object;
        private bool m_Forward;
        private bool m_Finished;
        private bool m_Transparency_Finished;
        private bool m_Reverse;
        private int m_Index;
        private double m_Original_Opacity;
        private double m_BackwardStartTime;
        private List<TransparencyAnimationHolder> m_AnimationListForward;
        private List<TransparencyAnimationHolder> m_AnimationListBackward;
        private List<TransparencyAnimationData> m_AnimationData;
        private bool m_NeedRebuild;

        public TransparencyAnimation(ObjectControl obj, List<AnimationData> list, double totalTrackTime, bool reverse)
        {
            m_AnimationData = list.Cast<TransparencyAnimationData>().ToList();
            m_Object = obj;
            m_Transparency_Finished = true;
            m_Finished = false;
            m_Reverse = reverse;
            m_Index = -1;
            m_BackwardStartTime = totalTrackTime - m_AnimationData[m_AnimationData.Count - 1].ToSeconds;
            m_NeedRebuild = false;

            // Original
            m_Original_Opacity = m_Object.Opacity;

            m_AnimationListForward = new List<TransparencyAnimationHolder>();
            if (m_Reverse)
            {
                m_AnimationListBackward = new List<TransparencyAnimationHolder>();
            }

            RebuildData();
        }

        private void RebuildData()
        {
            m_AnimationListForward.Clear();
            for (int i = 0; i < m_AnimationData.Count; i++)
            {
                if (!m_NeedRebuild)
                {
                    if (m_AnimationData[i].ToOpacity.IsDataVariable)
                        m_NeedRebuild = true;
                }

                TransparencyAnimationHolder holder = new TransparencyAnimationHolder();
                holder.opacity = new DoubleAnimation();

                if (i == 0)
                {
                    holder.opacity.From = m_Original_Opacity;
                }
                else
                {
                    holder.opacity.From = m_AnimationListForward[i - 1].opacity.To;
                }
                holder.opacity.To = m_AnimationData[i].ToOpacity.GetNumericData();

                // Fix potential range issues
                if (holder.opacity.To < 0)
                    holder.opacity.To = 0;

                if (holder.opacity.To > 1)
                    holder.opacity.To = 1;

                // Adjust zero seconds a bit. Having it zero seems to fail occasionally...
                double totalSeconds = m_AnimationData[i].ToSeconds - m_AnimationData[i].FromSeconds;
                if (totalSeconds <= 0)
                    totalSeconds = 0.05;

                holder.opacity.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));

                holder.opacity.Completed += AnimTransparency_Completed;
                m_AnimationListForward.Add(holder);
            }

            // Also reverse?
            if (m_Reverse)
            {
                m_AnimationListBackward.Clear();

                for (int i = m_AnimationListForward.Count - 1; i >= 0; i--)
                {
                    TransparencyAnimationHolder holder = new TransparencyAnimationHolder();
                    holder.opacity = new DoubleAnimation();
                    holder.opacity.From = m_AnimationListForward[i].opacity.To;
                    holder.opacity.To = m_AnimationListForward[i].opacity.From;

                    // Adjust zero seconds a bit. Having it zero seems to fail occasionally...
                    double totalSeconds = m_AnimationData[i].ToSeconds - m_AnimationData[i].FromSeconds;
                    if (totalSeconds <= 0)
                        totalSeconds = 0.05;

                    holder.opacity.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));

                    holder.opacity.Completed += AnimTransparency_Completed;
                    m_AnimationListBackward.Add(holder);
                }

            }
        }

        private void AnimTransparency_Completed(object sender, EventArgs e)
        {
            m_Transparency_Finished = true;
        }

        public void Initialize(bool forward)
        {
            m_Finished = false;
            m_Transparency_Finished = true;
            m_Forward = forward;
            m_Index = -1;

            // Only on forward rebuild.
            if (m_Forward && m_NeedRebuild)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => { RebuildData(); }));
            }
        }

        private void StartNext()
        {
            if (m_Finished) // This can happen if we have stopped the animation, but due to the Task.Delay havent noticed yet.
                return;

            if (m_Forward)
            {
                m_Object.BeginAnimation(OpacityProperty, m_AnimationListForward[m_Index].opacity);
            }
            else
            {
                m_Object.BeginAnimation(OpacityProperty, m_AnimationListBackward[m_Index].opacity);
            }
        }

        public bool Logic()
        {
            if (m_Finished)
                return true;

            // Animation finished?
            if (m_Transparency_Finished)
            {
                m_Transparency_Finished = false;
                int millisecondsToNext = 0;

                m_Index++;

                // All Done?
                if (m_Index == m_AnimationData.Count)
                {
                    m_Finished = true;
                    m_Transparency_Finished = true;
                    return true;
                }

                if (m_Forward)
                {
                    // Start next...
                    if (m_Index == 0)
                    {
                        millisecondsToNext = (int)(m_AnimationData[m_Index].FromSeconds * 1000);
                    }
                    else
                    {
                        millisecondsToNext = (int)((m_AnimationData[m_Index].FromSeconds * 1000) - (m_AnimationData[m_Index - 1].ToSeconds * 1000));
                    }
                }
                else
                {
                    // Start next...
                    if (m_Index == 0)
                    {
                        millisecondsToNext = (int)(m_BackwardStartTime * 1000);     // Start time backwards is longest-track's end time - this tracks end time
                    }
                    else
                    {
                        millisecondsToNext = (int)((m_AnimationData[m_Index].FromSeconds * 1000) - (m_AnimationData[m_Index - 1].ToSeconds * 1000));
                    }

                }

                // Possibly wait before starting next
                Task.Delay(millisecondsToNext).ContinueWith(_ =>
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => { StartNext(); }));
                });

            }

            return m_Finished;
        }

        public void Stop()
        {
            m_Finished = true;
            Reset();
        }

        public void Reset()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => { ResetLogic(); }));
        }

        private void ResetLogic()
        {
            m_Object.BeginAnimation(OpacityProperty, null);
            m_Object.Opacity = m_Original_Opacity;
        }
    }
}
