﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Animator;
using TwitchTool.Source;

namespace TwitchTool.Animations
{
    public class SkewAnimationData : AnimationData
    {
        public TextData ToXSkew;
        public TextData ToYSkew;

        public SkewAnimationData(double fromTime, double toTime, TextData toXSkew, TextData toYSkew) : base(fromTime, toTime)
        {
            ToXSkew = toXSkew;
            ToYSkew = toYSkew;
        }
    }
}
