﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.Animator;
using TwitchTool.Source;

namespace TwitchTool.Animations
{
    public class TransparencyAnimationData : AnimationData
    {
        public TextData ToOpacity;

        public TransparencyAnimationData(double fromTime, double toTime, TextData toOpacity) : base(fromTime, toTime)
        {
            ToOpacity = toOpacity;
        }
    }
}
