﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Animations
{
    public class AnimationSettings
    {
        public int m_RunTimes;
        public bool m_RunForever;
        public bool m_Reverse;
        public int m_Easing;

        public AnimationSettings()
        {
            m_RunTimes = 1;
            m_RunForever = false;
            m_Reverse = false;
            m_Easing = 0;
        }

    }
}
