﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Xml.Linq;
using TwitchTool.AnimationsEditor;
using TwitchTool.Animator;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    public class MoveAnimation : UserControl, IAnimation
    {
        private class MoveAnimationHolder
        {
            public DoubleAnimation x;
            public DoubleAnimation y;
        }

        public ObjectControl m_Object;
        private bool m_Forward;
        private bool m_Finished;
        private bool m_X_Finished;
        private bool m_Y_Finished;
        private int m_Index;
        private double m_Original_X;
        private double m_Original_Y;
        private double m_BackwardStartTime;
        private bool m_NeedRebuild;
        private bool m_Reverse;
        private List<MoveAnimationHolder> m_AnimationListForward;
        private List<MoveAnimationHolder> m_AnimationListBackward;
        private List<MoveAnimationData> m_AnimationData;

        public MoveAnimation(ObjectControl obj, List<AnimationData> list, double totalTrackTime, bool reverse)
        {
            m_AnimationData = list.Cast<MoveAnimationData>().ToList();  // Ineffektivt?
            m_Object = obj;
            m_X_Finished = true;
            m_Y_Finished = true;
            m_Finished = false;
            m_Index = -1;
            m_NeedRebuild = false;
            m_BackwardStartTime = totalTrackTime - m_AnimationData[m_AnimationData.Count - 1].ToSeconds;
            m_Reverse = reverse;

            // Original position
            Point p = m_Object.GetTransformComponent().GetCoordinates();
            m_Original_X = p.X;
            m_Original_Y = p.Y;

            m_AnimationListForward = new List<MoveAnimationHolder>();
            if(m_Reverse)
            {
                m_AnimationListBackward = new List<MoveAnimationHolder>();
            }

            RebuildData();
        }

        private void RebuildData()
        {
            m_AnimationListForward.Clear();
            for (int i = 0; i < m_AnimationData.Count; i++)
            {
                if (!m_NeedRebuild)
                {
                    if (m_AnimationData[i].ToX.IsDataVariable || m_AnimationData[i].ToY.IsDataVariable)
                        m_NeedRebuild = true;
                }

                MoveAnimationHolder holder = new MoveAnimationHolder();
                holder.x = new DoubleAnimation();
                holder.y = new DoubleAnimation();

                if (i == 0)
                {
                    holder.x.From = m_Original_X;
                    holder.y.From = m_Original_Y;
                }
                else
                {
                    holder.x.From = m_AnimationListForward[i - 1].x.To;
                    holder.y.From = m_AnimationListForward[i - 1].y.To;
                }
                holder.x.To = m_AnimationData[i].ToX.GetNumericData();
                holder.y.To = m_AnimationData[i].ToY.GetNumericData();

                // Adjust zero seconds a bit. Having it zero seems to fail occasionally...
                double totalSeconds = m_AnimationData[i].ToSeconds - m_AnimationData[i].FromSeconds;
                if (totalSeconds <= 0)
                    totalSeconds = 0.05;

                holder.x.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));
                holder.y.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));

                if (m_AnimationData[i].Easing > 0)
                {
                    SineEase s = new SineEase();
                    s.EasingMode = (EasingMode)m_AnimationData[i].Easing - 1;
                    holder.x.EasingFunction = s;
                    holder.y.EasingFunction = s;
                }

                holder.x.Completed += AnimX_Completed;
                holder.y.Completed += AnimY_Completed;

                m_AnimationListForward.Add(holder);
            }

            // Also reverse?
            if (m_Reverse)
            {
                m_AnimationListBackward.Clear();
                for (int i = m_AnimationListForward.Count - 1; i >= 0; i--)
                {
                    MoveAnimationHolder holder = new MoveAnimationHolder();
                    holder.x = new DoubleAnimation();
                    holder.y = new DoubleAnimation();

                    holder.x.From = m_AnimationListForward[i].x.To;
                    holder.y.From = m_AnimationListForward[i].y.To;
                    holder.x.To = m_AnimationListForward[i].x.From;
                    holder.y.To = m_AnimationListForward[i].y.From;

                    // Adjust zero seconds a bit. Having it zero seems to fail occasionally...
                    double totalSeconds = m_AnimationData[i].ToSeconds - m_AnimationData[i].FromSeconds;
                    if (totalSeconds <= 0)
                        totalSeconds = 0.05;

                    holder.x.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));
                    holder.y.Duration = new Duration(TimeSpan.FromSeconds(totalSeconds));

                    // Flip the easing (0=None, 1=In, 2=Out, 3=Both)
                    if (m_AnimationData[i].Easing == 1 || m_AnimationData[i].Easing == 2)
                    {
                        SineEase s = new SineEase();

                        if (m_AnimationData[i].Easing == 1)
                            s.EasingMode = EasingMode.EaseOut;
                        else
                            s.EasingMode = EasingMode.EaseIn;

                        holder.x.EasingFunction = s;
                        holder.y.EasingFunction = s;
                    }

                    holder.x.Completed += AnimX_Completed;
                    holder.y.Completed += AnimY_Completed;

                    m_AnimationListBackward.Add(holder);
                }
            }
        }

        public void Destroy()
        {
            for (int i = 0; i < m_AnimationListForward.Count; i++)
            {
                m_AnimationListForward[i].x.Completed -= AnimX_Completed;
                m_AnimationListForward[i].y.Completed -= AnimY_Completed;
            }
            m_AnimationListForward.Clear();

            if (m_AnimationListBackward != null)
            {
                for (int i = 0; i < m_AnimationListBackward.Count; i++)
                {
                    m_AnimationListBackward[i].x.Completed -= AnimX_Completed;
                    m_AnimationListBackward[i].y.Completed -= AnimY_Completed;
                }
                m_AnimationListBackward.Clear();
            }

            m_AnimationListBackward = null;
            m_AnimationListForward = null;
            m_Object = null;
        }

        public void Stop()
        {
            m_Finished = true;
            Reset();
        }

        public void Reset()
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() => { ResetLogic(); }));
        }

        private void ResetLogic()
        {
            m_Object.GetTransformComponent().m_TranslateTransform.BeginAnimation(TranslateTransform.XProperty, null);
            m_Object.GetTransformComponent().m_TranslateTransform.BeginAnimation(TranslateTransform.YProperty, null);
            m_Object.GetTransformComponent().SetCoordinates(m_Original_X, m_Original_Y);
        }

        public void Initialize(bool forward)
        {
            m_Finished = false;
            m_X_Finished = true;
            m_Y_Finished = true;

            m_Forward = forward;
            m_Index = -1;

            // Only on forward rebuild.
            if(m_Forward && m_NeedRebuild)
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(() => { RebuildData(); }));
            }
        }

        private void StartNext()
        {
            if (m_Finished) // This can happen if we have stopped the animation, but due to the Task.Delay havent noticed yet.
                return;

            if (m_Forward)
            {
                m_Object.GetTransformComponent().m_TranslateTransform.BeginAnimation(TranslateTransform.XProperty, m_AnimationListForward[m_Index].x);
                m_Object.GetTransformComponent().m_TranslateTransform.BeginAnimation(TranslateTransform.YProperty, m_AnimationListForward[m_Index].y);
            }
            else
            {
                m_Object.GetTransformComponent().m_TranslateTransform.BeginAnimation(TranslateTransform.XProperty, m_AnimationListBackward[m_Index].x);
                m_Object.GetTransformComponent().m_TranslateTransform.BeginAnimation(TranslateTransform.YProperty, m_AnimationListBackward[m_Index].y);
            }
        }

        private void AnimY_Completed(object sender, EventArgs e)
        {
            m_Y_Finished = true;
        }

        private void AnimX_Completed(object sender, EventArgs e)
        {
            m_X_Finished = true;
        }

        public bool Logic()
        {
            if (m_Finished)
                return true;

            // Animation finished?
            if(m_X_Finished && m_Y_Finished)
            {
                m_X_Finished = false;
                m_Y_Finished = false;
                int millisecondsToNext = 0;

                m_Index++;

                // All Done?
                if (m_Index == m_AnimationData.Count)
                {
                    m_Finished = true;
                    m_X_Finished = true;
                    m_Y_Finished = true;
                    return true;
                }

                if (m_Forward)
                {
                    // Start next...
                    if (m_Index == 0)
                    {
                        millisecondsToNext = (int)(m_AnimationData[m_Index].FromSeconds * 1000);
                    }
                    else
                    {
                        millisecondsToNext = (int)((m_AnimationData[m_Index].FromSeconds * 1000) - (m_AnimationData[m_Index - 1].ToSeconds * 1000));
                    }
                }
                else
                {
                    // Start next...
                    if (m_Index == 0)
                    {
                        millisecondsToNext = (int)(m_BackwardStartTime * 1000);     // Start time backwards is longest-track's end time - this tracks end time
                    }
                    else
                    {
                        millisecondsToNext = (int)((m_AnimationData[m_Index].FromSeconds * 1000) - (m_AnimationData[m_Index - 1].ToSeconds * 1000));
                    }

                }

                // Possibly wait before starting next
                Task.Delay(millisecondsToNext).ContinueWith(_ =>
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() => { StartNext(); }));
                });
                
            }

            return m_Finished;
        }

    }
}
