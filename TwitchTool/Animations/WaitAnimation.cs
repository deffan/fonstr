﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;
using TwitchTool.AnimationsEditor;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    public class WaitAnimation : UserControl, IComponent, IAnimation
    {
        public ObjectControl m_Object;
        public WaitEditorAnimation m_EditorComponent;
        private string m_ID;
        
        private DateTime m_End;
        private double m_WaitTime;

        public WaitAnimation(ObjectControl parent, bool editor)
        {
            m_Object = parent;

            if (editor)
            {
                m_EditorComponent = new WaitEditorAnimation(this);
            }
        }

        public void SetWaitTime(double wait)
        {
            m_WaitTime = wait;
        }

        XElement IXML.Serialize()
        {
            XElement node = new XElement("WaitAnimation");
            node.Add(new XElement("Id", m_ID));
            node.Add(new XElement("Seconds", m_WaitTime));
            return node;
        }

        public static WaitAnimation Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            WaitAnimation o = new WaitAnimation(parent, editor);

            o.SetID(node.Element("Id").Value);
            o.m_WaitTime = Convert.ToDouble(node.Element("Seconds").Value);
            o.UpdateEditorComponent();

            return o;
        }

        public void UpdateEditorComponent()
        {
            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetWaitTime(m_WaitTime);
            }
        }

        public bool IsRunning()
        {
            return false;
        }

        public void Stop()
        {
            
        }

        public bool CanRun(Command c)
        {
            return c.Type == Command.CommandType.StartAnimation;
        }

        public void Initialize(Command c)
        {
            m_End = DateTime.Now.AddSeconds(c.Delay);
        }

        public bool Logic()
        {
            return m_End > DateTime.Now;
        }

        public void RemoveComponent()
        {
            m_Object.ComponentRemoved(this);
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public void SetID(string id)
        {
            m_ID = id;
        }

        public string GetID()
        {
            return m_ID;
        }

        IEditorComponent IComponent.GetEditorComponent()
        {
            throw new NotImplementedException();
        }

        ObjectControl IComponent.GetParentObject()
        {
            throw new NotImplementedException();
        }

        void IComponent.RemoveComponent()
        {
            throw new NotImplementedException();
        }

        bool IComponent.CanRun(Command c)
        {
            throw new NotImplementedException();
        }

        bool IComponent.IsRunning()
        {
            throw new NotImplementedException();
        }

        void IComponent.Stop()
        {
            throw new NotImplementedException();
        }

        void IComponent.Initialize(Command c)
        {
            throw new NotImplementedException();
        }

        bool IComponent.Logic()
        {
            throw new NotImplementedException();
        }

        void IComponent.SetID(string id)
        {
            throw new NotImplementedException();
        }

        string IComponent.GetID()
        {
            throw new NotImplementedException();
        }
    }
}
