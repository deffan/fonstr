﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;
using TwitchTool.AnimationsEditor;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Animations
{
    public class AnimationOrderComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public AnimationOrderEditorComponent m_EditorComponent;
        private string m_ID;

        private List<AnimationOrder> m_Original;
        private List<AnimationOrder> m_Queue;
        private List<IComponent> m_Running;

        public class AnimationOrder
        {
            public IComponent Animation;
            public int Order;

            public AnimationOrder(IComponent animation, int order)
            {
                Animation = animation;
                Order = order;
            }
        }

        public AnimationOrderComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_Original = new List<AnimationOrder>();    // Original data. Do not modify.
            m_Queue = new List<AnimationOrder>();       // Queue, modifies while running.
            m_Running = new List<IComponent>();         // The currently running ones.

            // Tar in alla animationer på objektet.
            // Kan sätta ordningen med pilar upp/ner.
            // 
            // Default har alla ordningen "0" vilket innebär att de kör direkt.
            // Man kan ha flera animationer på samma ordning, så kör de samtidigt
            // 
            // UpDown   Order   Name
            // <>        [0]    AnimationName
            // 
            // Så man sätter ordernumret med text eller knappar. De borde också flyttas visuellt i listan, rent ordermässigt.

            // Kanske behövs denna komponent direkt det finns en animation?
            // Så den läggs alltid till (och kan inte tas bort) om det finns 1 animation på objektet, och tas bort automatiskt om sista animatinen tas bort.
            //
            // Det är alltså ALLTID från denna komponent animationerna körs via.
            // Även fast logiken individuellt är detsamma.

            // IAnimation <------------ skall INTE ingå i Listener. Måste filtreras bort.

            // Men, detta är för animationer som ska aktiveras, köras, sen resettas via kommando.
            //
            // På varje animation måste man kunna ange om man vill detta eller ej.

            // 
            // Antingen ska animationen starta och köra direkt fönstet startar.
            // De kan då inte påverkas av kommandon.


            // Generella inställningar för ALLA animationer:
            //-------------------------------------------------------
            // (x) Run on start _OR_ (x) Run on command                 <--- måste vi trigga ditläggning/borttagning automatiskt för listener(s) på objektet?
            // [x] Run [1..N] times                                          samt ur ev AnimationOrder komponent
            // (x) loop forever <--- GRAYED if "Run on command"
            // [x] Reverse on animation end
            // [x] Reset when finished      <- GRAYED if "Loop Forever"

            // Specifikt (Wait)
            // Wait for [0.0 .. N] seconds 

            // Specifikt (Move)
            // Move from [x,y] to [x,y] in [0.0] seconds

            // Specifikt (Fade)
            // Fade from [0] to [100] % in [0.0] seconds


            // Animationerna körs så här:
            //
            // Alla animationer på en viss ordning måste köras färdigt innan nästa kan börja.
            // allt ligger i en lista.
            //
            // allt på ordning 0 flyttas till "running" listan.
            // alla andra orders får nu -- i värde, så de är nu ett snäpp upp.
            // när alla i running kört färdigt, görs samma sak igen.
            // när listan är tom, har denna komponent kört färdigt.


            // Först nu då:
            // Skapa de två animationerna, deras logik (med hänsyn till GeneralAnimationSettings) och logiken mellan Editor/Komponent, samt serializering osv.
            // Sen gör att de går lägga till.
            // Sen skapa AnimationEditorComponent osv.
            // SEN kan man prova det på riktigt...


            if (editor)
            {
                m_EditorComponent = new AnimationOrderEditorComponent(this);
            }
        }

        XElement IXML.Serialize()
        {
            return null;
        }

        public static AnimationOrderComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            return null;
        }

        public bool CanRun(Command c)
        {
            return false;
        }

        public bool IsRunning()
        {
            return false;
        }

        public void Stop()
        {

        }

        public void Initialize(Command c)
        {
            m_Queue.AddRange(m_Original);
            Progress();
        }

        private void Progress()
        {
            // Move everything with order 0 to the running list, move all other orders down one step
            for (int i = m_Queue.Count - 1; i <= 0; i--)
            {
                if (m_Queue[i].Order <= 0)
                {
                    m_Running.Add(m_Queue[i].Animation);
                    m_Queue.RemoveAt(i);
                }
                else
                {
                    m_Queue[i].Order--;
                }
            }
        }

        public bool Logic()
        {
            for (int i = m_Running.Count - 1; i >= 0; i--)
            {
                if(m_Running[i].Logic())
                {
                    m_Running.RemoveAt(i);
                }
            }

            if(m_Running.Count > 0)
            {
                return false;   // Still running something
            }
            else
            {
                // Finished running everything with this order
                if(m_Queue.Count > 0)
                {
                    Progress();
                }
                else
                {
                    return true;    // Completely finished.
                }
            }
            return false;
        }

        public void RemoveComponent()
        {
            m_Object.ComponentRemoved(this);
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public void SetID(string id)
        {
            m_ID = id;
        }

        public string GetID()
        {
            return m_ID;
        }
    }
}
