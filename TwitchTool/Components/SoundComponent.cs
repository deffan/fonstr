﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Xml.Linq;
using TwitchTool.Commands;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Components
{
    public class SoundComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public SoundEditorComponent m_EditorComponent;
        private string m_ID;
        private MediaElement m_MediaElement;
        private string m_FilePath;
        private double m_Volume;
        private bool m_Running;

        private SoundCommandData m_RunningData;

        public SoundComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_MediaElement = new MediaElement();

            m_MediaElement.UnloadedBehavior = MediaState.Manual;
            m_MediaElement.LoadedBehavior = MediaState.Manual;
            m_MediaElement.MediaEnded += M_MediaElement_MediaEnded;
            m_MediaElement.MediaOpened += M_MediaElement_MediaOpened;
            m_MediaElement.MediaFailed += M_MediaElement_MediaFailed;

            m_FilePath = "";
            m_Volume = 1;
            m_Running = false;

            if (editor)
            {
                m_EditorComponent = new SoundEditorComponent(this);
            }
        }

        public void NewComponentConstructor()
        {

        }

        private void M_MediaElement_MediaFailed(object sender, System.Windows.ExceptionRoutedEventArgs e)
        {
            m_Running = false;
            m_RunningData = null;
        }

        private void M_MediaElement_MediaOpened(object sender, System.Windows.RoutedEventArgs e)
        {
            
        }

        private void M_MediaElement_MediaEnded(object sender, System.Windows.RoutedEventArgs e)
        {
            Stop();
        }

        public void SetVolume(double volume)
        {
            m_Volume = volume;
            m_MediaElement.Volume = m_Volume;
        }

        public bool SetSoundFile(string file)
        {
            try
            {
                m_FilePath = file;
                m_MediaElement.Source = new Uri(m_FilePath, UriKind.Relative);
                m_MediaElement.Position = TimeSpan.Zero;
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "SoundComponent->SetSoundFile - " + ex.Message);
                m_Running = false;
            }
            return false;
        }

        public string GetSoundFilePath()
        {
            return m_FilePath;
        }

        public void SetSoundFilePath(string path)
        {
            m_FilePath = path;
        }

        public void Play()
        {
            try
            {
                m_MediaElement.Play();
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "SoundComponent->Play - " + ex.Message);
                m_Running = false;
                m_RunningData = null;
            }
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.SOUND;
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public void Initialize(Command c)
        {
            m_RunningData = c.Data as SoundCommandData;

            m_Running = SetSoundFile(m_RunningData.m_Data.GetStringData(c.Event));
            if (!m_Running && m_RunningData.m_UseBackup)
            {
                m_Running = SetSoundFile(m_RunningData.m_BackupData.GetStringData(c.Event)); 
            }

            if(m_Running)
            {
                Play();
            }
            else
            {
                m_RunningData = null;
            }
        }

        public bool IsRunning()
        {
            return m_Running;
        }

        public bool Logic()
        {
            return !m_Running;
        }

        public void RemoveComponent()
        {
            m_Object.ComponentRemoved(this);
        }

        XElement IXML.Serialize()
        {
            XElement node = new XElement("SoundComponent");

            node.Add(new XElement("Id", ID));
            node.Add(new XElement("Path", GlobalHelper.Base64Encode(m_FilePath)));
            node.Add(new XElement("Volume", Math.Round(m_Volume, 2)));

            return node;
        }

        public static SoundComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            SoundComponent b = new SoundComponent(parent, editor);

            b.ID = node.Element("Id").Value;
            b.SetSoundFile(GlobalHelper.Base64Decode(node.Element("Path").Value));
            b.SetVolume(Convert.ToDouble(node.Element("Volume").Value, CultureInfo.InvariantCulture));

            if (editor)
            {
                b.m_EditorComponent.SetData(b.m_FilePath, b.m_Volume);
            }

            return b;
        }
        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }

        public void Stop()
        {
            try
            {
                m_MediaElement.Stop();
            }
            catch (Exception) { }
            m_Running = false;
            m_RunningData = null;
        }
    }
}
