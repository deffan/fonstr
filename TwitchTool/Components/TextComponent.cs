﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using System.Windows;
using TwitchTool.Commands;
using System.Globalization;

namespace TwitchTool.Components
{
    public class TextComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public TextEditorComponent m_EditorComponent;
        private TextControl m_TextControl;
        private string m_ID;
        private BrushPickerControl m_TextBrushpicker;
        private BrushPickerControl m_EffectBrushpicker;

        public TextComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_TextControl = new TextControl(parent, this);
            m_Object.Children.Add(m_TextControl);

            if (editor)
            {
                SetBrushPickers(new BrushPickerControl(), new BrushPickerControl());
                m_EditorComponent = new TextEditorComponent(this);
            }
        }

        public void NewComponentConstructor()
        {
            m_TextBrushpicker.SetSolidColor(new SolidColorBrush(Colors.Black));
        }

        public void SetBrushPickers(BrushPickerControl text, BrushPickerControl effect)
        {
            m_TextBrushpicker = text;
            m_EffectBrushpicker = effect;
        }

        public BrushPickerControl GetTextBrushPicker()
        {
            return m_TextBrushpicker;
        }

        public BrushPickerControl GetEffectBrushPicker()
        {
            return m_EffectBrushpicker;
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }


        public void SetTextAlignment(TextAlignment alignment)
        {
            m_TextControl.SetTextAlignment(alignment, true);
        }

        public void SetText(string txt)
        {
            m_TextControl.SetText(txt);

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetText(txt);
            }
        }

        public void SetBold(bool bold)
        {
            m_TextControl.SetBold(bold, true);
        }

        public void SetItalic(bool italic)
        {
            m_TextControl.SetItalic(italic, true);
        }

        public void SetUnderline(bool underline)
        {
            m_TextControl.SetUnderline(underline, true);
        }

        public void SetFont(FontFamily font)
        {
            m_TextControl.SetFont(font, true);
        }

        public void SetTextSize(double size)
        {
            m_TextControl.SetTextSize(size, true);
        }

        public void SetStrokeSize(double size)
        {
            m_TextControl.SetStrokeSize(size, true);
        }

        public void SetStrokeBrush(Brush brush)
        {
            m_TextControl.SetStrokeBrush(brush, true);
        }

        public void SetTextBrush(Brush brush)
        {
            m_TextControl.SetTextBrush(brush, true);
        }

        public void SetEffect(int effect)
        {
            m_TextControl.SetEffect(effect, true);
        }

        public void SetTextWrapping(bool on)
        {
            m_TextControl.SetTextWrapping(on, true);
        }

        public void SetTwitchEmoticons(bool use)
        {
            m_TextControl.SetTwitchEmoticons(use, true);
        }

        XElement IXML.Serialize()
        {
            XElement textComponentNode = new XElement("TextComponent");

            XElement idNode = new XElement("Id", ID);
            textComponentNode.Add(idNode);

            XElement text = new XElement("Text", GlobalHelper.Base64Encode(m_TextControl.GetOriginalText()));
            textComponentNode.Add(text);

            XElement textBrush = new XElement("TextBrush");
            textBrush.Add(GetTextBrushPicker().Serialize());
            textComponentNode.Add(textBrush);

            XElement effectBrush = new XElement("EffectBrush");
            effectBrush.Add(GetEffectBrushPicker().Serialize());
            textComponentNode.Add(effectBrush);

            XElement fontSize = new XElement("FontSize", m_TextControl.TheText.FontSize);
            textComponentNode.Add(fontSize);

            XElement fontStyle = new XElement("FontStyle", m_TextControl.TheText.FontStyle == FontStyles.Italic ? true : false);
            textComponentNode.Add(fontStyle);

            XElement fontFamily = new XElement("FontFamily", m_TextControl.TheText.FontFamily.ToString());
            textComponentNode.Add(fontFamily);

            XElement fontWeight = new XElement("FontWeight", m_TextControl.TheText.FontWeight == FontWeights.Bold ? true : false);
            textComponentNode.Add(fontWeight);

            XElement textAlignment = new XElement("TextAlignment", m_TextControl.TheText.TextAlignment.ToString());
            textComponentNode.Add(textAlignment);

            XElement textWrapping = new XElement("TextWrapping", m_TextControl.TheText.TextWrapping == TextWrapping.Wrap ? true : false);
            textComponentNode.Add(textWrapping);

            XElement textDecorations = new XElement("Underline", m_TextControl.m_Underline);
            textComponentNode.Add(textDecorations);

            XElement effect = new XElement("Effect", m_TextControl.GetEffect());
            textComponentNode.Add(effect);

            XElement strokeSize = new XElement("StrokeSize", m_TextControl.GetStrokeSize());
            textComponentNode.Add(strokeSize);

            XElement twitchEmotes = new XElement("TwitchEmoticons", m_TextControl.UseTwitchEmoticons());
            textComponentNode.Add(twitchEmotes);

            return textComponentNode;
        }

        public static TextComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            
            TextComponent t = new TextComponent(parent, editor);
            t.ID = node.Element("Id").Value;
            t.SetText(GlobalHelper.Base64Decode(node.Element("Text").Value));

            BrushPickerControl textBrush = BrushPickerControl.Deserialize(node.Element("TextBrush").Element("BrushPicker"));
            BrushPickerControl effectBrush = BrushPickerControl.Deserialize(node.Element("EffectBrush").Element("BrushPicker"));
            t.SetBrushPickers(textBrush, effectBrush);
            t.SetStrokeBrush(effectBrush.GetBrush());
            t.SetTextBrush(textBrush.GetBrush());

            Enum.TryParse(node.Element("TextAlignment").Value, out TextAlignment alignment);

            t.m_TextControl.SetAll(
                new FontFamily(node.Element("FontFamily").Value),
                Convert.ToDouble(node.Element("FontSize").Value, CultureInfo.InvariantCulture),
                Convert.ToBoolean(node.Element("FontWeight").Value),
                Convert.ToBoolean(node.Element("FontStyle").Value),
                Convert.ToBoolean(node.Element("Underline").Value),
                Convert.ToBoolean(node.Element("TextWrapping").Value),
                alignment,
                Convert.ToInt32(node.Element("Effect").Value),
                Convert.ToInt32(node.Element("StrokeSize").Value),
                textBrush.GetBrush(),
                effectBrush.GetBrush(),
                Convert.ToBoolean(node.Element("TwitchEmoticons").Value));

            if(t.m_EditorComponent != null)
            {
                t.m_EditorComponent.SetAll(
                    new FontFamily(node.Element("FontFamily").Value),
                    Convert.ToDouble(node.Element("FontSize").Value, CultureInfo.InvariantCulture),
                    Convert.ToBoolean(node.Element("FontWeight").Value),
                    Convert.ToBoolean(node.Element("FontStyle").Value),
                    Convert.ToBoolean(node.Element("Underline").Value),
                    Convert.ToBoolean(node.Element("TextWrapping").Value),
                    alignment,
                    Convert.ToInt32(node.Element("Effect").Value),
                    Convert.ToInt32(node.Element("StrokeSize").Value),
                    textBrush,
                    effectBrush,
                    Convert.ToBoolean(node.Element("TwitchEmoticons").Value));
            }

            return t;
        }

        public void RemoveComponent()
        {
            m_Object.Children.Remove(m_TextControl);
            m_Object.ComponentRemoved(this);
        }

        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.TEXT;
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public void Initialize(Command c)
        {
            string text = "";
            TextCommandData data = c.Data as TextCommandData;
            for (int i = 0; i < data.TextDataList.Count; i++)
            {
                text += data.TextDataList[i].GetStringData(c.Event);
            }

            m_TextControl.SetText(text);
        }

        public bool Logic()
        {
            return true;
        }

        public bool IsRunning()
        {
            return false;
        }

        public void Stop()
        {

        }
    }
}
