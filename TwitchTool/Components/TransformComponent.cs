﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Components
{
    public class TransformComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public TransformEditorComponent m_EditorComponent;
        public TranslateTransform m_TranslateTransform;
        public RotateTransform m_RotateTransform;
        public ScaleTransform m_ScaleTransform;
        public SkewTransform m_SkewTransform;
        private Point m_Coordinates;
        private Point m_Zero;
        private Size m_Size;
        private Rect m_Rect;
        private Size m_Scale;
        private Size m_Skew;
        private string m_ID;
        private int m_RotationOrigin;
        private int m_ScaleOrigin;
        private List<Action<int>> m_OnWidthChanged;
        private List<Action<int>> m_OnHeightChanged;

        public TransformComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_TranslateTransform = new TranslateTransform(0, 0);
            m_RotateTransform = new RotateTransform(0, 0, 0);
            m_ScaleTransform = new ScaleTransform();
            m_SkewTransform = new SkewTransform();
            m_Coordinates = new Point();
            m_Zero = new Point(0, 0);
            m_Size = new Size();
            m_Scale = new Size();
            m_Skew = new Size();
            m_Rect = new Rect();
            SetRotationOrigin(ORIGIN.CENTER);
            m_ScaleOrigin = 0;
            m_OnWidthChanged = new List<Action<int>>();
            m_OnHeightChanged = new List<Action<int>>();

            TransformGroup myTransformGroup = new TransformGroup();
            myTransformGroup.Children.Add(m_RotateTransform);
            myTransformGroup.Children.Add(m_ScaleTransform);
            myTransformGroup.Children.Add(m_SkewTransform);
            myTransformGroup.Children.Add(m_TranslateTransform);
            m_Object.RenderTransform = myTransformGroup;

            // Denna ordning ska gå ändra. Ska ha en ">" expander nedåt, som öppnar: Transform Order
            // Där man kan ordna vilken ordning detta skall fungera i.
            // Default är som ovan.

            if (editor)
            {
                m_EditorComponent = new TransformEditorComponent(this);
            }
        }

        public void NewComponentConstructor()
        {

        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public void DisableCoordinates()
        {
            m_EditorComponent.XTextBox.IsEnabled = false;
            m_EditorComponent.YTextBox.IsEnabled = false;
        }

        public void SetCoordinates(double x, double y)
        {
            m_TranslateTransform.X = (int)x;
            m_TranslateTransform.Y = (int)y;

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetXCoordinate(x);
                m_EditorComponent.SetYCoordinate(y);
            }
        }

        public void SetRotationOrigin(int origin)
        {
            m_RotationOrigin = origin;
            switch (origin)
            {
                case ORIGIN.TOP_LEFT:
                {
                    m_RotateTransform.CenterX = 0;
                    m_RotateTransform.CenterY = 0;
                    break;
                }
                case ORIGIN.TOP_RIGHT:
                {
                    m_RotateTransform.CenterX = m_Object.Width;
                    m_RotateTransform.CenterY = 0;
                    break;
                }
                case ORIGIN.CENTER:
                {
                    m_RotateTransform.CenterX = m_Object.Width / 2;
                    m_RotateTransform.CenterY = m_Object.Height / 2;
                    break;
                }
                case ORIGIN.BOTTOM_LEFT:
                {
                    m_RotateTransform.CenterX = 0;
                    m_RotateTransform.CenterY = m_Object.Height;
                    break;
                }
                case ORIGIN.BOTTOM_RIGHT:
                {
                    m_RotateTransform.CenterX = m_Object.Width;
                    m_RotateTransform.CenterY = m_Object.Height;
                    break;
                }
            }

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetRotationOrigin(origin);
            }
        }

        public void SetScaleOrigin(int origin)
        {
            m_ScaleOrigin = origin;
            if (origin == 0)
            {
                m_ScaleTransform.CenterX = 0;
                m_ScaleTransform.CenterY = 0;
            }
            else
            {
                m_ScaleTransform.CenterX = m_Object.Width / 2;
                m_ScaleTransform.CenterY = m_Object.Height / 2;
            }

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetScaleOrigin(origin);
            }
        }

        public void SetRotation(double rotation)
        {
            m_RotateTransform.Angle = (int)rotation;

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetRotation(rotation);
            }
        }

        public double GetRotationAngle()
        {
            return m_RotateTransform.Angle;
        }

        public void SetSize(double x, double y)
        {
            SetWidth(x);
            SetHeight(y);

            // These needs to be updated each time
            SetRotationOrigin(m_RotationOrigin);
            SetScaleOrigin(m_ScaleOrigin);

            m_SkewTransform.CenterX = m_Object.Width / 2;
            m_SkewTransform.CenterY = m_Object.Height / 2;

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetWidth(x);
                m_EditorComponent.SetHeight(y);
            }
        }

        public void SetXCoordinate(double x)
        {
            m_TranslateTransform.X = (int)x;

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetXCoordinate(x);
            }
        }

        public void SetYCoordinate(double y)
        {
            m_TranslateTransform.Y = (int)y;

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetYCoordinate(y);
            }
        }

        public void RegisterOnWidthChanged(Action<int> a)
        {
            m_OnWidthChanged.Add(a);
        }

        public void RegisterOnHeightChanged(Action<int> a)
        {
            m_OnHeightChanged.Add(a);
        }

        public void SetWidth(double x)
        {
            m_Object.MinWidth = (int)x;
            m_Object.Width = (int)x;
            m_Object.ObjectBorder.MinWidth = (int)x;

            for(int i = 0; i < m_OnWidthChanged.Count; i++)
            {
                m_OnWidthChanged[i]((int)x);
            }
        }

        public void SetHeight(double y)
        {
            m_Object.MinHeight = (int)y;
            m_Object.Height = (int)y;
            m_Object.ObjectBorder.MinHeight = (int)y;

            for (int i = 0; i < m_OnHeightChanged.Count; i++)
            {
                m_OnHeightChanged[i]((int)y);
            }
        }

        public Size GetSize()
        {
            m_Size.Width = m_Object.Width;
            m_Size.Height = m_Object.Height;
            return m_Size;
        }

        public Size GetScale()
        {
            m_Scale.Width = m_ScaleTransform.ScaleX;
            m_Scale.Height = m_ScaleTransform.ScaleY;
            return m_Scale;
        }

        public Size GetSkew()
        {
            m_Skew.Width = m_SkewTransform.AngleX;
            m_Skew.Height = m_SkewTransform.AngleY;
            return m_Skew;
        }

        public void SetScale(double x, double y)
        {
            m_ScaleTransform.ScaleX = Math.Round(x, 2);
            m_ScaleTransform.ScaleY = Math.Round(y, 2);

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetScale(m_ScaleTransform.ScaleX, m_ScaleTransform.ScaleY);
            }
        }

        public void SetSkew(double x, double y)
        {
            m_SkewTransform.AngleX = Math.Round(x, 2);
            m_SkewTransform.AngleY = Math.Round(y, 2);

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetSkew(m_SkewTransform.AngleX, m_SkewTransform.AngleY);
            }
        }

        public Point GetCoordinates()
        {
            m_Coordinates.X = m_TranslateTransform.X;
            m_Coordinates.Y = m_TranslateTransform.Y;
            return m_Coordinates;
        }

        public Rect GetRect()
        {
            m_Rect.X = m_TranslateTransform.X;
            m_Rect.Y = m_TranslateTransform.Y;
            m_Rect.Width = m_Object.Width;
            m_Rect.Height = m_Object.Height;
            return m_Rect;
        }

        XElement IXML.Serialize()
        {
            XElement transformComponentNode = new XElement("TransformComponent");

            XElement idNode = new XElement("Id", ID);
            transformComponentNode.Add(idNode);

            XElement w = new XElement("Width", (int)m_Object.Width);
            transformComponentNode.Add(w);

            XElement h = new XElement("Height", (int)m_Object.Height);
            transformComponentNode.Add(h);

            XElement x = new XElement("X", (int)m_TranslateTransform.X);
            transformComponentNode.Add(x);

            XElement y = new XElement("Y", (int)m_TranslateTransform.Y);
            transformComponentNode.Add(y);

            XElement rotation = new XElement("Rotation", (int)m_RotateTransform.Angle);
            transformComponentNode.Add(rotation);

            XElement rotationOrigin = new XElement("RotationOrigin", m_RotationOrigin);
            transformComponentNode.Add(rotationOrigin);

            XElement scalex = new XElement("ScaleX", m_ScaleTransform.ScaleX);
            transformComponentNode.Add(scalex);

            XElement scaley = new XElement("ScaleY", m_ScaleTransform.ScaleY);
            transformComponentNode.Add(scaley);

            XElement scaleOrigin = new XElement("ScaleOrigin", m_ScaleOrigin);
            transformComponentNode.Add(scaleOrigin);

            XElement skewx = new XElement("SkewX", m_SkewTransform.AngleX);
            transformComponentNode.Add(skewx);

            XElement skewy = new XElement("SkewY", m_SkewTransform.AngleY);
            transformComponentNode.Add(skewy);

            return transformComponentNode;
        }

        public static TransformComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            TransformComponent t = new TransformComponent(parent, editor);

            t.ID = node.Element("Id").Value;

            t.SetSize(Convert.ToInt32(node.Element("Width").Value), Convert.ToInt32(node.Element("Height").Value));

            t.SetCoordinates(Convert.ToInt32(node.Element("X").Value), Convert.ToInt32(node.Element("Y").Value));

            t.SetRotation(Convert.ToInt32(node.Element("Rotation").Value));

            t.SetRotationOrigin(Convert.ToInt32(node.Element("RotationOrigin").Value));

            t.SetScale(Convert.ToDouble(node.Element("ScaleX").Value, CultureInfo.InvariantCulture), Convert.ToDouble(node.Element("ScaleY").Value, CultureInfo.InvariantCulture));

            t.SetScaleOrigin(Convert.ToInt32(node.Element("ScaleOrigin").Value));

            t.SetSkew(Convert.ToDouble(node.Element("SkewX").Value, CultureInfo.InvariantCulture), Convert.ToDouble(node.Element("SkewY").Value, CultureInfo.InvariantCulture));

            return t;
        }

        public bool CanRun(Command c)
        {
            return false;
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public void Initialize(Command c)
        {

        }

        public bool Logic()
        {
            return true;
        }

        public void RemoveComponent()
        {
            m_Object.ComponentRemoved(this);
        }

        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }

        public bool IsRunning()
        {
            return false;
        }

        public void Stop()
        {

        }
    }

}
