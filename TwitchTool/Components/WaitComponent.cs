﻿using System.Timers;
using System.Xml.Linq;
using TwitchTool.Commands;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;


namespace TwitchTool.Components
{
    public class WaitComponent : IComponent
    {
        private Timer m_Timer;
        private string m_ID;
        private bool m_Running;

        public WaitComponent()
        {
            // This component is created and only exists while the wait is happening. It cant be seen or created in the editor.
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public void NewComponentConstructor()
        {

        }

        public ObjectControl GetParentObject()
        {
            return null;
        }

        public IEditorComponent GetEditorComponent()
        {
            return null;
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.WAIT;
        }

        XElement IXML.Serialize()
        {
            return null;
        }

        public static ImageComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            return null;
        }

        public void Initialize(Command c)
        {
            WaitCommandData w = c.Data as WaitCommandData;
            double wait = w.m_TextData.GetNumericData();
            if(wait <= 0)
            {
                m_Running = false;
            }
            else
            {
                m_Running = true;
                m_Timer = new Timer();
                m_Timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                m_Timer.Interval = wait * 1000;
                m_Timer.Start();
            }
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            m_Running = false;
        }

        public bool Logic()
        {
            return !m_Running;
        }

        public void RemoveComponent()
        {

        }

        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }

        public bool IsRunning()
        {
            return m_Running;
        }

        public void Stop()
        {
            if(m_Timer != null)
            {
                m_Timer.Stop();
            }
            m_Timer = null;
            m_Running = false;
        }
    }
}
