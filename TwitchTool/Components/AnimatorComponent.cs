﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using System.Xml.Linq;
using TwitchTool.Animations;
using TwitchTool.AnimationsEditor;
using TwitchTool.Animator;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Components
{
    public class AnimatorComponent : UserControl, IComponent, IEditorTab
    {
        public ObjectControl m_Object;
        public AnimatorEditorComponent m_EditorComponent;
        private string m_ID;
        private List<AnimationDataHolder> m_AnimationsList;
        private GeneralAnimationSettings m_GeneralSettings;
        private bool m_Rebuild;
        private bool m_Running;
        private bool m_GoingForward;
        private int m_RunTimes;
        private bool m_Loop;
        private bool m_Stopped;
        private List<AnimationRunner> m_RunningAnimations;

        private class AnimationRunner
        {
            public ObjectControl Object;
            public List<IAnimation> Animations;
            public bool Finished;

            public AnimationRunner()
            {
                Finished = false;
            }
        }

        public AnimatorComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_AnimationsList = new List<AnimationDataHolder>();
            m_GeneralSettings = new GeneralAnimationSettings();
            m_RunningAnimations = new List<AnimationRunner>();
            m_Rebuild = false;
            m_Running = false;
            m_GoingForward = true;
            m_RunTimes = 0;
            m_Loop = false;
            m_Stopped = false;

            // By default, this object is the only object.
            AnimationDataHolder newHolder = new AnimationDataHolder();
            newHolder.Object = m_Object;
            newHolder.Selected = true;
            m_AnimationsList.Add(newHolder);

            if (editor)
            {
                m_EditorComponent = new AnimatorEditorComponent(this);
            }
        }


        public void NewComponentConstructor()
        {

        }

        public bool Rebuild
        {
            get { return m_Rebuild; }
            set { m_Rebuild = value; }
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public GeneralAnimationSettings GetGeneralSettings()
        {
            return m_GeneralSettings;
        }

        public List<AnimationDataHolder> GetObjectsToAnimate()
        {
            return m_AnimationsList;
        }

        public void UpdateAnimationList(List<Tuple<ObjectControl, CheckBox>> objects)
        {
            // Check if we should add something
            for (int i = 0; i < objects.Count; i++)
            {
                bool found = false;
                for (int n = 0; n < m_AnimationsList.Count; n++)
                {
                    if (m_AnimationsList[n].Object.ID.Equals(objects[i].Item1.ID))
                    {
                        found = true;
                        break;
                    }
                }
                
                if(!found)
                {
                    AnimationDataHolder newHolder = new AnimationDataHolder();
                    newHolder.Object = objects[i].Item1;
                    m_AnimationsList.Add(newHolder);
                }
            }

            // Check if we should remove something
            for (int n = m_AnimationsList.Count - 1; n >= 0; n--)
            {
                bool found = false;
                for (int i = 0; i < objects.Count; i++)
                {
                    if (m_AnimationsList[n].Object.ID.Equals(objects[i].Item1.ID))
                    {
                        found = true;
                        break;
                    }
                }

                if(!found)
                {
                    m_AnimationsList.RemoveAt(n);
                    Logger.Log(LOG_LEVEL.DEBUG, "Removed object from AnimatorList which is no longer there.");
                }
            }
        }

        public void UpdateAnimationList(AnimationDataHolder holder)
        {
            for (int n = 0; n < m_AnimationsList.Count; n++)
            {
                if (m_AnimationsList[n].Object.ID.Equals(holder.Object.ID))
                {
                    m_AnimationsList[n] = holder;
                    break;
                }
            }
        }

        public void SelectForAnimation(bool select, ObjectControl o)
        {
            for (int i = 0; i < m_AnimationsList.Count; i++)
            {
                if (m_AnimationsList[i].Object.ID.Equals(o.ID))
                {
                    m_AnimationsList[i].Selected = select;
                    return;
                }
            }
        }

        public bool IsSelectedForAnimation(ObjectControl o)
        {
            for(int i = 0; i < m_AnimationsList.Count; i++)
            {
                if(m_AnimationsList[i].Object.ID.Equals(o.ID))
                {
                    return m_AnimationsList[i].Selected;
                }
            }
            return false;
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        XElement IXML.Serialize()
        {
            XElement node = new XElement("AnimatorComponent");

            XElement idNode = new XElement("Id", ID);
            node.Add(idNode);

            // General settings
            XElement general = new XElement("General");
            general.Add(new XElement("RunTimesRadio", m_GeneralSettings.RunTimesRadioButton.IsChecked));
            general.Add(new XElement("RunTimes", m_GeneralSettings.RunTimes.Value));
            general.Add(new XElement("RunForeverRadioButton", m_GeneralSettings.RunForeverRadioButton.IsChecked));
            general.Add(new XElement("DoNothingRadioButton", m_GeneralSettings.DoNothingRadioButton.IsChecked));
            general.Add(new XElement("ResetRadioButton", m_GeneralSettings.ResetRadioButton.IsChecked));
            general.Add(new XElement("ReverseRadioButton", m_GeneralSettings.ReverseRadioButton.IsChecked));
            node.Add(general);

            // Animations
            XElement animations = new XElement("Animations");
            for (int i = 0; i < m_AnimationsList.Count; i++)
            {
                XElement animation = new XElement("Animation");
                XElement obj = new XElement("Object", m_AnimationsList[i].Object.ID);
                XElement sel = new XElement("Selected", m_AnimationsList[i].Selected);
                XElement tracks = new XElement("Tracks");
                animation.Add(obj);
                animation.Add(sel);
                animation.Add(tracks);

                // Tracks
                for (int n = 0; n < m_AnimationsList[i].AnimationTracks.Count; n++)
                {
                    // Track/Type
                    XElement track = new XElement("Track");
                    track.Add(new XElement("Type", m_AnimationsList[i].AnimationTracks[n].AnimationType));

                    // Animation data
                    XElement animationData = new XElement("AnimationData");
                    switch (m_AnimationsList[i].AnimationTracks[n].AnimationType)
                    {
                        case ANIMATIONTYPE.MOVE:
                        {
                            for (int k = 0; k < m_AnimationsList[i].AnimationTracks[n].AnimationDataList.Count; k++)
                            {
                                MoveAnimationData d = m_AnimationsList[i].AnimationTracks[n].AnimationDataList[k] as MoveAnimationData;
                                XElement data = new XElement("Data");
                                data.Add(new XElement("FromSeconds", d.FromSeconds));
                                data.Add(new XElement("ToSeconds", d.ToSeconds));
                                data.Add(new XElement("ToX", JsonConvert.SerializeObject(d.ToX)));
                                data.Add(new XElement("ToY", JsonConvert.SerializeObject(d.ToY)));
                                data.Add(new XElement("Easing", d.Easing));
                                animationData.Add(data);
                            }
                            break;
                        }
                        case ANIMATIONTYPE.ROTATE:
                        {
                            for (int k = 0; k < m_AnimationsList[i].AnimationTracks[n].AnimationDataList.Count; k++)
                            {
                                RotateAnimationData d = m_AnimationsList[i].AnimationTracks[n].AnimationDataList[k] as RotateAnimationData;
                                XElement data = new XElement("Data");
                                data.Add(new XElement("FromSeconds", d.FromSeconds));
                                data.Add(new XElement("ToSeconds", d.ToSeconds));
                                data.Add(new XElement("ToAngle", JsonConvert.SerializeObject(d.ToAngle)));
                                data.Add(new XElement("Easing", d.Easing));
                                animationData.Add(data);
                            }
                            break;
                        }
                        case ANIMATIONTYPE.SCALE:
                        {
                            for (int k = 0; k < m_AnimationsList[i].AnimationTracks[n].AnimationDataList.Count; k++)
                            {
                                ScaleAnimationData d = m_AnimationsList[i].AnimationTracks[n].AnimationDataList[k] as ScaleAnimationData;
                                XElement data = new XElement("Data");
                                data.Add(new XElement("FromSeconds", d.FromSeconds));
                                data.Add(new XElement("ToSeconds", d.ToSeconds));
                                data.Add(new XElement("ToXScale", JsonConvert.SerializeObject(d.ToXScale)));
                                data.Add(new XElement("ToYScale", JsonConvert.SerializeObject(d.ToYScale)));
                                animationData.Add(data);
                            }
                            break;
                        }
                        case ANIMATIONTYPE.SKEW:
                        {
                            for (int k = 0; k < m_AnimationsList[i].AnimationTracks[n].AnimationDataList.Count; k++)
                            {
                                SkewAnimationData d = m_AnimationsList[i].AnimationTracks[n].AnimationDataList[k] as SkewAnimationData;
                                XElement data = new XElement("Data");
                                data.Add(new XElement("FromSeconds", d.FromSeconds));
                                data.Add(new XElement("ToSeconds", d.ToSeconds));
                                data.Add(new XElement("ToXSkew", JsonConvert.SerializeObject(d.ToXSkew)));
                                data.Add(new XElement("ToYSkew", JsonConvert.SerializeObject(d.ToYSkew)));
                                animationData.Add(data);
                            }
                            break;
                        }
                        case ANIMATIONTYPE.TRANSPARENCY:
                        {
                            for (int k = 0; k < m_AnimationsList[i].AnimationTracks[n].AnimationDataList.Count; k++)
                            {
                                TransparencyAnimationData d = m_AnimationsList[i].AnimationTracks[n].AnimationDataList[k] as TransparencyAnimationData;
                                XElement data = new XElement("Data");
                                data.Add(new XElement("FromSeconds", d.FromSeconds));
                                data.Add(new XElement("ToSeconds", d.ToSeconds));
                                data.Add(new XElement("ToOpacity", JsonConvert.SerializeObject(d.ToOpacity)));
                                animationData.Add(data);
                            }
                            break;
                        }
                        default: throw new Exception("AnimatorComponent Serialize: Invalid AnimationType: " + m_AnimationsList[i].AnimationTracks[n].AnimationType);
                    }
                    track.Add(animationData);
                    tracks.Add(track);
                }
                animations.Add(animation);
            }

            node.Add(animations);
            return node;
        }

        public static AnimatorComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            AnimatorComponent a = new AnimatorComponent(parent, editor);
            a.ID = node.Element("Id").Value;

            // General settings
            int onEnd = 0;
            if (Convert.ToBoolean(node.Element("General").Element("ResetRadioButton").Value))
            {
                onEnd = 1;
            }
            else if (Convert.ToBoolean(node.Element("General").Element("ReverseRadioButton").Value))
            {
                onEnd = 2;
            }
            a.m_GeneralSettings.SetData(Convert.ToBoolean(node.Element("General").Element("RunForeverRadioButton").Value),
                Convert.ToInt32(node.Element("General").Element("RunTimes").Value),
                onEnd);


            XElement animations = node.Element("Animations");
            foreach (XElement anis in animations.Elements())
            {
                AnimationDataHolder holder = new AnimationDataHolder();
                holder.Object = GlobalHelper.Find(a.m_Object, anis.Element("Object").Value);
                if(holder.Object == null)
                {
                    throw new Exception("AnimatorComponent Deserialize - Could not find object: " + anis.Element("Object").Value);
                }
                holder.Selected = Convert.ToBoolean(anis.Element("Selected").Value);

                XElement tracks = anis.Element("Tracks");
                foreach (XElement track in tracks.Elements())
                {
                    AnimationTrackData trackData = new AnimationTrackData();
                    trackData.AnimationType = Convert.ToInt32(track.Element("Type").Value);

                    XElement animationData = track.Element("AnimationData");
                    foreach (XElement aniData in animationData.Elements())
                    {
                        switch (trackData.AnimationType)
                        {
                            case ANIMATIONTYPE.MOVE:
                            {
                                    MoveAnimationData moveData = new MoveAnimationData(
                                    Convert.ToDouble(aniData.Element("FromSeconds").Value, CultureInfo.InvariantCulture),
                                    Convert.ToDouble(aniData.Element("ToSeconds").Value, CultureInfo.InvariantCulture),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToX").Value),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToY").Value),
                                    Convert.ToInt32(aniData.Element("Easing").Value));

                                trackData.AnimationDataList.Add(moveData);
                                break;
                            }
                            case ANIMATIONTYPE.ROTATE:
                            {
                                    RotateAnimationData rotateData = new RotateAnimationData(
                                    Convert.ToDouble(aniData.Element("FromSeconds").Value, CultureInfo.InvariantCulture),
                                    Convert.ToDouble(aniData.Element("ToSeconds").Value, CultureInfo.InvariantCulture),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToAngle").Value),
                                    Convert.ToInt32(aniData.Element("Easing").Value));

                                trackData.AnimationDataList.Add(rotateData);
                                break;
                            }
                            case ANIMATIONTYPE.SCALE:
                            {
                                ScaleAnimationData rotateData = new ScaleAnimationData(
                                    Convert.ToDouble(aniData.Element("FromSeconds").Value, CultureInfo.InvariantCulture),
                                    Convert.ToDouble(aniData.Element("ToSeconds").Value, CultureInfo.InvariantCulture),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToXScale").Value),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToYScale").Value));

                                trackData.AnimationDataList.Add(rotateData);
                                break;
                            }
                            case ANIMATIONTYPE.SKEW:
                            {
                                SkewAnimationData rotateData = new SkewAnimationData(
                                    Convert.ToDouble(aniData.Element("FromSeconds").Value, CultureInfo.InvariantCulture),
                                    Convert.ToDouble(aniData.Element("ToSeconds").Value, CultureInfo.InvariantCulture),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToXSkew").Value),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToYSkew").Value));

                                trackData.AnimationDataList.Add(rotateData);
                                break;
                            }
                            case ANIMATIONTYPE.TRANSPARENCY:
                            {
                                TransparencyAnimationData rotateData = new TransparencyAnimationData(
                                    Convert.ToDouble(aniData.Element("FromSeconds").Value, CultureInfo.InvariantCulture),
                                    Convert.ToDouble(aniData.Element("ToSeconds").Value, CultureInfo.InvariantCulture),
                                    JsonConvert.DeserializeObject<TextData>(aniData.Element("ToOpacity").Value));

                                trackData.AnimationDataList.Add(rotateData);
                                break;
                            }
                            default: throw new Exception("AnimatorComponent Deserialize: Invalid AnimationType: " + trackData.AnimationType);
                        }
                    }
                    holder.AnimationTracks.Add(trackData);      
                }

                if(editor)
                {
                    a.UpdateAnimationList(holder);
                }
                else
                {
                    a.m_AnimationsList.Add(holder);
                }
            }

            // If editor, load editor component last
            if (editor)
            {
                a.m_EditorComponent = new AnimatorEditorComponent(a);
                GlobalHelper.EditorWindow.AddAnimator(a);
            }

            return a;
        }

        public bool IsRunning()
        {
            return m_Running;
        }

        public void Stop()
        {
            for (int i = 0; i < m_RunningAnimations.Count; i++)
            {
                for (int n = 0; n < m_RunningAnimations[i].Animations.Count; n++)
                {
                    m_RunningAnimations[i].Animations[n].Stop();
                }
            }
            m_RunningAnimations.Clear();

            m_Stopped = true;
            m_Loop = false;
            m_Running = false;
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.START_ANIMATION || c.Data.Type == COMMAND_TYPE.STOP_ANIMATION;
        }

        public void Initialize(Command c)
        {
            if(c.Data.Type == COMMAND_TYPE.STOP_ANIMATION)
            {
                Stop();
                return;
            }

            m_Stopped = false;

            if (m_RunningAnimations.Count > 0)
                return;

            for (int i = 0; i < m_AnimationsList.Count; i++)
            {
                if (!m_AnimationsList[i].Selected)
                    continue;
                
                AnimationRunner runner = new AnimationRunner();
                runner.Object = m_AnimationsList[i].Object;
                runner.Animations = new List<IAnimation>();

                double totalTrackTime = m_AnimationsList[i].GetTotalTrackTime();
                for (int n = 0; n < m_AnimationsList[i].AnimationTracks.Count; n++)
                {
                    if (m_AnimationsList[i].AnimationTracks[n].AnimationDataList.Count == 0)
                        continue;

                    switch (m_AnimationsList[i].AnimationTracks[n].AnimationType)
                    {
                        case ANIMATIONTYPE.MOVE:
                        {
                            runner.Animations.Add(new MoveAnimation(m_AnimationsList[i].Object, m_AnimationsList[i].AnimationTracks[n].AnimationDataList, totalTrackTime, m_GeneralSettings.m_Reverse));
                            break;
                        }
                        case ANIMATIONTYPE.ROTATE:
                        {
                            runner.Animations.Add(new RotateAnimation(m_AnimationsList[i].Object, m_AnimationsList[i].AnimationTracks[n].AnimationDataList, totalTrackTime, m_GeneralSettings.m_Reverse));
                            break;
                        }
                        case ANIMATIONTYPE.SCALE:
                        {
                            runner.Animations.Add(new ScaleAnimation(m_AnimationsList[i].Object, m_AnimationsList[i].AnimationTracks[n].AnimationDataList, totalTrackTime, m_GeneralSettings.m_Reverse));
                            break;
                        }
                        case ANIMATIONTYPE.SKEW:
                        {
                            runner.Animations.Add(new SkewAnimation(m_AnimationsList[i].Object, m_AnimationsList[i].AnimationTracks[n].AnimationDataList, totalTrackTime, m_GeneralSettings.m_Reverse));
                            break;
                        }
                        case ANIMATIONTYPE.TRANSPARENCY:
                        {
                            runner.Animations.Add(new TransparencyAnimation(m_AnimationsList[i].Object, m_AnimationsList[i].AnimationTracks[n].AnimationDataList, totalTrackTime, m_GeneralSettings.m_Reverse));
                            break;
                        }
                        default: throw new Exception("AnimatorComponent Initialize: Invalid AnimationType: " + m_AnimationsList[i].AnimationTracks[n].AnimationType);

                        // SIZE: Width & Height kan ändras.
                        // SOLID_COLOR: Ändra från RGB till RGB
                        // GRADIENT:    Samma som solid, fast 2st + Angle.
                        // TEXTCOLOR:        Kan använda SOLID_COLOR & GRADIENT
                        // TEXTSTROKECOLOR:  Samma.
                        // TEXTSTROKESIZE:   From - To ... kanske oerhört laggigt?
                        // TEXTSIZE:         From - To ... samma här, eftersom texten behöver byggas om varje frame ...
                        // TEXTWAVE:         Varje bokstav går upp sen ner, som en våg i -> riktning. Vet inte exakt hur man kan göra detta. Måste göras per tecken. Glyph?

                        // Kanske kan vi göra så här:
                        // Objekt
                        //      Text
                        //          FOREGROUNDCOLOR
                        //          ...
                        //
                        // Just nu ligger allt under objektet, men Text borde gå animera på flera, vissa specifika sätt.
                        // Gäller även Image. 

                        // Måste implementera taggar i text. <b></b> osv.

                        // Inkludera 3D modeller/Blender? Animerade?

                    }
                }

                if(runner.Animations.Count > 0)
                {
                    m_RunningAnimations.Add(runner);
                }
                
            }

        }

        private void LoopLogic()
        {
            int finishedCount = RunAnimations();

            // Done?
            if (finishedCount == m_RunningAnimations.Count)
            {
                // Reverse?
                if (m_GeneralSettings.m_Reverse)
                {
                    m_GoingForward = !m_GoingForward;
                }

                // Re-start
                for (int i = 0; i < m_RunningAnimations.Count; i++)
                {
                    m_RunningAnimations[i].Finished = false;
                    for (int n = 0; n < m_RunningAnimations[i].Animations.Count; n++)
                    {
                        if (m_GeneralSettings.m_Reset)
                        {
                            m_RunningAnimations[i].Animations[n].Reset();
                        }
                        m_RunningAnimations[i].Animations[n].Initialize(m_GoingForward);
                    }
                }
            }
        }
        
        private int RunAnimations()
        {
            // First count "inner" which is each track. Then "outher" which is each object.
            int finishedCount = 0;
            for (int i = 0; i < m_RunningAnimations.Count; i++)
            {
                if (m_RunningAnimations[i].Finished)
                {
                    finishedCount++;
                    continue;
                }

                int innerFinishedCount = 0;
                for (int n = 0; n < m_RunningAnimations[i].Animations.Count; n++)
                {
                    if (m_RunningAnimations[i].Animations[n].Logic())
                        innerFinishedCount++;
                }

                if (innerFinishedCount == m_RunningAnimations[i].Animations.Count)
                {
                    m_RunningAnimations[i].Finished = true;
                }
            }
            return finishedCount;
        }

        public bool Logic()
        {
            if (m_Stopped)
            {
                return true;
            }

            if (m_Loop)
            {
                LoopLogic();
                return false;
            }

            // If not running, we are starting.
            if(!m_Running)
            {
                if (m_RunningAnimations.Count == 0)
                    return true;

                if (m_GeneralSettings.m_LoopForever)
                {
                    // Start
                    for (int i = 0; i < m_RunningAnimations.Count; i++)
                    {
                        m_RunningAnimations[i].Finished = false;
                        for (int n = 0; n < m_RunningAnimations[i].Animations.Count; n++)
                        {
                            m_RunningAnimations[i].Animations[n].Initialize(m_GoingForward);
                        }
                    }

                    m_Loop = true;
                    m_Running = true;
                    return false;    
                }

                m_RunTimes = m_GeneralSettings.m_RunTimes;

                if (m_GeneralSettings.m_Reverse)
                {
                    m_RunTimes *= 2;
                }

                // Start
                for (int i = 0; i < m_RunningAnimations.Count; i++)
                {
                    m_RunningAnimations[i].Finished = false;
                    for(int n = 0; n < m_RunningAnimations[i].Animations.Count; n++)
                    {
                        m_RunningAnimations[i].Animations[n].Initialize(m_GoingForward);
                    }
                }
                m_Running = true;
                return false;
            }

            int finishedCount = RunAnimations();

            // Done?
            if (finishedCount == m_RunningAnimations.Count)
            {
                m_RunTimes--;
                if(m_RunTimes <= 0)
                {
                    // Reset before ending
                    if (m_GeneralSettings.m_Reset) 
                    {
                        for (int i = 0; i < m_RunningAnimations.Count; i++)
                        {
                            for (int n = 0; n < m_RunningAnimations[i].Animations.Count; n++)
                            {
                                m_RunningAnimations[i].Animations[n].Reset();
                            }
                        }
                    }
                    else if (m_GeneralSettings.m_Reverse)
                    {
                        m_GoingForward = !m_GoingForward;
                    }

                    m_Running = false;
                    m_Stopped = true;
                    return true;    // Finished
                }
                else
                {
                    // Reverse
                    if (m_GeneralSettings.m_Reverse)
                    {
                        m_GoingForward = !m_GoingForward;
                    }

                    for (int i = 0; i < m_RunningAnimations.Count; i++)
                    {
                        m_RunningAnimations[i].Finished = false;
                        for (int n = 0; n < m_RunningAnimations[i].Animations.Count; n++)
                        {
                            if (m_GeneralSettings.m_Reset)
                            {
                                m_RunningAnimations[i].Animations[n].Reset();
                            }
                            m_RunningAnimations[i].Animations[n].Initialize(m_GoingForward);
                        }
                    }
                }
            }

            return false;
        }

        public void RemoveComponent()
        {
            GlobalHelper.EditorWindow.CloseTab(m_Object.ID);
            GlobalHelper.EditorWindow.RemoveAnimator(this);
            m_Object.ComponentRemoved(this);
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }

        // Mark for rebuild, tab must be closed to avoid any wierd behaviour or crashes
        public void MarkForRebuild()
        {
            GlobalHelper.EditorWindow.CloseTab(ID);
            Rebuild = true;
        }



    }


}
