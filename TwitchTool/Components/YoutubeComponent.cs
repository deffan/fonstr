﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;
using TwitchTool.Commands;
using TwitchTool.ComponentControls;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Components
{
    public class YoutubeComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public YoutubeEditorComponent m_EditorComponent;
        private string m_ID;
        private YoutubeControl m_YoutubeControl;
        private string m_Url;
        private bool m_Running;
        private bool m_Started;
        private int m_DurationTime;
        private int m_StartTime;
        private DateTime m_PlayerTimeout;
        private bool m_PlayerStarted;

        public YoutubeComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_YoutubeControl = new YoutubeControl(m_Object, this);
            m_Object.Children.Add(m_YoutubeControl);
            m_DurationTime = 0;
            m_StartTime = 0;
            m_Url = "";
            m_Running = false;
            m_Started = false;

            if (editor)
            {
                m_EditorComponent = new YoutubeEditorComponent(this);
            }

            // Register callbacks for when size changes 
            m_Object.GetTransformComponent().RegisterOnWidthChanged(WidthChanged);
            m_Object.GetTransformComponent().RegisterOnHeightChanged(HeightChanged);

            // Initially set the size to fit the object
            Size s = m_Object.GetTransformComponent().GetSize();
            WidthChanged((int)s.Width);
            HeightChanged((int)s.Height);
        }

        public void NewComponentConstructor()
        {

        }

        public void SetUrl(string url)
        {
            m_Url = url;

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetUrl(m_Url);
            }
        }

        private void WidthChanged(int width)
        {
            m_YoutubeControl.WebGrid.Width = width;
            m_YoutubeControl.WebGrid.MinWidth = width;
        }

        private void HeightChanged(int height)
        {
            m_YoutubeControl.WebGrid.Height = height;
            m_YoutubeControl.WebGrid.MinHeight = height;
        }

        public void SetDurationTime(int time)
        {
            if (time < 0)
                return;

            m_DurationTime = time;
            if(m_EditorComponent != null)
            {
                m_EditorComponent.SetDurationTime(time);
            }
        }

        public void SetStartTime(int time)
        {
            if (time < 0)
                return;

            m_StartTime = time;
            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetStartTime(time);
            }
        }

        public void PlayFromEditor()
        {
            string videoId = "";
            if (m_Url.Length == 11)
            {
                videoId = m_Url;
            }
            else
            {
                videoId = GlobalHelper.GetYouTubeVideoIdFromUrl(m_Url);
            }

            int start = GlobalHelper.GetYoutubeVideoStartTime(m_Url);
          
            if (start > 0 && start != m_StartTime)
            {
                MessageBoxResult r = MessageBox.Show("The URL contains a starting time ({0}) use this as start time?", "Youtube Component", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if(r == MessageBoxResult.Yes)
                {
                    SetStartTime(m_StartTime);
                }
            }

            m_YoutubeControl.Initialize(videoId, m_StartTime, m_DurationTime);
            m_YoutubeControl.Play();
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.YOUTUBE;
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public string GetID()
        {
            return m_ID;
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public void Initialize(Command c)
        {
            m_Started = false;
            m_Running = false;
            m_PlayerStarted = false;
            string id = "";
            int start = 0;
            int duration = 0;
            YoutubeCommandData data = (c as YoutubeCommand).Data as YoutubeCommandData;

            if (data.m_UseComponentSettings)
            {
                id = m_Url;
                start = m_StartTime;
                duration = m_DurationTime;
            }
            else
            {
                id = GlobalHelper.GetYouTubeVideoIdFromUrl(data.m_Data.GetStringData(c.Event));
                if(id.Length == 0 && data.m_UseBackup)
                {
                    id = GlobalHelper.GetYouTubeVideoIdFromUrl(data.m_BackupData.GetStringData(c.Event));
                }
                else
                {
                    // Start time, 0 is default
                    if(data.m_RunSpecific)
                    {
                        start = (int)data.m_StartData.GetNumericData(c.Event);
                    }
                    else if(data.m_RunFromStartLink)
                    {
                        start = GlobalHelper.GetYoutubeVideoStartTime(data.m_BackupData.GetStringData(c.Event));
                    }

                    // Duration, 0 is default and means full-video duration
                    if (data.m_DurationSpecific)
                    {
                        duration = (int)data.m_DurationData.GetNumericData(c.Event);
                    }
                }
            }

            m_YoutubeControl.Initialize(id, start, duration);
        }

        public bool IsRunning()
        {
            return m_Running;
        }

        public bool Logic()
        {
            if(m_Started)
            {
                if(!m_PlayerStarted)
                {
                    if(m_PlayerTimeout < DateTime.Now)
                    {
                        Logger.Log(LOG_LEVEL.WARNING, "Youtube Component timed out.");
                        Stop();
                    }
                }
            }
            else
            {
                m_Started = true;
                m_Running = true;
                m_PlayerTimeout = DateTime.Now.AddSeconds(5);
                Dispatcher.Invoke(m_YoutubeControl.Play);
            }

            return !m_Running;
        }

        public void RemoveComponent()
        {
            m_Object.Children.Remove(m_YoutubeControl);
            m_Object.ComponentRemoved(this);
        }

        XElement IXML.Serialize()
        {
            XElement componentNode = new XElement("YoutubeComponent");

            XElement idNode = new XElement("Id", m_ID);
            componentNode.Add(idNode);

            XElement start = new XElement("Start", m_StartTime);
            componentNode.Add(start);

            XElement duration = new XElement("Duration", m_DurationTime);
            componentNode.Add(duration);

            XElement url = new XElement("Url", GlobalHelper.Base64Encode(m_EditorComponent.UrlTextBox.Text));
            componentNode.Add(url);

            return componentNode;
        }

        public static YoutubeComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            YoutubeComponent i = new YoutubeComponent(parent, editor);
            i.SetID(node.Element("Id").Value);
            i.SetStartTime(Convert.ToInt32(node.Element("Start").Value));
            i.SetDurationTime(Convert.ToInt32(node.Element("Duration").Value));
            i.SetUrl(GlobalHelper.Base64Decode(node.Element("Url").Value));

            return i;
        }

        public void SetID(string id)
        {
            m_ID = id;
        }

        public void Stop()
        {
            m_Running = false;
            m_Started = false;
            m_PlayerStarted = false;
            Dispatcher.Invoke(m_YoutubeControl.Stop);
        }

        public void StopFromCtrl()
        {
            m_Running = false;
        }

        public void YoutubeError(string err)
        {
            if (m_EditorComponent != null)
            {
                MessageBox.Show(err, "Youtube Component", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void CtrlStarted()
        {
            m_PlayerStarted = true;
        }
    }
}
