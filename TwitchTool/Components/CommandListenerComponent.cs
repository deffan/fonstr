﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Components
{
    public class CommandListenerComponent : UserControl, IComponent
    {
        public class ListeningComponent
        {
            public bool Listening;
            public IComponent Component;

            public ListeningComponent(IComponent c, bool l)
            {
                Component = c;
                Listening = l;
            }
        }

        public void NewComponentConstructor()
        {
            m_ListenerID = "Listener_" + GlobalHelper.EditorWindow.GetListeners().Count;
            m_EditorComponent.IdTextBox.Text = ListenerID;
        }

        public ObjectControl m_Object;
        public CommandListenerEditorComponent m_EditorComponent;
        private object m_Lock;
        private List<CommandExecutioner> m_CommandExecutioner;
        private List<ListeningComponent> m_ComponentsThatAreListening;
        private string m_ID;
        private string m_ListenerID;
        private bool m_Stop;

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public CommandListenerComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_Stop = false;
            m_Lock = new object();
            m_CommandExecutioner = new List<CommandExecutioner>();
            m_ComponentsThatAreListening = new List<ListeningComponent>();

            // Get all components from the object, by default none are listening
            List<IComponent> components = m_Object.GetComponents();
            for (int i = 0; i < components.Count; i++)
            {
                if (components[i] is CommandListenerComponent)
                    continue;

                m_ComponentsThatAreListening.Add(new ListeningComponent(components[i], false));
            }

            if (editor)
            {
                m_EditorComponent = new CommandListenerEditorComponent(this);
            }
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public string ListenerID
        {
            get { return m_ListenerID; }
            set { m_ListenerID = value; }
        }

        public void RefreshEditorComponent()
        {
            if(m_EditorComponent != null)
            {
                m_EditorComponent.Refresh();
            }
        }

        public void RemoveComponentFromListener(IComponent component)
        {
            for(int i = 0; i < m_ComponentsThatAreListening.Count; i++)
            {
                if(m_ComponentsThatAreListening[i].Component.Equals(component))
                {
                    m_ComponentsThatAreListening.RemoveAt(i);
                    break;
                }
            }

            if (m_EditorComponent != null)
            {
                for (int i = 0; i < m_EditorComponent.ComponentsListBox.Items.Count; i++)
                {
                    if ((m_EditorComponent.ComponentsListBox.Items[i] as CheckBoxListItem).m_Component.Component.Equals(component))
                    {
                        m_EditorComponent.ComponentsListBox.Items.RemoveAt(i);
                        break;
                    }
                }
            }
        }

        public void AddComponentToListener(IComponent component)
        {
            ListeningComponent c = new ListeningComponent(component, false);
            m_ComponentsThatAreListening.Add(c);

            if (m_EditorComponent != null)
            {
                m_EditorComponent.ComponentsListBox.Items.Add(new CheckBoxListItem(c));
            }
        }

        public bool HasCommand(Command c)
        {
            lock (m_Lock)
            {
                for (int i = 0; i < m_CommandExecutioner.Count; i++)
                {
                    if (m_CommandExecutioner[i].command.Equals(c))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public List<ListeningComponent> GetComponentsThatAreListening()
        {
            return m_ComponentsThatAreListening;
        }

        public List<IComponent> GetComponentsThatCanRunThisCommand(Command c)
        {
            List<IComponent> compList = new List<IComponent>();
            for (int i = 0; i < m_ComponentsThatAreListening.Count; i++)
            {
                if (m_ComponentsThatAreListening[i].Listening && m_ComponentsThatAreListening[i].Component.CanRun(c))
                {
                    compList.Add(m_ComponentsThatAreListening[i].Component);
                }
            }
            return compList;
        }

        public void StartCommand(Command c)
        {
            // Fetch the components on this object that can run this command
            List<IComponent> components = GetComponentsThatCanRunThisCommand(c);

            // Special for WaitCommand.
            if(c.Data.Type == COMMAND_TYPE.WAIT)
            {
                components.Clear();
                components.Add(new WaitComponent());
            }

            // Check if any components are already running. Then we remove them from the list and inform this in a message.
            for (int i = components.Count - 1; i >= 0; i--)
            {
                // Rules?
                // - Can start many commands "while running" - Only relevant to components that can deal with that, such as CloningComponent.
                // - Can only run one at a time (like now)
                // - If running, automatically stop/reset and start this one instead, for example, a sound plays, that will stop and restart. Or play something else.

                if (components[i].IsRunning())
                {
                    Logger.Log(LOG_LEVEL.WARNING, COMMAND_TYPE.GetCommandName(c.Data.Type) + " (" + c.Data.Name + ") " + "did not start (component already running).");
                    components.RemoveAt(i);
                }
            }

            // Check if we have any components to run on, otherwise we end here.
            if (components.Count == 0)
            {
                Logger.Log(LOG_LEVEL.WARNING, COMMAND_TYPE.GetCommandName(c.Data.Type) + " (" + c.Data.Name + ") " + "did not start (no component to run on).");
                c.Finished = true;
                return;
            }

            // Send/Initialize the command into the component(s)
            for (int i = 0; i < components.Count; i++)
            {
                Logger.Log(LOG_LEVEL.DEBUG, "STARTING COMMAND: " + c.Data.Name + " on " + components[i].GetID());
                int index = i;
                Application.Current.Dispatcher.Invoke(new Action(() => 
                {
                    components[index].Initialize(c);
                }));
            }

            // Create the command executioner and add it to the list of executioners
            CommandExecutioner commandExecutioner = new CommandExecutioner();
            commandExecutioner.command = c;
            commandExecutioner.components = components;

            lock (m_Lock)
            {
                if(m_Stop)
                {
                    Logger.Log(LOG_LEVEL.WARNING, COMMAND_TYPE.GetCommandName(c.Data.Type) + " (" + c.Data.Name + ") " + "did not start (listener component was stopped).");
                    c.Finished = true;
                }
                else
                {
                    m_CommandExecutioner.Add(commandExecutioner);
                }
            }
        }

        public void Abort()
        {
            lock (m_Lock)
            {
                for (int i = 0; i < m_CommandExecutioner.Count; i++)
                {
                    m_CommandExecutioner[i].command.Abort(true);
                    for (int n = 0; n < m_CommandExecutioner[i].components.Count; n++)
                    {
                        m_CommandExecutioner[i].components[n].Stop();
                        m_CommandExecutioner[i].command.Abort(true);
                        m_CommandExecutioner[i].command.Finished = true;
                    }
                }

                for (int i = 0; i < m_ComponentsThatAreListening.Count; i++)
                {
                    m_ComponentsThatAreListening[i].Component.Stop();
                }
            }
        }

        public void AbortCommand(Command c)
        {
            lock (m_Lock)
            {
                for (int i = 0; i < m_CommandExecutioner.Count; i++)
                {
                    if (m_CommandExecutioner[i].command.Equals(c))
                    {
                        m_CommandExecutioner[i].command.Abort(true); 
                        for (int n = 0; n < m_CommandExecutioner[i].components.Count; n++)
                        {
                            m_CommandExecutioner[i].components[n].Stop(); 
                        }
                        return;
                    }
                }
            }
        }

        public void ListenerLogic()
        {
            lock (m_Lock)
            {
                // Loop all commands
                for (int i = 0; i < m_CommandExecutioner.Count; i++)
                {
                    // Loop all components running this command
                    for (int n = 0; n < m_CommandExecutioner[i].components.Count; n++)
                    {
                        if (m_CommandExecutioner[i].command.Aborted)
                        {
                            m_CommandExecutioner[i].components[n].Stop();
                        }

                        // If the components logic has finished
                        if (m_CommandExecutioner[i].components[n].Logic())
                        {
                            // Remove the component from the executioner
                            m_CommandExecutioner[i].components.RemoveAt(n);

                            // If this was the last component running the command, we are finished.
                            if (m_CommandExecutioner[i].components.Count == 0)
                            {
                                // Remove command from executioner and report back to window that this command is finished.
                                Command c = m_CommandExecutioner[i].command;
                                m_CommandExecutioner.RemoveAt(i);
                                c.Event.Window.CommandFinished(c);
                                return;
                            }
                            break;
                        }
                    }
                }
            }
        }

        XElement IXML.Serialize()
        {
            XElement listenerComponentNode = new XElement("CommandListenerComponent");

            XElement idNode = new XElement("Id", ID);
            listenerComponentNode.Add(idNode);

            // Listener id
            XElement name = new XElement("ListenerId", GlobalHelper.Base64Encode(ListenerID));
            listenerComponentNode.Add(name);

            // Listening components
            XElement componentsNode = new XElement("ListeningComponents");
            listenerComponentNode.Add(componentsNode);
            for (int i = 0; i < m_ComponentsThatAreListening.Count; i++)
            {
                XElement comp = new XElement("Component");
                XElement compIdNode = new XElement("Id", m_ComponentsThatAreListening[i].Component.GetID());
                XElement compListening = new XElement("Listening", m_ComponentsThatAreListening[i].Listening);
                comp.Add(compIdNode);
                comp.Add(compListening);
                componentsNode.Add(comp);
            }

            return listenerComponentNode;
        }

        public static CommandListenerComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            CommandListenerComponent c = new CommandListenerComponent(parent, editor);
            c.ID = node.Element("Id").Value;
            c.ListenerID = GlobalHelper.Base64Decode(node.Element("ListenerId").Value);

            List<ListeningComponent> componentList = c.GetComponentsThatAreListening();
            XElement components = node.Element("ListeningComponents");
            foreach (XElement comp in components.Elements())
            {
                string id = comp.Element("Id").Value;
                for (int n = 0; n < componentList.Count; n++)
                {
                    if(componentList[n].Component.GetID().Equals(id))
                    {
                        componentList[n].Listening = Convert.ToBoolean(comp.Element("Listening").Value);
                        break;
                    }
                }
            }
            c.RefreshEditorComponent();

            if(editor)
            {
                GlobalHelper.EditorWindow.AddListener(c);
            }

            return c;
        }

        public bool CanRun(Command c)
        {
            return false;
        }

        public void Initialize(Command c)
        {

        }

        public bool Logic()
        {
            return true;
        }

        public void RemoveComponent()
        {
            GlobalHelper.EditorWindow.RemoveListener(this);
            m_Object.ComponentRemoved(this);
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }

        public bool IsRunning()
        {
            return false;
        }

        public void Stop()
        {
            m_Stop = true;
            Abort();
        }
    }
}
