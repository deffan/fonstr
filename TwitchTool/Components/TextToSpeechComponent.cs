﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Xml.Linq;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;
using System.Speech.Synthesis;
using TwitchTool.Commands;

namespace TwitchTool.Components
{
    public class TextToSpeechComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public TextToSpeechEditorComponent m_EditorComponent;
        private string m_ID;
        private SpeechSynthesizer m_Speech;
        private bool m_RandomVoice;
        private bool m_Running;

        public TextToSpeechComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_Speech = new SpeechSynthesizer();
            m_Speech.SetOutputToDefaultAudioDevice();
            m_RandomVoice = false;
            m_Running = false;

            if (editor)
            {
                m_EditorComponent = new TextToSpeechEditorComponent(this);
            }
            m_Speech.SpeakCompleted += M_Speech_SpeakCompleted;
        }

        public SpeechSynthesizer GetSpeechSynthesizer()
        {
            return m_Speech;
        }

        public void NewComponentConstructor()
        {

        }

        private void M_Speech_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            m_Running = false;
        }

        public void Speak(string text)
        {
            try
            {
                if (m_RandomVoice)
                {
                    Random r = new Random();
                    m_Speech.SelectVoice(GetInstalledVoices()[r.Next(0, GetInstalledVoices().Count)].VoiceInfo.Name);
                }
                m_Speech.SpeakAsync(text);
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "Speak" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                m_Running = false;
            }
        }

        public void SetVoice(string name)
        {
            try
            {
                if (name.Equals("Random"))
                {
                    m_RandomVoice = true;
                }
                else
                {
                    m_Speech.SelectVoice(name);
                    m_RandomVoice = false;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "SetVoice" + Environment.NewLine + ex.Message + "\n" + ex.StackTrace);
                m_Running = false;
            }
        }

        public List<InstalledVoice> GetInstalledVoices()
        {
            return m_Speech.GetInstalledVoices().ToList();
        }

        XElement IXML.Serialize()
        {
            XElement ttsNode = new XElement("TextToSpeechComponent");

            XElement idNode = new XElement("Id", m_ID);
            ttsNode.Add(idNode);

            if (m_RandomVoice)
            {
                XElement vNode = new XElement("Voice", "Random");
                ttsNode.Add(vNode);
            }
            else
            {
                XElement vNode = new XElement("Voice", m_Speech.Voice.Name);
                ttsNode.Add(vNode);
            }

            return ttsNode;
        }

        public static TextToSpeechComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            TextToSpeechComponent b = new TextToSpeechComponent(parent, editor);

            b.SetID(node.Element("Id").Value);
            b.SetVoice(node.Element("Voice").Value);

            if (editor)
            {
                b.m_EditorComponent.SelectVoice(node.Element("Voice").Value);
            }

            return b;
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.TEXT;
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public string GetID()
        {
            return m_ID;
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public void Initialize(Command c)
        {
            m_Running = true;

            string text = "";
            TextCommandData data = c.Data as TextCommandData;
            for (int i = 0; i < data.TextDataList.Count; i++)
            {
                text += data.TextDataList[i].GetStringData(c.Event);
            }

            Speak(text);
        }

        public bool IsRunning()
        {
            return m_Running;
        }

        public bool Logic()
        {
            return !m_Running;
        }

        public void RemoveComponent()
        {
            m_Object.ComponentRemoved(this);
        }

        public void SetID(string id)
        {
            m_ID = id;
        }

        public void Stop()
        {
            m_Speech.SpeakAsyncCancelAll();
            m_Running = false;
        }
    }
}
