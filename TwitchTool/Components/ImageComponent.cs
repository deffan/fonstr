﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;


namespace TwitchTool.Components
{
    public class ImageComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public ImageEditorComponent m_EditorComponent;
        private ImageControl m_ImageControl;
        private bool m_IsURL;
        private string m_Path;
        private string m_ID;

        public ImageComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_ImageControl = new ImageControl(m_Object, this);
            m_Object.Children.Add(m_ImageControl);

            if (editor)
            {
                m_EditorComponent = new ImageEditorComponent(this);
            }


            // Register callbacks for when size changes 
            m_Object.GetTransformComponent().RegisterOnWidthChanged(WidthChanged);
            m_Object.GetTransformComponent().RegisterOnHeightChanged(HeightChanged);

            // Initially set the size to fit the object
            Size s = m_Object.GetTransformComponent().GetSize();
            WidthChanged((int)s.Width);
            HeightChanged((int)s.Height);
        }

        public void NewComponentConstructor()
        {

        }

        private void WidthChanged(int width)
        {
            m_ImageControl.TheImage.Width = width;
            m_ImageControl.TheImage.MinWidth = width;
        }

        private void HeightChanged(int height)
        {
            m_ImageControl.TheImage.Height = height;
            m_ImageControl.TheImage.MinHeight = height;
        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public string GetImagePath()
        {
            return m_Path;
        }

        public void SetImage(string path, bool? isUrl)
        {
            try
            {
                m_Path = path;

                if(isUrl.HasValue)
                {
                    m_IsURL = isUrl.Value;
                    if (m_IsURL)
                    {
                        SetImageAsUrl(path);
                    }
                    else
                    {
                        SetImageAsPath(path);
                    }
                }
                else
                {
                    if(!SetImageAsPath(path))
                    {
                        m_IsURL = SetImageAsUrl(path);
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, "SetImage" + Environment.NewLine + ex.Message);
            }
        }

        private bool SetImageAsPath(string path)
        {
            try
            {
                m_ImageControl.SetImage(new BitmapImage(new Uri(path)));
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, "SetImageAsPath - " + Environment.NewLine + ex.Message);
            }
            return false;
        }

        private bool SetImageAsUrl(string path)
        {
            try
            {
                m_ImageControl.SetImage(new BitmapImage(new Uri(path, UriKind.Absolute)));
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.DEBUG, "SetImageAsUrl - " + Environment.NewLine + ex.Message);
            }
            return false;
        }

        public void ImageUpdated(int originalWidth, int originalHeight)
        {
            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetImageInformation(m_Path, m_IsURL, originalWidth, originalHeight);
            }
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.IMAGE;
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        XElement IXML.Serialize()
        {
            XElement imageComponentNode = new XElement("ImageComponent");

            XElement idNode = new XElement("Id", ID);
            imageComponentNode.Add(idNode);

            // URL or Filepath
            XElement isUrl = new XElement("IsUrl", m_IsURL);
            imageComponentNode.Add(isUrl);

            // Path 
            XElement path = new XElement("Path", GlobalHelper.Base64Encode(m_Path));
            imageComponentNode.Add(path);


            return imageComponentNode;
        }

        public static ImageComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            ImageComponent i = new ImageComponent(parent, editor);
            i.ID = node.Element("Id").Value;
            i.m_IsURL = Convert.ToBoolean(node.Element("IsUrl").Value);
            i.SetImage(GlobalHelper.Base64Decode(node.Element("Path").Value), i.m_IsURL);
            return i;
        }

        public void Initialize(Command c)
        {

        }

        public bool Logic()
        {
            return true;
        }

        public void RemoveComponent()
        {
            m_Object.Children.Remove(m_ImageControl);
            m_Object.ComponentRemoved(this);
        }

        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }

        public bool IsRunning()
        {
            return false;
        }

        public void Stop()
        {

        }
    }
}
