﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;
using TwitchTool.Commands;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Components
{
    public class CloningComponent : UserControl, IComponent, IEditorTab
    {
        public class CloningSettings
        {
            public TextData LifeTimeData = new TextData("");
            public int LifeTime;
            public int Redirect;
            public bool RemoveCloneComponent;
        }

        public class CloneHolder
        {
            public CloningSettings Settings;
            public ObjectControl TheObject;
            public Event TheEvent;
            public DateTime StartedAt;
            public DateTime EndTime;

            public CloneHolder(ObjectControl o, Event e, CloningSettings s)
            {
                TheObject = o;
                TheEvent = e;
                Settings = s;
                StartedAt = DateTime.Now;

                if (s.LifeTime == LIFETIME_DATA)
                {
                    double seconds = s.LifeTimeData.GetNumericDataOnce().GetValueOrDefault();
                    if (seconds <= 0)
                    {
                        EndTime = StartedAt;
                    }
                    else
                    {
                        EndTime = StartedAt.AddSeconds(seconds);
                    }
                }
            }
        }

        // Settings 'enums'
        public const int LIFETIME_FOREVER = 0;
        public const int LIFETIME_EVENT = 1;
        public const int LIFETIME_DATA = 2;
        public const int REDIRECT_TO_CLONE = 0;
        public const int REDIRECT_TO_ORIGINAL = 1;
        public const int REDIRECT_TO_BOTH = 2;

        // Datamembers
        public ObjectControl m_Object;
        private bool m_Running;
        public CloningEditorComponent m_EditorComponent;
        private string m_ID;
        private List<CloneHolder> m_ClonedList;
        private object m_Lock;
        private Thread m_Thread;
        private CloningSettings m_Settings;

        public CloningComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;
            m_Running = false;
            m_ClonedList = new List<CloneHolder>();
            m_Lock = new object();

            if (editor)
            {
                m_EditorComponent = new CloningEditorComponent(this);
            }
        }

        public void NewComponentConstructor()
        {

        }

        public CloningSettings GetSettings()
        {
            return m_Settings;
        }

        public ObjectControl Clone()
        {
            ObjectControl c = ObjectControl.Deserialize(m_Object.Serialize(), m_Object.ObjectParent, false);
            c.CloneID = Guid.NewGuid().ToString();
            return c;
        }

        public ObjectControl Clone(ObjectControl o, bool editor)
        {
            ObjectControl c = ObjectControl.Deserialize(o.Serialize(), o.ObjectParent, editor);
            if(!editor)
            {
                c.CloneID = Guid.NewGuid().ToString();
            }
            return c;
        }

        // Generates new random ID's for each object and its components. But you can still identify the old. 
        public void GenerateNewIds(ObjectControl o, bool editor)
        {
            // If editor, we are copying an object
            if(editor)
            {
                o.ID = Guid.NewGuid().ToString();
            }
            else
            {
                o.ID = o.ID + "#CLONED_" + Guid.NewGuid().ToString();
            }
            
            List<IComponent> components = o.GetComponents();
            for (int i = 0; i < components.Count; i++)
            {
                if (editor)
                {
                    components[i].SetID(Guid.NewGuid().ToString());
                }
                else
                {
                    components[i].SetID(components[i].GetID() + "#CLONED_" + Guid.NewGuid().ToString());
                }
            }

            for (int i = 0; i < o.Children.Count; i++)
            {
                if (o.Children[i] is ObjectControl)
                {
                    GenerateNewIds(o.Children[i] as ObjectControl, editor);
                }
            }
        }

        // Generates new listenerids ID's for each listener. But you can still identify the old. Format is: OLD#NEW
        public void GenerateNewListeners(ObjectControl o, Dictionary<string, string> dictionary)
        {
            List<IComponent> components = o.GetComponents();
            for (int i = components.Count - 1; i >= 0; i--)
            {
                CommandListenerComponent c = components[i] as CommandListenerComponent;
                if (c != null)
                {
                    // Because listenerIDs (names) can be the same in many places, the new Ids must also be the same
                    if (!dictionary.ContainsKey(c.ListenerID))
                    {
                        dictionary.Add(c.ListenerID, c.ListenerID + "#CLONED_" + Guid.NewGuid().ToString());
                    }
                    c.ListenerID = dictionary[c.ListenerID];
                }
            }

            for (int i = 0; i < o.Children.Count; i++)
            {
                if (o.Children[i] is ObjectControl)
                {
                    GenerateNewListeners(o.Children[i] as ObjectControl, dictionary);
                }
            }
        }

        public void RemoveListeners(ObjectControl o)
        {
            List<IComponent> components = o.GetComponents();
            for(int i = components.Count - 1; i >= 0; i--)
            {
                if(components[i] is CommandListenerComponent)
                {
                    components.RemoveAt(i);
                }
            }

            for (int i = 0; i < o.Children.Count; i++)
            {
                if (o.Children[i] is ObjectControl)
                {
                    RemoveListeners(o.Children[i] as ObjectControl);
                }
            }
        }

        public void RemoveCloningComponents(ObjectControl o)
        {
            // Remove and temporarily store cloning components
            List<CloningComponent> temp = new List<CloningComponent>();
            List<IComponent> components = o.GetComponents();
            for (int i = components.Count - 1; i >= 0; i--)
            {
                if (components[i] is CloningComponent)
                {
                    temp.Add(components[i] as CloningComponent);
                    components.RemoveAt(i);
                }
            }

            // Remove each from listeners
            List<CommandListenerComponent> listeners = o.GetAllListeners();
            for (int i = 0; i < temp.Count; i++)
            {
                for (int n = 0; n < listeners.Count; n++)
                {
                    listeners[n].RemoveComponentFromListener(temp[i]);
                }
            }

            for (int i = 0; i < o.Children.Count; i++)
            {
                if (o.Children[i] is ObjectControl)
                {
                    RemoveCloningComponents(o.Children[i] as ObjectControl);
                }
            }
        }

        public void RedirectListeners(Event e, Dictionary<string, string> dictionary)
        {
            for(int i = 0; i < e.Commands.Count; i++)
            {
                for(int n = 0; n < e.Commands[i].Data.Listeners.Count; n++)
                {
                    if(dictionary.ContainsKey(e.Commands[i].Data.Listeners[n]))
                    {
                        e.Commands[i].Data.Listeners[n] = dictionary[e.Commands[i].Data.Listeners[n]];
                    }
                }
            }
        }

        public bool CanRun(Command c)
        {
            return c.Data.Type == COMMAND_TYPE.CLONE;
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public string GetID()
        {
            return m_ID;
        }

        public void SetID(string id)
        {
            m_ID = id;
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public void Initialize(Command c)
        {
            CloningCommand cloneCommand = c as CloningCommand;
            if (cloneCommand == null)
                throw new Exception("CloningCommand null in CloningComponent Initialize");

            // Clone the object
            ObjectControl clone = Clone();

            // Remove all cloning components
            if(m_Settings.RemoveCloneComponent)
            {
                RemoveCloningComponents(clone);
            }

            if(m_Settings.Redirect == REDIRECT_TO_CLONE)
            {
                // Generate new listener ids for the clone
                Dictionary<string, string> listenerIdDictionary = new Dictionary<string, string>();
                GenerateNewListeners(clone, listenerIdDictionary);

                // Redirect all commands in the event to the new listener ids, so everything in the event will reach the clone only.
                RedirectListeners(c.Event, listenerIdDictionary);
            }
            else if (m_Settings.Redirect == REDIRECT_TO_ORIGINAL)
            {
                // Remove all listeners in the cloned object, so no commands will ever reach it.
                RemoveListeners(clone);
            }
            else if (m_Settings.Redirect == REDIRECT_TO_BOTH)
            {
                // Do nothing.
            }

            // Assign to holder object, that contains the rules/settings for this cloning. Then add to the list.
            lock (m_Lock)
            {
                // Unless ofc, its suppose to live forever. Then we dont care.
                if (m_Settings.LifeTime != LIFETIME_FOREVER)
                {
                    m_ClonedList.Add(new CloneHolder(clone, c.Event, m_Settings));
                }

                // Add clone to parent object children (at the same level as this object)
                m_Object.ObjectParent.Children.Add(clone);
            }

            // Refresh the listeners once the object has been added to the object structure
            c.Event.Window.UpdateListeners(false);

            // Start thread if not running.
            if (!m_Running)
            {
                m_Running = true;
                m_Thread = new Thread(new ThreadStart(LogicLoop));
                m_Thread.Start();
            }
        }

        public bool IsRunning()
        {
            // Always set to not running, because we must be able to add new commands to it while running.
            return false;
        }

        private void LogicLoop()    
        {
            // Should probably, to avoid having too many threads, have this running from a shared "work thread" somwhere?
            // One thread per cloned object can get pretty bad fast.
            // 
            // Hmm, create a better ListenerLogic loop.
            // One where you must send true/false to dictate wether or not you are finished / updating.
            // One where you just always run. Like this one.
            //

            while (m_Running)
            {
                Thread.Sleep(100);

                lock(m_Lock)
                {
                    for (int i = m_ClonedList.Count - 1; i >= 0; i--)
                    {
                        // Check the lifetime rule
                        bool finished = false;
                        switch(m_ClonedList[i].Settings.LifeTime)
                        {
                            case LIFETIME_EVENT:
                            {
                                finished = m_ClonedList[i].TheEvent.Finished;
                                break;
                            }
                            case LIFETIME_DATA:
                            {
                                finished = DateTime.Now > m_ClonedList[i].EndTime;
                                break;
                            }
                            default: throw new Exception("Unknown lifetime setting in cloned object: " + m_ClonedList[i].Settings.LifeTime);
                        }

                        if(finished)
                        {
                            Logger.Log(LOG_LEVEL.DEBUG, "Removing clone.");
                            ObjectControl o = m_ClonedList[i].TheObject;
                            AppWindow w = m_ClonedList[i].TheEvent.Window;

                            Dispatcher.BeginInvoke(new Action(() => 
                            {
                                for (int n = 0; n < m_Object.ObjectParent.Children.Count; n++)
                                {
                                    ObjectControl child = m_Object.ObjectParent.Children[n] as ObjectControl;
                                    if(child != null && child.CloneID.Equals(o.CloneID))
                                    {
                                        child.PrepareForDestruction();
                                        m_Object.ObjectParent.Children.RemoveAt(n);
                                        w.UpdateListeners(true);
                                        break;
                                    }
                                }
                            }));
                            m_ClonedList.RemoveAt(i);
                        }
                    }
                }
            }
            m_ClonedList.Clear();
        }

        public bool Logic()
        {
            // This always returns true and finished instantly
            return true;
        }

        public void RemoveComponent()
        {
            m_Object.ComponentRemoved(this);
        }

        public XElement Serialize()
        {
            XElement node = new XElement("CloningComponent");
            XElement idNode = new XElement("Id", m_ID);
            if (m_EditorComponent != null)
            {
                m_Settings = m_EditorComponent.GetSettings();
            }
            XElement settingsNode = new XElement("Settings", JsonConvert.SerializeObject(m_Settings));
            node.Add(idNode);
            node.Add(settingsNode);

            return node;
        }

        public static CloningComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            CloningComponent c = new CloningComponent(parent, editor);
            c.m_ID = node.Element("Id").Value;
            c.m_Settings = JsonConvert.DeserializeObject<CloningSettings>(node.Element("Settings").Value);

            if(editor)
            {
                c.m_EditorComponent.SetSettings(c.m_Settings);
            }

            return c;
        }

        public void Stop()
        {
            m_Running = false;
        }
    }
}
