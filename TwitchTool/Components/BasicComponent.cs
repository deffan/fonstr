﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using TwitchTool.ComponentsEditor;
using TwitchTool.Source;
using TwitchTool.UserControls;

namespace TwitchTool.Components
{
    public class BasicComponent : UserControl, IComponent
    {
        public ObjectControl m_Object;
        public BasicEditorComponent m_EditorComponent;
        private string m_ID;
        private bool m_Active;
        private BrushPickerControl m_BrushPicker;

        public BasicComponent(ObjectControl parent, bool editor)
        {
            m_Object = parent;

            if(editor)
            {
                m_EditorComponent = new BasicEditorComponent(this);
                SetBrushPicker(new BrushPickerControl(), editor);
                SetBackgroundBrush(m_BrushPicker.GetBrush());
                m_EditorComponent.SetNewBrushPicker(m_BrushPicker);
            }
        }

        public void NewComponentConstructor()
        {

        }

        public string ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public bool Active
        {
            get { return m_Active; }
        }

        public ObjectControl GetParentObject()
        {
            return m_Object;
        }

        public void SetObjectName(string name)
        {
            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetText(name);
            }

            if (name == m_Object.ObjectName)
            {
                return;
            }
            else
            {
                m_Object.ObjectName = name;
            }
        }

        public void SetTransparency(decimal transparent)
        {
            try
            {
                m_Object.Opacity = Convert.ToDouble(transparent, CultureInfo.InvariantCulture);
            }
            catch (Exception) { }

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetTransparency(transparent);
            }
        }

        public BrushPickerControl GetBrushPicker()
        {
            return m_BrushPicker;
        }

        public void SetBrushPicker(BrushPickerControl b, bool editor)
        {
            m_BrushPicker = b;

            if (editor)
            {
                m_EditorComponent.SetNewBrushPicker(m_BrushPicker);
            }
        }

        public void SetBackgroundBrush(Brush brush)
        {
            m_Object.ChildCanvas.Background = brush;
        }

        public void SetActive(bool active)
        {
            m_Active = active;

            if (m_EditorComponent != null)
            {
                m_EditorComponent.SetActive(active);
            }
        }

        public string GetObjectName()
        {
            return m_Object.ObjectName;
        }

        public Brush GetBackgroundColor()
        {
            return m_Object.ChildCanvas.Background;
        }

        XElement IXML.Serialize()
        {
            XElement basicComponentNode = new XElement("BasicComponent");

            XElement idNode = new XElement("Id", ID);
            basicComponentNode.Add(idNode);

            // Object name
            XElement name = new XElement("Name", GlobalHelper.Base64Encode(m_Object.ObjectName));
            basicComponentNode.Add(name);

            // Background
            XElement background = new XElement("Background");
            background.Add(m_BrushPicker.Serialize());
            basicComponentNode.Add(background);

            // Transparency
            XElement transparency = new XElement("Transparency", m_Object.Opacity);
            basicComponentNode.Add(transparency);

            // Active
            XElement active = new XElement("Active", m_Active);
            basicComponentNode.Add(active);

            return basicComponentNode;
        }

        public static BasicComponent Deserialize(XElement node, ObjectControl parent, bool editor)
        {
            BasicComponent b = new BasicComponent(parent, editor);

            // Id
            b.ID = node.Element("Id").Value;

            // Name
            b.SetObjectName(GlobalHelper.Base64Decode(node.Element("Name").Value));

            // Background
            BrushPickerControl brushPicker = BrushPickerControl.Deserialize(node.Element("Background").Element("BrushPicker"));
            b.SetBrushPicker(brushPicker, editor);
            b.SetBackgroundBrush(brushPicker.GetBrush());

            // Transparency
            b.SetTransparency(Convert.ToDecimal(node.Element("Transparency").Value)); 

            b.SetActive(Convert.ToBoolean(node.Element("Active").Value));

            return b;
        }

        public bool IsRunning()
        {
            return false;
        }

        public void Stop()
        {

        }

        public bool CanRun(Command c)
        {
            return false;
        }

        public void Initialize(Command c)
        {

        }

        public bool Logic()
        {
            return true;
        }

        public void RemoveComponent()
        {
            m_Object.ComponentRemoved(this);
        }

        public IEditorComponent GetEditorComponent()
        {
            return m_EditorComponent;
        }

        public void SetID(string id)
        {
            ID = id;
        }

        public string GetID()
        {
            return ID;
        }
    }
}
