﻿using System;
using System.IO;
using System.Net.Sockets;
using TwitchTool.Source;

namespace TwitchTool.Twitch
{
    public class IrcClient
    {
        public string m_Name;
        private string m_Channel;
        private TcpClient _tcpClient;
        private StreamReader _inputStream;
        private StreamWriter _outputStream;

        public IrcClient()
        {

        }

        public bool Connect(string ip, int port, string userName, string password, string channel)
        {
            try
            {
                m_Name = userName.ToLower();
                m_Channel = channel;
                _tcpClient = new TcpClient(ip, port);
                _inputStream = new StreamReader(_tcpClient.GetStream());
                _outputStream = new StreamWriter(_tcpClient.GetStream());
                _outputStream.WriteLine("PASS " + password);
                _outputStream.WriteLine("NICK " + userName);
                _outputStream.WriteLine("USER " + userName + " 8 * :" + userName);
                _outputStream.WriteLine("JOIN #" + channel);
                _outputStream.Flush();
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "IRC-Constructor: " + ex.Message);
                return false;
            }
            return true;
        }

        public void Register(string capability)
        {
            try
            {
                _outputStream.WriteLine("CAP REQ :twitch.tv/" + capability);
                _outputStream.Flush();
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "IRC-Register: " + ex.Message);
            }
        }

        public void SendIrcMessage(string message)
        {
            try
            {
                _outputStream.WriteLine(message);
                _outputStream.Flush();
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "IRC-SendIrcMessage: " + ex.Message);
            }
        }

        public void SendPublicChatMessage(string message)
        {
            try
            {
                SendIrcMessage(":" + m_Name + "!" + m_Name + "@" + m_Name + ".tmi.twitch.tv PRIVMSG #" + m_Channel + " :" + message);
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "IRC-SendPublicChatMessage: " + ex.Message);
            }
        }

        public bool HasMessage()
        {
            return _tcpClient.GetStream().DataAvailable;
        }

        public string ReadMessage()
        {
            try
            {
                return _inputStream.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "IRC-ReadMessage: " + ex.Message);
            }
            return "";
        }
    }
}
