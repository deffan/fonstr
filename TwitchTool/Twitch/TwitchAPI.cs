﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using TwitchTool.Source;

namespace TwitchTool.Twitch
{
    public class TwitchAPI
    {
        public class TwitchResponse
        {
            public string Type;
            public string Data;
        }

        
        private const string CLIENT_ID = "n27kz3ebjjr6o2gi2jgiqe96vtqlfu";
        private const string CLIENT_SECRET = "rhns93xyzjcxopganq7c3kv40ir1jm";
        private const string BASEURL = "https://api.twitch.tv/kraken";
        private const string ACCEPT_HEADER = "application/vnd.twitchtv.v5+json";
        private const string AUTHORIZATION_HEADER = "Authorization: OAuth {0}";
        private const string CLIENTID_HEADER = "Client-ID: n27kz3ebjjr6o2gi2jgiqe96vtqlfu";
        private string ACCESS_TOKEN = "";
        private bool m_LoggingIn;
        public string USERID = "";
        public string USERNAME = "";
        public string USERLOGO = "";

        // TwitchBot
        private TwitchBot m_TwitchBot;
        public TwitchBot TwitchBot { get { return m_TwitchBot; } }

        // TwitchLoginWindow
        private TwitchLoginWindow m_LoginWindow;

        public TwitchAPI()
        {
            m_TwitchBot = new TwitchBot(this);
            m_LoggingIn = false;
        }

        public void Login(bool forceLogin)
        {
            if (m_LoggingIn)
                return;

            m_TwitchBot.Stop();

            Thread newWindowThread = new Thread(new ThreadStart(new Action(() =>
            {
                m_LoginWindow = new TwitchLoginWindow();
                try
                {
                    m_LoginWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    m_LoginWindow.Closed += (sender2, e2) => m_LoginWindow.Dispatcher.InvokeShutdown();
                    m_LoginWindow.Show();
                    m_LoginWindow.Login(LoginSuccess, forceLogin);
                    Dispatcher.Run();
                }
                catch (ThreadAbortException)
                {
                    m_LoginWindow.Close();
                    Dispatcher.CurrentDispatcher.InvokeShutdown();
                }

            })));
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();
        }

        private void LoginSuccess(string token)
        {
            m_LoggingIn = false;

            if (!string.IsNullOrEmpty(token))
            {
                Logger.Log(LOG_LEVEL.DEBUG, "Successfully logged in to Twitch.");
                ACCESS_TOKEN = token;
                SetUser();
            }
            else
            {
                Logger.Log(LOG_LEVEL.WARNING, "Login to Twitch failed.");
                USERID = "";
                USERNAME = "";
                USERLOGO = "";
                ACCESS_TOKEN = "";
            }
        }

        private void SetUser()
        {
            TwitchResponse r = GetUser();
            if(r.Type.Equals("GetResponse"))
            {
                dynamic data = JsonConvert.DeserializeObject<dynamic>(r.Data);
                USERID = data._id;
                USERNAME = data.name;
                USERLOGO = data.logo;
                Logger.Log(LOG_LEVEL.DEBUG, string.Format("Successfully fetched Twitch user data for '{0}'", USERNAME));

                //USERNAME = "shroud";
                //USERID = "37402112";

                USERNAME = "lirik";

                m_TwitchBot.Start(USERNAME);
            }
            else
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchAPI - " + r.Data);
            }
        }


        public TwitchResponse GetToken()
        {
            return Get(BASEURL, true, string.Format(AUTHORIZATION_HEADER, ACCESS_TOKEN));
        }

        public TwitchResponse GetUser()
        {
            return Get(BASEURL + "/user/", true, CLIENTID_HEADER, string.Format(AUTHORIZATION_HEADER, ACCESS_TOKEN));
        }

        public TwitchResponse GetUserById(string id)
        {
            return Get(BASEURL + "/users/" + id, false);
        }

        public TwitchResponse GetFollowers(string cursor)
        {
            string url = BASEURL + "/channels/" + USERID + "/follows";
            if(!string.IsNullOrEmpty(cursor))
            {
                url += "?cursor=" + cursor;
            }
            return Get(url, true, CLIENTID_HEADER);
        }

        private string GetResponse(WebResponse response)
        {
            using (StreamReader r = new StreamReader(response.GetResponseStream()))
            {
                return r.ReadToEnd();
            }
        }

        public TwitchResponse Get(string url, bool accept, params string[] headers)
        {
            TwitchResponse result = new TwitchResponse();
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                for (int i = 0; i < headers.Length; i++)
                {
                    request.Headers.Add(headers[i]);
                }

                // Special due to restricted header...
                if(accept)
                {
                    request.Accept = ACCEPT_HEADER;
                }
                
                result.Type = "GetResponse";
                result.Data = GetResponse(request.GetResponse());
            }
            catch (WebException wex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchAPI - " + wex.Message);

                result.Type = "WebException";
                if (wex.Response != null)
                {
                    result.Data = GetResponse(wex.Response);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, ex.Message);

                result.Type = "Exception";
                result.Data = ex.Message;
            }

            return result;
        }

    }
}
