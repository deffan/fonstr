﻿using mshtml;
using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using TwitchTool.Source;

namespace TwitchTool.Twitch
{
    /// <summary>
    /// Interaction logic for TwitchLoginWindow.xaml
    /// </summary>
    public partial class TwitchLoginWindow : Window
    {
        private Action<string> m_NewToken;
        private bool m_Success;

        public TwitchLoginWindow()
        {
            InitializeComponent();
            Height = 0;
            Width = 0;
            m_Success = false;
            //NativeMethods.SuppressCookiePersistence();    // Debug, then we dont want cookies.
        }

        public void Login(Action<string> newTokenCallback, bool forceLogin)
        {
            m_NewToken = newTokenCallback;
            try
            {
                string state = Guid.NewGuid().ToString("N");
                string url = string.Format("https://id.twitch.tv/oauth2/authorize?client_id={0}&redirect_uri={1}&response_type={2}&scope={3}&state={4}&force_verify={5}",
                "n27kz3ebjjr6o2gi2jgiqe96vtqlfu",
                "http://localhost",
                "token",
                "user_read",
                state,
                forceLogin);

                TheWebBrowser.LoadCompleted += TheWebBrowser_Login;
                TheWebBrowser.Navigate(url);
                Thread.Sleep(1000);
            }
            catch (WebException wex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchLogin > " + wex.Message);
                Close();
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchLogin > " + ex.Message);
                Close();
            }
        }

        private string GetAccessToken(string data)
        {
            try
            {
                int indexOfToken = data.IndexOf("#access_token=");
                if (indexOfToken < 0)
                    return "";
                indexOfToken += 14;
                int indexEndOfAuthValue = data.IndexOf("&", indexOfToken);
                return data.Substring(indexOfToken, indexEndOfAuthValue - (indexOfToken));
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchLogin AccessToken > " + ex.Message);
            }
            return "";
        }

        private void TheWebBrowser_Login(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            try
            {
                HTMLDocument doc = TheWebBrowser.Document as HTMLDocument;
                string accessToken = GetAccessToken(doc.URLUnencoded);
                if (!string.IsNullOrEmpty(accessToken))
                {
                    m_Success = true;
                    m_NewToken?.Invoke(accessToken);
                    Close();
                }
                else
                {
                    // By default, the window has a size of ZERO. Because most of the time, 
                    // you will be automatically logged in, and there will be no need to enter any user/pass.
                    // But if that fails, due to forced login or invalid token, you will be required to login again.
                    Height = 600;
                    Width = 800;
                    Activate();
                }
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchLogin Browser > " + ex.Message);
                Close();
            }
        }

        public static partial class NativeMethods
        {
            [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]
            private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int dwBufferLength);

            private const int INTERNET_OPTION_SUPPRESS_BEHAVIOR = 81;
            private const int INTERNET_SUPPRESS_COOKIE_PERSIST = 3;

            public static void SuppressCookiePersistence()
            {
                var lpBuffer = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(int)));
                Marshal.StructureToPtr(INTERNET_SUPPRESS_COOKIE_PERSIST, lpBuffer, true);

                InternetSetOption(IntPtr.Zero, INTERNET_OPTION_SUPPRESS_BEHAVIOR, lpBuffer, sizeof(int));

                Marshal.FreeCoTaskMem(lpBuffer);
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Image_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!m_Success)
            {
                m_NewToken?.Invoke(""); // Failure or Abort
            }
        }

    }
}
