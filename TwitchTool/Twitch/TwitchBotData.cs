﻿using System.Collections.Generic;

namespace TwitchTool.Twitch
{
    public class TwitchBotData
    {
        public string Type;
        public string Id;
        public string Bits;
        public string UserName;
        public string Message;
        public string Months;
        public string SubPlan;
        public string SubGiftRecipient;
        public string Raider;
        public string AmountOfRaiders;
        public string Timestamp;
        public List<string> Badges;

        public TwitchBotData()
        {
            Badges = new List<string>();
        }
    }
}

