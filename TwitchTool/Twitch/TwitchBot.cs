﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TwitchTool.EventListeners;
using TwitchTool.Events;
using TwitchTool.Events.Twitch;
using TwitchTool.Source;

namespace TwitchTool.Twitch
{ 
    public class TwitchBot
    {
        private Thread m_Thread;
        private IrcClient m_IRC;
        private bool m_Running;
        private string m_Channel;
        private TwitchListener m_TwitchBotListener;
        private bool m_ReceivingJoinPart;
        private TwitchAPI m_TwitchAPI;
        private List<string> m_BannedWords;

        private string m_PrivMsgString;
        private string m_PrivMsgMsgString;
        private string m_UserNoticeString;
        private string m_JoinString;
        private string m_PartString;

        public TwitchListener Listener
        {
            get { return m_TwitchBotListener; }
            set { m_TwitchBotListener = value; }
        }

        /*
            BotOfTheEarth
            g49OxDJg5cvBCVoy8l
        */

        public TwitchBot(TwitchAPI api)
        {
            m_TwitchAPI = api;
            m_IRC = new IrcClient();
            m_ReceivingJoinPart = false;
            m_Running = false;
            m_BannedWords = new List<string>();

            // Banned words.
            //m_BannedWords.Add("bits=");
        }

        public void Stop()
        {
            m_Running = false;
        }

        public bool Start(string channel)
        {
            // Connect
            m_Channel = channel;
            if (!m_IRC.Connect("irc.twitch.tv", 6667, "BotOfTheEarth", "oauth:o5dcj9v35xzpgyrqthmu9z0ws42rgi", m_Channel))
                return false;

            // Set strings
            m_PrivMsgString = "twitch.tv PRIVMSG #" + m_Channel;
            m_PrivMsgMsgString = m_PrivMsgString + " :";
            m_UserNoticeString = "twitch.tv USERNOTICE #" + m_Channel;
            m_JoinString = "twitch.tv JOIN #" + m_Channel;
            m_PartString = "twitch.tv PART #" + m_Channel;

            // Start the IRC thread
            m_Thread = new Thread(new ThreadStart(Run));
            m_Thread.Start();

            return true;
        }

        public void SendBotMessage(string msg)
        {
            // Not thread safe right now........
            m_IRC.SendPublicChatMessage(msg);
        }

        private void Run()
        {
            m_Running = true;

            // Register IRC capabilities
            m_IRC.Register("membership");
            m_IRC.Register("tags");
            m_IRC.Register("commands");

            /*
                This is the TwitchBot loop where we parse messages.
                But we also contact some Twitch API endpoints here because it fits well to do it in the PING/PONG interval.
            */
            while (m_Running)
            {
                try
                {
                    // Check for messages...
                    if (!m_IRC.HasMessage())
                    {
                        Thread.Sleep(1000);
                        continue;
                    }

                    // Fetch
                    string message = m_IRC.ReadMessage();

                    // Begin by checking for banned words.
                    //if (Banned(message))
                    //    continue;

                    // Parse the message
                    if (message.Contains("PING :tmi.twitch.tv") && !message.Contains(m_PrivMsgString))
                    {
                        // PONG!
                        m_IRC.SendIrcMessage("PONG :tmi.twitch.tv");

                        // If we are listening for JOIN/PART but the IRC server is not sending them (perhaps too many in chat) then we have to do it by polling the API instead.
                        // This is slow and inaccurate but better than nothing if the user wants this information.
                        if((Listener.HasJoinEvent() || Listener.HasPartEvent()) && !m_ReceivingJoinPart)
                        {
                            Listener.UpdateUserList();
                        }
                        m_ReceivingJoinPart = false;    // Reset

                        // Check followers
                        if(Listener.HasFollowerEvent())
                        {
                            Listener.UpdateFollowers();
                        }

                        // Check viewers
                    }
                    else if (message.Contains(m_UserNoticeString) && !message.Contains(m_PrivMsgString))
                    {
                        UserNotice(message);
                    }
                    else if (Listener.HasBitsEvent() && message.Contains("bits=") && message.Contains(m_PrivMsgString))
                    {
                        Bits(message);
                    }
                    else if ((Listener.HasJoinEvent() || Listener.HasPartEvent()) && !message.Contains(m_PrivMsgString))
                    {
                        if (message.Contains(m_JoinString))
                        {
                            Listener.JoinEvent(message.Substring(1, message.IndexOf('!', 1) - 1));
                            m_ReceivingJoinPart = true;
                        }
                        else if (message.Contains(m_PartString))
                        {
                            Listener.PartEvent(message.Substring(1, message.IndexOf('!', 1) - 1));
                            m_ReceivingJoinPart = true;
                        }
                    }
                    else if(Listener.HasCustomEvent() && message.Contains(m_PrivMsgString))
                    {
                        Custom(message);
                    }
                }
                catch(Exception ex)
                {
                    Logger.Log(LOG_LEVEL.ERROR, ex.Message);
                    if(ex.InnerException != null)
                    {
                        Logger.Log(LOG_LEVEL.ERROR, ex.InnerException.Message);
                    }
                    Logger.Log(LOG_LEVEL.ERROR, ex.StackTrace);
                }
            }
            m_TwitchBotListener.Reset();
        }

        private bool Banned(string data)
        {
            if (m_BannedWords.Count > 0 && data.Contains(m_PrivMsgMsgString))
            {
                int indexOfMsg = data.IndexOf(m_PrivMsgMsgString) + m_PrivMsgMsgString.Length;
                string messagePart = data.Substring(indexOfMsg, data.Length - indexOfMsg).ToLower();
                for (int i = 0; i < m_BannedWords.Count; i++)
                {
                    // The question is, should we only ban words that are actually WORDS (so we have to split the string) or any word in any word? 
                    // Right now, everything is banned. So if i write "everything" and i ban "thing" this would count.

                    if (messagePart.Contains(m_BannedWords[i]))
                    {
                        Logger.Log(LOG_LEVEL.DEBUG, "Banned word detected: " + m_BannedWords[i]);
                        Logger.Log(LOG_LEVEL.DEBUG, data);
                        // TwitchBotBannedWordEvent ?
                        return true;
                    }
                }
            }
            return false;
        }

        private void Custom(string data)
        {
            TwitchBotData twitchBotData = new TwitchBotData();
            twitchBotData.Type = "CUSTOM";
            twitchBotData.UserName = GetUserName(data);
            twitchBotData.Message = ParseTextMessage("PRIVMSG", data);
            twitchBotData.Badges = ParseBadges(data);
            Listener.CustomEvent(twitchBotData);
        }

        private string GetUserName(string data)
        {
            int endIndex = data.IndexOf(".tmi.twitch.tv ");
            if (endIndex < 0)
                return "";

            int indexOfAt = data.LastIndexOf('@', endIndex) + 1;
            if (indexOfAt < 0)
                return "";

            return data.Substring(indexOfAt, endIndex - indexOfAt);
        }

        private List<string> ParseBadges(string data)
        {
            List<string> badges = new List<string>();

            int startIndex = data.IndexOf("@badges=") + 8;
            if (startIndex < 0)
                return badges;

            int endIndex = data.IndexOf(";", startIndex);
            if (endIndex < 0)
                return badges;

            string[] badgesArray = data.Substring(startIndex, endIndex - startIndex).Split(',');
            for (int i = 0; i < badgesArray.Length; i++)
            {
                endIndex = badgesArray[i].IndexOf('/');
                if (endIndex < 0)
                    continue;
                badges.Add(badgesArray[i].Substring(0, endIndex));
            }
            return badges;
        }

        private string ParseString(string data, string start, string end)
        {
            try
            {
                int startIndex = data.IndexOf(start);
                int endIndex = data.IndexOf(end, startIndex);
                return data.Substring(startIndex + start.Length, endIndex - (startIndex + start.Length));
            }
            catch(Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchBot-ParseString: " + start + " | " + end  + " | " + ex.Message);
                Logger.Log(LOG_LEVEL.ERROR, data);
            }
            return "";
        }

        private string ParseTextMessage(string what, string data)
        {
            try
            {
                int indexOfMessage = data.LastIndexOf(" :");    // Will not exist if no message
                int indexOfChannel = data.LastIndexOf(what + " #" + m_Channel);
                if (indexOfMessage > indexOfChannel)
                {
                    return data.Substring(indexOfMessage + 2);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(LOG_LEVEL.ERROR, "TwitchBot-ParseMessage: " + ex.Message);
                Logger.Log(LOG_LEVEL.ERROR, data);
            }
            return "";
        }

        private void Bits(string data)
        {
            // For safety, we have to double check that nobody is trying to fake a bits message by saying "bits=1000" or similar.
            int indexOfMsg = data.IndexOf(m_PrivMsgMsgString) + m_PrivMsgMsgString.Length;
            string messagePart = data.Substring(indexOfMsg, data.Length - indexOfMsg).ToLower();
            if (messagePart.Contains("bits="))
            {
                Logger.Log(LOG_LEVEL.WARNING, "Attempt to fake bits message?");
                Logger.Log(LOG_LEVEL.WARNING, data);
                return;
            }

            TwitchBotData twitchBotData = new TwitchBotData();
            twitchBotData.Type = "BITS";
            twitchBotData.Bits = ParseString(data, "bits=", ";");
            twitchBotData.UserName = GetUserName(data);
            twitchBotData.Message = ParseTextMessage("PRIVMSG", data);
            twitchBotData.Badges = ParseBadges(data);
            Listener.BitsEvent(twitchBotData);
        }

        private void UserNotice(string data)
        {
            // Get the type of notice
            string type = ParseString(data, "msg-id=", ";");

            // First check if we are listening for this specific notice
            switch(type)
            {
                case "sub":
                case "resub":
                {
                    if (!Listener.HasSubscriberEvent())
                        return;
                    break;
                }
                case "raid":
                {
                    if (!Listener.HasRaidEvent())
                        return;
                    break;
                }
                case "subgift":
                {
                    if (!Listener.HasGiftedEvent())
                        return;
                    break;
                }
            }

            // Parse the data, and then create/send the event
            TwitchBotData twitchBotData = new TwitchBotData();
            twitchBotData.Type = "USERNOTICE";
            twitchBotData.Id = type;
            twitchBotData.UserName = ParseString(data, "login=", ";");
            twitchBotData.Message = ParseTextMessage("USERNOTICE", data);
            twitchBotData.Timestamp = ParseString(data, "tmi-sent-ts=", ";");
            twitchBotData.Badges = ParseBadges(data);

            if (type.Equals("sub") || type.Equals("resub"))
            {
                twitchBotData.Months = ParseString(data, "msg-param-months=", ";");
                twitchBotData.SubPlan = ParseString(data, "msg-param-sub-plan=", ";");
                Listener.SubEvent(twitchBotData);
            }
            else if (type.Equals("raid"))
            {
                twitchBotData.Raider = ParseString(data, "msg-param-login=", ";");
                twitchBotData.AmountOfRaiders = ParseString(data, "msg-param-viewerCount=", ";");
                Listener.RaidEvent(twitchBotData);
            }
            else if (type.Equals("subgift"))
            {
                twitchBotData.SubGiftRecipient = ParseString(data, "msg-param-recipient-user-name=", ";");
                Listener.GiftEvent(twitchBotData);
            }
        }
    }
}
