﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchTool.Twitch
{
    public class UserList
    {
        public Links _links { get; set; }
        public int chatter_count { get; set; }
        public Chatters chatters { get; set; }

        public class Links
        {
        }

        public class Chatters
        {
            public List<string> vips { get; set; }
            public List<string> moderators { get; set; }
            public List<string> staff { get; set; }
            public List<string> admins { get; set; }
            public List<string> global_mods { get; set; }
            public List<string> viewers { get; set; }
        }
    }

    public class Followers
    {
        public class User
        {
            public string _id { get; set; }
            public object bio { get; set; }
            public DateTime created_at { get; set; }
            public string display_name { get; set; }
            public object logo { get; set; }
            public string name { get; set; }
            public string type { get; set; }
            public DateTime updated_at { get; set; }
        }

        public class Follow
        {
            public DateTime created_at { get; set; }
            public bool notifications { get; set; }
            public User user { get; set; }
        }

        public string _cursor { get; set; }
        public int _total { get; set; }
        public List<Follow> follows { get; set; }
    }


}
