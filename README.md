# C# WPF application that can build, display and react to events such as Twitch, Keyboard input, etc. #

## Youtube video: ##
[![Youtube video demonstration](https://img.youtube.com/vi/bWtpbSkgHVs/0.jpg)](https://youtu.be/bWtpbSkgHVs)

Supposed to be a tool for Twitch streamers.

The tool can listen to events (keyboard, twitch-chat, etc).
When an event occurs, you can choose to execute a command.
The command is sent to a window (that you create in the tool editor).

The window can contain several components (that you create in the tool editor).
These components can then execute its function, for example play an animation, play a sound, etc.

